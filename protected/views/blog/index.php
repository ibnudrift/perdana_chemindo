<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl2 ?>hero-blogs.jpg');">
    <div class="prelative container py-5">
        <div class="py-5"></div>
        <div class="row py-5">
            <div class="col-md-30 py-5">
                <div class="insides_intext">
                    <h3 class="mb-2">BLOG</h3>
                    <div class="py-1"></div>
                    <p>Ketahui lebih lanjut tentang aneka informasi produk pupuk dan bermacam artikel penting lainnya.</p>
                    <div class="clear"></div>
                </div>

            </div>
            <div class="col-md-30"></div>
        </div>
    </div>
</section>

<section class="blog-sec-1 py-5 back-white">
    <div class="prelative container py-5">
        <div class="inners content-text text-left">

            <div class="lists_data_fx_blog">
              <div class="row">
                <?php for ($i=1; $i < 9; $i++) { ?>
                <div class="col-md-30">
                  <div class="items">
                    <div class="picture"><img src="<?php echo $this->assetBaseurl2 ?>banners-blog.jpg" alt="" class="img img-fluid"></div>
                    <div class="info py-3">
                      <div class="py-1"></div>
                      <span class="dates">19 December 2018</span>
                      <div class="py-1"></div>
                      <h2 class="title">Eklin group to give back to the community as part of 2018 CSR</h2>
                      <div class="py-2"></div>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque non gravida dolor. Fusce gravida, risus vel sodales posuere, mi metus tincidunt nisi, vitae finibus metus urna viverra turpis. </p>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
                <?php } ?>

              </div>
            </div>

            <div class="clear"></div>
        </div>
    </div>
</section>


<?php /*
<section class="breadcrumb-det">
    <div class="prelative container">
      <div class="row">
        <div class="col-md-45">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="#">Blog</a></li>
                </ol>
            </nav>
        </div>
        <div class="col-md-15">
          <div class="block-back-link text-right">
            <a href="#" onclick="window.history.back();">Back</a>
          </div>
        </div>
      </div>
      <div class="py-2"></div>
    </div>
</section>

<section class="product-sec-1">
  <div class="prelative container">
    <div class="row">
      <div class="col-md-15">
      <div class="box-konten-kiri">
          <h5>Blog</h5>        
      </div>
      </div>
      <div class="col-md-45 rights_cont_def">
        <div class="clear clearfix"></div>

        <?php if ($dataBlog): ?>
        <div class="row default-data">
            <?php foreach ($dataBlog->getData() as $key => $value): ?>
                <div class="col-md-20">
                    <div class="box-content">
                        <div class="image">
                            <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><img class="img img-fluid w-100"src="<?php echo $this->assetBaseurl.'../../images/blog/'; ?><?php echo $value->image ?>" alt=""></a>
                        </div>
                        <div class="title">
                            <p><?php echo ucwords($value->description->title); ?></p>
                        </div>
                        <div class="klik">
                        <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><p>Read More</p></a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        <?php endif ?>
        <div class="py-4"></div>
        <div class="textaboveheader-landing page">
              <?php 
               $this->widget('CLinkPager', array(
                  'pages' => $dataBlog->getPagination(),
                  'header'=>'',
                  'footer'=>'',
                  'lastPageCssClass' => 'd-none',
                  'firstPageCssClass' => 'd-none',
                  'nextPageCssClass' => 'd-none',
                  'previousPageCssClass' => 'd-none',
                  'itemCount'=> $dataBlog->totalItemCount,
                  'htmlOptions'=>array('class'=>'pagination'),
                  'selectedPageCssClass'=>'active',
              ));
           ?>
          </div>

      </div>
    </div>
  </div>
</section>
*/ ?>