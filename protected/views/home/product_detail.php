<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>hero-about.jpg');">
    <div class="outers_block_inner">
        <div class="prelative container">
            <div class="row">
                <div class="col-md-60">
                    <div class="insides_intext">
                        <h1>PRODUCTS</h1>
                        <div class="py-2"></div>
                        <h2><?php echo ucwords(str_replace('-',' ', $_GET['name'])) ?></h2>
                        <div class="py-2"></div>
                        <div class="back-lines_dncenter d-block mx-auto"></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php 

    
$data_spec_prod = [
                    'wastewater'=> [
                                    [
                                        'name'=>'tawas / alumimium sulfat 17%',
                                        'detail'=>'<p>Aluminium sulphate&nbsp;is a&nbsp;chemical compound&nbsp;with the&nbsp;formula&nbsp;Al2(SO4)3. It is soluble in water and is mainly used as a&nbsp;coagulating agent&nbsp;(promoting particle collision by neutralizing charge) in the purification of drinking water[3][4]&nbsp;and waste water treatment plants, and also in paper manufacturing.</p>',
                                        'packings'=>'25 kgs & 50 kgs',
                                    ],
                                    [
                                        'name'=>'poly aluminium chloride 30% (white / yellow)',
                                        'detail'=>'<p>PAC is used for several applications including the treatment of drinking water, swimming pool water, wastewater treatment, the treatment of sewage and industrial effluents. It is also used extensively in the pulp and paper processing industry.</p>',
                                        'packings'=>'25 kgs',
                                    ],
                                    [
                                        'name'=>'trichloroisocyanuric acid TCCA 90% (powder / granular / tablet 200gr)',
                                        'detail'=>'<p>Trichloroisocyanuric acid is an organic compound with the formula (C3Cl3N3O3). It is used as an industrial disinfectant, bleaching agent and a reagent in organic synthesis.[1][2][3] This white crystalline powder, which has a strong `chlorine odour,` is sometimes sold in tablet or granule form for domestic and industrial use.</p>',
                                        'packings'=>'1kg & 50 kg',
                                    ],
                                    [
                                        'name'=>'calcium hypochlorite / kaporit bubuk tjiwi kimia 60%',
                                        'detail'=>'<p>Calcium hypochlorite is an inorganic compound with formula Ca(OCl)2. It is the main active ingredient of commercial products called bleaching powder, chlorine powder, or chlorinated lime, used for water treatment and as a bleaching agent.[1] This compound is relatively stable and has greater available chlorine than sodium hypochlorite (liquid bleach).[2] It is a white solid, although commercial samples appear yellow. It strongly smells of chlorine, owing to its slow decomposition in moist air.</p>',
                                        'packings'=>'1kg & 15 kg',
                                    ],
                                    [
                                        'name'=>'kaporit cair / sodium hypochlorite',
                                        'detail'=>'<p>Sodium hypochlorite (commonly known in a dilute solution as bleach) is a chemical compound with the formula NaOCl or NaClO. Sodium hypochlorite is most often encountered as a pale greenish-yellow dilute solution referred to as liquid bleach, which is a household chemical widely used (since the 18th century) as a disinfectant or a bleaching agent.</p>',
                                        'packings'=>'35 kgs & 250 kgs',
                                    ],
                                    [
                                        'name'=>'hydrochloric acid / HCL 32%',
                                        'detail'=>'<p>Hydrochloric acid or muriatic acid is a colorless inorganic chemical system with the formula HCl. Hydrochloric acid is an important chemical reagent and industrial chemical, used in the production of polyvinyl chloride for plastic. In households, diluted hydrochloric acid is often used as a descaling agent. In the food industry, hydrochloric acid is used as a food additive and in the production of gelatin. Hydrochloric acid is also used in leather processing.</p>',
                                        'packings'=>'35 kgs & 250 kgs',
                                    ],
                                    [
                                        'name'=>'sodium carbonate / soda ash dense',
                                        'detail'=>'<p>Sodium carbonate, Na2CO3, (also known as washing soda, soda ash and soda crystals) is the inorganic compound with the formula Na2CO3.</p>',
                                        'packings'=>'50 kgs',
                                    ],
                                    [
                                        'name'=>'caustic soda flake 98%',
                                        'detail'=>'<p>Sodium hydroxide, also known as lye and caustic soda,[1][2] is an inorganic compound with the formula NaOH. used in many industries: in the manufacture of pulp and paper, textiles, drinking water, soaps and detergents, and as a drain cleaner. Packing 25 kg</p>',
                                        'packings'=>'25 kgs',
                                    ],
                                    [
                                        'name'=>'caustic soda liquid 32% & 48%',
                                        'detail'=>'<p>Sodium hydroxide, also known as lye and caustic soda,[1][2] is an inorganic compound with the formula NaOH. used in many industries: in the manufacture of pulp and paper, textiles, drinking water, soaps and detergents, and as a drain cleaner.</p>',
                                        'packings'=>'30 kgs & 330 kgs',
                                    ],
                                    [
                                        'name'=>'h2o2 / hydrogen peroxide',
                                        'detail'=>'<p>Hydrogen peroxide is a chemical compound with the formula H2O2. It is used as an oxidizer, bleaching agent, and antiseptic.</p>',
                                        'packings'=>'35 kgs & 250 kgs',
                                    ],
                                    [
                                        'name'=>'cooper sulfate 24.5%',
                                        'detail'=>'<p>Copper(II) sulfate, also known as copper sulphate, are the inorganic compounds with the chemical formula CuSO4. It can kill bacteria, algae, roots, plants, snails, and fungi.</p>',
                                        'packings'=>'25 kgs',
                                    ],
                                    [
                                        'name'=>'sodium metabisulfite',
                                        'detail'=>'<p>Sodium metabisulfite or sodium pyrosulfite (IUPAC spelling; Br. E. sodium metabisulphite or sodium pyrosulphite) is an inorganic compound of chemical formula Na2S2O5.  It is used as a disinfectant, antioxidant, and preservative agent.</p>',
                                        'packings'=>'25 kgs',
                                    ],
                                    [
                                        'name'=>'citric acid monohydrate',
                                        'detail'=>'<p>Citric acid is a weak organic acid that has the molecular formula C6H8O7. It occurs naturally in citrus fruits.</p>',
                                        'packings'=>'25 kgs',
                                    ],
                                    [
                                        'name'=>'cation resin',
                                        'detail'=>'<p>Ion-exchange resins are widely used in different separation, purification, and decontamination processes. The most common examples are water softening and water purification.</p>',
                                        'packings'=>'25 L',
                                    ],
                                    [
                                        'name'=>'oto PH & oto CHLORINE test kit',
                                        'detail'=>'<p>This test kit allows you to measure the pH and chlorine levels of your pool water. This pool water test kit is very user-friendly. The colour codes make it very easy to read the pool water values.</p>',
                                        'packings'=>'',
                                    ],
                                    
                                ],
                    'fiberglass_composite'=> [
                                    [
                                        'name'=>'E-Glass Chopped Strand Mat (CSM) / Fiberglass Mat Taishan Fiber CTG',
                                        'detail'=>'<p>Taishan fiber brand fiberglass chopped strand mat is a premium grade product suitable for all of your fiberglass composite needs. Using carefully selected high quality raw materials then produced in a state of the art facility. Type available 200, 300, 450, 600 GSM. Certification by BKI, Lloyd, DNV.</p>',
                                        'packings'=>'30 kgs & 54 kgs',
                                    ],
                                    [
                                        'name'=>'E-Glass Chopped Strand Mat (CSM) / Fiberglass Mat JUSHI',
                                        'detail'=>'<p>JUSHI fiber brand fiberglass chopped strand mat is a premium grade product suitable for all of your fiberglass composite needs. Using carefully selected high quality raw materials then produced in a state of the art facility. Type available 200, 300, 450, 600 GSM. Certification by BKI, Lloyd, DNV.</p>',
                                        'packings'=>'30 kgs & 54 kgs',
                                    ],
                                    [
                                        'name'=>'CHEMPOGLASS E-Glass Woven Roving (EWR)',
                                        'detail'=>'<p>Chempoglass brand woven roving is a premium grade product suitable for all of your fiberglass composite needs. Using carefully selected high quality raw materials then produced in a state of the art facility. Type available 200, 400, 600, 800 GSM.</p>',
                                        'packings'=>'40 kgs',
                                    ],
                                    [
                                        'name'=>'CHEMPOGLASS fiberglass mesh',
                                        'detail'=>'<p>Fiberglass mesh is used to strengthen the surface of the plaster layer all kinds of buildings. This mesh reinforced liquid waterproofing layers slabs and roofs. Fiberglass mesh used to impart mechanical strength filler floor coverings that are different self-leveling properties. avaiable size 80gsm 1mtr*50mtr</p>',
                                        'packings'=>'50m/roll',
                                    ],
                                    [
                                        'name'=>'Chempocore Honeycomb PP',
                                        'detail'=>'<p>Polypropylene (PP) honeycomb cores are a strong but flexible lightweight material that provides optimal mechanical performance in fiberglass composite sandwich structures. Type available 8mm 2m*1m, 8mm 122*244, 10mm 122*244, 20mm 122*244</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'Chempocore polyurethane (PU) foam sheet',
                                        'detail'=>'<p>2 components (polyol+isocyanates) Rigid polyurethane foam (PUR/PIR) is a closed-cell plastic. It is used as factory- made thermal insulation material in the form of insulation boards or block foam, and in combination with various rigid facings as a constructional material or sandwich panel. aplication for insulation, fiberglass core material. type available 5cm 2m*1m density 38-40</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'Polyurethane (PU) rigid foam liquid A+B',
                                        'detail'=>'<p>This two-part liquid, expanding rigid urethane foam is a closed cell, pourable foam, which will resist the absorption of water. Once fully cured this foam can be laminated over with any type of polyester, epoxy or vinyl ester resin without melting. 225 kgs polyol, 250 kgs isocyanates
                                        This product can be poured in multiple layers with excellent bonding between layers. The lightweight foam is commonly used for filling voids, filling under decks and practically any other flotation or insulation application. Higher densities are used for architectural castings, support applications and has virtually an unlimited number of potential uses.</p>',
                                        'packings'=>'225kgs part A & 250kgs part B',
                                    ],
                                    [
                                        'name'=>'miracle gloss mold release agent',
                                        'detail'=>'<p>MIRACLE GLOSS NO. 8 2.0 ADVANTAGES: Ease of application and buffing, Creates a durable wax surface, Remains intact for multiple pulls. Formulated to release GRP, FRP, and other composite applications. Used to release GRP, FRP, and other composites. Applies and buffs with ease, provides multiple pulls.</p>',
                                        'packings'=>'311 gr',
                                    ],
                                    [
                                        'name'=>'Pioneer Durasteel high temp epoxy',
                                        'detail'=>'<p>A two-component, versatile adhesive ideal for repairing damaged equipment and can also fill gaps.Cures quickly in 5 minutes to form a durable metallic mass.Can be sanded, drilled and machines once fully cured. Provides a strong, heavy duty and durable bond. melting point over 300c.</p>',
                                        'packings'=>'35 gr',
                                    ],
                                    [
                                        'name'=>'wacker HDK n20 / aerosil',
                                        'detail'=>'<p>HDK® N20 is applied as a thickening and thixotropic agent in many organic systems, e.g. in unsaturated polyesters, coatings, printing inks, adhesives, cosmetics and others. HDK® N20 is used as a reinforcing filler in elastomers, mainly silicone-elastomers. HDK® N20 acts as a free flow additive in the production of technical powders.</p>',
                                        'packings'=>'10 kgs',
                                    ],
                                    [
                                        'name'=>'talc powder liaoning / haichen',
                                        'detail'=>'<p>use for fillers.</p>',
                                        'packings'=>'25 kgs',
                                    ],
                                    [
                                        'name'=>'CHEMPOGLOW glow in the dark powder / phosporecent pigment',
                                        'detail'=>'<p>Glow Powder (also known as Glow-In-The-Dark Powder) is actually photo-luminescent pigment, which is the basis for almost all glow in the dark products. These pigments are non-toxic and non-radioactive. Available colour blue, green, red, orange, purple, yellow</p>',
                                        'packings'=>'10gr, 25gr, 100gr, 250gr, 500gr, 1kg',
                                    ],
                                    [
                                        'name'=>'infusion mesh',
                                        'detail'=>'<p>vacuum infusion material</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'seal tape',
                                        'detail'=>'<p>vacuum infusion material</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'peel ply',
                                        'detail'=>'<p>“Peel Ply,” also called “Release Fabric,” is a synthetic cloth that you drape over your epoxied surface as the epoxy sets up.  Once cured, you can peel off the fabric, and what’s left behind is a perfectly smooth surface that’s ready for a quick sanding, or for the next round of epoxy.  No runs, no sags, no low spots.</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'vacuum bag',
                                        'detail'=>'<p>vacuum infusion material</p>',
                                        'packings'=>'',
                                    ],
                                ],
                    'general_industrial'=> [
                                    [
                                        'name'=>'nitric acid 68% ex KOREA and BELGIUM',
                                        'detail'=>'<p>Etchant and cleaning agent. Packing 35 kgs</p>',
                                        'packings'=>'35 kgs',
                                    ],
                                    [
                                        'name'=>'sulfuric acid (h2so4) 98%',
                                        'detail'=>'<p>basic industrial raw materials.</p>',
                                        'packings'=>'40 kgs & 330 kgs',
                                    ],
                                    [
                                        'name'=>'Phosporic acid 85% food grade',
                                        'detail'=>'<p>use for fertilizer, food industry, metal industry.</p>',
                                        'packings'=>'35 kgs',
                                    ],
                                    [
                                        'name'=>'pioneer mighty tape',
                                        'detail'=>'<p>electrical tape with fire retardant</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'gypsum roving yarn',
                                        'detail'=>'<p>reinforce material for gypsum products.</p>',
                                        'packings'=>'20 kgs',
                                    ],
                                    [
                                        'name'=>'Siam gypsum powder (SGP)',
                                        'detail'=>'<p>raw material for gypsum products.</p>',
                                        'packings'=>'40 kgs',
                                    ],
                                    [
                                        'name'=>'sodium bicarbonate (soda kue) food grade',
                                        'detail'=>'<p>commonly known as baking soda or bicarbonate of soda, is a chemical compound with the formula NaHCO3.</p>',
                                        'packings'=>'25 kgs',
                                    ],
                                    [
                                        'name'=>'STPP / Sodium Tripoly Phospate tech grade',
                                        'detail'=>'<p>Na5P3O10 component of many domestic and industrial products, especially detergents.</p>',
                                        'packings'=>'25 kgs',
                                    ],
                                    [
                                        'name'=>'Trisodium phospate',
                                        'detail'=>'<p>Na3PO4. It is a white, granular or crystalline solid, highly soluble in water, producing an alkaline solution. TSP is used as a cleaning agent, builder, lubricant, food additive, stain remover, and degreaser.</p>',
                                        'packings'=>'25 kgs',
                                    ],
                                    [
                                        'name'=>'sodium sulphate',
                                        'detail'=>'<p>Sodium sulfate (also known as sodium sulphate or sulfate of soda) is the inorganic compound with formula Na2SO4 mainly used for the manufacture of detergents and in the kraft process of paper pulping.</p>',
                                        'packings'=>'50 kgs',
                                    ],
                                ],
                    'fiberglass_resin'=> [
                                    
                                    [
                                        'name'=>'Vinyl Ester Resin (EPOXY) ETERSET 2960',
                                        'detail'=>'<p>ETERSET 2960 epoxy vinyl ester resin is based on bisphenol-A epoxy resin. It provides resistance to a wide range of acids, alkalis, bleaches, industrial or chemical factory floor lining and organic compounds used in chemical processing industry applications.</p>',
                                        'packings'=>'200kgs',
                                    ],
                                    [
                                        'name'=>'unsaturated polyester resin (UPR) ETERSET 2597 with BKI certificate',
                                        'detail'=>'<p>Excellent wet-out, thixotropic, low viscosity, promoted, non-wax resin. ideal for large structures (yacht, fishing boat, silo tank, etc). Certification by BKI、DNV、LRs.</p>',
                                        'packings'=>'20 kgs & 220 kgs',
                                    ],
                                    [
                                        'name'=>'unsaturated polyester resin (UPR) YUKALAC 157',
                                        'detail'=>'<p>thixotropic, low viscosity, promoted, non-wax resin. ideal for large structures (yacht, fishing boat, silo tank, etc)</p>',
                                        'packings'=>'225 kgs',
                                    ],
                                    [
                                        'name'=>'Vinyl ester resin (EPOXY) R804 - RIPOXY ex JAPAN',
                                        'detail'=>'<p>Ripoxy is a modified epoxy resin with low viscosity, chemical resistance to acid & alkali. Aplication for chemical tank, floor lining.</p>',
                                        'packings'=>'200 kgs',
                                    ],
                                    [
                                        'name'=>'Vinyl ester resin (EPOXY) DERAKANE 411-350',
                                        'detail'=>'<p>DERAKANE 411 epoxy vinyl ester resin is based on bisphenol-A epoxy resin. It provides resistance to a wide range of acids, alkalis, bleaches, and organic compounds used in chemical processing industry applications.</p>',
                                        'packings'=>'205 kgs',
                                    ],
                                    [
                                        'name'=>'Vinyl ester resin (EPOXY) DERAKANE 470-300',
                                        'detail'=>'<p>DERAKANE 470 epoxy vinyl ester resin is based on bisphenol-A epoxy resin. It provides resistance to a wide range of acids, alkalis, bleaches, and organic compounds used in chemical processing industry applications.</p>',
                                        'packings'=>'205 kgs',
                                    ],
                                    [
                                        'name'=>'Chempoxy clear epoxy resin',
                                        'detail'=>'<p>Chempoxy2 components  low viscosity clear epoxy resin (2:1) used for carbon fiber composite, floor coating, table top, vacum infusion.</p>',
                                        'packings'=>'2 kgs resin + 1 kg hardener',
                                    ],
                                    [
                                        'name'=>'Pioneer non sag epoxy',
                                        'detail'=>'<p>A two-component adhesive best suited for marine applications. Will not sag or drip if applied up to 1/4 of an inch thick making it ideal for vertical applications.Adheres well to damp surfaces.</p>',
                                        'packings'=>'170gr, 400gr, 800gr, 1600gr, 3200gr, 6500gr',
                                    ],
                                    [
                                        'name'=>'epoxy lycal 1011',
                                        'detail'=>'<p>2 components epoxy resin+hardener (3:1) use for souvenir making and coating</p>',
                                        'packings'=>'3 kgs resin + 1 kg hardener',
                                    ],
                                ],
                    'carbon_fiber'=> [
                                    [
                                        'name'=>'Carbon fiber fabric 2x2 twill TORAY JAPAN',
                                        'detail'=>'<p>Carbon fibers have several advantages including high stiffness, high tensile strength, low weight, high chemical resistance, high temperature tolerance and low thermal expansion. These properties have made carbon fiber very popular in aerospace, civil engineering, military, and motorsports, along with other competition sports. However, they are relatively expensive when compared with similar fibers, such as glass fibers or plastic fibers. Type available 200 / 240 GSM</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'Carbon fiber unidirectional Grade A',
                                        'detail'=>'<p>Carbon fibers have several advantages including high stiffness, high tensile strength, low weight, high chemical resistance, high temperature tolerance and low thermal expansion. These properties have made carbon fiber very popular in aerospace, civil engineering, military, and motorsports, along with other competition sports. However, they are relatively expensive when compared with similar fibers, such as glass fibers or plastic fibers. Type available 300 GSM</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'Carbon fiber hybrid kevlar 2x2 twill 200 GSM',
                                        'detail'=>'<p>type available black/red, black/blue, black/yellow, black orange</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'Carbon fiber fabric 2x2 twill grade A',
                                        'detail'=>'<p>Carbon fibers have several advantages including high stiffness, high tensile strength, low weight, high chemical resistance, high temperature tolerance and low thermal expansion. These properties have made carbon fiber very popular in aerospace, civil engineering, military, and motorsports, along with other competition sports. However, they are relatively expensive when compared with similar fibers, such as glass fibers or plastic fibers. Type available 200 / 220 / 240 GSM</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'Carbon fiber fabric 4x4 twill TORAY JAPAN',
                                        'detail'=>'<p>Carbon fibers have several advantages including high stiffness, high tensile strength, low weight, high chemical resistance, high temperature tolerance and low thermal expansion. These properties have made carbon fiber very popular in aerospace, civil engineering, military, and motorsports, along with other competition sports. However, they are relatively expensive when compared with similar fibers, such as glass fibers or plastic fibers. Type available 280 GSM</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'carbon fiber tube',
                                        'detail'=>'<p>Carbon fiber tubes are used in numerous applications like tactical ladders, trusses, beams, and more. Carbon fiber is typically chosen over traditional materials such as aluminum, steel, and titanium because of the following properties: High strength and stiffness to weight. Excellent resistance to fatigue. available diameter 3mm-50mm</p>',
                                        'packings'=>'',
                                    ],
                                    [
                                        'name'=>'carbon fiber sheet / plate',
                                        'detail'=>'<p>Carbon fiber sheet are used in numerous applications like aeromodeling, drones, structural reinforcement, etc . Carbon fiber is typically chosen over traditional materials such as aluminum, steel, and titanium because of the following properties: High strength and stiffness to weight. Excellent resistance to fatigue. available thickness 1mm-5mm</p>',
                                        'packings'=>'',
                                    ],
                                ],
                    
                  ];

    $data = [
                1 => [
                    'pict'=>'ex-product-bg.jpg',
                    'name'=>'Wastewater & Water Treatment Chemicals',
                    'name_id'=>'Air Limbah & Bahan Kimia Pengolahan Air',
                    'desc' => '<h5>Water treatment is a process that attemps to improves the quality of water to make it appropriate for a specific end-use. The end use after a water treatment process can be for drinking water, industrial water supply, irrigation water, river flow maintenance, water recreation or many other uses, including being safely returned to the environment.</h5><h5>Perdana Chemindo Perkasa offers comprehensive Fiberglass Composite Material products with a so many variety and options to choose from. Please consult to our customer relation staff for better product understandin</h5>',
                    'desc_id' => '<h5>Pengolahan air adalah proses yang berusaha untuk meningkatkan kualitas air agar sesuai untuk penggunaan akhir tertentu. Penggunaan akhir setelah proses pengolahan air bisa untuk air minum, suplai air industri, air irigasi, pemeliharaan aliran sungai, rekreasi air atau banyak kegunaan lainnya, termasuk dikembalikan ke lingkungan dengan aman.</h5><h5>Perdana Chemindo Perkasa menawarkan produk Fiberglass Composite Material yang komprehensif dengan begitu banyak variasi dan pilihan untuk dipilih. Silakan berkonsultasi dengan staf hubungan pelanggan kami untuk pemahaman produk yang lebih baik</h5>',
                    'dt_table' => '',
                ],
                [
                    'pict'=>'product-fibreglass-composite-cover.jpg',
                    'name'=>'Fiberglass Composite Material',
                    'name_id'=>'Bahan Komposit Fiberglass',
                    'desc' => '<h5>Fiberglass Composite Material is used to form any kind of fiberglass products. Fiberglass Composite Material can produce the highest strength to weight ratio known to man. The end product of fiberglass is corrosion resistant – provide long term resistance to severe chemical and temperature environments. With fiberglass, you can have design flexibility – it can be molded into complex shapes at relatively low coast.</h5><h5>Perdana Chemindo Perkasa offers comprehensive Fiberglass Composite Material products with a so many variety and options to choose from. Please consult to our customer relation staff for better product understanding.</h5>',
                    'desc_id' => '<h5>Bahan Komposit Fiberglass digunakan untuk membentuk segala jenis produk fiberglass. Fiberglass Composite Material dapat menghasilkan rasio kekuatan terhadap berat tertinggi yang diketahui manusia. Produk akhir dari fiberglass adalah tahan korosi – memberikan ketahanan jangka panjang terhadap lingkungan kimia dan suhu yang parah. Dengan fiberglass, Anda dapat memiliki fleksibilitas desain – dapat dibentuk menjadi bentuk yang kompleks di pantai yang relatif rendah.</h5><h5>Perdana Chemindo Perkasa menawarkan produk Bahan Komposit Fiberglass yang komprehensif dengan begitu banyak variasi dan pilihan untuk dipilih. Silakan berkonsultasi dengan staf hubungan pelanggan kami untuk pemahaman produk yang lebih baik.</h5>',
                    'dt_table' => '',
                ],
                [
                    'pict'=>'product-general-chemicals.jpg',
                    'name'=>'General Industrial Chemical',
                    'name_id'=>'Kimia Industri Umum',
                    'desc' => '<h5>General Industrial Chemical are variety of chemicals that are every where, sorruound our lives. These basic industrial chemicals creates an immense variety of products which impinge on virtually every aspect of our lives. Products such as detergents, soaps and perfumes, are the applications of general industrial chemicals. Industries will uses wide range of general industrial chemical materials, from air and minerals to oil.</h5><h5>Perdana Chemindo Perkasa offers comprehensive General Industrial Chemical products with a so many variety and options to choose from. Please consult to our customer relation staff for better product understanding.</h5>',
                    'desc_id' => '<h5>Kimia Industri Umum adalah berbagai bahan kimia yang ada di mana-mana, di sekitar kehidupan kita. Bahan kimia industri dasar ini menciptakan berbagai macam produk yang menimpa hampir setiap aspek kehidupan kita. Produk seperti deterjen, sabun dan parfum, adalah aplikasi bahan kimia industri umum. Industri akan menggunakan berbagai bahan kimia industri umum, dari udara dan mineral hingga minyak.</h5><h5>Perdana Chemindo Perkasa menawarkan produk Kimia Industri Umum yang komprehensif dengan begitu banyak variasi dan pilihan untuk dipilih. Silakan berkonsultasi dengan staf hubungan pelanggan kami untuk pemahaman produk yang lebih baik.</h5>',
                    'dt_table' => '',
                ],
                [
                    'pict'=>'product-fibreglass-resin.jpg',
                    'name'=>'Fiberglass Resin Material',
                    'name_id'=>'Bahan Resin Fiberglass',
                    'desc' => '<h5>Fiberglass Resin Material are essentially polyester resins, and they are used for many different purposes. They are mainly used as a casting material, a wood filling, an adhesive, and for auto repairs. The excellent adhesive properties and durability of the fiberglass resin material make them an extremely useful construction material.</h5><h5>Perdana Chemindo Perkasa offers comprehensive Fiberglass Resin Material with a so many variety and options to choose from. Please consult to our customer relation staff for better product understanding.</h5>',
                    'desc_id' => '<h5>Bahan Resin Fiberglass pada dasarnya adalah resin poliester, dan digunakan untuk berbagai tujuan. Mereka terutama digunakan sebagai bahan pengecoran, pengisi kayu, perekat, dan untuk perbaikan mobil. Sifat perekat yang sangat baik dan daya tahan dari bahan resin fiberglass menjadikannya bahan konstruksi yang sangat berguna.</h5><h5>Perdana Chemindo Perkasa menawarkan Bahan Resin Fiberglass yang lengkap dengan begitu banyak variasi dan pilihan untuk dipilih. Silakan berkonsultasi dengan staf hubungan pelanggan kami untuk pemahaman produk yang lebih baik.</h5>',
                    'dt_table' => '',
                ],
                [
                    'pict'=>'product-carbon-fiber-cover.jpg',
                    'name'=>'Carbon Fiber Composites Material',
                    'name_id'=>'Bahan Komposit Serat Karbon',
                    'desc' => '<h5>Carbon Fiber Composites Material is a polymer and is sometimes known as graphite fiber. It is a very strong material that is also very lightweight. Carbon fiber is five-times stronger than steel and twice as stiff. Though carbon fiber is stronger and stiffer than steel, it is lighter than steel; making it the ideal manufacturing material for many parts. These are just a few reasons why carbon fiber is favored by engineers and designers for manufacturing.</h5><h5>Perdana Chemindo Perkasa offers comprehensive Carbon Fiber Composites<br> Material with a so many variety and options to choose from. Please consult to our customer relation staff for better product understanding.</h5>',
                    'desc_id' => '<h5>Bahan Komposit Serat Karbon adalah polimer dan kadang-kadang dikenal sebagai serat grafit. Ini adalah bahan yang sangat kuat yang juga sangat ringan. Serat karbon lima kali lebih kuat dari baja dan dua kali lebih kaku. Meskipun serat karbon lebih kuat dan kaku dari baja, namun lebih ringan dari baja; menjadikannya bahan manufaktur yang ideal untuk banyak bagian. Ini hanya beberapa alasan mengapa serat karbon disukai oleh para insinyur dan desainer untuk manufaktur.</h5><h5>Perdana Chemindo Perkasa menawarkan Bahan Komposit Serat Karbon<br> yang komprehensif dengan begitu banyak variasi dan pilihan untuk dipilih. Silakan berkonsultasi dengan staf hubungan pelanggan kami untuk pemahaman produk yang lebih baik.</h5>',
                    'dt_table' => '',
                ],
                
            ];

    $result = $data[intval($_GET['id'])];

?>

<section class="product-sec-1 py-5 back-white">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-40">
                <div class="blocks_out_breadcrumbs">
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb m-0 p-0 bg-white">
                        <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"><?php echo (Yii::app()->language == 'en')? "Products":"Produk"; ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo ucwords(str_replace('-',' ', $_GET['name'])) ?></li>
                      </ol>
                    </nav>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="col-md-20">
                <div class="text-right backs_t_prd">
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>" class="btn btn-link p-0"><?php echo (Yii::app()->language == 'en')? "Back to Products":"Kembali ke Produk"; ?></a>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

        <div class="py-4 my-2"></div> 
        <div class="pictures_full"><img src="<?php echo $this->assetBaseurl. $result['pict'] ?>" alt="" class="img img-fluid"></div>
        <div class="py-2"></div>
        <div class="row content-text py-4 boxs_prd_detail_info">
            <div class="col-md-30">
                <?php if (Yii::app()->language == 'en'): ?>
                    <?php echo $result['desc'] ?>
                <?php else: ?>
                    <?php echo $result['desc_id'] ?>
                <?php endif ?>
                <!-- <a href="#" class="btn btn-link p-0">
                    <img src="<?php echo $this->assetBaseurl.'ex-btn-tokped.png' ?>" alt="" class="img img-fluid">
                </a> -->
            </div>

            <div class="col-md-30">
                <div class="infos_table">
                    <table class="table">
                      <thead>
                        <tr>
                            <?php if (Yii::app()->language == 'en'): ?>
                                <th><?php echo $result['name'] ?></th>
                            <?php else: ?>
                                <th><?php echo $result['name_id'] ?></th>
                            <?php endif ?>
                          <th><?php echo (Yii::app()->language == 'en')? "Details": "Detil"; ?></th>
                          <th><?php echo (Yii::app()->language == 'en')? "Packings": "Kemasan"; ?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 

                        switch (($_GET['id'])) {
                            case '2':
                                $t_table = $data_spec_prod['fiberglass_composite'];
                                break;
                            case '3':
                                $t_table = $data_spec_prod['general_industrial'];
                                break;
                            case '4':
                                $t_table = $data_spec_prod['fiberglass_resin'];
                                break;
                            case '5':
                                $t_table = $data_spec_prod['carbon_fiber'];
                                break;

                            default:
                                $t_table = $data_spec_prod['wastewater'];
                                break;
                        }
                        ?>
                        <?php foreach ($t_table as $key => $value): ?>
                        <tr>
                            <td><?php echo ucfirst($value['name']) ?></td>
                            <td>
                                <a role="button" data-toggle="modal" data-target="#exampleModalCenter" href="#" data-title="<?php echo ucwords($value['name']); ?>" data-texts="<?php echo $value['detail'] ?>"><i class="fa fa-search"></i></a>
                            </td>
                            <td><?php echo ($value['packings'])? $value['packings'] : "-" ?></td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
                    <div class="clear"></div>
                </div>
            </div>

        </div>
        <!-- End Row -->

        <div class="py-3"></div>
        <div class="clear"></div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function(){

        $('#exampleModalCenter').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var body = button.data('texts')
          var titles = button.data('title')
          // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
          // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
          // console.log("adsfasdf" + titles);
          var modal = $(this)
          modal.find('.modal-body > .titles').text(titles)
          modal.find('.modal-body > .descs').html(body)
        });

    });
</script>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">
            <img src="<?php echo $this->assetBaseurl; ?>logo-headers.png" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid maw145">
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5 class="titles"></h5>
        <div class="py-2"></div>

        <div class="descs"></div>
        <div class="py-3"></div>
      </div>
    </div>
  </div>
</div>