<section class="backs_home1 back-white py-5">
    <div class="prelatife container">
        <div class="inners py-5 text-center">
            <div class="py-2"></div>
            <div class="tops_title">
                <?php if (Yii::app()->language == 'en'): ?>
                <h2 class="m-0">Perdana Chemindo Perkasa Products & Services</h2>
                <?php else: ?>
                <h2 class="m-0">Produk & Layanan Perdana Chemindo Perkasa</h2>
                <?php endif ?>
            </div>
            <div class="py-3"></div>
            <?php 
                $arr1 = [
                            1 => [
                                'pict'=>'banners-hm-1.png',
                                'name'=>'Wastewater & Water Treatment Chemicals',
                                'name_id'=>'Air Limbah & Bahan Kimia Pengolahan Air',
                            ],
                            [
                                'pict'=>'banners-hm-2.png',
                                'name'=>'Fiberglass Composite Material',
                                'name_id'=>'Bahan Komposit Fiberglas',
                            ],
                            [
                                'pict'=>'banners-hm-3.png',
                                'name'=>'General Industrial Chemical',
                                'name_id'=>'Kimia Industri Umum',
                            ],
                            [
                                'pict'=>'banners-hm-4.png',
                                'name'=>'Fiberglass Resin Material',
                                'name_id'=>'Bahan Resin Fiberglass'
                            ],
                            [
                                'pict'=>'banners-hm-5.png',
                                'name'=>'Carbon Fiber Composites Material',
                                'name_id'=>'Bahan Komposit Serat Karbon',
                            ],
                            
                        ];
            ?>
            <div class="lists_perdana_home1">
                <div class="row">
                    <?php foreach ($arr1 as $key => $value): ?>
                    <div class="col-md-12">
                        <?php
                        $name_lg = (Yii::app()->language == 'en')? $value['name'] : $value['name_id'];
                        $links = CHtml::normalizeUrl(array('/home/products_detail', 'id'=> $key, 'name'=>Slug::Create($name_lg), 'lang' => Yii::app()->language ));
                        ?>
                        <div class="items">
                            <div class="pict">
                                <a href="<?php echo $links; ?>"><img src="<?php echo $this->assetBaseurl.$value['pict']; ?>" alt="" class="img img-fluid d-block mx-auto">
                                </a>
                            </div>
                            <div class="info py-2">
                                <?php if (Yii::app()->language == 'en'): ?>
                                    <a href="<?php echo $links; ?>"><h4><?php echo $value['name'] ?></h4></a>
                                <?php else: ?>
                                    <a href="<?php echo $links; ?>"><h4><?php echo $value['name_id'] ?></h4></a>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="clear clearfix"></div>
        </div>
    </div>
</section>

<section class="backs_home2 back-blues py-5">
    <div class="prelatife container">
        <div class="inners py-5 text-center">
            <div class="py-2"></div>
            <div class="tops_title">
                <?php if (Yii::app()->language == 'en'): ?>
                <h2 class="m-0">Industry Application For Our Products</h2>
                <?php else: ?>
                <h2 class="m-0">Aplikasi Industri Untuk Produk Kami</h2>
                <?php endif ?>
            </div>
            <div class="py-3"></div>
            <?php 
                $arr2 = [
                            [
                                'pict'=>'icons_data_1.jpg',
                                'name'=>'Water Treatment',
                                'name_id'=>'Pengolahan air',
                            ],
                            [
                                'pict'=>'icons_data_2.jpg',
                                'name'=>'Marine',
                                'name_id'=>'Perkapalan',
                            ],
                            [
                                'pict'=>'icons_data_3.jpg',
                                'name'=>'Household',
                                'name_id'=>'Rumah tangga',
                            ],
                            [
                                'pict'=>'icons_data_4.jpg',
                                'name'=>'Industrial',
                                'name_id'=>'Industri',
                            ],
                            [
                                'pict'=>'icons_data_5.jpg',
                                'name'=>'Automotive',
                                'name_id'=>'Otomotif',
                            ],
                            [
                                'pict'=>'icons_data_6.jpg',
                                'name'=>'Construction',
                                'name_id'=>'Konstruksi',
                            ]
                        ];
            ?>
            <div class="lists_perdana_home2">
                <div class="row">
                    <?php foreach ($arr2 as $key => $value): ?>
                    <div class="col-md-10 col-30">
                        <div class="items">
                            <div class="pict"><img src="<?php echo $this->assetBaseurl.'ind_application/'.$value['pict']; ?>" alt="" class="img img-fluid d-block mx-auto"></div>
                            <div class="info py-2">
                                <?php if (Yii::app()->language == 'en'): ?>
                                    <h4><?php echo $value['name'] ?></h4>
                                <?php else: ?>
                                    <h4><?php echo $value['name_id'] ?></h4>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="py-3 d-none"></div>

            <div class="text-center views_all_application d-none">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/industry', 'lang' => Yii::app()->language)); ?>" class="btn btn-link"><?php echo (Yii::app()->language == 'en')? "Click Here To View All Industry Application": "Klik Di Sini Untuk Melihat Semua Aplikasi Industri"; ?></a>
            </div>
            <div class="clear clearfix"></div>
        </div>
    </div>
</section>

<section class="backs_home3 back-bottles py-5">
    <div class="prelatife container">
        <div class="inners py-5 text-center">
            <div class="row">
                <div class="col-md-30 my-auto text-left">
                    <div class="texts_cont">
                        <?php if (Yii::app()->language == 'en'): ?>
                        <h3>We are the secret behind your successful products</h3>
                        <h5>We make sure that our customers' demands are met by providing the highest chemical products available.</h5>
                        <p>To bring our customer the best source of their flavours, we work and comply with the world’s best brands and manufacturer of Natural Extracts, Aromatic Chemicals, Natural Flavour and Essential Oils. We make sure our customer holds the key to limitless possibilities for their taste explorations.</p>
                        <?php else: ?>
                        <h3>Kami adalah rahasia di balik kesuksesan produk Anda</h3>
                        <h5>Kami memastikan bahwa permintaan pelanggan kami dipenuhi dengan menyediakan produk kimia tertinggi yang tersedia.</h5>
                        <p>Untuk memberikan pelanggan kami sumber rasa terbaik, kami bekerja dan mematuhi merek dan produsen Ekstrak Alami, Bahan Kimia Aromatik, Perasa Alami, dan Minyak Esensial terbaik di dunia. Kami memastikan pelanggan kami memegang kunci kemungkinan tak terbatas untuk eksplorasi rasa mereka.</p>
                        <?php endif ?>
                        <div class="py-1"></div>
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'lang' => Yii::app()->language)); ?>" class="btn btn-info btns_customs_defaultn"><?php echo (Yii::app()->language == 'en')? "LEARN MORE": "LEBIH LANJUT"; ?></a>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-30"></div>
            </div>
            <div class="clear clearfix"></div>
        </div>
    </div>
</section>

<section class="backs_home4 back-cream py-5">
    <div class="prelatife container">
        <div class="inners py-5 text-center">
            <div class="tops_title">
                <?php if (Yii::app()->language == 'en'): ?>
                <h2 class="m-0">Why Perdana Chemindo Perkasa as your first choice for chemicals</h2>
                <?php else: ?>
                <h2 class="m-0">Mengapa Perdana Chemindo Perkasa sebagai pilihan pertama Anda untuk bahan kimia</h2>
                <?php endif ?>
            </div>
            <div class="py-3"></div>
            <div class="less_can_numbers">
                <div class="row">
                    <div class="col-md-20">
                        <div class="boxed">
                            <div class="tops"><span>990+</span></div>
                            <div class="info">
                                <?php if (Yii::app()->language == 'en'): ?>
                                <p>Variations of products <br>for you to choose</p>
                                <?php else: ?>
                                <p>Variasi produk <br>untuk kamu pilih</p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-20">
                        <div class="boxed">
                            <div class="tops"><span>20+</span></div>
                            <div class="info">
                                <?php if (Yii::app()->language == 'en'): ?>
                                <p>Years of experience in industrial <br>chemicals & fiberglass fabrication</p>
                                <?php else: ?>
                                <p>Pengalaman bertahun-tahun di industri <br>bahan kimia & fabrikasi fiberglass</p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-20 no-borders">
                        <div class="boxed">
                            <div class="tops"><span>200+</span></div>
                            <div class="info">
                                <?php if (Yii::app()->language == 'en'): ?>
                                <p>Tons of product delivery <br>every month</p>
                                <?php else: ?>
                                <p>Banyak pengiriman produk <br>setiap bulan</p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear clearfix"></div>
        </div>
    </div>
</section>

<section class="backs_home5">
    <div class="tops back-blue text-center py-3">
        <div class="prelatife container">
            <?php if (Yii::app()->language == 'en'): ?>
            <h2 class="substitle">OUR QUALITY CERTIFICATIONS</h2>
            <?php else: ?>
            <h2 class="substitle">SERTIFIKASI KUALITAS KAMI</h2>
            <?php endif ?>
        </div>
    </div>

    <div class="prelatife container">
        <div class="inners py-4 text-center">
            <div class="banners_quality_iso d-none d-sm-block mx-auto text-center">
               <img src="<?php echo $this->assetBaseurl ?>banns_logo_gmp_systems.jpg" alt="" class="img img-fluid ">
            </div>
            <div class="banners_quality_iso d-block d-sm-none mx-auto text-center">
                <img src="<?php echo $this->assetBaseurl ?>banns_logo_gmp_systems_mobiles.jpg" alt="" class="img img-fluid">
            </div>
            <div class="clear clearfix"></div>
        </div>
    </div>
</section>
