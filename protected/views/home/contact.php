<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>hero-about.jpg');">
    <div class="outers_block_inner">
        <div class="prelative container">
            <div class="row">
                <div class="col-md-60">
                    <div class="insides_intext">
                        <h1><?php echo (Yii::app()->language == 'en')? "CONTACT": "KONTAK"; ?></h1>
                        <div class="py-1"></div>
                        <div class="back-lines_dncenter d-block mx-auto"></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-sec-1 pb-5 back-white pt-4">
    <div class="prelative container">
        
        <div class="blocks_out_breadcrumbs">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb m-0 p-0 bg-white">
                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo (Yii::app()->language == 'en')? "Contact":"Kontak" ?></li>
              </ol>
            </nav>
            <div class="clear"></div>
        </div>

        <div class="py-4 my-2"></div> 

        <div class="row text-center content-text py-4">
            <div class="col-md-60">
                <?php if (Yii::app()->language == 'en'): ?>
                <h2>Let Us Help You</h2>
                <h5>Perdana Chemindo Perkasa is here to respond to all your inquiries.</h5>
                <p><strong>Store & Head Office</strong><br>Jl. Penghela 30A, Surabaya 60174<br>East Java - Indonesia</p>
                <p><a class="maps" href="https://goo.gl/maps/An8camz9tYREVeQd6">View on Google map</a></p>
                <?php else: ?>
                <h2>Biarkan Kami Membantu Anda</h2>
                <h5>Perdana Chemindo Perkasa hadir untuk menjawab semua pertanyaan Anda.</h5>
                <p><strong>Toko &amp; Kantor Pusat</strong><br>Jl. Penghela 30A, Surabaya 60174<br>Jawa Timur - Indonesia</p>
                <p><a class="maps" href="https://goo.gl/maps/An8camz9tYREVeQd6">Lihat di peta Google</a></p>
                <?php endif ?>
            </div>
        </div>
        <div class="py-1"></div>
        <div class="lines-grey"></div>
        <div class="py-1"></div>
        <div class="py-3"></div>
        <div class="blocks_contact text-center">
            <?php if (Yii::app()->language == 'en'): ?>
            <h5>Online Inquiry Form</h5>
            <div class="py-1"></div>
            <p>Please fill out the details below, and we’ll be back to you shortly.</p>
            <?php else: ?>
            <h5>Formulir Permintaan Online</h5>
            <div class="py-1"></div>
            <p>Harap isi detail di bawah, dan kami akan segera menghubungi Anda kembali.</p>
            <?php endif ?>    
            <div class="py-3"></div>
            <div class="maw550 mx-auto d-block">
                <?php echo $this->renderPartial('//home/_form_contact2', array('model'=>$model)); ?>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="py-2"></div>
        <div class="clear"></div>
    </div>
</section>



<section class="backs_greys_contactcall py-4">
    <div class="prelatife container">
        <div class="row justify-content-center text-center">
            <div class="col-md-30 br-rights">
                <div class="boxed">
                    <div class="icons"><img src="<?php echo $this->assetBaseurl ?>icons_bx-contact_1.jpg" alt="" class="img img-fluid"></div>
                    <div class="texts">
                        <p><strong>Whatsapp Hotline</strong><br>
                            <a href="https://wa.me/6287883000048">+62 878 8300 0048</a> <br>
                            <a href="https://wa.me/62818596618">+62 818 596 618</a> <br>
                            <a href="https://wa.me/6281231888898">+62 812 3188 8898</a>
                        </p>
                    </div>
                </div>
            </div>
            <!-- <div class="col-md-20 br-rights">
                <div class="boxed">
                    <div class="icons"><img src="<?php echo $this->assetBaseurl ?>icons_bx-contact_2.jpg" alt="" class="img img-fluid"></div>
                    <div class="texts">
                        <p><strong>Telephone</strong><br>
                            031 5471 381</p>
                    </div>
                </div>
            </div> -->
            <div class="col-md-30">
                <div class="boxed">
                    <div class="icons"><img src="<?php echo $this->assetBaseurl ?>icons_bx-contact_3.jpg" alt="" class="img img-fluid"></div>
                    <div class="texts">
                        <p><strong>Email</strong><br>
                        <a href="mailto:salesadmin@perdanachemindo.com">salesadmin@perdanachemindo.com</a></p>
                    </div>
                </div>
            </div>

        </div>
        <div class="clear"></div>
    </div>
</section>

<style type="text/css">
    a.maps{
        text-decoration: underline !important;
    }
    a.maps:hover{
        text-decoration: none !important;
    }
</style>