<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>hero-about.jpg');">
    <div class="outers_block_inner">
        <div class="prelative container">
            <div class="row">
                <div class="col-md-60">
                    <div class="insides_intext">
                        <h1><?php echo (Yii::app()->language == 'en')? "PRODUCTS": "PRODUK"; ?></h1>
                        <div class="py-1"></div>
                        <div class="back-lines_dncenter d-block mx-auto"></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="product-sec-1 py-5 back-white">
    <div class="prelative container">
        
        <div class="blocks_out_breadcrumbs">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb m-0 p-0 bg-white">
                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo (Yii::app()->language == 'en')? "Products": "Produk"; ?></li>
              </ol>
            </nav>
            <div class="clear"></div>
        </div>

        <div class="py-4 my-2"></div> 

        <div class="row justtify-content-center content-text py-4">
            <div class="col-md-55 text-center">
                <?php if (Yii::app()->language == 'en'): ?>
                <h5>Perdana Chemindo Perkasa is aware that each of our customers’ needs are different, therefore our chemical product & service consultant will be ready to help you with all your requirements. Feel free to browse through our product and service category.</h5>
                <?php else: ?>
                <h5>Perdana Chemindo Perkasa menyadari bahwa setiap kebutuhan pelanggan kami berbeda, oleh karena
                itu konsultan produk &amp; layanan kimia kami akan siap membantu Anda dengan semua kebutuhan Anda.
                Jangan ragu untuk menelusuri kategori produk dan layanan kami.</h5>
                <?php endif ?>
            </div>
        </div>
        <div class="py-4"></div>

        <!-- start products -->
        <?php 
                $arr1 = [
                            1 => [
                                'pict'=>'banners-hm-1.png',
                                'name'=>'Wastewater & Water Treatment Chemicals',
                                'name_id'=>'Air Limbah & Bahan Kimia Pengolahan Air',
                            ],
                            [
                                'pict'=>'banners-hm-2.png',
                                'name'=>'Fiberglass Composite Material',
                                'name_id'=>'Bahan Komposit Fiberglass',
                            ],
                            [
                                'pict'=>'banners-hm-3.png',
                                'name'=>'General Industrial Chemical',
                                'name_id'=>'Kimia Industri Umum',
                            ],
                            [
                                'pict'=>'banners-hm-4.png',
                                'name'=>'Fiberglass Resin Material',
                                'name_id'=>'Bahan Resin Fiberglass',
                            ],
                            [
                                'pict'=>'banners-hm-5.png',
                                'name'=>'Carbon Fiber Composites Material',
                                'name_id'=>'Bahan Komposit Serat Karbon',
                            ],
                            
                        ];
            ?>
            <div class="lists_perdana_home1">
                <div class="row">
                    <?php foreach ($arr1 as $key => $value): ?>
                    <div class="col-md-12">
                        <?php
                        $name_lg = (Yii::app()->language == 'en')? $value['name'] : $value['name_id'];
                        $links = CHtml::normalizeUrl(array('/home/products_detail', 'id'=> $key, 'name'=>Slug::Create($name_lg), 'lang' => Yii::app()->language ));
                        ?>
                        <div class="items">
                            <div class="pict">
                                <a href="<?php echo $links; ?>"><img src="<?php echo $this->assetBaseurl.$value['pict']; ?>" alt="" class="img img-fluid d-block mx-auto">
                                </a>
                            </div>
                            <div class="info py-2">
                                <?php if (Yii::app()->language == 'en'): ?>
                                    <a href="<?php echo $links; ?>"><h4><?php echo $value['name'] ?></h4></a>
                                <?php else: ?>
                                    <a href="<?php echo $links; ?>"><h4><?php echo $value['name_id'] ?></h4></a>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
        <!-- end products -->

        <div class="py-4 my-2"></div>
        <div class="clear"></div>
    </div>
</section>