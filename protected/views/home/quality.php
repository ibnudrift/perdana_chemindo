<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>hero-about.jpg');">
    <div class="outers_block_inner">
        <div class="prelative container">
            <div class="row">
                <div class="col-md-60">
                    <div class="insides_intext">
                        <h1><?php echo (Yii::app()->language == 'en')? "QUALITY": "KUALITAS"; ?></h1>
                        <div class="py-1"></div>
                        <div class="back-lines_dncenter d-block mx-auto"></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="quality-sec-1 py-5 back-white">
    <div class="prelative container">
        
        <div class="blocks_out_breadcrumbs">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb m-0 p-0 bg-white">
                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo (Yii::app()->language == 'en')? "Quality":"Kualitas" ?></li>
              </ol>
            </nav>
            <div class="clear"></div>
        </div>

        <div class="py-4 my-2"></div> 

        <div class="row text-center justify-content-center content-text py-4">
            <div class="col-md-55">
                <?php if (Yii::app()->language == 'en'): ?>
                <h2>Ensuring Your Success By Performing The Highest Quality Process</h2>
                <h5>Perdana Chemindo Perkasa has become a recognised name that sets the bar in terms of quality service. Even when you found the same products, we have better service to make sure that all the needs of products from our customer will be sufficient both in quantity and quality.</h5>
                <p>Perdana Chemindo Perkasa’s main marketing tool is in the quality experience that felt directly by our customers. We have learnt that when quality is prioritised, profit will follow. All of the hardwork is to build profit for a broader range of market, that is our customers’ success. We are proud of what we did, all the standard of practice and quality parameter were meant to build a strong and long partnership of chain businesses supply. Every action we take and every decision we made, is made with the vision of perfection in our customer's side.</p>
                <?php else: ?>
                <h2>Memastikan Kesuksesan Anda Dengan Melakukan Proses Kualitas Tertinggi</h2>
                <h5>Perdana Chemindo Perkasa telah menjadi nama yang diakui yang menetapkan standar dalam hal
                kualitas layanan. Bahkan ketika Anda menemukan produk yang sama, kami memiliki layanan yang lebih
                baik untuk memastikan bahwa semua kebutuhan produk dari pelanggan kami akan mencukupi baik
                secara kuantitas maupun kualitas.</h5>
                <p>Alat pemasaran utama Perdana Chemindo Perkasa adalah pengalaman kualitas yang dirasakan langsung
                oleh pelanggan kami. Kami telah belajar bahwa ketika kualitas diprioritaskan, keuntungan akan
                mengikuti. Semua kerja keras adalah untuk membangun keuntungan untuk pasar yang lebih luas, itulah
                kesuksesan pelanggan kami. Kami bangga dengan apa yang kami lakukan, semua standar praktik dan
                parameter kualitas dimaksudkan untuk membangun kemitraan bisnis rantai pasokan yang kuat dan
                panjang. Setiap tindakan yang kami ambil dan setiap keputusan yang kami buat, dibuat dengan visi
                kesempurnaan di sisi pelanggan kami.</p>
                <?php endif ?>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</section>

<?php 
$dn_procedures = [
                    [
                        'pict'=>'chemindo-quality-01.jpg',
                        'info'=>'Ultra Strict & Thorough Quality Checking Parameter',
                        'info_id'=>'Parameter Pemeriksaan Kualitas Ultra Ketat & Teliti',
                    ],
                    [
                        'pict'=>'chemindo-quality-02.jpg',
                        'info'=>'Chemical Products Batch Testing To Eliminate Defects',
                        'info_id'=>'Pengujian Batch Produk Kimia Untuk Menghilangkan Cacat',
                    ],
                    [
                        'pict'=>'chemindo-quality-03.jpg',
                        'info'=>'Consistent, Reliable And Punctual On Every Delivery',
                        'info_id'=>'Konsisten, Dapat Diandalkan, Tepat Waktu saat Pengiriman',
                    ],
                    
                ];
?>
<section class="quality-sec-1_2 pt-5">
    <div class="prelative container">
        <div class="row text-left content-text text-center">
            <div class="col-md-60">
                <?php if (Yii::app()->language == 'en'): ?>
                <h2>Quality Process Procedure</h2>
                <?php else: ?>
                <h2>Prosedur Proses Kualitas</h2>
                <?php endif ?>
                <div class="py-4"></div>
                <div class="lists_thumbbanners_cf">
                    <div class="row">
                        <?php foreach ($dn_procedures as $key => $value): ?>
                        <div class="col-md-20">
                            <div class="boxeds">
                                <div class="pict"><img src="<?php echo $this->assetBaseurl. $value['pict'] ?>" alt="" class="img img-fluid"></div>
                                <div class="info my-auto d-block d-sm-none">
                                    <?php if (Yii::app()->language == 'en'): ?>
                                        <h3><?php echo $value['info'] ?></h3>
                                    <?php else: ?>
                                        <h3><?php echo $value['info_id'] ?></h3>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="d-block d-sm-none py-4"></div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</section>


<section class="quality-sec-2">
    <div class="prelative container">
        <div class="row text-left content-text text-center">
            <div class="col-md-60">
                <div class="d-none d-sm-block">
                    <div class="lists_thumbbanners_cf">
                        <div class="row">
                            <?php foreach ($dn_procedures as $key => $value): ?>
                            <div class="col-md-20">
                                <div class="boxeds">
                                    <div class="info my-auto">
                                        <?php if (Yii::app()->language == 'en'): ?>
                                            <h3><?php echo $value['info'] ?></h3>
                                        <?php else: ?>
                                            <h3><?php echo $value['info_id'] ?></h3>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="py-5"></div>
                    <div class="py-1"></div>
                </div>

                <div class="text-center blobs_centers">
                    <div class="tags_top d-block mx-auto text-center">
                        <img src="<?php echo $this->assetBaseurl. 'icons_tag-whitenuriled.png' ?>" class="img img-fluid">
                        <div class="py-2 my-1"></div>
                        <?php if (Yii::app()->language == 'en'): ?>
                        <h4>We do everything with our customer in mind.</h4>
                        <?php else: ?>
                        <h4>Kami melakukan segalanya dengan mempertimbangkan pelanggan kami.</h4>
                        <?php endif ?>
                    </div>
                </div>
                <div class="py-5"></div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</section>


