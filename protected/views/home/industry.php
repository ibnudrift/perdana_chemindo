<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>hero-about.jpg');">
    <div class="outers_block_inner">
        <div class="prelative container">
            <div class="row">
                <div class="col-md-60">
                    <div class="insides_intext">
                        <h1>INDUSTRY APPLICATION</h1>
                        <div class="py-1"></div>
                        <div class="back-lines_dncenter d-block mx-auto"></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="industry-sec-1 pt-5 back-white">
    <div class="prelative container">
        
        <div class="blocks_out_breadcrumbs">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb m-0 p-0 bg-white">
                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Industry Application</li>
              </ol>
            </nav>
            <div class="clear"></div>
        </div>

        <div class="py-4 my-2"></div> 

        <div class="row content-text text-center py-4 justify-content-center">
            <div class="col-md-45">
                <h5>Various products from Perdana Chemindo Perkasa are widely used, but not limited to the industries below. Please contact us for further details on your needs.</h5>
            </div>
        </div>
        <div class="py-4"></div>

        <?php 
        $arr_data = [
                        [
                            'icons'=>'icon-industrys_loc_1.png',
                            'titles'=>'Water Treatment',
                            'picture'=>'perdana-chemical-industry-water-treatment.jpg',
                            'intro_desc'=>'<p>Perdana Chemindo Perkasa menyediakan berbagai produk lengkap untuk aneka keperluaan water treatment, untuk pengolahan waste water dan untuk keperluan pembersihan.</p>',
                            'desc_product'=>'<ul><li>alumunium sulfat / tawas</li><li>poly alumunium chloride white (CHINA/GERMAN)</li><li>poly alumunium chloride kuning (CHINA)</li><li>Trichloro isocyanuric acid TCCA 90% powder, granular, tablet</li><li>calcium hypochlorite/kaporit tjiwi kimia 60%</li><li>naocl/sodium hypochlorite 12%</li><li>HCL 32%</li><li>calcium carbonate/soda ash</li><li>caustic soda flake 98%</li><li>h2o2</li><li>naoh/caustic soda liquid</li><li>oto phenol/oto chlorine test</li><li>cooper sulfat 24.5%</li><li>sodium metabisulfite food grade</li><li>citric acid monohydrate foodgrade</li><li>trilite korea cation resin</li></ul>',
                            'desc_application'=>'<ul><li>water treatment</li><li>waste water treatment</li><li>pool</li><li>PDAM</li><li>body care</li><li>drinking water</li><li>tambak udang</li><li>desinfectant</li><li>food processing</li><li>marine</li><li>mining & drilling</li><li>oil & gas</li><li>MRO</li><li>construction</li><li>steel</li><li>textile</li><li>plantation</li><li>pulp&paper</li><li>paint&coating</li><li>household</li><li>fertilizer</li><li>food</li></ul>',
                        ],
                        [
                            'icons'=>'icon-industrys_loc_2.png',
                            'titles'=>'Marine',
                            'picture'=>'perdana-chemical-industry-marine.jpg',
                            'intro_desc'=>'<p>Perdana Chemindo Perkasa menyediakan berbagai produk lengkap untuk aneka keperluaan water treatment, untuk pengolahan waste water dan untuk keperluan pembersihan.</p>',
                            'desc_product'=>'<ul><li>unsaturated polyester resin ETERSET 2597</li><li>unsaturated polyester resin YUKALAC 157</li><li>Epoxy Vinyl ester resin Ripoxy R-804</li><li>Epoxy Vinyl ester resin DERAKANE MOMENTUM 411-350</li><li>epoxy vinyl ester resin DERAKANE MOMENTUM 470-300</li><li>chempocast epoxy resin</li><li>pioneer non sag epoxy</li><li>fiberglass mat 200/300/450/600 CTG taishan fiber</li><li>fiberglass mat 200/300/450/600 chempoglass</li><li>woven roving 200/400/600/800 chempoglass</li><li>carbon fiber fabric TORAY japan 200/220/240 2x2 twill</li><li>carbon fiber fabric TORAY japan 280 4x4 twill</li><li>carbon fiber fabric china A grade 200/220/240 2x2 twill</li><li>carbon fiber hybrid kevlar black/red, black/blue, black/yellow, black/orange 2x2 twill 200</li><li>carbon fiber unidirectional china A grade 300</li><li>honeycomb PP 8mm</li><li>PU foam sheet 5cm</li><li>PU rigid foam A+B liquid</li></ul>',
                            'desc_application'=>'<ul><li>fiberglass boat</li><li>fiberglass tank</li><li>waterproofing</li><li>general purpose fiberglass with BKI</li><li>DNV</li><li>Lloyd certf</li><li>fiberglass/carbon fiber composite</li><li>hot/cold insulation</li><li>floating buoys</li></ul>',
                        ],
                        [
                            'icons'=>'icon-industrys_loc_3.png',
                            'titles'=>'Household',
                            'picture'=>'perdana-chemical-industry-household.jpg',
                            'intro_desc'=>'<p>Perdana Chemindo Perkasa menyediakan berbagai produk lengkap untuk aneka keperluaan water treatment, untuk pengolahan waste water dan untuk keperluan pembersihan.</p>',
                            'desc_product'=>'<ul><li>caustic soda flake 98%</li><li>naoh/caustic soda liquid</li><li>sodium bicarbonate / soda kue food grade</li><li>trisodium phospate</li><li>sodium tripoly phospate</li><li>sodium sulfate</li></ul>',
                            'desc_application'=>'<ul><li>food</li><li>household</li><li>MRO</li><li>fertilizer</li><li>textile</li></ul>',
                        ],
                        [
                            'icons'=>'icon-industrys_loc_4.png',
                            'titles'=>'Industrial',
                            'picture'=>'perdana-chemical-industry-industrial.jpg',
                            'intro_desc'=>'<p>Perdana Chemindo Perkasa menyediakan berbagai produk lengkap untuk aneka keperluaan water treatment, untuk pengolahan waste water dan untuk keperluan pembersihan.</p>',
                            'desc_product'=>'<ul><li>resin LP1Q</li><li>Epoxy Vinyl ester resin Ripoxy R-804</li><li>Epoxy Vinyl ester resin DERAKANE MOMENTUM 411-350</li><li>epoxy vinyl ester resin DERAKANE MOMENTUM 470-300</li></ul>',
                            'desc_application'=>'<ul><li>marine</li><li>construction</li><li>anti-corossion coating</li><li>fiberglass</li></ul>',
                        ],
                        [
                            'icons'=>'icon-industrys_loc_5.png',
                            'titles'=>'Automotive',
                            'picture'=>'perdana-chemical-industry-automotive.jpg',
                            'intro_desc'=>'<p>Perdana Chemindo Perkasa menyediakan berbagai produk lengkap untuk aneka keperluaan water treatment, untuk pengolahan waste water dan untuk keperluan pembersihan.</p>',
                            'desc_product'=>'<ul><li>unsaturated polyester resin ETERSET 2597</li><li>unsaturated polyester resin YUKALAC 157</li><li>chempocast epoxy resin</li><li>pioneer durasteel epoxy 5 min</li><li>fiberglass mat 200/300/450/600 CTG taishan fiber</li><li>fiberglass mat 200/300/450/600 chempoglass</li><li>woven roving 200/400/600/800 chempoglass</li><li>carbon fiber fabric TORAY japan 200/220/240 2x2 twill</li><li>carbon fiber fabric TORAY japan 280 4x4 twill</li><li>carbon fiber fabric china A grade 200/220/240 2x2 twill</li><li>carbon fiber hybrid kevlar black/red, black/blue, black/yellow, black/orange 2x2 twill 200</li><li>carbon fiber unidirectional china A grade 300</li><li>honeycomb PP 8mm</li><li>PU foam sheet 5cm</li><li>PU rigid foam A+B liquid</li><li>sulfuric acid 98%</li></ul>',
                            'desc_application'=>'<ul><li>fiberglass boat</li><li>fiberglass tank</li><li>waterproofing</li><li>general purpose fiberglass with BKI</li><li>DNV</li><li>Lloyd certf</li><li>steel joint</li><li>fiberglass boat</li><li>self leveling floor coating</li><li>carbon fiber</li><li>hard surface table top</li><li>fiberglass with BKI</li><li>Lloyd</li><li>DNV certf</li><li>carbon fiber composite</li><li>core material for fiberglass/carbon fiber composite</li><li>hot/cold insulation</li><li>floating buoys</li></ul>',
                        ],
                        [
                            'icons'=>'icon-industrys_loc_6.png',
                            'titles'=>'Construction',
                            'picture'=>'perdana-chemical-industry-construction.jpg',
                            'intro_desc'=>'<p>Perdana Chemindo Perkasa menyediakan berbagai produk lengkap untuk aneka keperluaan water treatment, untuk pengolahan waste water dan untuk keperluan pembersihan.</p>',
                            'desc_product'=>'<ul><li>unsaturated polyester resin ETERSET 2597</li><li>unsaturated polyester resin YUKALAC 157</li><li>resin LP1Q</li><li>Epoxy Vinyl ester resin Ripoxy R-804</li><li>Epoxy Vinyl ester resin DERAKANE MOMENTUM 411-350</li><li>epoxy vinyl ester resin DERAKANE MOMENTUM 470-300</li><li>chempocast epoxy resin</li><li>pioneer non sag epoxy</li><li>pioneer durasteel epoxy 5 min</li><li>fiberglass mat 200/300/450/600 CTG taishan fiber</li><li>fiberglass mat 200/300/450/600 chempoglass</li><li>woven roving 200/400/600/800 chempoglass</li><li>carbon fiber fabric TORAY japan 200/220/240 2x2 twill</li><li>carbon fiber fabric TORAY japan 280 4x4 twill</li><li>carbon fiber fabric china A grade 200/220/240 2x2 twill</li><li>carbon fiber hybrid kevlar black/red, black/blue, black/yellow, black/orange 2x2 twill 200</li><li>carbon fiber unidirectional china A grade 300</li><li>honeycomb PP 8mm</li><li>PU foam sheet 5cm</li><li>PU rigid foam A+B liquid</li><li>gypsum roving yarn</li><li>fiberglass polyester mesh</li><li>HCL 32%</li><li>h2o2</li><li>nitric acid 68%</li><li>sulfuric acid 98%</li><li>phosporic acid 85% food grade</li><li>SGP siam gypsum powder</li><li>talc powder liaoning</li><li>pioneer mighty tape</li><li>sodium silicate/waterglass</li></ul>',
                            'desc_application'=>'<ul><li>fiberglass boat</li><li>fiberglass tank</li><li>waterproofing</li><li>general purpose fiberglass with BKI</li><li>DNV</li><li>Lloyd certf</li><li>industrial anti corossion from chemical</li><li>steel joint</li><li>fiberglass boat</li><li>self leveling floor coating</li><li>carbon fiber</li><li>hard surface table top</li><li>fiberglass/carbon fiber composite</li><li>hot/cold insulation</li><li>floating buoys</li></ul>',
                        ],
                        
                    ];
        ?>
        <!-- start industry -->
        <div class="lists_arr_icon_applications text-center">
            <div class="row no-gutters justify-content-center tops_nlist_icon">
                <?php foreach ($arr_data as $key => $value): ?>
                <div class="col-md-10 col-30">
                    <div class="boxeds s_<?php echo $key ?> <?php if ($key == 0): ?>actived<?php endif ?>" data-id="<?php echo $key ?>">
                        <div class="pict">
                            <img data-active="<?php echo $this->assetBaseurl.'industrys/on/'.$value['icons'] ?>" data-non-active="<?php echo $this->assetBaseurl.'industrys/'.$value['icons'] ?>" src="<?php echo $this->assetBaseurl.'industrys/'.$value['icons'] ?>" alt="" class="img img-fluid"></div>
                        <div class="info">
                            <h5><?php echo $value['titles'] ?></h5>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
        <!-- end industry -->
        <div class="clear"></div>
    </div>
</section>
<script type="text/javascript">
    
    $(function(){

        if ($('.lists_arr_icon_applications .boxeds').hasClass('actived')) {
            var sn_pic_on3 = $('.lists_arr_icon_applications .boxeds.actived').find('img').attr('data-active');
            $('.lists_arr_icon_applications .boxeds.actived').find('img').attr('src', sn_pic_on3);
        }

        $('.lists_arr_icon_applications .boxeds').on('mouseenter', function(){
            $('.lists_arr_icon_applications .boxeds').removeClass('actived');
            var sn_pic_on44 = $('.lists_arr_icon_applications .boxeds.s_0').find('img').attr('data-non-active');
            $('.lists_arr_icon_applications .boxeds.s_0').find('img').attr('src', sn_pic_on44);
            // end retur first active

            var sn_pic_active = $(this).find('img').attr('data-active');
            $(this).find('img').attr('src', sn_pic_active);
            
            // var n_ids = parseInt($(this).attr('data-id'));
            // $('#car_industry').carousel(n_ids);
            return false;
        });

        $('.lists_arr_icon_applications .boxeds').on('mouseleave', function(){
            var sn_pic_active = $(this).find('img').attr('data-non-active');
            $(this).find('img').attr('src', sn_pic_active);
        });

        $('.lists_arr_icon_applications .boxeds').on('click', function(){
            var n_ids = parseInt($(this).attr('data-id'));
            $('#car_industry').carousel(n_ids);
            return false;
        });
        // $('#car_industry').on('slid.bs.carousel', function () {
        //     currentIndex = $('div.active').index();

        //     // disable process
        //     $('.lists_arr_icon_applications .boxeds').removeClass('actived');
        //     var sn_pic_disab = $('.lists_arr_icon_applications .boxeds .pict').children('img').attr('data-non-active');
        //     // $('.lists_arr_icon_applications .boxeds').find('img').attr('src', sn_pic_disab);
        //     console.log(sn_pic_disab);

        //     // active process
        //     var sel_aktif = $('.lists_arr_icon_applications .boxeds.s_'+currentIndex);
        //     $(sel_aktif).addClass('actived');
        //     var pic_nact = $(sel_aktif).find('img').attr('data-active');
        //     $(sel_aktif).find('img').attr('src', pic_nact);
        // });

    });
</script>

<section class="industry-sec-2 py-5 back-bluedark">
    <div class="prelative container">

        <div class="py-5">

            <div id="car_industry" class="carousel slide" data-ride="carousel" data-interval="400000000">
              <div class="carousel-inner">
                <?php foreach ($arr_data as $key => $value): ?>
                <div class="carousel-item <?php echo ($key == 0)? 'active': '' ?>">
                  <div class="row text-left content-text">
                        <div class="col-md-30">
                            <div class="banner_default"><img src="<?php echo $this->assetBaseurl. $value['picture']; ?>" alt="" class="img img-fluid"></div>
                        </div>
                        <div class="col-md-30">
                            <div class="infos">
                                <h3><?php echo $value['titles'] ?></h3>
                                <?php echo $value['intro_desc'] ?>
                                <div class="py-2"></div>
                                <div class="row">
                                    <div class="col-md-35">
                                        <p class="mb-0"><b><?php echo $value['titles'] ?> Products:</b></p>
                                        <?php echo $value['desc_product']; ?>
                                    </div>
                                    <div class="col-md-25">
                                        <p class="mb-0"><b><?php echo $value['titles'] ?> Application:</b></p>
                                        <?php echo $value['desc_application']; ?>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
              </div>
            </div>
            <!-- end slider -->

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
</section>


<section class="industry-sec-3 bg-white">
    <div class="tops_dark_blue py-3 text-center">
        <h5>OUR QUALITY CERTIFICATIONS</h5>
    </div>
    <div class="prelative container py-2">
        <div class="row text-left content-text text-center py-5">
            <div class="col-md-60">
                <div class="picts_quality_certificate">
                    <img src="<?php echo $this->assetBaseurl.'certificates.jpg'; ?>" alt="" class="img img-fluid">
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>