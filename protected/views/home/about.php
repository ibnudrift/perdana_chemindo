<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>hero-about.jpg');">
    <div class="outers_block_inner">
        <div class="prelative container">
            <div class="row">
                <div class="col-md-60">
                    <div class="insides_intext">
                        <h1><?php echo (Yii::app()->language == 'en')? "About": "Tentang"; ?></h1>
                        <div class="py-1"></div>
                        <div class="back-lines_dncenter d-block mx-auto"></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-sec-1 py-5 back-white">
    <div class="prelative container">
        
        <div class="blocks_out_breadcrumbs">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb m-0 p-0 bg-white">
                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo (Yii::app()->language == 'en')? "About": "Tentang"; ?></li>
              </ol>
            </nav>
            <div class="clear"></div>
        </div>

        <div class="py-4"></div> 

        <div class="row text-left content-text py-4">
            <div class="col-md-30">
                <?php if (Yii::app()->language == 'en'): ?>
                <h2>About Perdana Chemindo Perkasa</h2>
                <h5>At Perdana Chemindo Perkasa, we understand each of the specific needs from our customers and we are ready to provide not just chemical product supplies as commodity, instead we enhanced each of customer’s experience with value added service solution, ready and tailored upon your request.</h5>
                <p>Our long and reputable existence in the water treatment, fiber glass related chemicals and industrial chemical products has provided us a huge business networking. We are known as a competitive supplier with service excellence and product availability that you can count on. Perdana Chemindo Perkasa carter to the needs of chemicals for various types of industries and business sizes, ranging from water treatment related needs, marine, construction, automotive, anti-corrosion coating, furniture, fiberglass industries, household industries with  sizes from giant public listed companies to small home industries.</p>
                <p>Perdana Chemindo Perkasa operates with “Customer First” in mind. We always emphasize of one to one sales, we greet you by the name and use up to bottom sales technique. We pride ourselves with superior chemical product quality at reasonable pricing. We response quickly to customers needs of chemical products by providing prompt delivery, technical services and support. We have an extensive range of knowledge in the chemical world, especially focusing to water treatment, figerglass, and industrial chemicals with excellent services and back up.</p>
                <?php else: ?>
                <h2>Tentang Perdana Chemindo Perkasa</h2>
                <h5>Di Perdana Chemindo Perkasa, kami memahami setiap kebutuhan spesifik dari pelanggan kami dan kami
                siap untuk menyediakan tidak hanya pasokan produk kimia sebagai komoditas, tetapi kami
                meningkatkan setiap pengalaman pelanggan dengan solusi layanan bernilai tambah, siap dan
                disesuaikan dengan permintaan Anda.</h5>
                <p>Keberadaan kami yang lama dan bereputasi baik dalam pengolahan air, bahan kimia terkait fiber glass
                dan produk kimia industri telah memberi kami jaringan bisnis yang besar. Kami dikenal sebagai pemasok
                yang kompetitif dengan keunggulan layanan dan ketersediaan produk yang dapat Anda andalkan.
                Perdana Chemindo Perkasa melayani kebutuhan bahan kimia untuk berbagai jenis industri dan ukuran
                bisnis, mulai dari kebutuhan terkait water treatment, marine, konstruksi, otomotif, anti korosi coating,
                furniture, industri fiberglass, industri rumah tangga dengan ukuran dari perusahaan publik raksasa ke
                industri kecil rumahan.</p>
                <p>Perdana Chemindo Perkasa beroperasi dengan mempertimbangkan “Utamakan Pelanggan”. Kami selalu
                menekankan one to one sales, kami menyapa Anda dengan nama dan menggunakan teknik penjualan
                yang up to bottom. Kami bangga dengan kualitas produk kimia yang unggul dengan harga yang wajar.
                Kami merespon dengan cepat kebutuhan pelanggan akan produk kimia dengan menyediakan
                pengiriman yang cepat, layanan teknis dan dukungan. Kami memiliki pengetahuan yang luas di dunia
                kimia, terutama berfokus pada pengolahan air, figerglass, dan bahan kimia industri dengan layanan dan
                cadangan yang sangat baik.</p>
                <?php endif ?>
                <div class="py-1"></div>
            </div>
            <div class="col-md-30">
                <div class="banner_default"><img src="<?php echo $this->assetBaseurl.'about-banner.jpg' ?>" alt="" class="img img-fluid"></div>
            </div>
        </div>

        <div class="py-5"></div>
        <div class="lines-grey h2"></div>
        <div class="py-5"></div>

        <div class="row blocks_vision_mission">
            <div class="col-md-30">
                <?php if (Yii::app()->language == 'en'): ?>
                <h2 class="titles">Vision</h2>
                <h5>Perdana Chemindo Perkasa to become the most reliable and long-term partner to enrich our businesses partners in water treatment, fiberglass related industry and industrial chemicals by unbeatable price versus value, smart services and efficient products with complete solution coverage.</h5>
                <?php else: ?>
                <h2 class="titles">Visi</h2>
                <h5>Perdana Chemindo Perkasa untuk menjadi mitra paling andal dan jangka panjang untuk memperkaya
                mitra bisnis kami dalam pengolahan air, industri terkait fiberglass dan bahan kimia industri dengan
                harga versus nilai yang tidak ada duanya, layanan cerdas dan produk efisien dengan cakupan solusi
                lengkap.</h5>
                <?php endif ?>
            </div>
            <div class="col-md-30">
                <?php if (Yii::app()->language == 'en'): ?>
                <h2 class="titles">Mission</h2>
                <ul>
                    <li>To provide high quality products and superior customer service with a focus on value-added products to ensure successful operations of our customers end process.</li>
                    <li>To be the first in adapting the latest product innovation and the first to introduce for customer’s benefitv </li>
                </ul>
                <?php else: ?>
                <h2 class="titles">Misi</h2>
                <ul>
                    <li>Untuk menyediakan produk berkualitas tinggi dan layanan pelanggan yang unggul dengan fokus
                    pada produk bernilai tambah untuk memastikan operasi yang sukses dari proses akhir pelanggan
                    kami.</li>
                    <li>Untuk menyediakan produk berkualitas tinggi dan layanan pelanggan yang unggul dengan fokus
                    pada produk bernilai tambah untuk memastikan operasi yang sukses dari proses akhir pelanggan
                    kami.</li>
                </ul>
                <?php endif ?>
            </div>
        </div>
        <div class="py-4"></div>

        <div class="clear"></div>
    </div>
</section>

<section class="about-sec-2 py-5 back-bluedark">
    <div class="prelative container">
        <div class="row content-text justify-content-center py-4 text-center">
            <div class="col-md-35">
                <div class="d-block mx-auto pic_centers text-center">
                    <img src="<?php echo $this->assetBaseurl; ?>small-bc_titles_white.jpg" alt="" class="img img-fluid">
                </div>
                <?php if (Yii::app()->language == 'en'): ?>
                <h3>Our value can only be measured by your success.</h3>
                <p>We are your long-term partner and we will always supply and facilitate your business through efficient and high quality chemical products.</p>
                <?php else: ?>
                <h3>Nilai kami hanya dapat diukur dengan kesuksesan Anda.</h3>
                <p>Kami adalah mitra jangka panjang Anda dan kami akan selalu menyediakan dan memfasilitasi bisnis
                Anda melalui produk kimia yang efisien dan berkualitas tinggi.</p>
                <?php endif ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


<section class="about-sec-3 py-5 bg-white">
    <div class="prelative container py-5">
        <div class="row text-left content-text text-center">
            <div class="col-md-60 pt-4 pb-3">
                <?php if (Yii::app()->language == 'en'): ?>
                <h2 class="titles">Our Commitment</h2>
                <h5>Perdana Chemindo Perkasa’s Standard Service Value</h5>
                <?php else: ?>
                <h2 class="titles">Komitmen kita</h2>
                <h5>Nilai Layanan Standar Perdana Chemindo Perkasa</h5>
                <?php endif ?>
                <div class="py-3"></div>

                <div class="lists_commitments_compet">
                    <div class="row">
                        <div class="col-md-20">
                            <div class="boxeds">
                                <div class="picture mb-3">
                                    <img src="<?php echo $this->assetBaseurl.'banners_commitments_pld_1.jpg' ?>" alt="" class="img img-fluid">
                                </div>
                                <div class="info">
                                    <?php if (Yii::app()->language == 'en'): ?>
                                    <h4>COMPETITIVE PRICE</h4>
                                    <p>We have our advantages in the head of the chain network, and we’ll do what it takes to provide the lowest cost to our customers.</p>
                                    <?php else: ?>
                                    <h4>HARGA BERSAING</h4>
                                    <p>Kami memiliki keunggulan kami di kepala jaringan rantai, dan kami akan melakukan apa yang diperlukan untuk memberikan biaya terendah kepada pelanggan kami.</p>
                                    <?php endif ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-20">
                            <div class="boxeds">
                                <div class="picture mb-3">
                                    <img src="<?php echo $this->assetBaseurl.'banners_commitments_pld_2.jpg' ?>" alt="" class="img img-fluid">
                                </div>
                                <div class="info">
                                    <?php if (Yii::app()->language == 'en'): ?>
                                    <h4>ONLY THE High Quality Products</h4>
                                    <p>We consistently check for each product’s quality, batch by batch to ensure our customer satisfaction.</p>
                                    <?php else: ?>
                                    <h4>HANYA PRODUK Berkualitas Tinggi</h4>
                                    <p>Kami secara konsisten memeriksa kualitas setiap produk, batch demi batch untuk memastikan kepuasan pelanggan kami.</p>
                                    <?php endif ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-20">
                            <div class="boxeds">
                                <div class="picture mb-3">
                                    <img src="<?php echo $this->assetBaseurl.'banners_commitments_pld_3.jpg' ?>" alt="" class="img img-fluid">
                                </div>
                                <div class="info">
                                    <?php if (Yii::app()->language == 'en'): ?>
                                    <h4>RELIABLE & pUNCTUAL Supply</h4>
                                    <p>We maintain our stock availability for long term supply and therefore we can be the best when it comes to on time delivery.</p>
                                    <?php else: ?>
                                    <h4>Pasokan YANG DAPAT DIANDALKAN & TEPAT WAKTU</h4>
                                    <p>Kami menjaga ketersediaan stok kami untuk pasokan jangka panjang dan oleh karena itu kami dapat menjadi yang terbaik dalam hal pengiriman tepat waktu.</p>
                                    <?php endif ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>
