<?php 
    $arr1 = [
                1 => [
                    'pict'=>'banners-hm-1.png',
                    'name'=>'Wastewater & Water Treatment Chemicals',
                    'name_id'=>'Air Limbah & Bahan Kimia Pengolahan Air',
                ],
                [
                    'pict'=>'banners-hm-2.png',
                    'name'=>'Fiberglass Composite Material',
                    'name_id'=>'Bahan Komposit Fiberglass',
                ],
                [
                    'pict'=>'banners-hm-3.png',
                    'name'=>'General Industrial Chemical',
                    'name_id'=>'Kimia Industri Umum',
                ],
                [
                    'pict'=>'banners-hm-4.png',
                    'name'=>'Fiberglass Resin Material',
                    'name_id'=>'Bahan Resin Fiberglass',
                ],
                [
                    'pict'=>'banners-hm-5.png',
                    'name'=>'Carbon Fiber Composites Material',
                    'name_id'=>'Bahan Komposit Serat Karbon',
                ],
                
            ];
?>
<footer class="foot py-5 pb-4">
    <div class="prelatife container">
        <div class="inners py-0">
            <div class="row">
                <div class="col-md-15">
                    <div class="subs_footer">
                        <div class="top_foot pb-2 mb-2">
                            <span><?php echo (Yii::app()->language == 'en')? "Browse Our Site": "Jelajahi Situs Kami"; ?></span>
                        </div>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>"><?php echo (Yii::app()->language == 'en')? "About": "Tentang"; ?></a></li>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/industry')); ?>"><?php echo (Yii::app()->language == 'en')? "Industry Application": "Aplikasi Industri"; ?></a></li>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>"><?php echo (Yii::app()->language == 'en')? "Quality": "Kualitas"; ?></a></li>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>"><?php echo (Yii::app()->language == 'en')? "Contact": "Kontak"; ?></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-15">
                    <div class="subs_footer">
                        <div class="top_foot pb-2 mb-2">
                            <span><?php echo (Yii::app()->language == 'en')? "Products": "Produk"; ?></span>
                        </div>
                        <ul class="list-unstyled">
                            <?php foreach ($arr1 as $key => $value): ?>
                            <?php
                            $links = CHtml::normalizeUrl(array('/home/products_detail', 'id'=> $key, 'name'=>Slug::Create((Yii::app()->language == 'en')? $value['name']: $value['name_id']) ));
                            ?>
                            <li><a href="<?php echo $links; ?>"><?php echo (Yii::app()->language == 'en')? $value['name']: $value['name_id']; ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-15">
                    <div class="subs_footer">
                        <div class="top_foot pb-2 mb-2">
                            <span><?php echo (Yii::app()->language == 'en')? "Inquire Now": "Tanyakan Sekarang"; ?></span>
                        </div>
                        <ul class="list-unstyled">
                            <li><a href="https://wa.me/6287883000048"><i class="fa fa-whatsapp"></i> &nbsp;+62 878 8300 0048</a></li>
                            <li><a href="https://wa.me/62818596618"><i class="fa fa-whatsapp"></i> &nbsp;+62 818 596 618</a></li>
                            <li><a href="https://wa.me/6281231888898"><i class="fa fa-whatsapp"></i> &nbsp;+62 812 3188 8898</a></li>
                            <!-- <li><a href="https://wa.me/6281231888898"><i class="fa fa-whatsapp"></i> &nbsp;+6281 231 8888 98</a></li> -->
                            <!-- <li><a href="tel:+62315353668"><i class="fa fa-phone"></i> &nbsp;+62 31 547 1381</a></li> -->
                            <li><a href="mailto:salesadmin@perdanachemindo.com"><i class="fa fa-envelope"></i> &nbsp;salesadmin@perdanachemindo.com</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-15">
                    <div class="subs_footer">
                        <div class="top_foot pb-2 mb-2 text-right">
                            <span>PT. Perdana Chemindo Perkasa</span>
                        </div>
                            <div class="t-copyrights text-right">
                                Copyright &copy; 2020,  All Rights Reserved <br>
                                Website design by <a target="_blank" title="Website Design Surabaya" href="https://markdesign.net">Mark Design.</a>
                            </div>
                         <div class="clear"></div>
                     </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</footer>

<section class="live-chat">
    <div class="row">
        <div class="col-md-60">
            <div class="live">
                <a href="https://wa.me/6281246666968">
                    <img src="<?php echo $this->assetBaseurl; ?>icon-whatsapp-float.svg" class="img img-fluid" alt="">
                </a>
            </div>
        </div>
    </div>
</section>

<section class="nfoot_mob_wa d-block d-sm-none">
    <div class="row">
        <div class="col-md-60">
            <div class="nx_button">
                <a href="https://wa.me/6281246666968"><i class="fa fa-whatsapp"></i> Whatsapp</a>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .imgs_vold img{
        max-width: 255px;
    }
    .live-chat .live img{
        max-width: 175px;
    }
</style>