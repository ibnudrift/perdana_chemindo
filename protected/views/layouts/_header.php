<?php 
$e_activemenu = $this->action->id;
$controllers_ac = $this->id;

$active_menu_pg = $controllers_ac.'/'.$e_activemenu;

$criteria = new CDbCriteria;
$criteria->addCondition('active = "1"');
$criteria->order = 't.date_input ASC';
$mod_kategh = ViewGallery::model()->findAll($criteria);
?>

<header class="head headers <?php if ($active_menu_pg == 'home/index' or $active_menu_pg == 'home/abouthistory' or $active_menu_pg == 'home/aboutquality' or $active_menu_pg == 'home/aboutcareer'): ?>homes_head<?php endif ?>">
  <div class="prelative container d-none d-sm-block">
    <div class="">
      <div class="row">
        <div class="col-md-20 col-lg-20">
          <div class="logo_heads">
            <div class="d-inline-block align-middle">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang' => Yii::app()->language)); ?>">
                <img src="<?php echo $this->assetBaseurl; ?>logo-headers.png" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid">
              </a>
            </div>

          </div>
        </div>

        <div class="col-md-40 col-lg-40">
          <div class="rights_wa_info text-right">
            <span>Whatsapp&nbsp; <i class="fa fa-whatsapp"></i> &nbsp;<a href="https://wa.me/6287883000048">+62 878 8300 0048</a>
            &nbsp;&nbsp;&nbsp;&nbsp;
              <?php
              $get = $_GET;
              $get['lang'] = 'en';
              ?>
              <a class="langs <?php if (Yii::app()->language == 'en'): ?>active<?php endif ?>" href="<?php echo $this->createUrl($this->route, $get) ?>">ENG</a>
              &nbsp;|&nbsp;
              <?php
              $get['lang'] = 'id';
              ?>
              <a class="langs <?php if (Yii::app()->language == 'id'): ?>active<?php endif; ?>" href="<?php echo $this->createUrl($this->route, $get) ?>">IND</a>
            </span>
            <div class="clear"></div>
          </div>
          <div class="py-1"></div>
          <div class="text-right top-menu">
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang' => Yii::app()->language)); ?>">Home</a>
              </li>
              <li class="list-inline-item">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang' => Yii::app()->language)); ?>"><?php echo (Yii::app()->language == 'en')? "About": "Tentang"; ?></a>
              </li>
              <li class="list-inline-item">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'lang' => Yii::app()->language)); ?>"><?php echo (Yii::app()->language == 'en')? "Products": "Produk"; ?></a>
              </li>
              <!-- <li class="list-inline-item">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/industry', 'lang' => Yii::app()->language)); ?>">Industry Application</a>
              </li> -->
              <li class="list-inline-item">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang' => Yii::app()->language)); ?>"><?php echo (Yii::app()->language == 'en')? "Quality": "Kualitas"; ?></a>
              </li>
              <li class="list-inline-item">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang' => Yii::app()->language)); ?>"><?php echo (Yii::app()->language == 'en')? "Contact": "Kontak"; ?></a>
              </li>
            </ul>
          </div>
          <div class="clear clearfix"></div>
        </div>
      </div>
      <div class="clear"></div>
    </div>

  </div>
  
  <div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
        <img src="<?php echo $this->assetBaseurl.'logo-headers.png' ?>" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang' => Yii::app()->language)); ?>">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang' => Yii::app()->language)); ?>">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/products', 'lang' => Yii::app()->language)); ?>">Products</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/industry', 'lang' => Yii::app()->language)); ?>">Industry Application</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang' => Yii::app()->language)); ?>">Quality</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang' => Yii::app()->language)); ?>">Contact</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>

</header>
