-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 08, 2021 at 03:01 AM
-- Server version: 10.3.24-MariaDB-cll-lve
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `richmore_perdana_chemindo`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_image`
--

CREATE TABLE `about_image` (
  `id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cs_customer`
--

CREATE TABLE `cs_customer` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `group_member_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `date_join` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cs_customer_address`
--

CREATE TABLE `cs_customer_address` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `postal_code` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gal_gallery`
--

CREATE TABLE `gal_gallery` (
  `id` int(11) NOT NULL,
  `topik_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image2` varchar(225) DEFAULT NULL,
  `active` int(11) NOT NULL,
  `date_input` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `last_update_by` varchar(255) NOT NULL,
  `writer` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gal_gallery`
--

INSERT INTO `gal_gallery` (`id`, `topik_id`, `image`, `image2`, `active`, `date_input`, `date_update`, `insert_by`, `last_update_by`, `writer`, `city`) VALUES
(1, 1, 'f3d3e-cover-dining-kitchen-nippo.jpg', 'f3d3e-dining-kitchen-nippo-003.jpg', 1, '2019-12-25 21:30:34', '2019-12-27 10:39:26', 'info@markdesign.net', 'info@markdesign.net', '', ''),
(2, 1, '458b7-cover-bedroom-livingroom-nippo.jpg', '00f13-Layer-2.jpg', 1, '2019-12-26 10:34:24', '2020-02-10 10:11:31', 'info@markdesign.net', 'info@markdesign.net', '', ''),
(3, 1, '2d13a-cover-nippo-doors.jpg', '97aeb-thumb_c9018-layer-3_adaptiveResize_403_465.jpg', 1, '2019-12-26 10:39:21', '2020-02-10 10:10:45', 'info@markdesign.net', 'info@markdesign.net', '', ''),
(4, 1, 'db7ba-cover-nippo-office-photography.jpg', 'db7ba-office-photography-001.jpg', 1, '2019-12-26 10:40:08', '2020-02-10 10:11:12', 'info@markdesign.net', 'info@markdesign.net', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `gal_gallery_description`
--

CREATE TABLE `gal_gallery_description` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_title` text NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gal_gallery_description`
--

INSERT INTO `gal_gallery_description` (`id`, `gallery_id`, `language_id`, `title`, `sub_title`, `content`) VALUES
(7, 1, 2, 'DINING & KITCHEN', '<p>Nippo Home Furnishing Gallery in Surabaya presents the high quality luxury Dining & Kitchen furniture for your special home. You will expect only superb quality on every detail of our furnitures. Furnitures build by Nippo Techs are a piece of investment that combines both modern machine technology and high craftsmanship quality. We pride ourself as the best because we believe our customers deserve only the best.</p>', '<p>Come to Nippo Gallery to shop your luxurious Dining & Kitchen furniture in Surabaya. We are the leading home furnishing gallery in Surabaya providing bespoke furniture made by Nippo Techs factory and also provide collections of world’s best designer brand such as Giorgetti, Zeyko, etc.</p>'),
(15, 2, 2, 'BEDROOM & LIVING ROOM', '<p>Nippo Home Furnishing Gallery in Surabaya presents the high quality luxury Bedroom & Livingroom furniture for your special home. You will expect only superb quality on every detail of our bedframes, bed side table, wardrobe cabinetries and a comprehensive leather upholstery - sofa from our Italian curated collection. Bedroom & Livingroom Furnitures build by Nippo Techs are a piece of investment that combines both modern machine technology and high craftsmanship quality. We pride ourself as the best because we believe our customers deserve only the best.</p>', '<p>Come to Nippo Gallery to shop your luxurious Dining & Kitchen furniture in Surabaya. We are the leading home furnishing gallery in Surabaya providing bespoke furniture made by Nippo Techs factory and also provide collections of world’s best designer brand such as Giorgetti, Zeyko, etc.</p>'),
(13, 3, 2, 'DOORS', '<p>Nippo Home Furnishing Gallery in Surabaya presents the high detailed and sophisticated technology of luxury quality door making. We have worked with International and reputable hotels, apartments and luxury homes to provide thousands of systematics and custom doors. You will expect only superb quality on every detail of our doors. Doors build by Nippo Techs are a piece of investment that combines both modern machine technology and high craftsmanship quality. We pride ourself as the best because we believe our customers deserve only the best.\r\n</p>', '<p>Come to Nippo Gallery to shop your luxurious Dining & Kitchen furniture in Surabaya. We are the leading home furnishing gallery in Surabaya providing bespoke furniture made by Nippo Techs factory and also provide collections of world’s best designer brand such as Giorgetti, Zeyko, etc.</p>'),
(14, 4, 2, 'OFFICE', '<p>Nippo Home Furnishing Gallery in Surabaya presents the high quality luxury Modern & Contemporary Office furniture for you. You will expect only superb quality on every detail of our office furnitures. Office furnitures build by Nippo Techs are a piece of investment that combines both modern machine technology and high craftsmanship quality. We pride ourself as the best because we believe our customers deserve only the best.</p>', '<p>Come to Nippo Gallery to shop your luxurious Dining & Kitchen furniture in Surabaya. We are the leading home furnishing gallery in Surabaya providing bespoke furniture made by Nippo Techs factory and also provide collections of world’s best designer brand such as Giorgetti, Zeyko, etc.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `gal_gallery_image`
--

CREATE TABLE `gal_gallery_image` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gal_gallery_image`
--

INSERT INTO `gal_gallery_image` (`id`, `gallery_id`, `image`) VALUES
(24, 1, '2e8da-dining-kitchen-nippo-002.jpg'),
(23, 1, 'f3d3e-dining-kitchen-nippo-001.jpg'),
(72, 2, '458b7-bedroom-livingroom-nippo-005.jpg'),
(65, 3, '07b68-nippo-doors-008.jpg'),
(64, 3, '07b68-nippo-doors-007.jpg'),
(67, 4, 'f1f1e-nippo-office-photography-003.jpg'),
(63, 3, '07b68-nippo-doors-006.jpg'),
(62, 3, '07b68-nippo-doors-005.jpg'),
(61, 3, '07b68-nippo-doors-004.jpg'),
(60, 3, '07b68-nippo-doors-003.jpg'),
(59, 3, '07b68-nippo-doors-002.jpg'),
(58, 3, '07b68-nippo-doors-001.jpg'),
(71, 2, '458b7-bedroom-livingroom-nippo-004.jpg'),
(70, 2, '458b7-bedroom-livingroom-nippo-003.jpg'),
(69, 2, '458b7-bedroom-livingroom-nippo-002.jpg'),
(68, 2, '458b7-bedroom-livingroom-nippo-001.jpg'),
(25, 1, '2e8da-dining-kitchen-nippo-004.jpg'),
(26, 1, '2e8da-dining-kitchen-nippo-005.jpg'),
(66, 4, 'db7ba-nippo-office-photography-002.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `instagram`
--

CREATE TABLE `instagram` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `bio` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instagram`
--

INSERT INTO `instagram` (`id`, `username`, `first_name`, `bio`) VALUES
(3, 'iintan_rosalina_h', '????Intan Rosalina Herman ????', '????Intan Rosalina Herman ???? Just ordinary girl ???? Simple young girl ???? Be my self,, ???? Hoteliers @Purnama Hotel Batu ???? As be Front Desk Agent ???? '),
(4, 'sashopmalang', 'AWLSA Shop', 'AWLSA Shop ????Grup BBM (TERSEDIA) ?Ada album di FB ????KOTA MALANG ????RESELLER WELCOME  ?Cek #testisashopmalang                ORDER/CP? COMMENT? COD.DEPANSARDO.JL.GAJAYANA/SUHAT.MALANGKOTA-SENGAJA.GAK.NYANTUMIN.CONTACT.SERIUS.ORDER-COMMENT.SAJA-YES.WE.ARE.TRUSTED'),
(5, 'banyakfollowersaktif', 'tambah followers', 'tambah followers Hanya disini penjual followers instagram aktif serta berani memberikan garansi. Segera hub Pin:5479b225 LINE:JUALANLIKE Waspada.penjual.followers.gadungan'),
(6, 'cosette_mci', 'pelangsing pemutih mci', 'pelangsing pemutih mci ????Welcome???? ????Ig Owner: @anyza_cosette ????WA/Line: 081233647778 ????BBM: 57B22942 Ready: PRODUK MCI ORIGINAL Budayakan membaca yah???? instagram.com/p/7IsUS2sc0l//atau//mci-anyza.blogspot.com'),
(7, 'artsuppliesindonesia', 'Jual Alat Lukis & Handycraft????', 'Jual Alat Lukis & Handycraft???? ???? JNE ¦ JKT - SRB ¦ BRI ¦ ? Shipping Indonesia ???? BBM 56ae3760 ???? LINE : miss.pratama ? SMS : 081958220710 Update Harga 17 September 2015 cacorner.wordpress.com'),
(8, 'claclaonlineshop', '#EANF#', 'Supplier Tas Branded, Baju & Kalung Like a pic like a real Bbm 59DFAFEE Line clacla3 BARANG READY STOCK. WELCOME RESELLER....???? '),
(9, 'karismawidia', 'karismawidia', 'karismawidia  '),
(10, 'labeautys', 'La Beauty\'s', 'La Beauty\'s ?Minimum Order 50rb? ????READYSTOCK JAKARTA???? ????RESELER&DROPSHIP ????SENIN,RABU,JUMAT *Line: labeautys *Line: breinerputri  BBM:7BFA06D3 HIT&RUN BLACKLIST '),
(11, 'daus4009', 'Mohd Firdaus', 'Mohd Firdaus  '),
(12, 'etlabora.shop', 'Et Labora Shop', 'Et Labora Shop Tas murah batam dari berbagai macam merek. Welcome Reseller. info lebih lanjut hub: WhatsApp 081991095791, pin BB: 5AB0A84B '),
(13, 'iklan_adv', 'ACCOUNT IKLAN MURAH', 'ACCOUNT IKLAN MURAH REAL FOLLOWERS !! Testi? #iklanadv_testi  line 1: imefaries operating hours: 8A.M - 11P.M free : off '),
(14, 'doobeeshop', 'ACCESSORIES HP MURAH!!', 'ACCESSORIES HP MURAH!! ???? JNE Jakarta  ???? BCA ???? Senin - Sabtu (09.00-18.00) ???? WA +62 81213885548 ???? LINE @ doobee.shop (harus pakai \"@\" ya) ?? line.me/ti/p/%40doobee.shop'),
(15, 'jewelshop', 'FOLLOW 2nd IG @KEBAYAJEWELSHOP', 'FOLLOW 2nd IG @KEBAYAJEWELSHOP WE ARE SHIPPING WORLDWIDE ?? Hashtag: #TestiJewelShop ?WA: +6281373071000 ???? fastrespond ORDER VIA LINE? KLIK LINK DIBAWAH INI???????? line.me/ti/p/%40lul4435a'),
(16, 'ratnapurvita', 'Ratna purvita sari', 'Ratna purvita sari VIITHAA SHOP SINCE 2012 BBM : 54DC1AA8 WA / SMS : 081326286822 LINE : PURVITA19 Fb : Ratna purvitasari BRI.BCA.BNI DAN MANDIRI '),
(17, 'sparkling.id', 'BAJU ANAK BAJU CEWEK MURAH', 'BAJU ANAK BAJU CEWEK MURAH PLEASE READ >> #sparklingtestimoni ????Monday - Friday ???? 10:00-19:00 BBM : 5257901F ???? LINE: line.me/ti/p/%40sparkling.corp'),
(18, 'iklan_olshopmuuu', 'Iklan_olshopmuuu | RJ014', 'Iklan_olshopmuuu | RJ014 ????Trusted JasProm since 2012???? Testimoni ???????? @testimoniiklanolshopmu PRICELIST @PHK47274L(PAKAI @) ????Member @komunitasjaspro???? Biar cepet add???????? line.me/ti/p/@phk4742l'),
(19, 'badminton.jog', 'Badminton_Shop', 'Badminton_Shop Info to Order  BBM 53BC565F LINE Hendrawan4641 CP 0812-5558-7273 '),
(20, 'julichan_os_medan', 'juli chan', 'juli chan Fast Respon  Pin : 518FACDE Line id : julichan88 °Slow Respon for comment ig° Reseller Welcome ?SINCE 2012? '),
(21, 'nio_olshop', 'Kurniawati Hasanah', 'Kurniawati Hasanah Pin bb:525e4252 Open order sista Reseller welcome Lokasi : Klaten WA/sms:085642373165 m.facebook.com/kurnia.wati.39501'),
(22, 'matoshop2_', 'New&PreLoved AUTHENTIC????', 'New&PreLoved AUTHENTIC???? #TESTIMATOSHOP - ????NO BARTER???? ????Good Condition ????Line: Matoshop  ????Pontianak - ?Acc Under 50k @matoshop_ - ????Setiap Pembelian Dapat Free Gift '),
(23, 'mediaberiklan2', 'MEDIA IKLAN  INSTAGRAM | RJ010', 'MEDIA IKLAN  INSTAGRAM | RJ010 Close member TRUSTED.PROMOTE.ACCOUNT'),
(24, 'iklan.lingerie.surabaya', 'Iklan Kimochi Me', 'Iklan Kimochi Me ?????????????????????????????????? Untuk produk2 selengkapnya, Follow langsung: ???? ???? ????  @kimochime ? ? ? Akun yang ini sewaktu2 bisa ditutup???? ????????????????????? '),
(25, 'canneehouse', 'CANEHOUSE???? (DIBAWAH 100RB)', 'CANEHOUSE???? (DIBAWAH 100RB) START FROM 40K ???????????? JAKARTA, INDONESIA ????TRUSTED ONLINESHOP ????whatsapp : 087878159293  ????BBpin : 2379103F ????BCA ONLY SOLD=DELETE LINE click ???? line.me/ti/p/%40sqm1895k'),
(26, '_miraclestore', 'PO 21 NOVEMBER ????????????????', 'PO 21 NOVEMBER ???????????????? ????TANGERANG Testimoni ???? #TestimoniMiracleStore PEMBAYARAN VIA BRI Order/ask? ???? ????WA/SMS: 082299436753 [NO CALL] ????Twitter: @_MiracleStore ????Line: ???? line.me/ti/p/@fkx5351u'),
(27, 'rumahijabbyrestavia', 'RumaHijab', 'RumaHijab ???? MALANG ???? ????Line: oktahartika ????BBM: 5A2F2063 ????WA: 082141794420 ????@oktahartika & @reremarda ????Mandiri ???? JNE CREATE YOUR FASHION IN HERE ???????????????? Perumahan.Graha.deFath.BlokIV.No4.Malang'),
(28, 'sperocloth', 'SPEROCLOTH', 'SPEROCLOTH Supplier 1st Hand Ready Stock.. . Watch : @sperowatch . For Order (Send Picture to) BBM : 5AA9C656 Line : yusi.apriana '),
(29, 'maizah_shop', 'HIJABERS SYARI MALANG', 'HIJABERS SYARI MALANG PIN: 596BDE1F LINE resitaelena27 SMS/WA 087759750034 SOLO | MALANG BCA | BRI | MANDIRI | WU SHIPPING WORLDWIDE Pengiriman H+1 | RESI H+5 NO COD WAJIB.SABAR//BUY3GETDISC5RB/PCS'),
(30, 'bajupria_id', 'DAGELAN RAISA 66990 PEVPEARCE', 'DAGELAN RAISA 66990 PEVPEARCE ???????? ALL ITEM READY STOCK!!  ???? BB : 54EBC80E ???? Sms / WA : 085712205229 ???? ???? RESELLER WELCOME ???? TRUSTED OLSHOP ???????? OPENORDER.SENIN-SABTU.08.00-21.00'),
(31, 'nokiyozz', 'OZZ olshop bags (tas branded)', 'OZZ olshop bags (tas branded) ???? Ready Stock !!! \"????TRUSTED & CHEAP\" ?? ORDER (pilih salah satu) ???? WA : 0823 1947 7666 ???? LINE : nokiikiyoo ????Shipping Batam???? '),
(32, 'sistaholicstore', 'STRIPE TOP 40K', 'STRIPE TOP 40K Trusted Olshop ???? ????READY STOK and PREORDER ????BRI ????FOR ORDER/ASK        LINE ID: dyasistadk       SCROLL DOWN ???? ????FREE ONGKIR PACITAN DAN SOLO '),
(33, 'wyprameswari', 'Winda||Gosh & JoeyFootwear', 'Winda||Gosh & JoeyFootwear Line : winda_zaf BBM : 53EF666F Sms/Wa : 085743115555 Shipping pake ijx logistics Barang New, semua dr Store yaw Happy Shopping Gosholic www.ijxlogistics.com'),
(34, 'chiwietshop', 'Chiwiet Shop', 'Chiwiet Shop ????BANDUNG ???? testi : #testichiwiet ????Mandiri,BCA,BRI ????BBM 5A3F4210 / 2B3C4B5B ????WA 087722593986 ????LINE @PSY1046J (PAKE @ YA) ATAU KLIK LINK DIBAWAH???? line.me/ti/p/%40psy1046j'),
(35, 'azeeza_house', 'for order n WELCOME RESELLER', 'for order n WELCOME RESELLER WELCOME Azeeza bags house Keep no cancel Pin bb : 581c07be Line : dian anggraini Wa : 085263240186 BATAM Happy shopping bunda n sist ... :-) :-* :-* '),
(36, 'shashuway_shoes', 'SHA SHU WAY™', 'SHA SHU WAY™ BBM:54C88196 WHATS APP:082121981552 LINE ID:ridwan_32 (ready gruop) Testimoni #sha_testi UPDATEAN 2 MINGGU LEBIH=SOLD OUT carousell.com/shashuway_store'),
(37, 'arenajersey', 'Arenajersey', 'Arenajersey Sepatu adidas ori, tas, topi custom , jersey dll semua di jamin murah  bbm : 26bca33c wa: 081213609579/ 08979500579 line muhammadnajibh reseller wlcm www.tumpukresi.blogspot.com'),
(38, 'vishandbear81shop', 'vie vien olshop', 'vie vien olshop menerima pembuatan sepatu/sandal cew/cow full kulit by REQUEST..kirim ft item  yg mau dibuat 7/10 hr selesai pemesanan:bbm- 59E3B094 hp:085720515567 '),
(39, 'modesty_fashionshop', 'Trendy, Beautiful & Elegant', 'Trendy, Beautiful & Elegant Trusted Online Fashion Shop ?????  ???? sms/WA : 085641101911 ???? bbm : 59F1B826 ???? JNE/POS/TIKI ???? BCA/Mandiri Happy shopping ???????????? '),
(40, 'iraolshop25', 'IO -> Ira OlShop', 'IO -> Ira OlShop Online Shop Bandung | 589C87DE | Line @irahmawati25 | WA 089662513204 ? Format order: 1. Nama 2. Alamat lengkap 3. No. Hp 4. Pesanan (send pict) www.facebook.com/iraolshop25'),
(41, 'an_nur_115', 'Gudang sepatu Bandung', 'Gudang sepatu Bandung PEMESANAN: PIN BB>56C33A7F WA>08997733363  LINR>@ZCE1970M (pake @) AL_MISBAH_171(jaket,sweater dll) #testimoni_almisbah171_annur115 PEMAKETAN VIA JNE line.me/ti/p/%40zce1970m'),
(42, 'larva_olshop', 'Pusat Baju Cewek&Cowok Murah', 'Pusat Baju Cewek&Cowok Murah ????LINE : @OUR8509T ????BBM : 742F9DF9 (Elvinna N) ????BRI/MANDIRI ????RESELLER WELCOME ????JNE -HAPPY SHOPPING, WE ARE READY STOCK- ?   ?   ?   ?   ?    ? '),
(43, 'penikmat_senja_', 'jual perlengkapan gunung murah', 'jual perlengkapan gunung murah ????????Line id : penikmat_senja ???? BBM : 544C32C0 ?????? WA : 082210180404     FOLLOW IG OWNER : www.instagram.com/nurkhotimah'),
(44, 'reivishavitalika', 'Reishop', 'Reishop SHOES BRANDEDTEE&TUMBLRTEE ????For Order ????PIN BB : 553F2643 ????Line: Shavitalika ????BTN,BCA ????JAKARTA-????JNE/TIKI OWNER :@Shavitalika '),
(45, 'oip_shop', 'Oip Shop ????', 'Oip Shop ???? ???? JAKARTA                SOLD DI.....DELETE ???????????????????? Line ? Olipoip ? BB ? 519AC7B8 ? WA ? 08567778870 ? ????  BCA Only '),
(46, 'labellee_acc', 'SUPPLIER AKSESORIES', 'SUPPLIER AKSESORIES PK (paid keroyokan) HUB LINE . ???? Contact : Admin 1 (BBM)  : 53D01FD6 Admin 2 (LINE) : @qto9469w Admin 3 (WA)   : 089505708832 NO GROUP line.me/ti/p/%40qto9469w'),
(47, 'barbielookss', 'Barbielooks House', 'Barbielooks House 100% ORIGINAL(Export). All brand new! Jakarta | ship by GOJEK/JNE  BBM:  7FA5506F 218FC722 LINE: andreaf_barbielooks WA/SMS: 083890195260 082123843189 www.facebook.com/barbielookshouse'),
(48, 'jeclothes', 'Knitwear', 'Knitwear EXCLUSIVE KNIT NO BOOKING! FIRST PAY, FIRST GET! SOLD : DELETE Sms : 0811939669 Line : jehannayaka Payment : BCA Shipping : SABTU TESTI : #jeclothes '),
(49, 'jasapromote_ok8', 'Jasapromote_ok8', 'Jasapromote_ok8 Berminat jadi member jasapromote_ok? Add line : kurniawanrachmattt , trusted. Harga terjamin murah???? '),
(50, 'promote.asera', 'Case Iphone Murah', 'Case Iphone Murah Hanya untuk promote, kalau mau order langsung klik link dibawah yaa line.me/ti/p/%40ymp6967m'),
(51, 'tafhelstorepromote', 'Tafhelstore Jakarta', 'Tafhelstore Jakarta Akun Iklan tafheltafhelicious wa / iMessage : 0813 50941004  Line : tafhelicious (fast respons) Testi : #testitafhel ??Fast respons fast shipping?? komen.difoto.gabakal.dibalas'),
(52, 'promoaurelcase', 'dagean Agnesmooo', 'dagean Agnesmooo Akun khusus iklan FOLLOW OUR OFFICIAL INSTAGRAM @aurelcase LINE : @aurelcase WHATSAPP: 0812 3079 3809 ready.stock.surabaya'),
(53, 'womanclothing_shop', 'UPLOAD TIAP HARI ???? IMPORT HQ', 'UPLOAD TIAP HARI ???? IMPORT HQ Line : @womenclothes  KEEP : TRF Hitnrun : blacklist BBM : 5A957F71 jakarta {?RESEL}{ ? GOJEK }  ???? senin - jmt max trf jam 2  4 pcs diskon 10k/pcs line.me/ti/p/%40whx1993d'),
(54, 'rosettaolshop', 'Rosetta Olshop', 'Rosetta Olshop Fashion cewek ???????????????? ???? Fix order/tanya\" :  ???? 5711C9D3 ???? BCA/BRI  ? JNE/Wahana #testirosettaolshop Atau bisa langsung add line dibawah ? line.me/ti/p/%40dkf5052r'),
(55, 'sunnindostore', '????MONOCHROME SPECIALIST????', '????MONOCHROME SPECIALIST???? ISI FORM = FAST RESPOND! ????SCROLL DOWN???? ????WA: 087838498118 ????Line: @EYY8894J (pakai @) ????BBM: 53D776BA ????JNE ????BRI Be smart and patiently buyer! NO.COD.NO.HIT.AND.RUN'),
(56, 'minncloth', 'JAKARTA READYSTOCK MURAH', 'JAKARTA READYSTOCK MURAH BCA  We are new here but trusted. Untuk informasi. Bisa dihubungi via: Wa:  0852 6443 4493 Line: yusnita '),
(57, 'julianne_closet', 'FASHION IMPORT & PO BANGKOK', 'FASHION IMPORT & PO BANGKOK ????100% IMPORT STUFF???? ????????HIGH QUALITY???????? ????SMS/WA : 0821-245-22222 ????LINE : decytriwidya ????DROPSHIP WELCOME ????Jakarta Based CHECK ????????#testijulianne '),
(58, 'promote.shabeau', '????Sha Beau Self Promote????', '????Sha Beau Self Promote???? ????IG; shabeau????LINE; shabeau????WA/SMS; +6281288349365????info.shabeau@yahoo.co.id?Worldwide Shipping '),
(59, 'lovahyunshop', 'LovaHyun', 'LovaHyun \"READY\"RESTOCK\"REAL PICT\" Open : Senin - Jumat  [10.00-20.00] Minat Contact : WA : 0896-1218-9462 Pin BB : 28CDEFBB \"JNE BATAM\" IG.Baru.Ya.Dears.Kmrin.udh.500.followers'),
(60, 'twentyone07', 'TURUN HARGA!!', 'TURUN HARGA!! ????Surabaya Line: dinapwg (Dina)  Sms: 085731772263 Bbm: 578E16C8 (Gita) ???????? Mandiri/BCA PENGIRIMAN/READY/COD SETIAP RABU&SABTU Happy Shopping? '),
(61, 'xerxesmee_shop', 'Meme ?', 'Meme ? ?Batam Bbm : 542975e8 *fastresponse*???? Line : xerxesmee  Sms/whatsapp :+6281977265566 ???? Trusted Seller ???? OPEN ???? '),
(62, '13thcase', '13thcase', '13thcase ????RESELLER OF INDONESIAN FINEST BAGS BRAND???? Line : 13thcase WhatsApp : 087769268777 SCROOL KEBAWAH BANYAK YG KEREN JARANG.BALES.KOMEN.LEBIH.BAIK.HUBUNGI.KONTAK.KITA.SAJA.TERIMAKASIH'),
(63, 'jasapromote_ok33', 'jasapromote_ok33', 'jasapromote_ok33 Bermnt jadi member jasapromote_ok33? Contac add line : kurniawanrachmattt Trusted???? harga di jamin murah '),
(64, 'beautehealtheshoppe', 'Pemborong Kosmetik Murah', 'Pemborong Kosmetik Murah FREE POS SM (TNC) SMS/WHATSAPP 01121992199 ????% ORIGINAL, BUKAN KILANG FAKE READY STOK UOLLS! MANYAK DUIT BYR LEBIH  BELI ONLINE KENA SABAR ???????????? facebook.com/wholesalekosmetikmurah'),
(65, 'eclairboutique', 'READY STOCK FASHION IMPORT', 'READY STOCK FASHION IMPORT ???? Jakarta, Indonesia ???? BCA/Mandiri ???? JNE (senin-sabtu) ???? SMS/WA : 08128-345-7188 ???? BBM : 793CF743 ???? LINE : @eclairboutique (pakai @ yaa) ????auto add???? line.me/ti/p/%40eclairboutique'),
(66, 'intan_adijuma', 'intan', 'intan Whatsapp 0199642890 '),
(67, 'testimonial_larisashop', 'Larisa Shop (Grosir dan Ecer)', 'Larisa Shop (Grosir dan Ecer) Paling MURAH - Kualitas OKE ???? ????OpenOrder : 08.00-16.00 ? 546EBC24 ? 5346D2C4 ? 7A76DCB8 ganti pin 5980E60C ???? ? 754240DB Line: @dtp0785g (pake @ ya) line.me/ti/p/%40dtp0785g'),
(68, 'bellezafacion', 'BellezaFacion Supplier Jogja', 'BellezaFacion Supplier Jogja ????Harga Murah 30rb-an CODan daerah jogja ???? Reseller Welcome ????Yogyakarta ????BCA/ BRI/ Bukopin.  ID Line : bellezafacion WA : 082136223636 '),
(69, 'promosiindonesiadua', 'promosi dua', 'promosi dua  '),
(70, 'demennya_makan', 'DemenMakan', 'DemenMakan Yogyakarta '),
(71, 'astrauniverse_ad', 'follow @AstraUniverse', 'follow @AstraUniverse Ready Stock! Follow @AstraUniverse ini akun iklan Pin BB: 23BCC887 LINE: Kelvinsetiawan88 No Hp: 083898797676 Alamat: ITC Mangga Dua lt.2 Blok C no 93 www.facebook.com/kelvinastrafoto'),
(72, 'amy_smile_miss_18yo', '????????????', '???????????? These girls are bored… '),
(73, 'tbmcollections', 'TBM Collections', 'TBM Collections Untuk order atau sekedar tanya-tanya Line: nursetiawatiwali Pin BB: 5431970E Free ongkir untuk daerah Pinrang dan Parepare Lets Order guys???????? '),
(74, 'qimbagshop2', 'qimbagshop2', 'qimbagshop2 qmieonlineshop disable from instagram???? ORDER : Line : qmiirena BBM : 2971B41B Transaksi : Via BCA  / Mandiri Lokasi : Jakarta '),
(75, 'dasilva_collection', 'Fico Dasilva Bags', 'Fico Dasilva Bags ????Batam ????pin Bbm 5764126B  ????SMS/WHATSAPP 087773781121  ????Line : mellynda_karlina ?????: FB FICO DASILVA BAGS (DASILVA COLLECTION) Moment ID:8319634 www.facebook.com/mellynda.karlina'),
(76, 'd.ateliershop', 'd\'atelier', 'd\'atelier READY STOCK TAS BRANDED & ACCESSORIZE ORIGINAL ???? Line: ginaalfa (NO SPAM) ????BNI weekend closed SHIPPING: JKT & SBY '),
(77, 'ruth_woman_lovely_19yo', '????????????', '???????????? These girls are bored… '),
(78, 'lemignoncloset', 'ALL READY - FIRSTHAND SHOES', 'ALL READY - FIRSTHAND SHOES ???? Monday-Sunday ???? JAKARTA ???? JNE ???? ORDER ???? Line : @lemignoncloset (pake @) KLIK (fast response) ?????? line.me/ti/p/%40lemignoncloset'),
(79, 'raninuranishop', 'Rani Nurani Fashion Shop', 'Rani Nurani Fashion Shop Sicepat Mantap (Mon-Fri) Jam Operasional = Weekday 09am-5pm Hbs=Ganti barang/refund Wa=085320110020 Bbm=594097CD Line=raninuranishop ???????????????????????? '),
(80, 'promotemisslobunshop', 'Iklan akun @misslobunshop', 'Iklan akun @misslobunshop Silahkan buka akun IG @misslobunshop ????? For consultation / order : LINE : salsafirdausia LINE@ : @uzy1070w WhatsApp : 085211950044 BBM : 7CFCD6BF '),
(81, 'topkosmetik', 'HAFAHA STORE', 'HAFAHA STORE Contact : BBM (596B39FB) LINE : HAFAHASTORE SMS/WA : (081314156386) www.hafahastorekosmetik.wordpress.com'),
(82, 'best_softlens', 'GRATIS SOFTLENS S/D 15NOV', 'GRATIS SOFTLENS S/D 15NOV ? TRUSTED SINCE 2013. God Bless Me ???? ? FREE ONGKIR JNE/POS ???? ? BISA BEDA MINUS ???? ? LINE : @best_softlens (pakai @ ya) KLIK LINK ? line.me/ti/p/%40best_softlens'),
(83, 'rambutsambungan', 'TERMURAH SEINSTAGRAM', 'TERMURAH SEINSTAGRAM READY JKT NO COD , No Tipu\" , 100% HUMAN HAIR BERANI UANG KEMBALI  ???? BB 295CE92E fast respond ???? PHP BLOCKED @rambutsambungan @grosirrambutsambungan '),
(84, 'yellow.os', 'KOSMETIK MURAH ????ORI & REPLIKA', 'KOSMETIK MURAH ????ORI & REPLIKA Harga Murah Kualitas OK???? ?ORDER=SABAR ????BCA ????line : yellow.os ????Bbm : 73e260ee ????WA : 08998989339 ?TRUSTED #testiyellowos ????Jakarta & Makassar '),
(85, 'skin79bbcpromo2', '#EANF#', 'SMS / Wa: 0852 1080 5112  Pin BB : 2B2B F266 atau 228 46 95B ???? 100% ORIGINAL - importir langsung pabrik???? Senin-sabtu pkl 8-17 FAST RESPON www.skin79bbc.blogspot.com'),
(86, 'crownbox_us', 'Nyx Bourjois Colourpop Pixiglo', 'Nyx Bourjois Colourpop Pixiglo Op hours : 08.30 - 17.30 contact personal shopper via  ????????crownbox_store (LINE) browse, shop, pay by your self simple and fast , go to ???? www.crownboxstore.com'),
(87, 'sun_promote28', 'Sun Promote', 'Sun Promote 1??3000 followers dlm 30 hari 2??2000 spam like/hari 3??Promote 500 foto/hari 4?30 hari manage ig kamu ????24 JAM NONSTOP???? BBM 74CE98A5 Line @sun_manager '),
(88, 'venntastic.shop', 'VENNtastic Shop', 'VENNtastic Shop Luxury Beauty Storage ???? Jakarta ???? BCA/Mandiri Trusted. Ragu? Check #testivenn Mau Free Ongkir ke seluruh Indonesia? Contact admin for more info : line.me/ti/p/@vvo9859f'),
(89, 'phil_followers', 'Phil_Followers', 'Phil_Followers Follow @juraganfollowerss #testijuraganfollowers BCA/NIAGA BBM: 53FC0A19 Line: @utf0933d Atau klik link dibawah ini http://line.me/ti/p/%40nua8650s '),
(90, 'banyakinfollowers', 'banyakin followers', 'banyakin followers Cara banyakin followers instagram Aktif indonesia yg bisa like dan bisa comment. Line1: jualfollowers Line2: followersaktif Pin:7f7e7d04 Testi klik: jasafollowers.blogdetik.com/testimoni'),
(91, 'kinghastag', 'obralfollowersaktif', 'obralfollowersaktif Jual jasa tambah followers aktif asli indonesia di jamin trusted serta aman hanya di sini followers awet berkualitas pin:53BE70ED line:indofollowers '),
(92, 'jasafollowersterbaik', 'jasa followers terbaik', 'jasa followers terbaik Kami adalah jasa followers terbaik, telah melayani puluhan ribu costumers sejak dulu. Pin:5479b225 Line:jualanlike BAHAYA.AUTO.FOLLOWERS'),
(93, 'akunjualanlike', 'Jual like instagram', 'Jual like instagram Jual followers aktif asli indonesia bukan bajakan hanya disini Line : juallike Pin:5479b225 jasafollowers.blogdetik/testimoni'),
(94, 'rajaposting', 'Raja posting', 'Raja posting Rajanya auto posting, akun ini posting otomatis..mau beli follower ig seger hub admin. Line:jualanlike Pin:5479b225 Akun.ini.posting.secara.otomatis'),
(95, 'akunaktif', 'jualfollowersaktif', 'jualfollowersaktif Jual followers aktif instagram & like, bukti followers aktif follow akun>@jualjasa Pin: 7F7E7D04 Line1: jualfollowers (full) Line2: followersaktif jualfollowersinstagram.blogspot.com'),
(96, 'folowersmurah', 'followers murah', 'followers murah Follow @juraganfollowerss #testijuraganfollowers BCA/NIAGA Line: @nua8650s Atau klik link dibawah ini line.me/ti/p/%40nua8650s'),
(97, 'indonesiafollowers', 'indonesia followers', 'indonesia followers Jual followers instagram indonesia,followers AKTIF indonesia bisa like & comment.  Pin:7f7e7d04  Line1: jualfollowers Line2: followersaktif Testi: jasafollowers.blogdetik.com'),
(98, 'nambahinfollowers', 'jualfollowers', 'jualfollowers Jual followers aktif instagram & like Pin: 7f7e7d04 Line1:jualfollowers Line2: followersaktif bukti testi artis di website???? jualfollowersinstagram.blogspot.com'),
(99, 'lesacase_id', 'CASE IPHONE DAN SAMSUNG MURAH', 'CASE IPHONE DAN SAMSUNG MURAH Reseller and Dropshiper welcome ???? Order? (Pilih salah satu) Bbm : 595D6DDD Line : @xqz2748z (harus pakai @) Atau klik Link di bawah ini ???????????? line.me/ti/p/%40xqz2748z'),
(100, 'iklan_yuwastore', 'IKLAN YUWASTORE', 'IKLAN YUWASTORE ???? Hanya Akun Iklan . Kunjungi @YUWASTORE . More Info: Line: ArendraRyan BBM: 565A5794 WWW.YUWASTORE.COM'),
(101, 'hanya.iklan_', '#EANF#', 'IG ini hanya untuk iklan dari @tokomurahbanget Untuk lihat produk lengkapnya, silahkan follow: @tokomurahbanget @tokomurahbanget @tokomurahbanget '),
(102, 'promotejualantafhel', 'tafhelstore iklan', 'tafhelstore iklan Akun iklan tafheltafhelicious Testi : #testitafhel ???? wa : 081350941004 ???? line : tafhelicious (fast respons) Shipping : jakarta instagram.com/tafheltafhelicious'),
(103, 'lanlejustle', 'Lauren Pressley', 'Lauren Pressley ?G.?e?t? 10.003+?. folllowers? and Likes? Now?? 10013Followers.WIN'),
(104, 'promote.21gadgetstore', 'Promote 21 Gadget Store', 'Promote 21 Gadget Store  instagram.com/21gadgetrstore'),
(105, 'fashionstore_smg', 'J - Chiq Shop', 'J - Chiq Shop SUPPLIER BANGKOK COLLECTIONS ???????? A part of @fashionstoresmg .. Line : claudiocanigg Pin BB 26C04034 WA 08970569318 .. ???? JKT/SMG www.facebook.com/jchiqshop'),
(106, 'cathenna', 'Cathenna', 'Cathenna ???? PREMIUM NECKLACE  ???? READY & LIMITED STOCK ???? order via WA: +6285719231933 ???? JAKARTA,INDONESIA  WEEKEND = SLOW RESPON '),
(107, 'infinitejewellery', 'Infinitejewellery', 'Infinitejewellery TRUSTED ONLINE SHOP ???? DIJAMIN MURAH!  JNE ???? BCA ???? Jakarta Pin: 535D9438 line : lebelle8 DISKON 5% SETIAP PEMBELIAN DIATAS 288 RIBU! FAST.RESPOND'),
(108, 'promoaksesoris', 'promosi kalung murah', 'promosi kalung murah  '),
(109, 'flizconceptadv', 'Full collection @flizconcept', 'Full collection @flizconcept Whatsapp : 0899 722 0383 Line : flizconcept ????#flizring #flizearring #flizpendant #flizbracelet #fliznecklace #flizclothing '),
(110, 'divashopid', 'Pusat ????????| RIARICIS1795 DAGELAN', 'Pusat ????????| RIARICIS1795 DAGELAN Cirebon & Sby Line: ardinintya BBM: 596FA69B (fast respon ????grup) WA only: 085733294324 ????Mandiri ????Jne  ????COD #DivashopTesti #CARAORDERDIVASHOP welcome.Reseller'),
(111, 'trusted_olshop_promo', 'TRUSTED OLSHOP PROMO', 'TRUSTED OLSHOP PROMO Khusus Iklan Ekskusif Member Hanya Trusted Olshop!! Langsung contact olshop bersangkutan utk belanja '),
(112, 'grosirjammurah_', 'GROSIR JAM TANGAN', 'GROSIR JAM TANGAN OPEN RESELLER & DROPSHIP CONTACT ORDER : LINE : grosirjammurah_ PIN : 5AE164A2 (FAST RESPOND) WA : 0895344540229 ECER.HARGA.GROSIR'),
(113, 'lotus_promote', 'Promosi Murah!', 'Promosi Murah! Untuk Promote saja! Semua pesan disini tidak akan di balas '),
(114, 'collectionswindy', 'FIRSTHAND READYSTOCK REBUTAN!', 'FIRSTHAND READYSTOCK REBUTAN! ????ALL UNDER 150k  Respon 9am-7pm LINE alvinawindy SMS 085700008758 BBM 5254034F ????NO CANCEL???? TRANSFER = KEEP inshopmation.com/jewelry'),
(115, 'julysproject', 'JULY\'s.', 'JULY\'s. we sell some adorable shit to complete your aesthetic look.  line: @LTM7537k  BRI / JNE. line.me/ti/p/%40ltm7537k'),
(116, 'adv9pro', 'adv9', 'adv9 FOLLOW @9project JKT-INA ????Line : ayumrs ????WA:089605361564 ????bbm:52820675 INI.AKUN.IKLAN.KETERANGAN.LENGKAP.CEK.IG @9PROJECT #testimo9project '),
(117, 'promote_girlplays', 'Custom Pins', 'Custom Pins Akun promosi More info ???? @shopgirlplays '),
(118, 'iklanbebranded', 'BE BRANDED', 'BE BRANDED GROSIR ACCESORIES TERBESAR , TERMURAH dan TERLENGKAP di MEDAN GO FOLLOW @be_branded  GO FOLLOW @be_branded2 Di sini hanya jasa iklan saja. '),
(119, 'yuan_fobranded', 'Sepatu tas original n import', 'Sepatu tas original n import // online shop authentic n import things Pin BBM 2676A6A5 SMS/WA/LINE 082257491960 add my FB : yuan FO - yuan margaretha - yuan part II www.tokopedia.com/people/5822643'),
(120, 'pastel.store', 'FASHION TAS IMPORT & BRANDED', 'FASHION TAS IMPORT & BRANDED BEST PRICE! WELCOME RESELLER & DROPSHIP INFO & ORDER SMS : 085888181700 BB : 554889BA LINE : pastel.store JNE/BATAM #testimonipastelstore01 '),
(121, 'truegshop', 'Pusat Jam Murah Original', 'Pusat Jam Murah Original TRUSTED  1000% ORIGINAL Tipe Lain? Silakan Tanyakan. Open Cicilan Bbm: 74899F8D WA ONLY : 089668217155 Testi: #testitruegshop Add Line di bawah ini ? line.me/ti/p/%40skp9425k'),
(122, 'whatgirlneeds_', 'GENEVA? & BRANDED CLOTHES ORI', 'GENEVA? & BRANDED CLOTHES ORI ALL READY STOCK & BEST PRICE???? . Contact to:  •LINE: @TNA4026Y (with @) •WA ONLY: 0857-1111-4182 •BBM: 7F38AE43 . ???? ADD OUR LINE FOR SALE ITEMS???? line.me/ti/p/%40tna4026y'),
(123, 'bangshopp', 'forever21 preloved bajumurah', 'forever21 preloved bajumurah ????jakarta ????Fixed Price Order Contact line: qurratatasya WA: 081382518189 owner: @tasyaqurrata '),
(124, 'rayaretro_art', 'UNIQUE AND ETHNIC CLUTCH', 'UNIQUE AND ETHNIC CLUTCH Rayaretro\'s Art Design Good Quality #testimonialrayaretros Order :  WA / SMS 08885144120 Line : emmaraya BBM 75412016 BUY.MIN.3.PCS.DISCOUNT.10.000.PER.PCS'),
(125, 'ulfahadifahbags_2', 'ADIFAH BAGS / NEW ACCOUNT', 'ADIFAH BAGS / NEW ACCOUNT FOLLOW IG: ULFAHADIFAHBAGS1 ????BBM:2BA794B1 ????LINE: Wirliyah_ulfah ????WA:089688866780(Slow) ???? MANDIRI,BCA ????#TESTIMONIULFAH_2 ????#RULESULFAH IG.ULFAHADIFAHBAGS2.DISABLED.KOLEKSI.PALING.LENGKAP.CEK.TESTIMONI.FOLLOW.IG.ULFAHADIFAHBAGS1.DI.GEMBOK.TIDAK.MEMBALAS.COMENT.DI.IG'),
(126, 'zatul_wadrobe', '????????New Trend Shoes????????', '????????New Trend Shoes???????? ????Menjual kasut Lelaki&Wanita ????Ready Stock&Pre Order ????Whatsapp 0175809813 ????Utk baju2 follow @zatulz250 '),
(127, 'exsistshop', 'EXIST SHOP | Est. 2010', 'EXIST SHOP | Est. 2010 Import Heels,Branded Bags,Fashion Accessories Nine West,E-Heels,YSL,ect ????BBM : 2B4ACF2E ????WA : 081901262828 ????LINE : exsistshop WELCOME RESELLER???? m.facebook.com/exsist.shop'),
(128, 'promote.tootsieshop', 'FOR ORDER FOLLOW @tootsieshop', 'FOR ORDER FOLLOW @tootsieshop Bisa request barang dr WEBSITE Testi #tootsietestimonial LINE: clintanatasa (FIX ORDER) WhatsApp: 0856-2425-7606  LINE UNTUK ORDER KLIK DI SINI ???????????? line.me/ti/p/@wxs6513c'),
(129, 'riensa_pro', 'TOTEBAG DOMPET SLINGBAG MURAH', 'TOTEBAG DOMPET SLINGBAG MURAH ???? WA   : 0896-7826-4561 ???? Line : @TGP0365R  ???? Pin   : 586c0bf5 ???? Bandung ???? Spam Like Free Totebag (TRUSTED) Terima reseller & grosir '),
(130, 'copyhastag', 'Penjual followers', 'Penjual followers Jual followers aktif instagram & like @jasafollowersmurah Pin:7f7e7d04  Line1: jualfollowers Line2: followersaktif jualfollowers.blogdetik.com'),
(131, 'pakarnyafollowers', 'jasa followers', 'jasa followers Jual followers aktif instagram & like  Pin: 7F7E7D04 Line1: jualfollowers (full contact) Line2: followersaktif Cek Testi artis di WEB? jualfollowersinstagram.blogspot.com'),
(132, 'irwanjeanshouse', 'Irwan', 'Irwan GROSIR & ECER KUALITAS EXPOR IMPOR PHOTO = BARANG ASLI FIRST HAND Menerima Reseller SOLD OUT = DELETE JKT Pin: 29e57c9f line: irwanbudiartoputra17 '),
(133, 'peninggibadannhcpdanzinc', 'Peninggi Badan NHCP dan Zinc', 'Peninggi Badan NHCP dan Zinc 08992000244, Peninggi Badan NHCP, Peninggi Badan NHCP Tiens, Peninggi Badan NHCP Zinc, Peninggi Badan NHCP di Surabaya, Jogya, Bandung, Kaskus. line.me/ti/p/%40tiens'),
(134, 'nadsterofficial', 'Beli 3 baju apa saja, gratis 1', 'Beli 3 baju apa saja, gratis 1 ???????????????????? ???? line: nadyalestari ???? bbm: 7a44f1e7 ???? wa: 081221999542 ???? Bandung ? KEEP MAX 1X24 JAM=NO CANCEL  Feel free to ask, Happy shopping! ???????? '),
(135, 'safaristoreee', 'SKINNY 29 PASMINA 28 CARDI 58', 'SKINNY 29 PASMINA 28 CARDI 58 Ready solo ???? kecuali katalog raihan shop Order ???????? Wa 085647156574 | Line frieskadyanneza BRI | BNI | Mandiri #safaristoretesti '),
(136, 'mind.room', 'MindRoom', 'MindRoom Contact  ???? Bbm 5591257F/7D2A1421 ???? WA   081298793078/08568489330 ???? Line erminavinakusuma ???? JNE Depok SOLD = DELETE Read.Caption.Please'),
(137, 'namorahijab', 'Namora Hijab', 'Namora Hijab ???? Produksi sendiri ???? FREE & DISKON ONGKIR* (t&c app) ???? WA/SMS: +6281293100263 ????LINE ID: namorahijab ???? BCA & Mandiri ???? Jakarta '),
(138, 'iklan12_89', '#EANF#', 'Jasa Iklan OnlineShop - sudah lebih dari 20 akun @iklan12 - Berminat olshopny kita promote? ????Hubungi: Line: iklan_12 Bbm: 58695666 ????Terima Kasih???? '),
(139, 'gellepata', 'Low Price Hijab', 'Low Price Hijab ???? Malang 1st hand Order? Line ID: faridalaksita WA / sms: +6285735989855   ???? via JNE COD malang kota area kampus '),
(140, 'alifia569', 'Cynthia Alifia Putri Yuwono', 'Cynthia Alifia Putri Yuwono ?Kediri ?Java language teacher ?like travelling-culinary Line cynthiaalifia Wa 085749163208 Thanks for following me and your like:-) '),
(141, 'kasihprinting', 'MOH CETAK', 'MOH CETAK Menyediakan perkhidmatan cetak t-shirt,baju corporate,sticker,brochure,gift,kad kawin & lain2 - silk screen - Heat press - Sulaman Wasup 014-2657432 '),
(142, 'filoshopee', 'Filosofi Shopee', 'Filosofi Shopee All you needs under 100k ???? ????Line: markunitt ??Close: Sabtu-Minggu ????Subang - Jawa Barat '),
(143, 'nineshoppe', '#EANF#', 'Whatsapp  0818650682 Line : minda_gui Ready stok '),
(144, 'pink.etude', 'Korean Etude', 'Korean Etude 100% Original Etude House from Korea ?? ???? Yogyakarta Wa: 082231508220 ???????? Line: degitapink ???????? For price ?? langsung chat ?????????? Happy Shopping ???????? '),
(145, 'bulletz.id', 'BULLETZ', 'BULLETZ Preloved & New Stuff BBM : 7CAB7773 Whatsapp : 081246799089 Line : ayuushintaa Bali-Indonesia Owner: @ayubrowniez & @erlinezendy BUY NOW OR CRY LATER '),
(146, 'dennise_shop', 'supplier dompet harga grosiran', 'supplier dompet harga grosiran Dompet import ,makeup,beauty,skincare,dll Info/Order? WA : +62 8211-448-6623 Bbm : 524C594D ????CILEDUG - TANGERANG Owner: @rayniethacazier shopee.co.id/dennise_shop'),
(147, 'grosir_hijab_murah', 'tanah abang, pgmta, semarang', 'tanah abang, pgmta, semarang Dikirim H+1 Cek barang ready -> #readyghm Kirim dari semarang Line : ruzannaamanina Wa/Hp : 082329703095 Pin bb: 5A74C2B2 '),
(148, 'cotton.my', 'My Cotton', 'My Cotton ????Batik Cotton Exclusive  ????Postage Monday to Friday ????Swap review free  ????open for retail and borong '),
(149, 'distributorbantalhamil', 'Supplier Bantal Ibu Hamil', 'Supplier Bantal Ibu Hamil Order by: +62 856 4657 5910 (WhatsApp) 23837A7F (pin BB) www.facebook.com/new.familyshop Owner @thari_smile JNE & POS Rek BCA Malang-Indonesia '),
(150, 'ykhalifah_store', 'Children\'s Paradise', 'Children\'s Paradise Welcome to Your Children\'s Paradise ????Baby & Kids Store???? ????: 012-2954710 ????: Free postage 1 Malaysia on selected item www.facebook.com/YoungKhalifahStore'),
(151, 'koishoes', 'Koi Shoes', 'Koi Shoes Nike Adidas NB #testimonikoishoes > Trusted >Line :KoiShoes >Shipping JNE (EVERYDAY) CP: 082298352091 PIN BB : 7CE51557 #koishoes  Klik link line: line.me/ti/p/@amg0321y'),
(152, 'pettyscloset', 'FIRSTHAND | JKT | SUPPLIER', 'FIRSTHAND | JKT | SUPPLIER ? WA : 0821.1052.7195(no call) ? LINE : patreciatheda ? BBM : 74391D52 - - - - - - - - - - - - - - -  ????NO BOOKING ????HIT & RUN  / CANCEL : BLOCKED since.june.2014.com'),
(153, 'meicollection_adv2', 'meicollectionadv2', 'meicollectionadv2 Open monday - saturday : 9-5 Sunday OFF Bbm : 516CAFB7 WA : 081333743827 Main ig account : meicollection Web : www.meicollection.com line.me/ti/p/n167wUvkox'),
(154, 'vennera_shop', 'vennera_shop', 'vennera_shop We are lifestyle shop, all import. Terima COD :) ???? SMS / Whatsapp : 083804807998 / 08970789256 ???? Line : Rachelaprila ? BBM  : 25D50D70  (Vicky) '),
(155, 'decomplete1', 'Premium Accessories', 'Premium Accessories Ready Stock Jakarta ????BB : 59BEF957 ?WA : 081219805408  ????Line : decomplete1 Free ongkir JABODETABEK Reseller disc 10rb/pcs bisa.ikut.PO.2.minggu'),
(156, 'autumnsymphony', 'Hair Clip & Accessories Shop ????', 'Hair Clip & Accessories Shop ???? HANDMADE ACCESSORIES  For order/ask/request order please contact: ????Line: autumnsymphony ??:autumnsymphonyshop@gmail.com Open: Mon-Fri 10.00-20.00 '),
(157, 'shawty.id', 'SLING BAG, SKINNY SKIRT', 'SLING BAG, SKINNY SKIRT SHAWTY. For everyday fashion Est 2013 Bandung, Indonesia ????LINE : @ZPH8407F (use \'@\') / andreazulfiah ????BBM : 7CCCC2CC or click line.me/ti/p/%40zph8407f'),
(158, 'meilsbeautycorner', 'UNINAMU & IMPORT CLOTHING', 'UNINAMU & IMPORT CLOTHING ???? All ready stock ???? Based in Tangerang ???? NO HIT N RUN ???? Terima Reseller  ???? Payment via BCA Order ? Langsung klik line.me/ti/p/@gru3060a'),
(159, 'pinky_shopee', 'Pinky Shopee', 'Pinky Shopee Supplier first hand baju & aksesoris import READY STOCK.JKT. ????28782995 ????????0823 1753 5758 Line: oky-pinkyshopee Reseller are very welcome. '),
(160, 'sweet_theme23', 'TEMA LINE TERMURAH', 'TEMA LINE TERMURAH ? JANGAN FOLOW ! AKUN SPAM ? LINE ( @xgh8332s ) Yang mau order atau tanya2 dulu, Bisa klik link dibawah ini ya         ????? line.me/ti/p/%40xgh8332s'),
(161, 'alfas_app', 'STICKERLINEMURAH', 'STICKERLINEMURAH ????SUPPORT LOLIPOP & IOS 9 ????ITUNES ????STICKER &THEMES LINE ????BCA-TSEL ????BBM: 294495E7 ????LINE (FAST RESPON) klik ???????????? line.me/ti/p/%40bco8024z'),
(162, 'iklan.riyani_appstore', 'ACCOUNT KHUSUS IKLAN', 'ACCOUNT KHUSUS IKLAN JANGAN DIFOLLOW?? FOLLOW KE @riyani_appstore ???? LINE: riyani_appstore???????? line.me/ti/p/kdTwHg4ES1'),
(163, 'iklantopbrand', '#EANF#', 'Kumpulan Iklan Top Follow @rlymetta '),
(164, 'promotekikioshop', 'OPEN RESELLER MURAH', 'OPEN RESELLER MURAH PROMOTE AKUN @kikio_shop ORDER : Line : @ycb9819y (pakai @) atau klik link dibawah ? line.me/ti/p/%40ycb9819y'),
(165, 'iklansellstoreid', 'Hanya Untuk Iklan Sellstoreid', 'Hanya Untuk Iklan Sellstoreid ????TESTIMONI? CEK @TESTISELLSTOREID . ????Yang mau tanya2 atau order bisa langsung add???? ????LINE; @efs2396e ( pake @ ) ????atau langsung klik???? line.me/ti/p/@efs2396e'),
(166, 'jualjaketonline', 'JUAL JAKET ONLINE', 'JUAL JAKET ONLINE Jaket distro keren TERMURAH & GRATIS ONGKIR Pin BB : 315BA491 SMS/WA : 08885568319 Line : jualjaketonline Follow Twitter: @jualjaketonline jualjaketonlen.blogspot.com'),
(167, 'promoteosmurah04', 'JASAPROMOTE JASAIKLAN MURAH', 'JASAPROMOTE JASAIKLAN MURAH TRUSTED PROMOTE PALING MURAH AKUN DOWAPROMOTE KE 4 FREE3XPROMOTETIAPHARI LINE: dowapromoteos1 & dowapromoteos2 '),
(168, 'gazamastore', 'Snapback ????: @magazastore', 'Snapback ????: @magazastore ???? Bandung, 2012. Line owner : putraddn // ssherlie  WA: 081992392543 PIN: OFF ????Testimonial: #gzm_testi FAST RESPON ???? line.me/ti/p/@ixa3545k'),
(169, 'angelicshop', 'AngelicShop(DIST TASREPLIKA)', 'AngelicShop(DIST TASREPLIKA) ????Line: @zdj3268q(pke @ saat search) ????Pin bb:587FFA65/ channel bbm C002DE989 ?Wa:081313456069 ????JNE(JKT) ????via BCA/Danamon Klik ????????untuk add line : line.me/ti/p/%40zdj3268q'),
(170, 'orizashop11', 'Fashion Murah pria wanita', 'Fashion Murah pria wanita serius order bbm : 7B381FC8 (join grup pm) wa : 085729855245 Line : dwipuji_a Keep langsung Transfer resi H+2 BRI kesamaan barang dengan gambar 75-90% '),
(171, 'octa.ov', 'OCTA\'s SHOP WA 085649928227', 'OCTA\'s SHOP WA 085649928227 ON LINE SHOP TERPERCAYA  PENGIRIMAN DARI YOGYAKARTA TRANSFER VIA BCA ONLY SERIUS ORDER  WA 085649928227  BBM 51F23CA3 line.me/ti/p/6Z7X67VL8O'),
(172, 'guttenstore', 'MEDAN FREE ONGKIR MIN 2 PCS', 'MEDAN FREE ONGKIR MIN 2 PCS Fast Pre Order BKK???? Location : Medan ???? BB PIN : 26A04CA7/541516B2 Line : Ferasiska Via Jne / Kurir  #guttentestimoni ???? LINEBBM.SEMUAFASTRESPON'),
(173, 'hnieniceshop', 'niceshop', 'niceshop Open order Tas Branded Minat chat  Wa : 082116644998 NO SMS, NO CALL Line : Hnie1009 PIN : 5842840C           27D9A53F Buat reseller bisa dropship '),
(174, 'yosichanstore', 'TAS SLINGBAG 40-55RB', 'TAS SLINGBAG 40-55RB Jember Bbm : 56e7f86c Line : yosiyosikaa / sms 083893516643 BBM HANYA YG UNTUK SERIUS ORDER JNE / TIKI / BRI ONLY scroll down = restock sewaktu-waktu BUDAYAKAN.MEMBACA.SEBELUM.BERTANYA'),
(175, 'shoppaholic_eva', 'Eva Chan', 'Eva Chan ???? Pin bb 2A50AB15  ???? Line evapachan ? WA 087824843812 (no call) ? Pengiriman dari bandung ???? TRUSTED  ????sorry..just for serious buyer only!! '),
(176, 'promosiolshop.murah', '#EANF#', 'Apakah ingin mempromosikan olshop anda dgn harga murah yaitu cuma TIGA PULUH RIBU SAJA? Langsung aja 57230F29 untuk lengkapnya! NO FOLLBACK?=UNFOLLOW '),
(177, 'eeclatrath', 'BagClothes Murah', 'BagClothes Murah Makassar LINE 1: liiaasyaf LINE 2: @egz6900o (pakai @) ???? : BNI ???? : JNE/Gojek/COD COD sesuai S&K  SOLD=DELETE HIT AND RUN? BLACKLIST?? ATAU KLIK line.me/ti/p/%40egz6900o'),
(178, 'foodgek', 'Tommy', 'Tommy Lover of All Foods???? Coffee Drinker?? World Traveler?? Geek from NYC???? foodgek.com'),
(179, 'deliciasdr', 'Delicias RD ????', 'Delicias RD ???? ???? Porque amamos los dulces????!  Envia tu foto tomada a cualquier delicia no importa donde estes por DM o al correo: deliciasrs01@gmail.com ???? '),
(180, 'emmaquine', 'Emma Quine', 'Emma Quine Life through the eyes of a acupuncturist and Chinese herbalist who specialises in fertility, pregnancy & post natal care in Sydney\'s Inner West. www.emmaquine.com'),
(181, 'onepurenz', 'Onepurenz', 'Onepurenz Leading New Zealand Mineral Water. FREE Shipping in NZ www.onepure.co.nz'),
(182, 'camerono_o', '#EANF#', 'Golfer, drummer, I\'m single ;) '),
(183, 'new_leaf_new_life', 'April', 'April Fitness journey Recipes, support, inspiration?? Feel better. Look better. BE better.  ???? Division Supply Co. ???? Coupon code: APRILZ for 20% off '),
(184, 'ngocmaruko', 'K?o C?m Xúc Maruko ~ ?????', 'K?o C?m Xúc Maruko ~ ????? ??????????????????? Vietnamese girl ???????? study Information Management at USSH ~ VNU ???? Not all those who wander are lost ???? facebook.com/dang.ngoc2'),
(185, 'thecorporatemummy', 'The Corporate Mummy', 'The Corporate Mummy All things MUM | Support & Advice | Tips | We are all in this together ?? #thecorporatemummy to feature E:thecorporatemummy@hotmail.com thecorporatemummy.wordpress.com'),
(186, 'tastygrammar', '#EANF#', 'Tasting tasty food everywhere #tastygrammar '),
(187, 'ms_am1997', '??', '?? from almadinah ? { Life so exciting }? Queen of octuber ?15 I study in DE ???? 2015 My ???? My age 19?                           PIN:5AEF6505 '),
(188, 'food.cd', '?????????????', '????????????? ???????? ???????instagram ?????????????? ???????,???????????,???????????? ?????????  ?????????????????? ???????? ?????????????????? ???????? ???????????? '),
(189, 'jakemeister91', 'Jake', 'Jake Barista By Day But By Night... Theme Park Hobbyist Cinema Admirer Halloween Obsessed Ashley Lover UCF Film Student Horror Movie Fan HHN Nerd '),
(190, 'observantbeing', '#EANF#', '????Business Administration Father, Chasing academic goals ????????????Becoming a gym ???? Trying to be healthy '),
(191, 'artsyckdope', 'itzjustforfun', 'itzjustforfun Idc about the likes or follower i got,what\'s more important is how happy am i with my acc and i likes sharing memories with people,let\'s be happy! :D aslongasudgafimfineeee.com'),
(192, 'wheretofin_bangkok', 'Where To Fin Bangkok', 'Where To Fin Bangkok ???? The Easiest way to Fine Dining. ???????????? ???? FB: wheretofinbangkok ???? WEB : www.wheretofin.com ???? More info or Reservation call 02 - 633 - 3998 goo.gl/ypvyao'),
(193, 'monicaquerrales', 'Mónica Rose', 'Mónica Rose De Venezuela? Y de Brown skin? #uc \"Constantemente evolucionando\" twitter.com/Mquerral'),
(194, 'xavier_32', 'Just a guy who likes to cook', 'Just a guy who likes to cook Vegan + healthy doesn\'t have to mean yoga, tofu, and kale. Follow me to see how a healthy plant-based lifestyle doesn\'t suck '),
(195, 'pamiye', 'Palm Nopphamas', 'Palm Nopphamas Eating, travelling and playing (toys) by pamiyé Thank you to visit and follow my page. Photos by me with G1XMark2, D90, Note3LTE www.facebook.com/pamiye'),
(196, 'officialfoodgroup', 'Official Food Group', 'Official Food Group 8 friends who travel the country looking for great food. Based in MD/CA/FL/VA/DC #officialfoodgroup to be featured Contact officialfoodgroup@gmail.com '),
(197, 'dalpenge', 'Jessica', 'Jessica Isaiah 45:2 YVR '),
(198, 'wulanstore_grosirtasnew', 'id wulanstore_grosirtas1 eror', 'id wulanstore_grosirtas1 eror 2 pc-8 k 3 pc- 5 k/pcs  Serian -10K JAKARTA/BCA+MANDIRI #testimowulanstore ????pin 581c1623. ????wa 082240302563 ????line @yme6002c    Klik bawah line.me/ti/p/%40yme6002c'),
(199, 'jasapromoteiklan3', 'Jasa promote termurahhhhh', 'Jasa promote termurahhhhh Untuk pricelist promote, kontak kami LINE : @JXZ7226G  Atau klik ???????? line.me/ti/p/%40jxz7226g'),
(200, 'ccenfashion.id', 'FIRSTHAND SUPPLIER????????', 'FIRSTHAND SUPPLIER???????? For serious buyer only?? Bbm : 5A9FD967 Line : @fgq9390r or add by link? TRUSTED???????? CHOCO CRUST!! JANSPORT JUST 85K !!! since 2013  Medan-jakarta???? line.me/ti/p/%40fgq9390r'),
(201, 'danargallery1', 'danar gallery', 'danar gallery Pengiriman dari Jakarta Order fast respon : BB 227A7AD3 BB : 7FABDD9D / 7FC00A3D WA : 0856 9310 3964 / 0812 1347 908 Harga blm ongkir IG lama DISABLED '),
(202, 'motrahfashion_shop', 'FASHION SET DAN SEPATU MURAH', 'FASHION SET DAN SEPATU MURAH Order?  Pin : 5AFD9822 (ada group) Wa / Line : 081212156566 ???? : BCA  ???? : JNE Jakarta & Bandung Pengiriman H+2 No Trf = No Keep ????Happy Shopping???? BARANG.CEPAT.HABIS.READY.STOK'),
(203, 'azzahraacollection', 'Azzahraa Collection', 'Azzahraa Collection ???????? MOKAMULA MODIPLA HEEJOU ERFA MORDIVA KONOKA SIMPLEIZY KATIA KINATA MAIKA FADZILA ???????? bbm: 5801C7AA sms/wa: 085643023531 line: lafiinb TRUSTED !! www.facebook.com/azzahraacollection/'),
(204, 'delarosabag', 'Delarosa Bag', 'Delarosa Bag Beautiful and Trendy bags SMS/WA : 081387071075 tokotasdelarosa@yahoo.com www.facebook.com/delarosabag www.tasgrosirdelarosa.com'),
(205, 'kanezka_shop', 'Authentic Branded Bags', 'Authentic Branded Bags ?WA : 081809094933 ?Bbm : 73993FB7 ????Line : kanezkashop ???? Ready & PO (DP 50%) ???????????????????? ???? Don\'t trust, Don\'t buy ????? '),
(206, 'shoppink_preloved', 'New & Preloved Stuff', 'New & Preloved Stuff ???????????????????????????????? Yogyakarta  FIRST PAY FIRST GET  ???? BCA / Mandiri  ????JNE / POS / Wahana  ???? Jl. Ganjuran, Bantul  Wa : 085743433845 Line : evipungky '),
(207, 'agfa_shoes', 'charles & keith Vinci Montego', 'charles & keith Vinci Montego 100% original  Line agfa_shoes Bbm 571EA786 Sms 085642999757 IG ke-2 @agfa.shop (tas original) www.agfa-shoes.blogspot.com'),
(208, 'fashionchannel_shop', 'HEAD TO TOE UNDER 250K', 'HEAD TO TOE UNDER 250K ???? TAS ? JAM TANGAN ???? BAJU  Was @batam_bagz  Line/WA : psp_smiley / 087773454690 First IG : @hanniosh  BCA - BATAM & JKT Beli 2 diskon Scroll down ???? '),
(209, 'caroline.bag', '????CATHKIDSTON & KIPLING MURAH', '????CATHKIDSTON & KIPLING MURAH READY STOCK  100% REAL PICT JNE JKT/GOJEK  SOLD DELETE ????WA.082186297929 LINE KLiK DI BAWAH  ???? line.me/ti/p/%40hle2040f'),
(210, 'widya_mamaegio', 'Widya_Gio Fashionstuff', 'Widya_Gio Fashionstuff SMS : 085725246024 WA : 081548539283 PIN BB : 277115F8  FB : Widya Mamaegio Line : widya.mamaegio '),
(211, 'glamastores', 'glamastores', 'glamastores READY STOCK WOMAN CLOTHES HIGH QUALITY  Line:  chyamayra  Bbm: 7CFF279E Wa: 081542626425 NO CANCEL AFTER BOOKED Kode angka & huruf beda pengiriman '),
(212, 'muubags', 'TAS IMPORT ASLI (READY BATAM)', 'TAS IMPORT ASLI (READY BATAM) ???? BATAM ????JNE  ????Cara order barang : ????Phone:089685869801 ????LINE   : @vwu190g ( pake @) ????WA     : 081235904555 ????BBM  : 51BB47F0 ???????????? '),
(213, 'tascowok', 'tascowok', 'tascowok Menerima grosir dan resellers 1 ST HAND  Lokasi : batam www.bajulaki.com Line : tascowok Bbm : 58867b79 Whatsapp : 081275744949 Sold out :DELETE www.bajulaki.com'),
(214, 'rinatasbranded_iklan5', 'AKUN PROMOTE', 'AKUN PROMOTE FOLLOW AKUN UTAMA @rinatasbranded1 . WA 081280995917 LINE rinatasbranded1 BBM 574C0078 TRUSTED #testirinagaragesale rinatasbranded.wordpress.com'),
(215, 'arl_widh', 'DJ_arL', 'DJ_arL ???.Arlykha.??? Awali setiap langkah dengan kata-kata yg indah dan niatan yg baik serta pikiran yg positif.. ?.semoga hidup ini barokah.? '),
(216, 'scorpio_shine', 'Jyoti', 'Jyoti I Smile, I Laugh, I Love, I Hope, I Try, I Desire and at times I fear. Socialite#Fashion#Brands#Glam#Friends#Travel#Food. Love my city Toronto ?? '),
(217, 'chelseaclark', 'chelseaclark', 'chelseaclark Journalist currently writing about food & all things lifestyle @couriermail | I also teach kiddies how to dance @langshawdancity '),
(218, 'guguhh21', 'guguhh21', 'guguhh21  '),
(219, 'newcarolyn', 'Carolyn McKay', 'Carolyn McKay traveller, photographer, educator www.carolynmckayphotography.co.uk'),
(220, 'alkapoweraustralia', 'Ionic Alkaline Water', 'Ionic Alkaline Water ???????? Pure & Simple, It\'s Better Water ???????? ??Absolute Purity ??Ultimate Hydration ??Stable pH 9-10 ??Clean Refreshing Taste www.alkapower.com.au'),
(221, 'marocasluis', 'Mario Luis', 'Mario Luis  '),
(222, 'rianindraprabowo', 'Rian Indra Prabowo', 'Rian Indra Prabowo 24 years old Musician | Rhytim Guitarist | Traveller | Foodlikers | Gms Jogjakarta Manokwari > Yogyakarta '),
(223, 'joaniesdelights', 'Joanie\'s Delights', 'Joanie\'s Delights Fresh food, made with love. www.facebook.com/joaniesdelights'),
(224, 'radiosobe', 'Radio Bar South Beach', 'Radio Bar South Beach Your local bar in SoFi. Happy Hour daily 6-9pm | 814 First Street Miami Beach, FL 33139 www.radiosouthbeach.com'),
(225, 'olorarclos', 'Monica Ford', 'Monica Ford ?G_?E??T?10k+?! followerz?? / Likes? Instantly?? 10012Followers.WIN'),
(226, 'dinelafoods', 'Dine LA Foods', 'Dine LA Foods Join our homemade food adventures???????? Lets leave a taste on the tongue ????????delivery?order possible???? ???? dinelagreek@gmail.com 2 cookers ???? GR????TR '),
(227, 'alcohol120grados', 'Alcohol120°', 'Alcohol120° Media-promoción  809-517-5115 underConstruction.com'),
(228, 'aidennyc', 'Paul Thompson', 'Paul Thompson Reside in Connecticut with my beloved partner at our circa 1830 Willow Brook Estate. '),
(229, 'aidennyc', 'Paul Thompson', 'Paul Thompson Reside in Connecticut with my beloved partner at our circa 1830 Willow Brook Estate. '),
(230, 'djkillen14', 'David James Killen', 'David James Killen Struggling Pickleball player '),
(231, 'leilatinasian_atx', 'Leilani Lim-Villegas', 'Leilani Lim-Villegas Mami of 2 Super Machitos. Food Critic. Finance Expert. Piano Freestyle Musician. Globetrotter. Sports Chick. Jives w/ good vibes in Austin, Texas! ????? www.myfab5.com/leilatinasian_atx'),
(232, 'bgkdesigns', 'BGK Designs', 'BGK Designs Organic | Fun | Unique Jewelry...make your soul sing www.bgkdesigns.com'),
(233, 'thrower.stephen', 'Stephen C. Thrower II', 'Stephen C. Thrower II 26. NC & VA????GA. ATL???????????????? chattahoocheefrog@gmail.com ???? '),
(234, 'shezikashop', 'Shezikashop', 'Shezikashop Pin  : 28A2AF56 Pin  : 5527C9B7 Line: iekashezika  Join grup add line aja:) Wa: 085260239059 (khusus wa no call/sms) Tas&jam tangan tersedia disini '),
(235, 'al_tas_olshop', 'al_tas', 'al_tas Ig lama @al_tas (DISABLE) Order/tanya pm ke Bbm : 5869103E Wa/line : 085786446444 Cek testi di #testial_tas '),
(236, 'crazyshopping.addict', 'crazyshopping', 'crazyshopping Happy shopping! ???????? TOTE BAG, TAS IMPORT???? RANSEL,DOMPET,TOPI DIBAWAH 100k!!!! ????Line ???? crazyshopping.addict ? WA ???? 085714447924 '),
(237, 'teenage_shopp', 'MURAH MERIAH!!!', 'MURAH MERIAH!!! ???? Line : depitan  BBM : 59567D0D WA???? : 0878-8297-4340 ???? REAL PICTURE '),
(238, 'linstore__', 'HOUSE OF MODE ????????????????????', 'HOUSE OF MODE ???????????????????? ????line : @mqr7744i (Pakai @ ya depannya )  ? : +62819-3980-5335 (WhatsApp Only)  ???? : JNE, POS ???? : BCA ????READY STOCK ????REAL PICTURE #testiLINS '),
(239, 'rumah_kaela', 'Tas Kain Handmade', 'Tas Kain Handmade JNE - BCA-Kirim H+1  Ready stock only  Follower aktif banyak promo MINGGU SUPER SLOW RESPON BBM: 5A83703F Serius order n info via LINE ???? line.me/ti/p/%40sqf6562n'),
(240, 'snardiy', 'SNArdiy -- JualBajuCewek', 'SNArdiy -- JualBajuCewek Bismillah Dengan Menyebut Nama Allah Jual Baju Cewek SMS/WA: 08998567499 Line: nov_ardiy kalo mau tanya2 chat ya kalo order chat disertai gambar ya ^^ '),
(241, 'iklandemishoes', '#EANF#', 'Akun iklan untuk @demishoes Untuk katalog lengkap silahkan follow @demishoes Komen di sini gak akan dibalas '),
(242, 'nomiishop', 'nomiii', 'nomiii ????New&preloved stuffs???? ????Fb: blanja nomi ?NO REFUND&BARTER? ????JAKARTA ????JNE ????BCA/MANDIRI ????SMS/WA: 085770004802 ????BBM: 51BA8F03 ????LINE: nomishop '),
(243, 'iklanbarang_niagaa', 'iklanbarang_niagaa', 'iklanbarang_niagaa Paid promote termurah..?? Minat jadi member.!! Contact id line : Ciella17 BBM : 585DBEC7 '),
(244, 'meryaxian', 'dagelan kuliner traveling', 'dagelan kuliner traveling ?Ready n preorder  Whatsapp : ?+62 815-2094-9330? Order line  : merya.xian  Pin bb 59E0A1AE Bisa dropship, testi.ketik.hashtag.testimeryaxian.igcuma.meryaxian.dan.goodfoodpekanbaru.yah'),
(245, 'lovelynclothing', 'Lovelyn Clothing', 'Lovelyn Clothing Info lebih lanjut hubungi ?? Contact: ????Wa : 085763326661 ????Id Line: linastone '),
(246, 'idakristiani2', 'idakristiani2', 'idakristiani2 Oder langsung inv pin  35BD3D8D, 38087D09 ~ id line.      : idamuhidin ~ WA/Tlp     : 085641890470 Pengiriman : JNE, TIKI, POS READY BATAM. '),
(247, 'fstore_id', 'Tas Fashion', 'Tas Fashion Trusted Olshop Open 07 am - 05 pm ???? Tas fashion import ? Jam tangan branded ? Open Order @fstoreid ???? Via JNE ???? 081 904 117 905 ???? @fstore_ LINE ? line.me/ti/p/~fstore_'),
(248, 'jualtasmurahbyha', 'Sepatu Tas Branded', 'Sepatu Tas Branded ???? @waynawanis ???? ORDER? WA: 089650078477 Line: wayna16 BBM: 276F0693 Reseller Welcome???? HARGA yg tertera BELUM termasuk ONGKIR READY STOCK!!! HANYA.MELAYANI.YANG.SABAR');
INSERT INTO `instagram` (`id`, `username`, `first_name`, `bio`) VALUES
(249, 'zainstore', 'Pusat JansportKiplingWaistbag', 'Pusat JansportKiplingWaistbag ????TERMURAH???? ????pin bb : 543DA80D ????WA 088210511949 ????Line zainstore Shipping JNE bandung Payment BCA | BNI Dilarang ambil pict tanpa ijin SOLD=DELETE line.me/ti/p/luOL6P_2DK'),
(250, 'veezeefashion', 'VeeZeeFashion', 'VeeZeeFashion ????VeeZee Fashion???? ???? 0812 9896 2211 ???? WA  08777765 8228 ???? BBM  21ACCC02-7A018071???? TWIT veezeequeenzell ????VeeZee Fashion, Your Fashion Your Style???? facebook.com/pages/VeeZee/1417544025171719'),
(251, 'ms.susi', 'CnK HushPuppies Vincci Fossil', 'CnK HushPuppies Vincci Fossil ????% original ????Bbm : 563eff03 ????Line : mzsusi ???? Wa by request ????Jakarta/????bca **Repeat order disc 10k  time limit : 3-mo Cari item : #cekmssusi LINEforFastResponse.cekmssusi.ScrollSampaiBawah.WeekendSlowResponse.commentdiIGdibalaslama.1ST.PAY.1ST.GET.NoKeep.NoBook.LimitedStock.GrabItFast.followerapaadanyaa.NiceService.feelfreetoask.smileyface.com'),
(252, 'hesti_shop', 'HESTI SHOP', 'HESTI SHOP TAS MULAI 75 RBU.AN FOR ORDER CHAT BBM : 5AA00FB7 (fast respon) WA/TEXT : 085815148623 LINE : hesti_olshop ALL KIRIMAN MOJOKERTO HAPPY SHOPPING '),
(253, 'bunnyhouse_vintagebags', 'SEMUA TAS PO - YOGYAKARTA', 'SEMUA TAS PO - YOGYAKARTA ???? KIRIM H+2 STLH READY ORDER : 083869013335(SMS/WA) LINE : @MIMINBUNNY (pakai @) BBM : 56D8A32F #TestimonialByGirindani ????ALAMAT TOKO tnya admin ya detile.apapun.tidak.bisa.diubah.setelah.transfer'),
(254, 'patilere_ozel', 'Petyasam', 'Petyasam ????Ödeme Eft-Havale?????ade yoktur.????Kargo al?c?ya aittir?WhatsApp:05437272688 '),
(255, 'leon_tasarim', '3Ds Max Modelleme', '3Ds Max Modelleme Tüm Tasar?mlar Firmam?za Aittir  Modelleme ve tasar?m hizmeti için ileti?im 05333369126 Designer @hasancnt leontasarim.com'),
(256, 'istanbulmucevherat_', '?stanbulMucevherat', '?stanbulMucevherat Hayal Kurmakla Ba?lar Her?ey Hayalinizdeki Tasar?mlar?n?z? Bu Sayfadan ?steyebilrisiniz Nede Olsa istanbul Suprizlerlerle Dolu?? '),
(257, 'nilo3306', 'lina', 'lina Mor sever zeynep linanin annesi????FENERBAHÇE????????    ikizler    ankara_mersin    erkekler engellenir? çekili?lere katiliyorum ki?isel hesap '),
(258, 'fatihbayankuaforu', 'Fatih Bayan Kuaförü', 'Fatih Bayan Kuaförü gültepe mahallesi deliorman caddesi no:83-a k.çekmece/istanbul 02124251886 '),
(259, 'tutu.tasarim', '#EANF#', 'TÜTÜ TAKIMLAR, TSORTLER, KAPI SÜSLER?, TOKALAR, PAT?KLER S?PAR?? ?Ç?N DM MESAJ ÖDEME HAVALE ~ EFT KARGO ALICIYA A?TT?R '),
(260, 'appleside_bracelet', 'By Arda & Burcu', 'By Arda & Burcu ???? Special Designs For Bracelets ?? 925 K Silver/14-18 K Gold Rozary-Necklace-Badge Designs ????EFT/Money Transfer/Paypal ????+905357713388 (Whatsapp) www.etsy.com/shop/ApplesideDesigns'),
(261, 'happyartstore', 'Happy Art Store', 'Happy Art Store ????Sipari?leriniz için WhatsApp  05336977597 ?????ste?iniz üzerine her model de renk ve desen tasarlan?r. www.facebook.com/happyartstore & www.happyartstore.com'),
(262, 'handcraft_atolye', 'by fls ylds', 'by fls ylds el eme?i özel tasar?m ?istenilen renklerde haz?rlan?r.10-15 gün teslim süresi ödemeler havale seklindedir.siparis için whatsapp veya DM den ileti?im? '),
(263, 'aysnr_inan', 'Aysenur ?nan', 'Aysenur ?nan  '),
(264, 'saziyye', '#EANF#', 'O bir  anne  o bir hobisi olan kad?n   o bir çal??may?  çok  seven üretken kad?n   Veeeee Musmutluuu ????????????????????????????????????????? '),
(265, 'cemmerman', 'Cem Erman', 'Cem Erman interior architecture / designer  cemerman@hotmail.com.tr OP?O GALATA içmekan ve ürün tasar?m? '),
(266, 'd.lightkstorebag', 'D.LIGHTKSTOREBAG', 'D.LIGHTKSTOREBAG Sell-bag premium quality                ????BATAM- JNE ????LINE : @qih8064v (Pake @) ????WA/SMS: 081278293497                ???? BBM : 5A029CCD line.me/ti/p/@qih8064v'),
(267, 'prelovednabilahshop', 'TERIMA TITIP JUAL MURAH', 'TERIMA TITIP JUAL MURAH ???? : @nabillah___shop Whatsapp : 0 8 5 6 4 1 7 4 5 5 5 2 Line : @ihm4112o ???? JAKARTA  F A S T. R E S P O N T ??? line.me/ti/p/%40ihm4112o'),
(268, 'iklan.bos.asi', 'Busui & baby stuff import', 'Busui & baby stuff import Please follow @bos.asi utk update stok Order: Line : bos.asi WA : 0898.200.5453 BBM  : FULL Pengiriman H+1 stlh trf TANGERANG www.bosasi.com'),
(269, 'deasy.irawati', 'Deasy Irawati', 'Deasy Irawati Open order fashion batik, rajutan, souvenir. Handy craft. WA : 081802514476 Fb : Deasy Irawati Pin BB by request Email : ibu.wiby@gmail.com '),
(270, 'geklokitabukian', 'Lokita\'s Olshop', 'Lokita\'s Olshop Contact Line id : geklokitabukian untuk tanya tanya atau order. Harga belum termasuk ongkir ya. Lewat Pin BBM 7628CA4D juga bisa. '),
(271, 'dis_fashions', 'alghazali7 Dagelan Raisa6690', 'alghazali7 Dagelan Raisa6690 IG LAMA @disfashions Order via  Sms 081267404692 Bbm 26C3993E Line : disfashions WWW.DIS-FASHIONS.COM WWW.DIS-FASHIONS.COM'),
(272, 'bellsneedbag', 'SUPPLIER TAS&DOMPET BRANDED', 'SUPPLIER TAS&DOMPET BRANDED SCROLL SAMPAI BAWAH???? ????Pin : 74283246 ????Line : @fot1694q  ???? WA : 082111710171 TRF = KEEP ???? BCA & BNI Atau klik link dibawah ini???? line.me/ti/p/@fot1694q'),
(273, 'promosi_cici_shop_3', 'AKUN PROMOSI KHUSUS RESELLER', 'AKUN PROMOSI KHUSUS RESELLER AKUN KHUSUS RESELLER AKUN KHUSUS RESELLER AKUN KHUSUS RESELLER . F O L L O W F O L L O W F O L L O W . cicishop_39rb . cicishop_39ribu . FOLLOW YAAAAA '),
(274, 'erniyani06', 'erni yani 06', 'erni yani 06  '),
(275, 'fenskayoen', 'Fenska Yoen', 'Fenska Yoen [??? ?]  Line: fenska_yoen '),
(276, 'sisterzoneshop', 'TAS UNYU UNDER 100RB SAMARINDA', 'TAS UNYU UNDER 100RB SAMARINDA ???? S-SHOP ???? Totebag & ransel Transfer via BCA & BRI Line : sacilkecil Whatsapp : 085753087374 ????Samarinda Minat? Ask for details Owner @hafizhahsarah '),
(277, 'moo_babyshop', 'moo babyshop', 'moo babyshop ???? Baby and Kids Collection ???? Surabaya ? WA / sms : 081515420301 ? BBM : 56C3C7F7 ? Line : @moobabyshop #moobabyshop #moobabyshopsale ???? SOLD=DELETE '),
(278, '_brandedvbag', 'BrandedVbag', 'BrandedVbag Follow first ya!! ?? Real Picture????% Contact fix Order ?????? Line: @wgk7596L  BB: 2A6DD023  Shipping from Batam & Jkt (Fast Respons) ????????Klik?? line.me/ti/p/%40wgk7596l'),
(279, 'mariabrandedbag', 'DAGELAN RAISA 6690', 'DAGELAN RAISA 6690 ????TAS BRANDED???? ORDER ????BBM: 5422CFE6 ????TEX-WA: 0822-4000-2660 (NO CALL) ????LINE: Mariabrandedbag ????BATAM ????BRI-BNI ???? JNE,TIKI #testimariabrandedbag '),
(280, 'nefthali_mg', 'Thalí', 'Thalí Estudiante de  #teatro y asesor de #Joyeriaswarovski #asesor #Swarovski #México #love, #instagood, #me, #tbt, #cute, #follow, #followme, twitter.com/Nefthalimg'),
(281, 'ksonerkaya', '#EANF#', '| L\'art est problématique. behance.net/ksonerkaya'),
(282, 'design_vt', 'Vahdettin Toker', 'Vahdettin Toker Graphic Designer  Dü?ündü?ünü yapan yapt???n? dü?ünen... vahdettintoker.carbonmade.com'),
(283, 'kelebek_takidunyam', 'Melek', 'Melek Bismillahirrahmanirrahim.. Fiyat ve bilgi için DM&Whatsap05556892806 *KAP?DA ÖDEME  *KAR?ILIKLI REKLAM YAPILIR K?MSEDE OLMAYAN TAK?LAR SEN?N OLSUN m.facebook.com/profile.php?id=390986050980246'),
(284, 'dugunhediyelerim', 'nikah?ekerleri kinamalzemesi', 'nikah?ekerleri kinamalzemesi ????söz  ????nisan ????bebeksekeri ????nikahsekeri ????kinamalzemesi WHATSAPP ????0506 173 5000 ???? EFT&HAVALE ????Rüstempasa mh sabuncuhan cd no 68 EMINONU/IST www.dugunhediyelerim.com'),
(285, 'kagankytasarim', 'Elif Cesur', 'Elif Cesur ????????????????????????sipari? ve bilgi icin Whatsapp????+90 0545 288 06 66 yada ????kagankytasarim@gmail.com Not: Aras kargo ile Kap?da ödeme mevcuttur?havale/Eft '),
(286, 'momstil', 'momstil', 'momstil Anne-cocuk tasarim.. iletisim:05059820882 Momstiltasarim@gmail.com baski degildir kumas boyasiyla cizilmistir.üzerine istediginiz yazilar yaz?labilir '),
(287, 'muhammedcimtay', 'Muhammed Çimtay', 'Muhammed Çimtay ?stanbul/ Anadolu Yakas?/ /Taki tasar?m '),
(288, 'aibar_butik', 'Reklam ?s Birlikleri????Whatsp DM', 'Reklam ?s Birlikleri????Whatsp DM ????Tasarim Ve Vintage ??Musteri hiz. Watsapp ????0541 2210350 ????Iade Kapida odeme yok ????Teslim Suresi Hemen&22 29 isgun ????Havale Eft/paypal/Western/ '),
(289, 'hobimatik', '#EANF#', 'Sizde dm\'den kendin yap projelerinizi paylasabilirsiniz.. '),
(290, 'saat_pazarii', '#EANF#', '????Urunler yurtdsndan getirilmektedr ?Teslimat suresi 15-27 gundr ?Shipping worldwide ????Iade,degisim,kapida odeme yoktur???? ????Whatsapp: +90(554)896 8287 '),
(291, '__vijdan__', '????München/Frankfurt  U?ak 64 ????', '????München/Frankfurt  U?ak 64 ????  '),
(292, 'tugbanin_tasarim_dunyasi', 'TASARIM DÜNYAM~', 'TASARIM DÜNYAM~ Tasar?ma dair her?ey???? ?irinelere ?irin etekler???? Missabun~mista? ???? '),
(293, 'vivian_butique', 'Mango Zara Cnk Guess', 'Mango Zara Cnk Guess Trusted Seller  Ready Stock Jne-Sap express Order: Line: vivian_btq  BB: 7640FA7B WA: 08566626815 SMS 085363550072 Resi +h1 '),
(294, 'kharisfauzi90', 'kharisfauzi', 'kharisfauzi  '),
(295, 'auspost', 'Australia Post', 'Australia Post We are travelling the country and capturing images of #AustraliaConnected. Keep an eye out for your postcode. www.loadandgochina.com.au/article/episode-1-park-life'),
(296, '510tay', 'Taylor Griffith ????????????', 'Taylor Griffith ???????????? Photographer ~ Explorer  University of La Verne  ?????????? Bay Area -> LA  Canon ~ Leica ~ Hasselblad  Member of the #Creatorclass taylorgriffithphoto.com'),
(297, 'coulorpet', 'courtney', 'courtney leopard print enthusiast | elaine benes is my idol | voldemort wants a chippy | ???????? '),
(298, 't_racey001', 'Tracey Hunter', 'Tracey Hunter Lover of life, beauty, nature, and my 2 girls ?????? '),
(299, 'lady_j3ss1ca', 'lady_j3ss1ca', 'lady_j3ss1ca Jessica????25????Flight attendant ????? \"If you\'re going to be weird, be confident about it\" '),
(300, 'jineuz', '#EANF#', 'Just a simple normal guy from ipoh who loves coffee. =) '),
(301, 'cessiah.alice.millinery', 'Cessiah Alice Millinery', 'Cessiah Alice Millinery Milliner | Hobart  Headpieces available at Brooke Street Pier (Hobart), Ruby & Leo (Highton VIC) or by contacting me directly: cessiah.h@hotmail.com www.facebook.com/cessiahalicemillinery'),
(302, 'darthveganblog', 'Jay Clair', 'Jay Clair I vegan I scream I write I equality  I speak I be fit www.jayclair.com'),
(303, 'jacq.gray', 'jacq.gray', 'jacq.gray  '),
(304, 'nickesares', 'Nick Esares', 'Nick Esares The plan is to have no plan - Colorado is home www.facebook.com/nickesares'),
(305, 'jessplaining', 'Jessica Ellen', 'Jessica Ellen Based in Melbourne, Australia.  Travel, food, puppies, feminism and things that matter. ????  New video on Essena O\'Neill, Alexis Ren & Jay Alvarrez ???????? youtu.be/DEBIcvt7fPA'),
(306, 'darrellrobbiec4', 'Darrell Robbie Choong', 'Darrell Robbie Choong Why yes. These are my photos. '),
(307, 'ilaria.raf', 'Ilaria Raffaele', 'Ilaria Raffaele  '),
(308, 'buffbutlerstas', 'Jarryd', 'Jarryd #buffbutlerstasmania Waiters Strippers Entertainers Facebook: buff butlers tasmania '),
(309, 'theoldwoolstore', 'The Old Woolstore Hotel', 'The Old Woolstore Hotel The Old Woolstore\'s accommodation in Hobart, Tasmania is centrally located in the city centre and two blocks away from Hobart\'s majestic waterfront. www.oldwoolstore.com.au'),
(310, 'furbabiesandfriendsphotography', 'Deb Sulzberger', 'Deb Sulzberger 2015 AippTasmanian Pet Phtographer Of The Year find me on facebook....https://www.facebook.com/pages/Fur-Babies-Friends-Photography/436213809748883 www.furbabiesandfriendsphotography.com'),
(311, 'factsandhunt', 'Facts & Hunt', 'Facts & Hunt A monumental journey through the past... | Coming Soon | factsandhunt@gmail.com '),
(312, 'amped_up_entertainment', 'AMPED UP ENTERTAINMENT', 'AMPED UP ENTERTAINMENT  www.facebook.com/AUSBbattles'),
(313, 'overthetoptextiles', 'texture colour taste create ????', 'texture colour taste create ???? My farm life living alongside a fresh water river on 40 sustainable acres between the mountain and the ocean ???? Eve Cottage Kiora NSW airbnb '),
(314, 'she_shreds_tasmania_crew', '#EANF#', 'She Shreds Tasmania is a girls only skateboarding crew in Australia, no age limit and only rule is to have fun! Facebook -She Shreds Tasmania www.sheshreds.info'),
(315, 'vilen_aries', 'Vilen shop', 'Vilen shop Semua barang ready ya ? ????Mandiri Pin 51a952ee Beli banyak dapat diskon Happy shopping ^_^ '),
(316, 'annisahijabcantik', 'AZAFA RUMAH ONLINE', 'AZAFA RUMAH ONLINE Suplier Fashion Solo : Hijabfashion, Tas, Sepatu, dll ???? Pin BB 7EAE1E3A ???? WA 087835146157 ???? Order via BBM / WA ????Welcome Reseller & Dropship???? www.azafafashionshop.com'),
(317, 'olshopclothing', 'TAS BRANDED HARGA GROSIR', 'TAS BRANDED HARGA GROSIR Jual OnlineShop Tas,Jaket,sepatu,Jam Tangan Order dan tanya\" langsung hubungi  kontak???? ????BBM: 24d1ec8e ???? JNE BANDUNG ???? Payment BCA HRG Blum Ongkir '),
(318, 'walletbagstuff', 'JOYAGH TAS & DOMPET TERMURAH', 'JOYAGH TAS & DOMPET TERMURAH REALPICT  READYSTOCK SCROLL DOWN BARANG JELEK= UANG KEMBALI LINE @:@keb1129Q  JKT MANDIRI #caraorderwalletbagstuff  follow @walletbagstuff.testimonial line.me/ti/p/%40keb1129q'),
(319, 'lokomotifbag', 'TAS BRANDED IMPORT GROSIR', 'TAS BRANDED IMPORT GROSIR Ready Stock  BBM: 2AD946D1 Abc/Lokomotif Bag  Tel/Sms/ WA: 081533841896  Website: www.tas200.com Kami tidak me-monitor komentar di Instagram. www.lokomotifBag.com'),
(320, 'jualmasker_spirulina', 'Jual Masker Tien\'s Bandung', 'Jual Masker Tien\'s Bandung  '),
(321, 'leahnataleah', 'FALTSHOES 75rb DPT 3psg (MIX)', 'FALTSHOES 75rb DPT 3psg (MIX) i\'m fulltime MOMMY form my daughter @princessanayla line : @leahnataleahstyle2 ( jgn lupa pkai @ ya ) '),
(322, 'jual_peninggi_badan_tiens_', 'Agen Peninggi Tien\'s', 'Agen Peninggi Tien\'s  '),
(323, 'al.bagshop', '????TAS IMPORT MURAH????', '????TAS IMPORT MURAH???? Order or Ask : ? LINE : monicalvianto ? BBM : 534DC5A5 ? Sms/WA : 0857.2722.2323 Trusted???? : #testi_albagshop www.facebook.com/al.bagshop'),
(324, 'meta_kei', 'Yurita Metariana', 'Yurita Metariana Ready stock slingbag ....our own product..feel free to contact 085742851200, pin bb 2a9651fd '),
(325, 'iklan_vintanabag', 'IKLAN VINTANA BAG', 'IKLAN VINTANA BAG Akun ini HANYA UNTUK PROMOSI, untuk INFO dan ORDER,  FOLLOW akun resmi @vintanabag @vintanabag  @vintanabag  FAST RESPONS ORDER KLIK LINK ?? line.me/ti/p/%40oyc7864m'),
(326, 'kytashop', 'Tas Batam Murah', 'Tas Batam Murah Real Picture & Ready Stock Batam JNE & BCA only Cek: #kyta_testimoni SERIUS ORDER!! ????BB: 553F14E8 ????WA: 083813976990 ????Line: @kytashop (pake @) atau line.me/ti/p/%40kytashop'),
(327, 'nanaayu_fashion', 'all about fashion', 'all about fashion Lokasi malang,indonesia Pusat aksesoris,baju,&tas murah Order via bbm  Pin bb: 7D351603 WA : 081231395563 Readystock Welcome reseller n dropship line.me/ti/p/%40jqc6201i'),
(328, 'republikdomba', 'PABRIK JAKET KULIT', 'PABRIK JAKET KULIT Pabriknya Kerajinan Kulit.! Siap produksi hingga ribuan pcs dgn desain, ukuran & brand sesuai permintaan. Pin bbm : 2A1E1A7F (sms/wa) : 085222864666 www.republikdomba.com'),
(329, 'charmshop99', 'charmshop99', 'charmshop99 BAG FASHION BATAM???????????? Ready Stock For Order & Availability stock(ftokan gmbr) Line       : huinaa_tan  BBM       : 58c76267 Happy Shopping ???? '),
(330, 'advoctostuff', 'AdvOctoStuff', 'AdvOctoStuff \"OCTOPUS BRAND\" STUFF ! Follow @OctoStuff, Totebag, Tshirt, WalletBag dll, LOW PRICE GOOD QUALITY Line ID: @LWN5048C '),
(331, 'larazpalamarta', 'Laraz Authentic Branded', 'Laraz Authentic Branded SURABAYA, INDONESIA For detail info, price BBM: 53C9570F / 583A9747 LINE: larazpalamarta WA: 0811340340 Happy shopping girls???????? NO HIT&RUN! www.facebook.com/LarazShop'),
(332, 'nathgallery', 'NATH GALLERY', 'NATH GALLERY For further info text me to :  Line : arsitaarianto SMS/WA : 081230394980 Pin BB : 7630d8ec NO BARTER ???? ORIGINAL GUARANTEE (SOLD DELETE) open ttpjual '),
(333, 'ariskadi_bags', 'ARISKADIBAGS | BRANDED BAG', 'ARISKADIBAGS | BRANDED BAG BANYAK PROMO (Beli min 3 harga khusus) ???? BCA only ???? BBM : 51952F48 (fix order) ???? Wa : 088215281651 (no call/sms) ???? ID LINE klik ????(katalog lgkap) line.me/ti/p/%40tot9378d'),
(334, 'prelovedstuff.jogja', 'PROMO TITIP JUAL 5K', 'PROMO TITIP JUAL 5K \"New And Preloved\" ???????????? ???????????? LINE: PrelovedStuffJogja WA: 085743855595 BBM™: 5AF0918F #TestiPrelovedStuffJogja . . Order?? Klik dibawah ini ???????????? line.me/ti/p/~prelovedstuffjogja'),
(335, 'widbeautypreloved', 'owner : @nonariia', 'owner : @nonariia Preloved but goodquality Kosmetik ready stok pesan langsung antar Notipu2, fastrespon, minat hub ????089666058819 Bbm : 52725E7D PONTIANAK '),
(336, 'bongkarlemarinta', '#EANF#', 'TRUSTED???? MILIK PRIBADI SERIOUS BUYER?? No HIT n RUN?? NO BARTER?? BCA???? Jakarta?????? @rintairtanto NEW n PRELOVED Line : rintairtanto  WA : 081280578889 '),
(337, 'next_yours', 'Pre-loved Stuff', 'Pre-loved Stuff ???? Jakarta ???? koleksi pribadi good condition 90% ? No refund. No barter. No keep. Pengiriman rabu&sabtu ?  087788735060 (Text & WA) '),
(338, 'mr_projects', 'The Second MARLYNROSE Projects', 'The Second MARLYNROSE Projects ???? MIN ORDER 50.000 ? SOLD = DELETE  ? Scrol sampe bawah  ???? No refund #testimrprojects ???? 089687122305 LINE ORDER KLIK ? line.me/ti/p/%40hsc0477x'),
(339, 'mamaboim', 'ayuHD', 'ayuHD ??cinta untuk Pratama hadi nugroho & azka syafiq ibrohim ???????? Selalu bahagia '),
(340, 'babypopcorns', 'New & Preloved Stuffs', 'New & Preloved Stuffs ????Jakarta ????BCA & BNI ???? 08988857588 (WA/SMS ONLY) ???? New&Preloved all woman stuffs ?? Booked = no cancel. No return. No hit&run Barter-NoRibet-SameRules.com'),
(341, 'thecloset.queen', 'THE CLOSET QUEEN', 'THE CLOSET QUEEN PRELOVED STUFF???????? ???? WA: 082115331996 ???? BCA only ???? Jakarta - BSD ?? NO BARTER ?? '),
(342, 'pat.hairstylist', 'Patrick Cervantes', 'Patrick Cervantes Salon Owner Salon IG: PatrickHenrySalon HTX TornadoRed Mk7 GTI Click below for fooood! youtu.be/0N7ZlhFA6iE'),
(343, 'babyfaye926', '#EANF#', 'Shanghai.New York ??????????. '),
(344, 'melodielamoureux', 'Mélodie L ????', 'Mélodie L ???? Mile-End (MTL) | lifestyle-lady | Journal de Montréal ???? [ melodie.lamoureux@quebecormedia.com ] '),
(345, 'a.passianoto', 'Anderson Passianoto', 'Anderson Passianoto Equilíbrio ???? '),
(346, 'splash_into_life', 'Stacie', 'Stacie My life on film! ???? Taking the paths less traveled and splashing in the puddles of life! ???? See the world thru my blue eyes! ???????? '),
(347, 'djdallasgreen', 'djdallasgreen', 'djdallasgreen Creator of @madefromscratchnyc / Eve Tour DJ / Gumball 3000 Tour DJ / NBA Resident DJ / Get Schooled Event DJ www.mixcrate.com/djdallasgreen/eats-beats-mix-1-10014742'),
(348, 'americanbars', 'AmericanBars.com', 'AmericanBars.com BAR & DRINKING FANS: Find bars, beers, liquors, cocktails, play a trivia game and more. APP is coming soon. BARS: Claim your bar profile, it\'s FREE! www.americanbars.com'),
(349, 'webbo91', 'Will  Webster', 'Will  Webster Chef at hipping hall  23yrs old leeds boy '),
(350, 'max_monde', 'MaxMonde', 'MaxMonde A Lifestyle Company www.MaxMonde.com'),
(351, 'montclairfoodie', 'Montclair Foodie', 'Montclair Foodie Love of Food, Wine, and everything in between in and around Montclair, NJ montclairfood.com'),
(352, 'gfccatering', 'GFCCatering', 'GFCCatering Down Home Food with an Upscale Touch. Ask how you may receive FREE crab cakes for your next event! 804-218-6146 804-971-3268 (mobile) www.gfccateringva.com'),
(353, 'bennybutch', 'Ben Butcher', 'Ben Butcher Something new for 2015 '),
(354, 'whiskyandalement', 'whiskyandalement', 'whiskyandalement Est. 2010. Whisky bar, malt bottle shop & School of Whisky open 7 days. 600+ whiskies & 9 passionate staff Everyone’s welcome at our little local bar! www.whiskyandale.com.au'),
(355, 'nelgarciav', 'Nelson García', 'Nelson García Proudly Venezuelan  Lawyer ????: nelgarv  New York, NY '),
(356, 'foodwinefun', 'Food.Wine.Fun...', 'Food.Wine.Fun... Amo comer bem, beber bons drinks e ser feliz! Compartilho... I love to eat good food, drink nice drinks and having fun! Sharing... All my pics! www.foodwinefun.net'),
(357, 'khanty.kyoto', 'Khanty Restaurant', 'Khanty Restaurant Premium Dinning Bar in the heart of Kyoto ???????????????? - Now Open! - ::::::::Email::::::::khanty.kyoto@gmail.com::::: '),
(358, 'jenkrulix', 'Jen', 'Jen Family time, vintage finds, wine, fashion, and anything that makes you laugh. On the side I\'m a Stella & Dot Independent Stylist www.stelladot.com/sites/jenkrulicki'),
(359, 'kmiloavc', 'Camilo Velasquez', 'Camilo Velasquez Lo que te cambia whatsapp.  3118486966 '),
(360, 'hautepinkpretty', 'Style Blogger - An Dyer', 'Style Blogger - An Dyer ????EMAIL: an@hautepinkpretty.com ????????SnapChat/????????Twitter @HautePinkPretty Based in ? Los Angeles, CA ???? ????Married to @SupermanDyer???? bit.ly/HAUTEPINKPRETTY'),
(361, 'andhitadyorita', 'BAGS, SHOES AND WATCH BRANDED', 'BAGS, SHOES AND WATCH BRANDED Arleta\'s Mom | Wife| Clinical Psychologist |  * FOTO PRODUK = ASLI  * 100% ORIGINAL SEMI PREMIUM * ADA HARGA ADA KUALITAS :)  * WA = 083867274662 '),
(362, 'nc_preloved', 'nasya\'s preloved stuff', 'nasya\'s preloved stuff Nasya\'s preloved stuff with a good condition :) Line : neysa_rei WA : 087520343626 Bandung '),
(363, 'qisthy_gallery', 'Fitriyani', 'Fitriyani MOKAMULA*MAKARA*TROJIKA* ZONANOVA dll... || TIRAI MAGNET More info:  ???? WA    : 087788280288 ???? Bbm  : 752028D0 Line         : fitriyani zakiyah '),
(364, 'shanum_totebag', 'PUSAT TOTEBAG NO 1', 'PUSAT TOTEBAG NO 1 Kota Tangerang Welcome Os instagram -Dropship -Reseller Kontak order : Line : shanum707 Wa.  : 0882-1294-0292 Bbm : 5A0BF32C Terimakasih '),
(365, 'rzvinandasyari', 'DIVINE.MELANGIT.ROK CELANA', 'DIVINE.MELANGIT.ROK CELANA FITTUEL.KHUMAIRA.TOTEBAG DAKWAH.dll WA/SMS: 085648209988 LINE: rozevinanda BBM: 56D9A3A9 SOLD=DELETED #testirzvinandasyari Surabaya.JawaTimur'),
(366, 'cherrypink_bags', 'TAS IMPORT MURAH SYEKALI!', 'TAS IMPORT MURAH SYEKALI! [TRUSTED] NO KEEP / FIX PRICE ???? BATAM Info / Order ????: BBM CS???? 54755EE9 (Atik) - (BB Group ON) SMS / WA : 0878-53373699 LINE ???? : cherrypink_bags ???? line.me/R/ti/p/~cherrypink_bags'),
(367, 'telagafashion', 'telaga fashion', 'telaga fashion suplayer tas import harga murah dan terjangkau '),
(368, 'boudoins', 'Sepatu Cewek READY STOCK', 'Sepatu Cewek READY STOCK TRUSTED SELLER ???? BCA ???? line : @olc4768c (pake \'@\') ???? WA : 08118819885 ?? Bandung Lets get sophisticated!!???????? Fast respon contact this link???? line.me/ti/p/@olc4768c'),
(369, 'lingshop_collection', 'Welcome^^', 'Welcome^^ ????Ready stock???? ????Affordable price ????Good Quality ????Trusted OS Pay Via BCA ? C.P: Line : @ooa5995b (@) Bbm : 599C8A59 Reseller ? WELCOME ^^ '),
(370, 'sepatucewe', 'Ade Nia Utami', 'Ade Nia Utami ??089602007398  ???? mandiri ????Lokasi Bekasi ???? Harga belum ongkir ????sms hanya untuk yg really keep ?? ongkir www.jne.co.id '),
(371, 'my_elbisiconcept', 'TOTEBAG TAS RANSEL DOMPET', 'TOTEBAG TAS RANSEL DOMPET Serius Order :  ???? Line : myelbisi2 ???? Wa & sms : 082218087001 ???? BBM : 58 514 620 / 2A 62 E9 E9 ???? BELUM ONGKIR  ???? JNE BANDUNG pengiriman.senin.sampai.jumat.tanggal.merah.libur.pengiriman'),
(372, 'surabaya.online.shop', 'Paket Tas Dompet Sepatu Jam', 'Paket Tas Dompet Sepatu Jam For Serious Buyer No Keep No Retured No Cancel Surabaya Free Ongkir BBM : 5AD781A0 Line  :sby.online.shop WA.   : 085704930493 Welcome Reseller '),
(373, 'bagsnesiaa', '???? READY BAGS JAKARTA ????', '???? READY BAGS JAKARTA ???? Order/Tanya ???? Pin : 58812120 ???? Wa : 087784446092 ???? Line : nesia22 ???? By JNE/Sicepat ???? by BCA & BNI ???? Jakarta FORMAT.ORDER.NAMA.ALAMAT.NO.TELP.ORDERAN.'),
(374, 'backpackbonjour', 'FOLLOW AJA DULU, TASNYA KEREN2', 'FOLLOW AJA DULU, TASNYA KEREN2 . ???? JNE ???? BCA ???? BANDUNG - ?NO RESELLER ????ALL REAL PIC ???? READY STOCK ???? WEEKEND = SLOW . ????WA: 0888-0921-0077 ????LINE: @JuraganTasLokal (pakai @) BACA.CAPTIONNYA.DULU.HARGA.DAN.DETAIL.TAS.ADA.DI.CAPTION.JANGAN.MALES.BACA.YA'),
(375, 'newbrandstore_id_stuff', 'FLASH SALE TODAY????', 'FLASH SALE TODAY???? READY STOCK????????? Follow: Newbrandstore_id Newbrandstore_id_bag Newbrandstore_id_watch CONTACT: ?WA: 0822-1327-3020 ????BBM: 57DFA5EC  ????LINE: KLIK???? line.me/ti/p/OMcHBSiPn_'),
(376, 'taashop15', 'MANGO,ZARA,CHARLES AND KEITH', 'MANGO,ZARA,CHARLES AND KEITH Real Pict-Min 2pc disc 10rb  LINE : taashop15  WA 087778384949. BBM : 51FAE3C9 NO TRF = NO ORDER Reseller-Dropship Welcome NOT REFUNDABLE Line@ ???????????? line.me/ti/p/%40qbs9244g'),
(377, 'ayo_laris', 'ayo_laris', 'ayo_laris  '),
(378, 'melodysproject', 'NEW & SECOND PRELOVED', 'NEW & SECOND PRELOVED Handmade Bagcharm TitipJual 5k/foto . ????Line : _gracemelody_ ???? WA  : 083820388527 . BNI - BDG . TESTI = #MP_testimoni #MP_bartertesti RESI = #MP_Resi '),
(379, 'girenshop', 'CEK #testimonialgirenshop', 'CEK #testimonialgirenshop For order LINE > JessicaDeviyani WA > 085691135095 ???? JAKARTA ???? JNE / GOJEK Paid Promote 20k '),
(380, 'thdvt', 'Devita,Th', 'Devita,Th ? Animal Rescue Support ? Vegetarian ???? Unpic Travel Fanatics ? Generation Y ???? HR, Freelance Designer ???? +62.singlefighter '),
(381, 'nn.stuff', 'TITIP JUAL (PRELOVED & NEW)', 'TITIP JUAL (PRELOVED & NEW) ????/WA: 081310030193 ????Line: nindddyL Payment ????mandiri & BCA #testinnstufff ????JAKARTA SCROLL SAMPAI BAWAH YA???? '),
(382, 'sepatuwanitaid', 'sepatuwanitaID', 'sepatuwanitaID Order & tanya2 silahkan ke link dibawah ya line.me/ti/p/%40xoy5906i'),
(383, 'sepatuimportjkt', 'toko online terpercaya', 'toko online terpercaya ONLY FOR SERIUS ORDER ADD CONTACK BB     :  54D6E26C LINE :   SEPATUIMPORT WA   :   081536130246 PAYMENT BCA MANDIRI BRI Koment di IG ga di bales '),
(384, 'rinshop76', '#EANF#', 'Supplier hijab, baliratih ???? ecer, grosir, seragam, reseller Payment via BCA ? COMMENT srng NGGAK kebaca ????WA:082217260617  line.me/ti/p/%40wxe9978o '),
(385, 'good_fashioon', 'Grosir & eceran GOOD FASHION', 'Grosir & eceran GOOD FASHION  '),
(386, 'shoescraft', 'SEPATU HANDMADE & IMPORT', 'SEPATU HANDMADE & IMPORT ???? Reseller Zone ???? Ready Stock. Real Photo. Sold ? Delete Order / Ask for serious buyer. Only BBM : 5132836A '),
(387, 'alkatiry_shop', 'alkatiry shop malang', 'alkatiry shop malang Fashion cewek •tas •baju •garskin •behel fashion dan permanen •kosmetik Order Pin: 57DB9278 Wa : 081357557105 Line: wuland_allkatiry Payment BRI&BNI '),
(388, 'rendigetshoes', 'rendi get bollemayeur', 'rendi get bollemayeur Ready & PreOrder ONLY ORIGINAL SHOES ? WHATSAPP / LINE / SMS: 081293368116 ? BBM: 26DE4442 ? OWNER: @Rendiget11 ? TANGERANG - SELATAN (INDO) '),
(389, 'anto_olshop', '#EANF#', 'SEPATU PRIA, WANITA, & ANAK... ¤¤¤ SILAKAN CEK HARGA ¤¤¤ INFO STOK ATAU ORDER: PIN BB: 7CA8581B WA: 081212067174 '),
(390, 'dkstore.id', 'ALL ABOUT SHOES', 'ALL ABOUT SHOES FREE ONGKIR JAWA(kec. KAB),BALI,JBODETABK LINE :dwikaandhika WA : 082234711675 BBM : 57AC765A JNE REG | MANDIRI ONLY  cek #testidk // #testidwikastore HANYA.MELAYANI.YANG.SABAR//SEMUA.HARGA.PASS//SABTU.MINGGU.CLOSE//SEMUA.BARANG.STOK.TERBATAS.//JANGANNNNNNNNN.SCROLL.SEMAKIN.BAWAH.KEMUNGKINANNNNN.SOLD.!!!!'),
(391, 'shoes.picious', 'SEPATU MURAH BANDUNG', 'SEPATU MURAH BANDUNG AKUN KEDUA DARI @GROSIR_M2M READY . SOLD = DELETE FORMAT ORDER ? 1.KIRIM FOTO ORDERAN 2.ALAMAT LENGKAP CHAT KE : LINE =  LADIMERAAA ???? = JNE BANDUNG SUDAH.HARGA.UNTUK.RESELLER'),
(392, 'santa_gav', 'SANTA_GAV', 'SANTA_GAV Fashion Shoes Replika ????Line : San2007 ???? WA : 083879735500 ????BBM : 56FF4453 ????Shipping By JNE ????Jakarta ????BCA, Mandiri, CIMB Weekend Slow Respon ???? www.shopious.com/shop/santa_gav'),
(393, 'dach_store', 'SEPATU VANS MURAH! BACA BIO!', 'SEPATU VANS MURAH! BACA BIO! FOLOWERS TWITTER & INSTAGRAM? DISCOUNT 10% ! ORDER VANS FREE POMADE JOIN GROUP: 570869E1 / NEW_DACHSTORE ORDERING: 51E5DA88 / 081586111941 LINE ???????????? line.me/ti/p/%40vvr8059d'),
(394, 'devan69store', 'NIKE CONVERSE VANS etc', 'NIKE CONVERSE VANS etc ????ALL READY STOCK???? ???? VIA BRI&BCA ???? JNE..sby-sda COD kurir ORDER???????? ????  PIN: 56B6EC52 ????  WA : 082132005288 LINE ~ dighastsurabaya LINE@app???????????? line.me/ti/p/%40uqe2325p'),
(395, 'anggi.shop01', 'Grand Opening - Harga Promo', 'Grand Opening - Harga Promo Trusted and Recomended Order hubungi di  BBM           : 521CFEAC WhatsApp : 0822-9821-4046 Hanya Untuk Yg Serius Order ! RESELLER WELCOME '),
(396, 'yasheilaa', 'YASHEILAA BAJU SEPATU ORIFLAME', 'YASHEILAA BAJU SEPATU ORIFLAME • BAJU  • SEPATU  Mau order? Hubungi ???? Bbm: 75F857D9 Line : sheilarifanzi Wa: 089634591730 SOLD=DELETE '),
(397, 'rumbangstore', 'Pantofel Boots Casual Sneaker', 'Pantofel Boots Casual Sneaker Buka Jam 10-16 Minggu TUTUP . SEPATU CEWEK @RumbangStore2 #testirumbangstore . Order : kirim gambar + size  LINE: @xkf5834h (fast respon) Bbm:53ce534d line.me/ti/p/%40xkf5834h'),
(398, 'homefootwear25', 'Pusat Sepatu Murah', 'Pusat Sepatu Murah •KUALITAS GRADE ORI/PREMIUM• MELAYANI DENGAN SABAR BBM: 29685A8E SMS: 081252170019 JNE/TIKI SOLD=DELETE HARGA BELUM TERMASUK ONGKIR '),
(399, 'neut_online_shop', 'neut_online_shop', 'neut_online_shop Sepatu kaos sweater dll Pin bb : 51E923C5 Line&WA : 085222483040 Bandung-Jawa Barat Pengiriman Via JNE Transfer Via BCA Fb : Tyara Nez Rahman Part II '),
(400, 'fact_shoes', 'Fact Sepatu Murah', 'Fact Sepatu Murah SEPATU MURAH Serious Only: ? WA: 085728812666 (no call) ? Line: factesoul ? Transfer BCA ? Bandung via JNE Partner @factesoul_store @fact_jaket.store '),
(401, 'vozonlineshop', 'RAGU? CEK: #voztestimo', 'RAGU? CEK: #voztestimo TRUSTED SUPLIER 1ST HAND???? BUTUH BANYAK RESELLER  !!!SKRNG PIN BARU : 57756B04 (Fast Respon)!!!! WA: 085945702667 (Fast Respon) JAKARTA-JNE INVITE.PIN.DARI.BIO.BUKAN.DARI.CAPTION.POSTINGAN.ITU.PIN.LAMA'),
(402, 'aluna_fashion', 'Aluna Fashion Shoes', 'Aluna Fashion Shoes Shoes below 100k IDR Free shipping area Bandung & Jabodetabek.  Standard size Indonesia Reseller are welcome. Phone : 0813-2295-9587 Bbm    : 5764A277 line.me/ti/p/%40gpt2500h'),
(403, 'bukasepatu', 'Suppliers Sepatu', 'Suppliers Sepatu ????BBM : 5525D5AB ????Line : @bukasepatu (pakai @ ) ????Wa   : 0811225626 ???? Location Bandung Order klik??? line.me/ti/p/%40lus9807w'),
(404, 'pray_shoes', 'GUDANG  SEPATU IMPORT TERMURAH', 'GUDANG  SEPATU IMPORT TERMURAH (OPEN RESELLER) FIX ORDER : ????PIN   : 55161159  ????LINE  : PRAY_SHOES (FAST RESPON)? ????WA    :081311226607 FROM JAKARTA SELATAN '),
(405, 'huruflepas_store', '????JUAL SEPATU JAKET FLANEL', '????JUAL SEPATU JAKET FLANEL Reseller Welcome ????Cuss order: ????BBM: 5A4EAC62 ????Id Line:hurufl ????Fb: Adriyan Lutfi ????GRATIS ONGKOS KIRIM P.JAWA???? ?KENDAL '),
(406, 'lintongshop', 'ROSHERUN, LUNAR SALE!', 'ROSHERUN, LUNAR SALE! Mengwi, Badung ? COD Puspem Badung/JNE ? Payment: BRI ? Tidak menerima tukar size ? Melayani yang sabar ? @storiousbag_bali ? Line ID: @flm9008z ? line.me/ti/p/%40flm9008z'),
(407, 'mustbuyfootwear', 'Mustbuy Footwear', 'Mustbuy Footwear All about original footwear!! Jakarta-Bekasi-Indonesia 0821-1009-1665 (whatsapp) 2AEB7A16 (BBM) Offifial line : @wgv1402f  mustbuy footwear (facebook) line.me/ti/p/%40wgv1402f'),
(408, 'brawijayasports', 'Baju Sepatu Tenis SURABAYA', 'Baju Sepatu Tenis SURABAYA ????BCA ??JNE-POS-LTH ????BBM:56E83502(FAST) ????WA:081216826898 ??LINE:alhumahera #BSSize (SIZE sepatu) #BSCaraUkur (SIZE kaki) #BSKTestimo (Bukti) '),
(409, 'probandup69', 'happy shopping', 'happy shopping Official akun @join_sepatu  Order via Line id: joinsepatu bbm: 5a3f6c56 Hp:089679136660 Trfr bri or bca Jakarta Jne & gojek Cek testi #join_sepatu '),
(410, 'cyber_footwear', 'Jual alas kaki murah', 'Jual alas kaki murah Jual sepatu Casual, boots, sport dll Info dan pemesanan: ????BBM: 571BAF21 ????LINE: Rfshoes ????Pengiriman dari bandung  Via JNE Transaksi BCA/BRI HARGA.BELUM.TERMASUK.ONGKIR.dan.SCROLL.KE.BAWAH.BARANG.READY.SEMUA'),
(411, 'queentsa_shop', 'Happy Shopping', 'Happy Shopping Queentsa Shop PIN BB : 57BDCA08  WA : 087887548338 Line : -semutz- '),
(412, 'prelovedlemarisda', 'Preloved and New ?', 'Preloved and New ? ???? owner ramah ???? sidoarjo - surabaya ???? wahana & JNE ???? line: shindywks Bonus cantik pembelian di atas 50 ribu. Open barter . Sold - delete '),
(413, 'jasshoppe', 'RUBI SHOES from Rp.75rb', 'RUBI SHOES from Rp.75rb READY & PO ????ORIGINAL ????WA: +60166304243 ????line: niaup  for kids follow ??minijasshop?? facebook.com/jasmine.enimsaj.315'),
(414, 'jualsepatutatasport', 'Olshop Sepatu Import', 'Olshop Sepatu Import Pengiriman dari jakarta. Kontak. Pin BBM : 54E1F822 Hp : 08568771036 Like Fanpage Facebook Kita : www.facebook.com/Juasepatufutsal'),
(415, 'fancy26shoes', 'NIKE.ADIDAS.CONVERSE.KEDS.VANS', 'NIKE.ADIDAS.CONVERSE.KEDS.VANS ????SNEAKERS HIGH QUALITY ????LOW PRICE ????BANDUNG ????BRI ONLY ????Ask or order? ????Line : amailiaputria // BBM: 7de20bce ????100% TRUSTED '),
(416, 'yotfootwear', 'YOT FOOTWEAR', 'YOT FOOTWEAR Pin   : 589E5A3B / 0896 8678 3740 Line : yayotzz Free ongkir se jabodetabek '),
(417, 'puyolars_shop', 'raisa6690 pevpearce dubsmashid', 'raisa6690 pevpearce dubsmashid Owned by @sindhyrosa  WELCOME RESELLER & DROPSHIP Harga LEBIH MURAH khusus Reseller&Dropship ???????? ? 085776944385 Pin: 516e0208 Line: _sindhyrosa DOCMART.JAMTANGAN.SWEATER.JAKET.TAS.SEPATU.ANEKA.BAJU.DAN.CELANA.????????'),
(418, 'inferno_inc', 'INFERNO_INC BANDUNG - Est 2014', 'INFERNO_INC BANDUNG - Est 2014 ???? BB    : 3285DFF0 ???? Line : INFERNO.INC ???? BCA / BRI ???? Shipping From Bandung ---?FIX ORDER FAST RESPOND ?--- ---????Click For Serious Order ????--- line.me/ti/p/Qfk9fBJZ2G'),
(419, 'rtrshopp', 'SEPATU LOKAL NO IMPORT', 'SEPATU LOKAL NO IMPORT ????Admin1: Wa:081910033868 Line : @SXD1108K (Pake @ ya) ????Admin2:  ?:089605900168 Pin:5367434D ????JNE&BCA only ?SENIN,KAMIS,SABTU #TESTIRTRSHOPP www.facebook.com/pages/RTR-Shop-Bandung/446058115470025'),
(420, 'jasa_iklan.3', 'jasa iklan termurah&trusted', 'jasa iklan termurah&trusted Daftar iklan  Line: punkpinkpop '),
(421, 'trulyconverse', 'CONVERSE ADDICTED', 'CONVERSE ADDICTED Lebih murah 70%? YAKIN ORIGINAL? CEK #trulyfaq  SURABAYA (NO COD)  BBM: 570F77E1 SMS: 083856655341 (no call)  LINE? Click Link belows ?? line.me/ti/p/%40qfc7063x'),
(422, 'banana.advert', 'sneaker store indonesia', 'sneaker store indonesia ???????? Contact  ???????? Cek Stock ???????? Testimonial Follow ???????? @sneaker.banana ???? via JNE ????bisa COD  Ini Akun Iklan Pertanyaan follow ???????? @sneaker.banana '),
(423, '3stripsaddict', '3StripsAddict', '3StripsAddict ALL ORIGINALS!!!!! WhatsApp : 082113133349 Line : 3StripsAddict BBM : 525071BB Cek Reputasi/Testi Follow @3StripsAddict '),
(424, 'vondshop', 'SNEAKERS dan CASE', 'SNEAKERS dan CASE Pemesanan/ Tanya tanya hubungi Line: denitask WA: 085782541653 '),
(425, 'giggleolshop', 'SEPATU MURAH,BAJU,KOSMETIK', 'SEPATU MURAH,BAJU,KOSMETIK ?Bbm : 5AD5B23D ?Line : doodoo94 ?Via BCA ?Trusted seller ¤Reseller very welcome¤ Ga ad harga khusus Shipping Bandung & JKT #testigiggle : testimoni '),
(426, 'hedisport', 'hedisport', 'hedisport OLSHOP BANDUNG MURAH  PIN BB : 545A363A LINE     : HEDIKUSDIANA  WA         : 089651753204 PENGIRIMAN VIA JNE '),
(427, 'tyany_shop', 'TOKO ONLINE PALING MURAH', 'TOKO ONLINE PALING MURAH Minat langsung add kontak kami: ????  via. JNE  ???? BCA | BNI  ????Fast respond : •WA       : 083813579842 •Line      : tyany_shop •PIN       :  ? 523f6d00 '),
(428, 'kumi_cumcum', 'Mamakumee_olshop||JAFRA', 'Mamakumee_olshop||JAFRA Mom&kids wear, order contact Line id: Kumee| BBM: 7F7F5A1D ????Shipping: Senin-Jumat & Sabtu*) from JAKARTA moms.sepatu.sandal.tas.blouse.gamis.rok.jilbab.lokal.impor.kids.pajamas.stelan.kaos.sepatu.sandal.rok.pants.lokal.impor'),
(429, 'urbabyneeds', 'BABY & KIDS STUFFS MURAH', 'BABY & KIDS STUFFS MURAH ???? Ship from BOGOR Order: ????LINE@ : @michnyel (pakai @ ya) ????WA: 0896.3803.2179 ????BBM: 2a1c341c Atau bisa langsung klik link???????????? line.me/ti/p/%40michnyel'),
(430, 'gregbebeshop', 'Baju anak paket 100rb', 'Baju anak paket 100rb line : pakeini.com..Sms/wa: 0896.3756.3432 '),
(431, 'ichsan_alshop', 'ichsan arifagama', 'ichsan arifagama Sepatu murah tapi Berkualitas,Bisa Request. #READY BBM :2AB28B42 WA :0822 4240 7633 Based in SOLO indonesia JNE/TIKI (open order everyday/READY STOCK) gerobaksepatu.blogspot.com'),
(432, 'arloji_sepatuku', 'Vans Converse Kickers', 'Vans Converse Kickers FIRST PAY FIRST GET. No keep. ???? WA : 085648245470 - no call ???? Line : @uwl4646s (pakai @) ???? Bbm : 220601e0 ???? JNE - SURABAYA '),
(433, 'fashion_yuli', 'GIRLS SHOP', 'GIRLS SHOP FASHION YULIATUN KARIMAH ????Jakarta ?BBM 5A447989 ?LINE yuliatun27 ?WA 0857-7601-5427 ????BCA ???? JNE '),
(434, 'mahkota99.store', 'PALING MURAH DAN TERPERCAYA', 'PALING MURAH DAN TERPERCAYA BUTUH BANYAK RESELER GROSIR.ECER.DROPSHIP #CONTACK ME:???????? ????BB: 5401414D ????LINE: mahkota99.store ????JNE.TIKI.POS JAKARTA  ????LINE bisa klik ???????? line.me/ti/p/7MaEYLewhQ'),
(435, 'bandunghandcraftshoes', 'sepatu handmade bandung', 'sepatu handmade bandung pin bbm  576AE91C / sms : 08156151656 '),
(436, 'gilanggunasaputra', 'Gilang gunasaputra', 'Gilang gunasaputra BANDUNG. ID  custom shoes men and girl contact line : gilangguna_s bbm : 56C9CEA2 wa : 083821555096 ????gilanggunasaputra1@gmail.com '),
(437, 'erik_pranajaya', 'pranajaya23', 'pranajaya23 Bbm: 59410430 Line: pranajaya.23 Cp: 089631009528 Transfer via BCA, BRI, BNI pengiriman via JNE No ressi h+1 setelah transfer '),
(438, '1204shop', 'Pakaian, Tas, Sepatu', 'Pakaian, Tas, Sepatu BBM: 7D73B9DD 5415F6F0 LINE: sen_0412 psenjaku SMS: 081254447040 (NoCall) SILAHKAN HUBUNGI KONTAK YG TERTERA (Fast Respon) OPEN.RESELLER.AND.DROPSHIPPER.'),
(439, 'caccell_collection', 'caccel collection', 'caccel collection Toko online  sepatu jakarta  Bbm: 5A363AAD Line : caccel Heppy shopink '),
(440, 'shoppingarcade', 'Supplier Sepatu Termurah', 'Supplier Sepatu Termurah SEMUA REAL PICT ???? iMessage/WA : 08567588883 ???? LINE : @cta8744c (pakai @) ???? BB Pin : 7913C2EB ???? FROM JKT ???? TESTI CEK CAPTION/FOTO KAMI.SAMAKAN.HARGANYA.JIKA.ADA.YG.LEBIH.MURAH'),
(441, 'mynew_storeofficial', 'dagelan bikinrame pathindo', 'dagelan bikinrame pathindo Tnyakan dulu stoknya Open order senin-sabtu 10.00-17.00 BBM: 75E45CD2 LINE: mynew_store Utk testimoni cek fav. ditwitter kita/search #TestiMynew_store twitter.com/mynew_store'),
(442, 'converse100rb', 'DAGELAN RAISA6690 PEVPEARCE', 'DAGELAN RAISA6690 PEVPEARCE SUPPLIER CONVERSE 100RB!!! KATALOG LENGKAP FOLLOW INSTAGRAM ?? @republiksneakers LINE ID: nikeepr  WA : 085775487355 PIN BB : 545D42D7 JAKARTA-BCA. '),
(443, 'akangical', 'NIKE ADIDAS VANS Magnum.Shop', 'NIKE ADIDAS VANS Magnum.Shop RESELLER WELCOME All about fashion available in here LINE :akangical  PIN:5a702314/32976576 (debby) WA:08813502330  Melayani COD sby,Luar Kota Via JNE '),
(444, 'promote_shop_id', 'Jasa Promosi Online Shop', 'Jasa Promosi Online Shop Trusted!  1 bulan 75rb (5 foto / hari)  Promo inquiry ke line id: @promoolshop  (pake @) atau click link bio ???????????? line.me/ti/p/%40promoolshop'),
(445, 'baby_alshop', 'baby al shop', 'baby al shop ?Baby AL shop? BBM 57F48338 WA 087822534726 LINE anindyasukma #WEEKEND SLOW RESPON# Resi H+3 SERIOUS BUYER ???? WA / LINE ???? '),
(446, '_agasta', 'AGASTA', 'AGASTA ???? JAKARTA WAS: AGASTBABY ???? JNE.TIKI WA/iMessge: 087770206642 Line: tissasas ?? SENIN-RABU-JUMAT '),
(447, 'liziesale', 'Sepatu Heels Prewalker Baby', 'Sepatu Heels Prewalker Baby Uk. Sepatu 0-18bulan Cek stock : @LizieFashion  Line : LizieFashion02 WA : 0896-5016-9056 Pin: 7FA3EA51 JNE (JKT) Untuk order add link dibawah line.me/ti/p/%40lbh7236i'),
(448, 'candra_leathershop', 'candra leather shop', 'candra leather shop List order silahkan hubungi ???? Wa : 085353683568 BBM : 56F439FA \"Cintai dan Pakai Produk dalam Negri\" candraleathershop.wordpress.com'),
(449, 'zainist_sneaker', 'Zainist Store', 'Zainist Store ORIGINAL ONLY: Asics, Onitsuka Tiger, Adidas, Gola, Munich, etc Pin BB: 7F88A6A8 Call/Sms/Whatsapp: 081249417440 LINE: zainist.com NO RETURN & REFUND www.zainist.com'),
(450, 'nerostore', 'sepatu murah', 'sepatu murah REAL PICT! ???? BBM : 5872902C ???? Whatsapp : 0896-4637-9668 ???? Line : @PUY7457C (pakai @ ya) ???? Payment BCA only line.me/ti/p/%40puy7457c'),
(451, 'ckrshoes', 'Custom Kick Rules', 'Custom Kick Rules ORIGINAL HANDMADE SHOES MADE BY ORDER WORLD WIDE SHIPPING Wa : +62 856 20 15156 PIN BB : 285AF6A5 EMAIL : customkickrules@yahoo.com www.handmadeckrshoes.blogspot.com'),
(452, 'braderlook.id', 'Braderlook.id', 'Braderlook.id 100% Authentic & Original NO FAKE Whatsapp 08122167342  Or +60105489411 (fast response) Email braderlook.id@gmail.com '),
(453, 'mahesta_shop', 'herlambang', 'herlambang ORIGINAL SHOES RECOMENDED...!!! PIN BB: 526C5244 WA/CP: 085776371381 Shipment by JNE/TIKI NO BOOKED \"Siafa Cefat Dia Dafat\" NO COD \"Cek track record\" '),
(454, 'oboes_shoes46', 'SEPATU: SPORT / BOOTS / CASUAL', 'SEPATU: SPORT / BOOTS / CASUAL BBM : 32396B0E  WA : 089650589464 LINE : oboes46 - Payment : BCA  - Pengiriman : JNE - TESTIMONI ( #OBOESTESTIMONI ) '),
(455, 'sensofstyle', 'FIRSTHAND SUPPLIER SPORT SHOES', 'FIRSTHAND SUPPLIER SPORT SHOES -SENS.OF.STYLE- ????Online Shop Jakarta???? ?RESELLER WELCOME? Pemesanan: ????Bb : 53A3DE9B / 527F2926 ????Whatsapp : 081283212306 ????Line : ???????? LINE ???????? line.me/ti/p/HZ2ljzdRdw'),
(456, 'lyzellestuff', 'HARGA DIBAWAH 100RB ?', 'HARGA DIBAWAH 100RB ? ???? heels, wedges, flatshoes. JAKARTA - replika kualitas lokal ???? #testilyzellestuff - ???? BBM : 5722D93C ???? WA : 082182888979 ???? LINE OFFICIAL ???? line.me/ti/p/%40dgt9495i'),
(457, 'anneta_ma', 'ORNELLA & SHOE UNLIMITED', 'ORNELLA & SHOE UNLIMITED Based in Bandung,ID Line : Anneta_MA Bbm : 549C362F Wa : 08112269191 No Exchange & Refund Reseller Welcome Also available at Tokopedia & Buka Lapak '),
(458, 'shoebaglicious', 'SEPATU MURAH UNDER 100K', 'SEPATU MURAH UNDER 100K ????LINE : @msf0169h (pakai @ yah) ????BBM 5852001D ????082291193932 ????JNE (H+2 setelah trf) ????TRUSTED.OLSHOP???? Line (fast respon) klik ???????????? line.me/ti/p/%40msf0169h'),
(459, 'erola_shop', '#EANF#', 'OLSHOP MURAH!!! new but TRUSTED ???? ???? bbm 5840A0D5 ? pengiriman via JNE  ???? Indonesia ???? pembayaran via Cimb Niaga HAPPY SHOPPING???????????????? '),
(460, 'variousshop29', 'Various Shop', 'Various Shop Shoes and Bags : Nike Crocs Fitflop Kipling Phone and WA 089632378588 (sms only) BBM 2B5F9379  Line @ygp9999j Shipment Jne Melayani Grosir & eceran '),
(461, 'jual_peninggi_langsing_dll8', 'Dr. Konsultan Herbal:', 'Dr. Konsultan Herbal: • Peninggi Badan • Pelangsing • Penggemuk • Masker jerawat • Pemutih • Dll PIN: 59A3C190 WA:08972693021 LINE: @QHF9997U More info klik link di bawah. line.me/ti/p/%40qhf9997u'),
(462, 'emonshoes_id', 'PROMO HARGA RESELLER', 'PROMO HARGA RESELLER ????Jakarta For order you can chat me:  ????Bbm : 5A8A0951 ????ID LINE: emonshoes_id ????WA : 085719117976 ????Pengiriman via:  jne  ????????BRI,BCA,BNI '),
(463, 'buyorbye', 'COTTON PANTS  PALEMBANG', 'COTTON PANTS  PALEMBANG ????PALEMBANG???? FAST RESPON  ????PIN :59787957 ????TEXT:081958622800 CELANA ANTI BEGAH CELANA ANTI KETAT TIDAK BERKEPENTINGAN LANGSUNG DELCON YAH..SORRY ???????? MAU.TANYA.LANGSUNG.TOTHEPOINT.MAU.ORDER.BIKIN.FORMAT.COM'),
(464, 'serlykf', 'Serly', 'Serly Shs 4 probolinggo 16 y.o '),
(465, 'caesarningtyashop', 'Jam Tangan n Sepatu Murah', 'Jam Tangan n Sepatu Murah FAST RESPON Mandiri only!  Semua disini ORI dan GRADE ORI(PREMIUM) Wa : 082331573763 Bbm : 579E7EF2 Harga blm termasuk ongkir Line : klik ???? line.me/ti/p/%40wug6683m'),
(466, 'javathreesport', 'javathreesport', 'javathreesport Barang tidak ori silahkan dikembalikan dan uang anda kembali 100% bbm :53f8451 Wa :089505713531 Line : line.me/ti/p/0QLazAqAQy '),
(467, 'iklan_sepatu01', 'iklan_sepatu', 'iklan_sepatu Quetion and order bbm 5A4E3710 wa : 082350719417 Line @zat669v3 (pake @) Ig galery : doeland_footwear Kami ada karena cinta indonesia line.me/ti/p/%40zat6693v'),
(468, 'rasheda_store', 'Rasheda Store', 'Rasheda Store  m.facebook.com/rashedastore'),
(469, 'vandani_shop', 'GROSIR SEPATU BANDUNG', 'GROSIR SEPATU BANDUNG \"moofeat, bradleys, prodigo, mr.joe, dr.paris, kickers, toods\" ? For order: Whatsapp: 081930200555 (fast) Line (fast respon)? line.me/ti/p/%40dov3223h'),
(470, 'shoppyroom', 'PRELOVED BRANDED & GARAGE SALE', 'PRELOVED BRANDED & GARAGE SALE ????????????????????????? ????via JNE-POS WA : 081296126309 ?SOLD - Hapus? Jakarta '),
(471, 'white_hanger', 'White Hanger', 'White Hanger New n preloved with good condition and quality ???? Bandung ???? BCA ???? trusted ? No barter ???? Line: chatrinesetiawan ???? WA: 0878 21 878 858 '),
(472, 'secondprelovedsby', 'Owner : @Ririsoctavia', 'Owner : @Ririsoctavia ????SOLD = DELETE Surabaya / JNE ? NO VIA PULSA ? HIT AND RUN = BLACKLIST ? NETT ? BRI ? TIDAK ADA GRUP BBM order ?? Bbm : 53E00CAB Line : RirisOctavia NO.NEGO.NO.FREE.ONGKIR.ONLY.BRI.KIRIM.HANYA.PAKAI.JASA.JNE.MINAT.LANGSUNG.CAPTURE.LALU.CONTACT.OWNER'),
(473, 'preloved.rash', 'bismillah aja ^^', 'bismillah aja ^^ Line : in_rash  WA : 0852.1661.9663 Jual santai jadi santai aja :p COD sesuaikan owner ya ^^ yogyakarta.indonesia'),
(474, 'twinnistuff', 'Jogja Preloved Stuff', 'Jogja Preloved Stuff Second and new stuff Good condition Trusted No hit and run Based in Yogyakarta For order chat on, Whatsapp : 085727187188 Line : ammaliasekar BNI only '),
(475, 'gelukig', 'GELUKIG \"everything lucky\"', 'GELUKIG \"everything lucky\" Order? LINE : @hmb7204f (pake @ nya yah) ????FOLLOW? GET SURPRISE ! ????READY STOCK ????PRICE IDR 28-125k ????BDO // BCA ????0838-2652-7110 www.tokopedia.com/gelukig'),
(476, 'owlashopsemarang', 'owlashopsemarang', 'owlashopsemarang Preloved and New (baca caption) Fix sah hanya via : Line @hellositaa (fix) WA / MESSAGE 089602828246 SOLD  = DELETE '),
(477, 'putritiwipm', 'Putri Tiwi', 'Putri Tiwi ????: putritiwii '),
(478, 'devpreloved', 'Mom & Baby Preloved Bandung', 'Mom & Baby Preloved Bandung Jual Santai.. Minat? Tanya2 hubungi kontak Line id : @ famelala Wa : 085770002212 '),
(479, 'nextshop2', 'NEXT SHOP 2', 'NEXT SHOP 2 Sandal - sepatu - tas Part of @next.shop Order/Ask : WA / SMS : 0857-2364-5161 BBM :  5A2482B7 Line : nextshop2 Happy shopping all ???????????? '),
(480, 'detashoe', 'SEPATU dan TAS MURMER T.O.P????????', 'SEPATU dan TAS MURMER T.O.P???????? ????Pin BB: 5AF67EC5 ????WA/SMS:085726119444 ????Add url line ???? ????  http://line.me/ti/p/%40crz1365a ????BANDUNG ????WELCOME RESELLER ???? '),
(481, 'aznie_shoppaholic', '????????????????????????? Based On Bandung', '????????????????????????? Based On Bandung ???? ORDER Send Pict VIA : LINE   : aznie_shoppaholic ?WA   : 089616702926 ?BBM :  5834B1B6 Testimoni cek #testiaznieshop Seriously Buyer Only '),
(482, 'olshop.makassar', 'FASHION ALA SELEBGRAM????', 'FASHION ALA SELEBGRAM???? ???? BBM : 57ECD053 ???? Free ongkir Makassar ???? Member diskon 7rb perpcs ???? Beli 2pcs diskon 10rb ???? Makassar '),
(483, 'paidpromote.005', 'JASAPROMOTE JASAIKLAN', 'JASAPROMOTE JASAIKLAN AKUN KE 005 Line : trustedosindo 30rb : 10 hari (1 hari 10x) 55rb : 30 hari (1 hari 10x) 95rb : 60 hari (1 hari 10x) '),
(484, 'queenof_shopping', 'SUPPLIER SEPATU DAN BAJU MURAH', 'SUPPLIER SEPATU DAN BAJU MURAH SCROLL DOWN!!! ;) (order atau nanya2 silahkan hub. kontak dibawah ini) LINE : belladahlan BBM : 526C35DE SMS : 089613902838 Ready & PO (min 3 hari) Menjual.BAJU.SEPATU.TAS.AKSESORIS'),
(485, 'kamiliashoesandbags', 'SEPATU DOCMART ???? & SLINGBAG ????', 'SEPATU DOCMART ???? & SLINGBAG ???? ???? NEW ACCOUNT ???? HIGH QUALITY & TRUSTED! ???? BBM: 595E58C4 ???? Whatsapp: 083807081833 ???? LINE: kiranaastari Shipping from Bandung ???? READY STOCK '),
(486, 'arestakidsshoes', 'Trusted since 2010', 'Trusted since 2010 Tangerang???? JNE???? Wa: 0818134181, 081311008083. Bbm: 5996F9AF Line: arestakidsshoes Check testi?? #testiarestakidsshoes www.arestakidsshoes.com'),
(487, 'jeryan_store', 'JERYANSTORE SEPATUMURAH', 'JERYANSTORE SEPATUMURAH PUSAT SEPATU TERMURAH WA 089691936515 PIN 51B916F6 / 2AEDEA3F LINE @jeryanstore Testimoni #testi_jeryanstore Welcome Reseller & Dropship FAST RESPON???? line.me/ti/p/TevD0wu5sG'),
(488, 'dvfashiongirl', '#EANF#', '\"CHOOSE YOUR STYLE\" . ORDER: Line: devaniariesthas bbm: 56B9555E . TESTI: Check #DVFGirlTesti '),
(489, 'njemode', 'SHOES N BAGS', 'SHOES N BAGS Hastag Tas di #tasnjemode Aksesoris di @njemode2 Ready.bisaGOJEK Order di salah satu  Line: nandaje / Bbm: 5A389DC9 (group) Testi???? #ndtst13 '),
(490, 'jualan_sepatuonline', 'Jual Sepatu Online', 'Jual Sepatu Online ????serius order?hubung???? ????cs LINE: wayudi16 ????cs BBM: 7e60264d ????Cs WA:085609759421 Jakarta -order barang???? Resseler,dropship,Eceran,Grosir. '),
(491, 'promosi_sepatubagus', '#EANF#', 'NB: INI AKUN CUMA UNTUK PROMOSI SAJA YAH GAN SIS '),
(492, 'dinalia_store', ' TOKO SEPATU MURAH ', '\" TOKO SEPATU MURAH \" SHIPPER SETIAP HARI, Via JNE Minat, add and chat via ? ?BBM:  7D4FF2F7 ?LINE: DINALIA_STORE ?W/A : 081212792323 ?RESELLER WELLCOME? '),
(493, 'ls_shoes', 'SNEAKERS MURAH', 'SNEAKERS MURAH ????RESELLER and DROPSHIP???? ????Open: Senin-Minggu ????Pin Bb : 5478D419 ????Line : muchaevan ????WA: +6282218537775 ?FAST RESPON ????JNE ????BRI '),
(494, '2ff_shoes', '#EANF#', 'Pin 5A1592F0 More info invite pin '),
(495, 'importstuff_indo', 'Import Stuff Indonesia', 'Import Stuff Indonesia Contact Person:  Line: (@OYY4916O) Kik: importstuff_indo  Email: importstuff_indo@yahoo.com Check dis out?? #testiimportstuffindo TRUSTED SELLER???????????? line.me/ti/p/%40oyy4916o'),
(496, 'zaininovit', '#znstore', '#znstore 087821441030 sms/wa 57F62008 bbm Visit our facebook fanpage for more collection: facebook.com/znstore.apparel'),
(497, 'iklandaldal', 'Iklan Daldal Shoes', 'Iklan Daldal Shoes Akun promosi @daldalshoes Silahkan follow @daldalshoes untuk katalog lengkap '),
(498, 'mimimbem', 'Mbem Onlineshop', 'Mbem Onlineshop ???? BBM : 20EC61E6 ???? Line  : mbemme ???? : Transfer BCA ONLY ???? : Bless every inch my day JESUS Love eat and sneakers that much '),
(499, 'acm_sport', 'PROMO HARGA RESELLER', 'PROMO HARGA RESELLER For order you can chat me: ????BBM: 51746766 ????WA: +6285239113690 Pengiriman via JNE ????BNI&BRI '),
(500, 'anggitagigit', 'AnggitaKidsWear', 'AnggitaKidsWear SMS & WA 081284088439 LINE @anggitagigit PIN 555403A5 BNI & BCA Bandung ');
INSERT INTO `instagram` (`id`, `username`, `first_name`, `bio`) VALUES
(501, 'malangolshop', 'malangolshop', 'malangolshop @malangolshop   JASA Iklan GRATIS  #malangolshop #olshopmalang Kumpulan olshop di kota malang Malangolshop '),
(502, 'mayoutfit_shoes', 'SHOES FIRSTHAND SUPPLIER', 'SHOES FIRSTHAND SUPPLIER WELCOME RESELLEER JNE BANDUNG Testi scoll ke bawah yah FIX ORDER? PIN : 5AE6F6CE LINE : mayoutfit_shoes/klik???????? line.me/ti/p/%40fkl4459o'),
(503, 'pesona_herbal_alami_murah4', 'pesona herbal PENGGEMUK MURAH', 'pesona herbal PENGGEMUK MURAH ????PENINGGI ????PELANGSING ????PENGGEMUK ????PEMUTIH ????MASKER Jakarta? ????BBM : 5855BA99 ????SMS/WA: +62822 1014 3752 ????WeChat: pesona_herbal ?LINE KLIK ? ? line.me/ti/p/4aCp2ZC81a'),
(504, 'bibiercollection', 'Bibier Collection ?', 'Bibier Collection ? Pin BB : 28f5ced8 WA/Line : 089630848780 Store :  Mall Season City GF2/A08/03 \"KEEP NO CANCEL\" '),
(505, 'haniza_hani', 'Haniza Hani', 'Haniza Hani badan sihat, hati memikat, cantik di mata biar sampai jatuh ke hati www.umiadam.com'),
(506, '_llshopiklan', '#EANF#', 'Line : @DAO9228J (pakai @ ya) Wa : 083867300543 (no sms or call) Atau klik link di bawah ini ???? line.me/ti/p/%40dao9228j'),
(507, 'jualtienstermurah', 'Jual Tiens Termurah', 'Jual Tiens Termurah Jual Tiens Peninggi Badan, Jual Tiens Lampung, Jual Tiens Murah, Jual Tiens Kaskus, Jual Tiens Jakarta, Jual Tiens Malang, Jual Tiens Zinc , '),
(508, 'dekna_clothing', 'Dekna clothing', 'Dekna clothing baju kaos distro premium berkualitas tinggi,bahan cotton combed 24-30s, nyaman digunakan, adem. @75.000 (belum ongkir) 085279762030 & BBM   59963A91 '),
(509, 'peninggi.badan.tiens', 'JUAL PENINGGI BADAN TIENS', 'JUAL PENINGGI BADAN TIENS Cara Menambah Tinggi Badan, Cara Menambah Tinggi Badan Secara Cepat, Peninggi Badan Herbal Alami, Pelangsing Herbal 089665206002 PIN:5A4D04E2 posturideal.blogspot.com'),
(510, 'dehanshop', 'JAM TANGAN MURAH?? & Ima Scraf', 'JAM TANGAN MURAH?? & Ima Scraf TRUSTED OLSHOP!  Ask/order? ????LINE: hannamardiana ?? WA: 085711518152 ????BEKASI Real Pict! Harga belum termasuk ongkir. Dropship '),
(511, 'satubajucom', 'SatuBaju.com', 'SatuBaju.com SatuBaju.com menyediakan pilihan kaos dengan ribuan desain unik dan trendi. SMS Only: (0878) 257 86604, TELP: (022) 520 7737. Email: cs@satubaju.com www.satubaju.com'),
(512, 'pandasleepycollection', 'Panda Sleepy Collection', 'Panda Sleepy Collection Trusted olshop since 2011 Reseller welcome ???? Bbm : 57CF117B Fast respon klik ???????????? line.me/ti/p/%40dqa8647s'),
(513, 'bajuanakkita', 'Baju Anak', 'Baju Anak  www.bajuanakkita.com'),
(514, 'jualpeninggitiens', 'JUAL PENINGGI TIENS HERBAL', 'JUAL PENINGGI TIENS HERBAL Jual Peninggi Badan Tiens, Penggemuk Badan Yang Aman, Penggemuk Badan Tiens, Pelangsing Perut, Pelangsing Herbal 089665206002 PIN:5A4D04E2 posturideal.blogspot.com'),
(515, 'demarians_shoppe', 'De\'Marians Shoppe', 'De\'Marians Shoppe SSM No: SA0350376-M Trusted Seller Wholesale & Retail Pakaian Wanita,Lelaki & Kanak2. ORDER MELALUI FACEBOOK KAMI Email bonitachikas@gmail.com facebook.com/demarianshoppe'),
(516, 'nayla.closet', 'NaylaCloset palembang', 'NaylaCloset palembang ???? order  ????Pin : 59DA81C8 ????Wa : 089678178336 (no call) ???? transfer = keep Free ongkir palembang  / cod owner yang tentuin  ???? cod sabtu / minggu ada.kualitas.ada.harga'),
(517, 'kiosjegeur69', 'Ivan L-sHop #kiosJEGEUR BDG?', 'Ivan L-sHop #kiosJEGEUR BDG? REAL ACCOUNT    JOIN NOW????  Pin BBM : 581BAFF6  LINE : ivanLshop  WA/? : 08975501935 ???? BCA, BRI, CIMB, Mandiri ???? reseller.welcome.buruan.order.sebelum.keabisan.barangnya'),
(518, 'kir_da', 'KIRDA Syar\'i By PURI', 'KIRDA Syar\'i By PURI ???? Open Reseller ???? WA : 085221711212 ???? BBM : 55F0B7F2 ???? Line : mykirda ???? Produksi sendiri '),
(519, 'srihardinajamil', 'Sri Hardina Jamil', 'Sri Hardina Jamil Odin\'c shop Serius order add line @srihardina22 Pin 54601447 Welcome Reseller ({}) Harga barang belum trmaksud ongkir JNE '),
(520, 'theparadise_olshop', '@TheParadise_Olshop', '@TheParadise_Olshop \" SURGANYA BERBELANJA ONLINE???????? \" ???? Palembang ????WA : 08990037355 (???? call) ????5509C817  ?Happy shopping guys ???????????????????????????????????? '),
(521, 'new_stary', 'SUPPLIER BAJU BANGKOK READY', 'SUPPLIER BAJU BANGKOK READY STARY SHOP READY STOCK order.sfs???? ???? sms/WA: 085641551323 ???? line: @PZR1641P ? pin bb: by request www.facebook.com/new.stary line.me/ti/p/%40pzr1641p'),
(522, 'cherryfransiska', 'CHERRY COLLECTION', 'CHERRY COLLECTION Minat PM d line/WA/Pin\"(coment d IG no respon)   READY N PO (ada keterangannya) Line: cherry_fransiskaaa. WA : 08118110512 Pin : 53e149a6 BCA/MANDIRI '),
(523, 'ur_shoppinghouse', 'HANDBAG MANGO MURAH', 'HANDBAG MANGO MURAH ????BAWAL KANGGAROO & BAWAL CHIFFON ???? ????DRESS ????????SHOES ????w/s 0105470270 ???? X no cod  ????Pos everyday #urshoppinghousefeedback '),
(524, 'syauqina_muslimah', 'longdress#busui#sar\'i#hijab', 'longdress#busui#sar\'i#hijab *Akhwat only* Tias St Jkt - Indonesia Untuk pemesanan Bbm : 5A4A491D Wa  : 08118110985  Please no booked no cancel Yuk order mom sebelum kehabisan '),
(525, 'rna.skate', 'Skate Apparel Bandung', 'Skate Apparel Bandung Contact Person For OnlineOrder ???? Line ID : RNA.SKATE ???? BBM : 5969942F ???? Phone : 088218140736 Transfer Via ????BCA & ????BRI ???? Grosir & Eceran '),
(526, 'mameecollection', 'PRELOVED - LIPSTIKARAB(NEW)', 'PRELOVED - LIPSTIKARAB(NEW) Second / Preloved Lipstik Hare/Arab NEW  JOGJA SCROLL KEBAWAH TERUS YAA WA : 08983467224 Line : syamurti_r SOLD=DELETE '),
(527, 'herbalife_murah_asli', 'Herbalife', 'Herbalife Akun ini hanya buat promosi Akun utama silahkan add @CiaOlShop99 Line: CiaOlShop99 line.me/ti/p/pYlA0yEafg'),
(528, 'ferry_koreanfashion', 'Ferry Korean Fashion Shop', 'Ferry Korean Fashion Shop Korean Fashion For Men High Quality Fabric With Reasonable Prices T R U S T E D Line  : ferrysugiantofeto BBM : 59F68640 Reseller Welcome ! '),
(529, 'daylinecollections', 'daylinecollections', 'daylinecollections ????Jakarta Ready stock baju import Low price with premium quality  BCA only Order : BBM : 58100E2C Line : @ttj0697f (pakai @) Add official line? line.me/ti/p/%40ttj0697f'),
(530, 'jessie.wardrobe', 'PO • Ready Bangkok Supplier', 'PO • Ready Bangkok Supplier Est. 2013 • Jakarta •     LineID: @jessie.wardrobe (with \'@\')     BB: 76675552/ WA: 081210532777 #jessiewardrobetesti ???????? ???? ORDER ?? KLIK ???? line.me/ti/p/%40jessie.wardrobe'),
(531, 'nancyshop20', 'Olshop termurah mulai 35k', 'Olshop termurah mulai 35k ???? Pin BB 1: 26EBDED8 		BB 2: 5819626F  Line 1: dewipurnama20 Line 2: sintaa05 WA: 081299151354 Shipping BY JNE Location Jakarta   #testinancyshop20 facebook.com/uniquenecklace'),
(532, 'al_collection', 'A*L olshop', 'A*L olshop Pakaian, tas, sepatu& Aksesoris lainnya Order&tanya pm: Pin: 54C439F8 WA/sms: 088210317128 Supplier Jakarta Pengiriman dalam (via jne) dan LN '),
(533, 'promote_laceandfleece', 'promote_laceandfleece', 'promote_laceandfleece HANYA UNTUK PROMOSI UNTUK TANYA2 DAN LIHAT DETAIL SILAHKAN  follow @laceandfleece for high quality clothes & affordable price! '),
(534, 'popytshop', 'READY STOCK & REAL PIC', 'READY STOCK & REAL PIC ???? Jl. Terusan Danau Maninjau Barat 2 B3-A1, Sawojajar Malang ???? @testipopyt Whatsapp : +6282233940677 Ofc LINE : @popytshop (pakai @) Or klik ?? line.me/ti/p/%40popytshop'),
(535, 'hilshoppp', 'Zara, Mango, Hnm Dan Etc', 'Zara, Mango, Hnm Dan Etc Price and stock langsung kontak bio || line : hilwah_s '),
(536, 'iklan.murah.olshop', '#EANF#', 'Bagi yg di follow, mhn follbacknya y! NO FOLLBACK ???? UNFOLLOW! buat olshop yg mau iklan tga plh ribu/bln bc postingan! '),
(537, 'produsentascw1', '#EANF#', 'Bag | Organizer | Wallet | Souvenir Supplier  Fanpage: fb.com/carolinewongbagstore Wa : 08122681595 Pin BBM: by request www.produsentascw.com'),
(538, 'dearsonshop', 'De\'Ars Onshop', 'De\'Ars Onshop ¤ All Items are READY STOCK Check fb for more pictures & details yaa... Fb : Dears House / Dears Onshop II Faster response ----> Line : dearsonshop '),
(539, 'athoelqibtia', 'Athoel Qibtia', 'Athoel Qibtia QueenaraMom???? love my litlle family???? Bussines consultan Oriflame Owner Queenara shop???????????? Sms/wa : 087773924908 Bbm : 7cf8d22a Line : athoelqibtia '),
(540, 'celmimo_garagesale', 'garage sale NEW & PRELOVED', 'garage sale NEW & PRELOVED Line : Celmimo WA : 0838.777.999.44 (dita) ???? Jakarta comment di foto jarang dijawab Klik & baca link dibawah ini? instagram.com/p/51oLJ6pxcv/'),
(541, 'awaniel_boutique', 'Awaniel Boutique 0194203407', 'Awaniel Boutique 0194203407 ???? MUMMY DADDY AND BABY SHOP ???? ???? ROLLING DUIT KASEH SAYANG ???? m.facebook.com/amanileboutique'),
(542, 'nuyshop24', 'Sale Baju-Baju Murah', 'Sale Baju-Baju Murah Minat langsung add (pilih salah satu) : ???? LINE : nuyshop24 ???? WA     : 0857 1561 3130 (no call/sms) ???? JAKARTA ????KEEP = NO CANCEL???? '),
(543, 'sriwedari_olshop', 'Catokan Kosmetik - SFS Min 9K', 'Catokan Kosmetik - SFS Min 9K ???? BEAUTY STORE ????  ???? 081511196677 (WA) ???? Line @aif8277i (pakai @) ???? BCA/ MANDIRI  ???? JNE/ Tiki (Setiap hari) ???? Ready Jakarta Klik????????untuk ORDER line.me/ti/p/%40aif8277i'),
(544, 'shabilla_babykidsshop', 'BabyShop\'Moment\'FruitCarica', 'BabyShop\'Moment\'FruitCarica ????Order via: ????Bbm: 7F462848 (group) ????Sms/WA:081228821717 ????Line:shinta_sputri No booked whithout payment???? TT: Rek BRI???? ????HAPPY SHOPPING MOM???? '),
(545, 'girly.accs', 'SUPPLIER BAJU LOKAL READY', 'SUPPLIER BAJU LOKAL READY ?Budayakan membaca caption dl? ???? JNE : RABU & JUMAT ???? Based : Jakarta ???? Rek BCA only  ???? WA : 087823728828 ? NO GRUP  ?LINE FAST RESPON? line.me/ti/p/%40wdf3055b'),
(546, 'marketingid', '#EANF#', 'Promote_gudangpakaian @gudangpakaian '),
(547, 'girlssshoping', 'BajuTasSepatuMurmer', 'BajuTasSepatuMurmer ready stock!scroll down smpe bawah????order? langsung contact admin????line: intaanpr_ ????bbm: 7EB8FC6C ????Jakarta Selatan ?sold=delete?? '),
(548, 'basic_project', 'NEW COLLECTION', 'NEW COLLECTION For shoes @bpshoes.id | WA 085776627744 | Line @hpx1613g (pake @) atau • LINE, Klik Link di bawah ini ???? line.me/ti/p/%40hpx1613g'),
(549, 'balqis_hijabb', 'BALQIS_HIJAB', 'BALQIS_HIJAB ????gamis set bergo/jilbab/khimar syar\'i???? ????????WA 087722070740/bb 513F361B LINE????balqis_hijab READY STOCK Delete=sold ????SHIPPING WORLWIDE ????MANDIRI&BCA '),
(550, 'debeadscollection', 'debeadscollection', 'debeadscollection Tempahan jahit manik & pukal Kelas jahit manik Jual manik & tudung  Whatsapp: Ayu: 0172836592 Shop: A-28, 1st floor Brother\'s Mall  Wholesale City USJ www.debeadscollection.com'),
(551, 'safira_house', 'FLH MLB ORIBY MB maCHI AYN', 'FLH MLB ORIBY MB maCHI AYN For ask or order ????Pin 59DA753B  ????WA 08891781137  ????Pengiriman dari solo ????BRI only  ?Tiki/jne/pos '),
(552, 'n_nmshop', 'N&Mshop', 'N&Mshop ????Solo-Indonesia OPEN Senin-Jumat 09.00-17.30 Sabtu 09.00-14.00 ????Mandiri KEEP=TRANSFER ????Info&Order (CP) BBM:755A1CAE LINE: nnmshop line.me/ti/p/k9gX_hKym3'),
(553, 'butikmeletop', 'Butik Meletop A&A', 'Butik Meletop A&A Online Purchase Only (10am-11pm) *Butik & Kosmetik* ????????????????????????????????? ****TRUST SELLER**** Postage Isnin ~ Jumaat ???? Whatsapp???? +6012-3761137 '),
(554, 'namirazkashop', 'COAT BOOTS KOREA IMPORT', 'COAT BOOTS KOREA IMPORT WA : 081296331920  LINE : Nadiasalbilaa BBM : 53F8E845 (slow ) HIT & RUN = BLACK LIST MORE COLLECTION? @Namirazkafashion COAT READY ? PM?? '),
(555, 'kaoscouplejogja', 'jaket couple terlengkap', 'jaket couple terlengkap INFO ORDER / RESELLER SmS / WA - 085.6596.88880 / 08571.44.77888 Line(pake @) :    @KaosCouple BBM - request www.bajucoupleonline.com'),
(556, 'seasonzzshopist', 'TasImporSepatuRentalSewaGaunPe', 'TasImporSepatuRentalSewaGaunPe seasonzzshopist.blogspot.com sewagaunjualgaunpesta.blogspot.com SMS/WA: 085765201685-7D43F3D1/23ba8b4b Wechat/line/twitter/IG/FB PAGE: seasonzzshopist www.facebook.com/seasonzzshop'),
(557, 'butikbajunamy', 'Kurung.Jubah.BjMelayu.Kurta', 'Kurung.Jubah.BjMelayu.Kurta Butik Online. Readystock. Kain berkualiti. Jahitan kemas.No COD. No Return. Wajib ukur sblm pilih saiz. Friendly & Trusted Seller. '),
(558, 'anyafashionshop', 'anyafashionshop', 'anyafashionshop Baju fashion murah Fix Order / no php : Wa : 0896 1764 9594 Bbm : 58501AD2 ????Transfer BCA Jelly shoes????IG @anyabungsushop www.instagram.com/anyabungsushop/'),
(559, 'clanarokshop', 'Clanarok Shop', 'Clanarok Shop ???? WELCOME RESELLER ???? Contacts: Id Line: linastone WA: 085763326661 ???? feel free to chat us instagram.com/linayanti90'),
(560, 'promo_rekomendasi', 'Follow @firsthandindonesia', 'Follow @firsthandindonesia ????????????Harga Cuci Gudang kualitas import ??? FREE ONGKIR ???????????? PROMO BELI 2 GRATIS 1 Klik buat dapet Harga Sadis ???????????? line.me/ti/p/@xvf3486g'),
(561, 'ninetis_', 'knitwear45rb', 'knitwear45rb Upload everyday ORDER/ASK:  ???? LINE ? tarilang 1st pay 1st get JNE KUDUS ????Jl lambao 5 kudus Testi? ? @ninetesti sory.we.are.NO.COD.temporary.thanks'),
(562, 'lintangmomsneed', 'BABY SHOP TERLARIS !!!', 'BABY SHOP TERLARIS !!! ? ?Wa 087854101060 ? ?PIN Bb 5963dcae ? ?Line id lintangmomsneed ????GROSIR follow @lintangmomsneedgrosir moms-need.blogspot.com'),
(563, 'iklan_olshopmuu', 'iklan_olshopmuu • RJ014', 'iklan_olshopmuu • RJ014 ????Trusted JasProm since 2012???? Testimoni ???????? @testimoniiklanolshopmu PRICELIST @PHK47274L(PAKAI @) ????Member @komunitasjaspro???? Biar cepet add???????? line.me/ti/p/@phk4742l'),
(564, 'visionkidzstore', '#EANF#', 'MAINAN BABY STUFF BAJU ANAK Since 2007       BBM 794E09F9 LINE visionkidz WA 088213457542 Pengiriman Senin - Rabu - Jumat dari Jakarta '),
(565, 'papimami_aqiqahmedan', 'PapiMami Aqiqah Medan', 'PapiMami Aqiqah Medan Dekorasi aqiqah khusus medan & jakarta. SMS: 082166576530 PIN BB: 2833AB47 LINE: mirakojino www.facebook.com/babypapimami'),
(566, 'iklan_belanja2', 'IKLAN PROMOTE SFS ENDORSE', 'IKLAN PROMOTE SFS ENDORSE ????TERMURAH !! ????SUPER HI-QUALITY ????PROFESIONAL ????REAL FOLLOWERS ????SINCE JAN 2013^^ Testi???? #iklan_belanja2 . ????RECOMMENDED OLSHOP . Line us !  ???????????? line.me/ti/p/%40vsv1594c'),
(567, 'aslambabyshop', 'aslambabyshop grosir retail', 'aslambabyshop grosir retail Menjual pakaian bayi dan anak2... ????Tangerang ????Wa 0823-1115-6556 ????Bbm 5765A686 Tokopedia : aslambajuanak Fp : aslambabyshop Bukalapak : aslambabysho '),
(568, 'grosirkaoscouple_id', 'GROSIR BAJU COUPLE MURAH KAOS', 'GROSIR BAJU COUPLE MURAH KAOS Jakarta - READY STOCK Pusat Grosir Baju Couple Terbesar di Indonesia-Termurah-Terpercaya. ORDER & INFO RESELLER (bisa DROPSHIP). SMS/WA +6289630300215 www.grosirkaoscouple.net'),
(569, 'mayqueenstore', 'OBRAL Start 30rb! ????', 'OBRAL Start 30rb! ???? FIRSTHAND JAKARTA Line: embemmaya WA: 08998089193 Shipping JNE Jakarta (H+1 setelah transfer, Senin - Jumat) SOLD = DELETE NO RETURN LANGSUNG.DAPET.HARGA.RESELLER'),
(570, 'grosirbajucouple_id', 'GROSIR BAJU COUPLE MURAH KAOS', 'GROSIR BAJU COUPLE MURAH KAOS Jakarta - READY STOCK Pusat Grosir Baju Couple Terbesar di Indonesia-Termurah-Terpercaya. ORDER & INFO RESELLER (bisa DROPSHIP). SMS/WA +628990765765 www.grosirbajucouple.com'),
(571, 'u.store_korea', 'Baju korea', 'Baju korea ? JAKARTA ? SHIPPING JNE ? BCA BBM   : 59d03c88 WA/SMS : 082213469875 LINE : @uqt1927p ? CLICK TO ADD OFFICIAL LINE line.me/ti/p/%40uqt1927p'),
(572, 'bajucouple', 'kaoscouple', 'kaoscouple Utk pemesanan / info reseler : SMS/WA :  0857.234.77888 085.6596.88880 LINE(pakai @):     @KaosCouple Pin BB : by Request www.bajucoupleonline.com'),
(573, 'distrocowok_stcoll', 'Baju, Celana, Tas, Aksesoris', 'Baju, Celana, Tas, Aksesoris ????For Ask & Order, please add: ???? Pin: 7402e87c ???? Line :Sthaibin /082149869744 ? Pengiriman: Via JNE ???? Stok: Jakarta, Bandung '),
(574, 'jizzystore', 'JIZZY T-SHIRT BAND COLLECTION', 'JIZZY T-SHIRT BAND COLLECTION LINE :  @ttv6662b (jangan lupa pake @ ya) BBM : 53A5ED40 JNE : Monday - Friday ???? ???? BNI #jizzystore READY STOCK! '),
(575, 'iklan_ifstore79', '#EANF#', '????JANGAN FOLLOW AKUN INI????  Akun ini khusus iklan @ifstore79  ????langsung cek dan follow IG???? @ifstore79  ????Atau hub ????line: firsanb '),
(576, 'boyshop_ragil', 'SPAMLIKE FREE T-SHIRT ????', 'SPAMLIKE FREE T-SHIRT ???? -  ?Line : sakti09 ????BBM : 51C90BFA (Fast respon) ? WhatsApp : 089675050263 - ????SOLD=DELETE - ????SPECIAL PRICE FOR MEMBER - ????OPEN TIAP HARI ????TRUSTED JAKARTA.JNE.MANDIRI.'),
(577, 'iklan_menslovefashion', 'Iklan_menslovefashion', 'Iklan_menslovefashion IG ini utk iklan, utk liat display & pemesanan,  Follow : @MENSLOVEFASHION utk account jualan bajucowok IG.INI.HANYA.UNTUK.IKLAN.SAJA'),
(578, 'teenlit_outfit', 'Your destination outfit ????????', 'Your destination outfit ???????? Material: Cotton tissue, Cotton Slub, Spandex korea ???? RESELLER WELCOME ???? ADMIN: VITA ? Order ???? Line: vrisdiana ???? whatssap: 0822.3341.5229 ????: Bca '),
(579, 'pixie_butik', 'Pixie Butik', 'Pixie Butik Ready stock Lokasi Semarang,bs kirim seluruh Indonesia Line @pixiebycynthia (pakai @ ya) WA +6282329405051 www.tokopedia.com/pixiebycynthia line.me/ti/p/%40pixiebycynthia'),
(580, 'naomypinkstore', 'ORIGINAL BRANDED TERLENGKAP', 'ORIGINAL BRANDED TERLENGKAP ????line : @naomypink (pake@) fast respon ????wa : 08112119333 ????Bbm : 599EE636 baju anak : #NPSFORKIDS SALE : #NPSONSALE Wajib klik very fast respon???????? line.me/ti/p/%40naomypink'),
(581, 'styleloftos', 'PREMIUM QUALITY, LOCAL PRICE', 'PREMIUM QUALITY, LOCAL PRICE ????  JAKARTA ???? BCA / MANDIRI / CIMB ???? JNE (RABU,SABTU) ????FIRST PAY, FIRST GET? ????TRUSTED ONLINE SHOP ????WEEKEND SLOW RESPON CLICK TO ORDER ???? line.me/ti/p/%40but6942i'),
(582, 'postingiklan', 'PROMOTE INSTAGRAM ONLINESHOP', 'PROMOTE INSTAGRAM ONLINESHOP ????LINE : @postingiklan (pakai @ yaa) ????WA : +6285772993768 Twitter: @postingiklan . ?OPERASIONAL : senin-sabtu 08.00 s/d 22.00 Official LINE (y) (y) line.me/ti/p/%40postingiklan'),
(583, 'fabulous_brandedtee', 'Ready To Wear', 'Ready To Wear ???? Jakarta ???? Size Baby- XXXL All Ready Stock ????LINE: ivanaoktaviani (fast respon) ????WA : 087739398887 ????Pelayanan 24 jam NON STOP???? '),
(584, 'bebekpromote', 'BAJU IMPORT TUMBLR', 'BAJU IMPORT TUMBLR Open promote murah Elegant.Simple.Trendy'),
(585, 'neilysroomid', 'Neily\'s Fashion Room', 'Neily\'s Fashion Room FASHION IMPORT HIGH QUALITY ???? Jakarta ???? BCA ONLY ????Line ID : utianandita ???? BBM :  518D3DDF ???? WA : by request ????MONDAY-SATURDAY / 09.00-17.00 READY.STOCK.NO.PO./LINE.FOR.FAST.RESPONSE'),
(586, 'otshop_promote', 'PROMOTE OTSHOP', 'PROMOTE OTSHOP AKUN INI HANYA UNTUK PROMOTE ADD AKUN LGSG: @OTSHOP . ALL ITEMS 40.000!! SUPPLIER RAJUT TERMURAH! . MAU 1JT SETIAP HARI? ADD ???? line.me/ti/p/@dvx1680y'),
(587, '21olshop_banjarmasin', 'BANJARMASIN ONLINE SHOP', 'BANJARMASIN ONLINE SHOP All stuff???????????????????? ????Line:alifa_azzahra(fast respon) ????Pin:553DA0FC Happy shopping????????????SERIOUS ORDER HUB BIO KOMEN IG JARANG KEBALES NO.HIT.AND.RUN.CANCEL.BLACKLIST.SABAR'),
(588, 'feedeeshop2', 'ig ke 2 dr feedeeshop', 'ig ke 2 dr feedeeshop •sms 085624805714/wa 081214836053 •pin bb: 2607ec7b/line:fitriadiantari •#testifeedeeshop2 •ready stock •domisili bandung •since 2011 '),
(589, 'halo.ramaniya', 'PASMINA MONOCHROME', 'PASMINA MONOCHROME ? BUKAN UNTUK DIFOLLOW . BELANJA ECER, GROSIR JILBAB, GAMIS BUSUI, MUKENA FOLLOW ? @RAMANIYAS . WA: 085725225860 BBM: 58484483 instagram.com/ramaniyas'),
(590, 'bajuhargamurahjkt', 'Bajuhargamurah ???? ????', 'Bajuhargamurah ???? ???? ????Place order: SMS 081316529398 BBM 26AA3175 ????NO Keep ???? Payment via BCA & BRI ???? Shipping JNE hari Senin-Sabtu, H+1 setelah transfer ???? @testibhmjkt '),
(591, 'hijaab.belle', '????????Hijaab Belle Collection', '????????Hijaab Belle Collection ?Biospray Member. (MSI0353163) ????Line: Meliana590 (Fast Respon) ????Sms/WA: 089601915104/085778422068 ????Pin bb ask me! ???? Jakarta/Bandung TRUSTED. '),
(592, 'mujigaefashion', 'Supplier Baju Murah', 'Supplier Baju Murah ????BB: 580A3896 ????LINE: @int2133u (pakai @ ya) ????Whatsapp: 087781369393 ????Lokasi: BOGOR -BRG YG DIBELI TDK BS DIKEMBALIKAN -KRM SELASA&JUMAT -RESI H+1 '),
(593, 'pernikibunda', 'Pernik Ibunda  #REOS', 'Pernik Ibunda  #REOS #Agen Cutetrik Ohbaby ? LaBella ????BBM: 59BB26B9 ????WA/SMS: 0857 175 57435 ???? LINE: @itsna_pernikibunda FP:  Galeri Labella Cutetrik Agen Bogor www.facebook.com/labellacutetrikjakarta'),
(594, 'davielkids', 'DavieLkids shop', 'DavieLkids shop Jual kaos anak branded bermacam-macam merk, bahan cotton.. Harga murah , terjangkau .....???????? ???? wa/sms : 08568803130 ???? BNI ???? bbm : 54D1FF10 ???? JNE '),
(595, 'kiranachampakidswear', 'kirana', 'kirana (minimal pembelian 15pcs, korting 10-13rb/pcs) Pin BB 51aaf860 WA:+6289613172545 Line: @kiranachampa jakarta ready stock sold=delete '),
(596, 'iklan_ig_terpercaya4', '???? IKLAN INSTAGRAM RJ OO4 ????', '???? IKLAN INSTAGRAM RJ OO4 ???? ???? part of @komunitasjaspro???? ??REAL FOLLOWERS AND AKTIV?? ????MINAT? LINE : OWNER.IKLAN (pakai titik)???? ????IKLAN HARI SENIN-SABTU???? '),
(597, 'wennieandriani', 'Wennie Andriani', 'Wennie Andriani Rangga\'s wife???? Zia\'s mommy ???? Full of Love?? '),
(598, 'nuno_babykidy', 'Nuno_Babykidy', 'Nuno_Babykidy Ready stock real pict for order line: @nuno_babykidy (pake @) bbm: 578158DC WA: 085719522295 BEKASI '),
(599, 'bajuwanita.murah', 'Baju Wanita Murah (713_Shop)', 'Baju Wanita Murah (713_Shop) Contact us : ????Bbm :595D5071 ? wa/line :0857 1159 6964 ???? sms :088-210-139077 ????via JNE ???? transfer via Bank BCA ???? Shipping from Jakarta & Bandung www.facebook.com/titieolshop'),
(600, 'berzeliastuff', 'Jenna B Fashion Stuff', 'Jenna B Fashion Stuff ?A homemaker doing fashion project - mommylicious :D ?BBM 54DF41C7 ?Sms/Whatsapp:081317301723. ?Line : jennaberzelia ?Bls Comment klo sempet yaa ;) '),
(601, 'promotewinkyyshop2', 'promotewinkyyshop2', 'promotewinkyyshop2 PENGIRIMAN DR JKT Line 1 : winkyyyy ( Y nya 4x ) Line 2 : yennywinky WA : 087897592239 BBM : 7DB10A58 TESTI?#TESTIWINKYYSHOP UTK PROMOSI @WINKYYSHOP2 www.facebook.com/winkyyshop'),
(602, 'ahlamyshop', 'Ahlamy Shop', 'Ahlamy Shop ????All ready stock items ????ahlamy_shop@yahoo.com  ?? 016-2335906 / 03 9081 8550 ????Boutique Lot 288-A jln hulu langat infront (masjid Al Ehsan) '),
(603, 'kamar_perempuan', 'mediana', 'mediana Trusted online shop  Weekend off. ????BBM : 58166BB0 (angka 0) ????LINE : quen_pin_pin lemari_cwe = kamar_perempuan #testikamarperempuan ???? BANDUNG '),
(604, 'vanillas_stuff', 'Ur OL Hijab Stuff Dstination', 'Ur OL Hijab Stuff Dstination Jakarta WA : 087771690580  BBM : i_sukses IG Owner : wulan_suci Line : @vanillas_stuff (pake @ ) SOLD = DELETE ORDER KLIK LINK INI ???????? line.me/ti/p/%40vanillas_stuff'),
(605, 'zaracoutures', 'BAJU LAWA-LAWA ADA DI SINI ????????', 'BAJU LAWA-LAWA ADA DI SINI ???????? ????BISMILLAH???? ? NO CALL, WA  +60198325356 ONLY???? ???? REPLY WITHIN 24 HOURS???? ????9AM - 10PM???? ???? SERIOUS BUYER, ? FUSSY BUYER ???? nospam.nofussybuyer.pleasereadcaptionssayangsayang.co'),
(606, 'asyiqmuslimangah', 'AsyiQmuslim Angah', 'AsyiQmuslim Angah SYURGA PAKAIAN ANDA                      Any order: +60166264161 www.asyiqmuslim.com'),
(607, 'preloverstuff_elka', 'Preloverstuff', 'Preloverstuff Second stuff | Wa : 089677460881 | Line : indahelkarim | BSD city | max booked 1X24 jam | BRI  ? ig owner : indahelkarim Testi #testimonielkarimshop indahelkarim.wordpress.com'),
(608, 'secondfresh_lobi.lobi', 'Mirzaard', 'Mirzaard Whatsapp: 081226910188 Line id mirzaardiany '),
(609, 'onl.stuff', 'New or Secondstuff ????????????????', 'New or Secondstuff ???????????????? Secondstuff good condition ????????  ????Line : oqthavia09  ????WA : 081213974746  ???? BCA only  ????Jakarta, Indonesia TRUSTED ????  Titip Jual 5K for 3 item ???????????? '),
(610, 'garageware', 'Terima Titip Jual 10 rb', 'Terima Titip Jual 10 rb Line @FQS7088N (pakai @)  JNE Tangerang ship H+1 Resi H+5 BCA Mandiri NO BARTER/CANCEL/SPAM 1st pay Cicil OK Transaksi titip jual better use rekber line.me/ti/p/@fqs7088n'),
(611, 'nickendwaymadden', 'Guzel Niken', 'Guzel Niken Be who u wanna be @-}--  Student Twitter @nickendhapsa ;) https://m.facebook.com/nikenhapsariwaymadden '),
(612, 'niranuri14', 'Nira Indriyani Nuri', 'Nira Indriyani Nuri 23 y.o a wife to Lingga Ardhaniwinata.S ????Tangerang '),
(613, 'meiyinlee22', 'Grosir Baju& Sepatu Anakimport', 'Grosir Baju& Sepatu Anakimport 1stHand Supplier Import Kids Fashion Line: meiyinlee22 2B4E4190 WA 08158989333 2A3988DD WA 081285585355 5A364F29 WA 081285584855 www.facebook.com/meiyinlee22'),
(614, 'ichacaa', 'jennershop', 'jennershop For order or information contact us  Wa : 081223269991 Line : ichaca99 shipment _ senin, rabu, jumat Based ~ '),
(615, 'bos_alex', 'Alexandra', 'Alexandra BAJU DRESS TAS HARGA GROSIR BATAM Minat : Line: bos.alex Bbm : 5a7a2528 Contact saya hny yg saya cantumkan disini, selain itu buka sy  Keep no cancel www.facebook.com/BosAlexandra'),
(616, 'delianishop', 'DS-Collections', 'DS-Collections In assosiate with @delianishop_shoes_bags Import clothes, Branded Bag, Import Shoes For order : Line : lie_atmadja WA : 0818-822-805. Bb : 7F20ED5F. '),
(617, 'completeshope', 'kristina', 'kristina All new fashion import -Ready stock SMS/WA : 087881577774 Line. :completeshope PIN BB :5A4F1DD2 *LD = lebar dada '),
(618, 'twims.apparel', 'Twims Apparel', 'Twims Apparel Clothing line for kids from Bandung, Indonesia. '),
(619, 'btsshopping', 'BAJU & PASHMINA MURAH', 'BAJU & PASHMINA MURAH ????Transfer uang 2 x 24 jam ????BNI/BCA ????Jakarta ???? JNE Order? Add line kita??? line.me/ti/p/%40dba9848t'),
(620, 'iklan12_107', '#EANF#', 'Jasa Iklan Olshop???? - sudah lebih dari 20 akun @iklan12 - Berminat olshopny kita promote? ????Hubungi: Line: iklan_12 Bbm: 58695666 Terima Kasih '),
(621, 'lea_ichigo', 'Lea Ichigo Shop', 'Lea Ichigo Shop Ready stok Jakarta????????????  Order?  (For Fast Response)???? Line1: Lea_ichigo Pin BB: 54DF365F ????Testimoni @Leaichigoshop_testi www.facebook.com/lea.ichigo'),
(622, 'arizha_shop', 'arizha_shop', 'arizha_shop Serba serbi Fashion ????SOLO WA   : 08572812244 BBM : 51ED7AEA line  : arizha shop NO TRANFER=NO BOOKED NO COD <3 happy shoping <3 '),
(623, 'sendy_collection', 'SUPLIER TERMURAH BERKUALITAS', 'SUPLIER TERMURAH BERKUALITAS SEGALA ADA GOOD THINGS , GOOD QUALITY ,GOOD PRICE REAL PICT 80%-99%  ???? LINE : sendy_sensen ???? BBM : 7F39706B *BCA/BNI *JNE/POS TESTY #sendyshoptesty OPEN.ORDER.setiap.barang.ready.stock.stp.saat.dpt.berubah.chat.personal.untuk.tanya.stock'),
(624, 'ommistore', 'Ommi Ommi', 'Ommi Ommi Jual cepat berbagai macam benda berkualitas dengan harga miring. ? / whatsapp: 0857 6188 9051.  BBM: 599F0518 Happy shopping ???? '),
(625, 'amal.preloved', 'prelovedmurah', 'prelovedmurah ????Bandar Lampung ????BNI, BRI, Cimb Niaga ????089656740328 (sms/wa/line) ????BBM 586A3E36 ????NO HIT & RUN ????NO RETURN ? Barter max ongkir 20k ????Sold ? Delete '),
(626, 'prelovedprettychic', '#EANF#', 'CUCI GUDANG, MAU PINDAHAN ?TERMURAH DI IG, GOOD QUALITY ???? ????LINE : cynthia_jasson ????BCA & JNE only ????Tangerang ?BARTER ?RETUR ?REFUND TRUSTED.NO.TIPU.TIPU'),
(627, 'io_uniqshop', 'Uniqshop', 'Uniqshop MOM&BABY???????? WA  : 087870006081 Line : irenaoct SALE TIME!  #uniqshopgaragesale  No transfer=No keep Xoxo???? '),
(628, 'fhbyilikhijab', 'Fitriyaniilik', 'Fitriyaniilik  '),
(629, 'cocolovebutik', 'Termurah di IG | FIRSTHAND ????????', 'Termurah di IG | FIRSTHAND ???????? ???? sms: 081288647895 ???? bbm: 52822D09 SOLD ? delete ????% Real followers! Quick link order ? line.me/ti/p/%40nsa1956n'),
(630, 'vinsaelwholesale', '#EANF#', 'Supplier tangan pertama baju fashion wanita  ?Reseller ?Dropship dari jakarta ?Testimonial #testivinsaelwholesale  ?bbm : 238A558A ?sms : 081314458225 www.facebook.com/vinsaelwholesale'),
(631, 'funkyfyzadotcom', '????MOMS, BABY & KIDS STUFFS????', '????MOMS, BABY & KIDS STUFFS???? ????SSM REGISTERED SELLER???? ??Open: Everyday 24/7 ????Postage: Weekdays ??WhatsApp: 011-12373942 ????funkyfyzadotcom@gmail.com ????Swap Review: 2Hrs/unlimited m.facebook.com/funkyfyzadotcom'),
(632, 'mdlicious_', 'by @adeqseha_', 'by @adeqseha_ • Shawl | Bawal | Innersnowcap • WhatsApp +6017.2807217 • TAG & WIN Contest Every Month • ALL ITEMS AVAILABLE '),
(633, 'ana.qaseh_collections', 'Ana Qaseh', 'Ana Qaseh Whatsapp? 0184749455 Wechat      ? 0194811902 ? Rohana77 Telegram   ? 0184749455 Fb Page  Ana Qaseh ? Ana Qaseh Collections '),
(634, 'terataksongket', 'Pn Siti', 'Pn Siti Menyediakan Sampin Songket dan Set Tanjak dengan perkhidmatan di hujung jari. ? READY STOCK ? FAST DELIVERY To order Whatsapp  ????+60129589864 facebook.com/terataksongket'),
(635, 'loneranger.se', 'Outfit Of The Day ????°', 'Outfit Of The Day ????° ???? Bismillaahir Rahmaanir Rahiim ???? ???? LOOK SIMPLE BUT GORGEOUS ???? 017.341.8375 ( WA Only ???? ) ???? Serious Buyer '),
(636, 'kainpasangajjar', 'contact lens and dullsatin', 'contact lens and dullsatin Pos Contact lens : weekdays Dull satin / cotton : monday je X COD X BACKOUT + FUSSY BUYER . Whatsapp no +60146771725 #ajjarsfeedback '),
(637, 'avantgarde_apparels', 'Nik Muhammad Asri / AVANTGARDE', 'Nik Muhammad Asri / AVANTGARDE FOUNDER of AVANTGARDE ENT. HARGA BAJU TERBAIK!! Clothing Supply & Distribution, Shirt Printing, Heat Press, Graphic Designing. ?? 014 5357469 www.facebook.com/avantgardeprint'),
(638, 'sha_sha_style', 'Pintudung,brooch,pinshawl', 'Pintudung,brooch,pinshawl ????Work hard????have fun?no drama????? ???? WhatsApp / telegram???? 011-12118312????  ???? Wechat ???? shaafif???? ???? kluang,johor ???????????????? '),
(639, 'dz_garcinia_official', 'garcinia murah!!', 'garcinia murah!! > stokis larome _glutacaps_azanis???????? > ???? SSM (AS036729-P) > ????Kesan seawal 3 HARI > ????Harga murah dari lain :) >?? sms/whatapp 0193544763 '),
(640, 'qisnaraa_fashionstyle', 'TEKAN \'MORE\' UTK FULL CAPTION', 'TEKAN \'MORE\' UTK FULL CAPTION ??????? Ready stock  ???? Serious Buyer? Jom WhatsApp  ??????? No Calls Allowed ???? Baca Caption Plsss ??????? Buy More pay less ???? Happy Shopping '),
(641, 'tyqa_olshop', 'Hijab Fashion', 'Hijab Fashion Order - Nama: - Almt Lgkp: - NoPe: - Kode: - Warna: - Pb: ???? 55502F8C ???? 085855956426 ???? Via Mandiri Booked no Cancel '),
(642, 'dress_codeid', 'dresscode', 'dresscode Import Apparel-Slingbag-Accessories Location: Medan BB: 5AFFE487 (fast respon) Line: jenvilim No DP = No Keep Payment via BCA/ Mandiri '),
(643, 'f.wishlist', 'PO & Ready Stock', 'PO & Ready Stock ???? MEDAN Reseller get special price ???? Order / more info : line : angelliachen bbm : 7CD88E22 line : @YQL6987Z (pakai @ atau klik link di bawah ini) line.me/ti/p/%40yql6987z'),
(644, 'allshopssbkk.clothline', 'FIRSTHAND HQ READYSTOCK IMPORT', 'FIRSTHAND HQ READYSTOCK IMPORT ????TERLENGKAP&TERMURAHH 100%IMPORT ????READYSTOCK ???????? TESTIMONI:#allshopsstesti  Sms/WA :085381880209 LINE: allshopssbkk BBM: 54925709 ADD OUR @ LINE ???? line.me/ti/p/%40yee0948a//DROPSHIP.SATUAN.GROSIR//HARGA.TERTERA.BLOM.DISC//WEEKEND.SLOW.RESPON'),
(645, 'little_redribbon', 'Handmade with love ?', 'Handmade with love ? ???? ASK,ORDER,REQUEST PLEASE : Sms/whatsapp : 0818175411 Line id : erikadwi Jakarta *Indonesia only ????FB fanpage : www.facebook.com/madebylittleredribbon'),
(646, 'fml_collection', 'fml_collection', 'fml_collection ????% TRUSTED REAL FOLLOWERS ???? ~???????? ? Jakarta ???? ~ Mandiri & BRI syariah ??SOLD->Delete ???????? Happy Shopping Dear ???????????? '),
(647, 'princess_prettii', 'Jual baju anak & kostum', 'Jual baju anak & kostum Ready stock ???? Jambi, ID Order/tanya2 Line: princess_prettii WA : 0821.8408.9789 Open : Mon-Sat 09.00-20.00 Masukan/kritik/complain BBM: 537F2E35 '),
(648, 'bustiershop', 'BUSTIER SHOP', 'BUSTIER SHOP BUSTIER TULANG 4,8,MUSLIM,SATIN S M XL JUMBO, 32+colours READY STOCK & PO LINE: tryast BBM:  57DD2EE5 WA/SMS : 0857 7172 7969 JKT-TRUSTED SINCE 2011 dalaman.kebaya.kaftan.bajumuslim.blazer.MEGA.BEST.SELLER.100RB3PCS'),
(649, 'yuliantina_aminy', 'yuliantina aminy (Wolshop)', 'yuliantina aminy (Wolshop) Suplier 1hand???????????????? Reseller n dropship welcome Harga grosir tanpa minimal order JNE Mandiri n BRI payments ????PIN 1 5A832D70 ????PIN 2 29377F69 ???????????? www.facebook.com/yuliantinayusuf'),
(650, 'ratushop0312', 'SALE!!!!! diskon 50%', 'SALE!!!!! diskon 50% Surabaya. Banyak SALE!!!  Olshop Toserba!!! Semua ada disini!!!  Fanspage fb: Ratushop0312 Bbm :2ab6cc50, wa:085648677082 id line : ratushop0312 '),
(651, 'monicalimshop', 'BAJU &TAS IMPORT MURAH-1stHand', 'BAJU &TAS IMPORT MURAH-1stHand ????READY STOCK???? ????BATAM GROSIR BAJU IMPORT - MURAH RESELLER WELCOME ????% TRUSTED SELLER FOR ORDER        ?????? ????? ? Line : monica_lim95 ????Bbm: 5726C44D ALL.ITEM.READY.STOCK.JUAL.SKINCARE.DAN.TAS'),
(652, 'mc_fashion_olshop', 'Mc Fashion Shop', 'Mc Fashion Shop Toko online baju import murah berkualitas dan terpercaya Pin BlackBerry : 2BA34E8C WA : 085747898321 Line : 08882513606 Sms : 082213373862 facebook.com/mcfashion.os'),
(653, 'graceshop11', 'Supplier and Konveksi Bekasi', 'Supplier and Konveksi Bekasi SUPPLIER FASHION TERMURAH since 2012 BEKASI ????BBM : 2156CA98 (FAST) ????WHATSAPP : 0889-1402-038 ???? JNE / POS /AMBIL D TOKO /GOJEK  ???? BCA & BNI www.facebook.com/graceshopp11'),
(654, 'fadkhera', 'Fadkhera', 'Fadkhera Koko-Hem dengan brand Fadkhera tetap gaya dan syar\'i. Desain detail dan nyaman dipakai. Order: 0821-8697-1991 (WA) | 54A707CD (BBM) '),
(655, 'dompetmurah100ribu', 'DOMPET BRANDED 100RIBUAN ????????', 'DOMPET BRANDED 100RIBUAN ???????? Follow @Monmonshop_ (pakai garis bawah) ????KW SUPER???? ????SURABAYA ?JNE POS Sms: 08384.917.4241 Line: monmonshop2 Bb: 210d59cb   ???? HARGA MULAI 51RIBUAN instagram.com/monmonshop_'),
(656, 'jrizkyshop', 'gelangkulitcostum', 'gelangkulitcostum Scrolldown???? Tumblr Tee???? Gelang Kulit Costum? Line:Janisaaaa (slow respon) Pin: 546EBC38 (fast respon) Sabtu&Minggu SLOW RESPON BRI???? Open reseller???? '),
(657, 'rumahnyahandicraftndecoration', 'Dwi Abnita', 'Dwi Abnita Homemade & handmade soft furnishing and vinyl craft.  ? WA 082166489079 (no call) ? pin bb: 76A07BA0 ? PRODUK READY STOCK CEK DI : #dwiabnita www.facebook.com/rumahnyahandicraftanddecoration'),
(658, 'geniobabyshop', 'geniobabyshop', 'geniobabyshop Baby & Kids Products Toko Buka: SENIN-JUMAT 08.00-17.00 Sabtu-Minggu off \"jangan komen di foto, PM langsung\" www.geniobabyshop.com'),
(659, 'preluvlemari', '#EANF#', '????New and preloved stuff for women, kids and babies ?backout buyers ? refund ? return Interested ? ????whatsapp @ 0125950454 or ????telegram @ 01115139211 '),
(660, 'keydisclothing', 'PRODUK ASLI', 'PRODUK ASLI Ready for retail Wanna chat????? PinBB : 5AF4F94C LineID : keydiscloth sms/wa : 081210914493 ????BRI-BCA ????JNE-TIKI ????BDG www.tokopedia.com/keydisclothing'),
(661, 'glamour.garage', 'OOTD CANTIK ????????????????CONFIRMED????', 'OOTD CANTIK ????????????????CONFIRMED???? ???? Tops.Cardis.Dresses.Pants ???? Ready stock ???? Daily postage ???? Registered with SSM ???? Whatsapp 013 272 3918 ???? Free swap review '),
(662, 'jual_peninggi_langsing_dll7', 'Dr. Konsultan Herbal:', 'Dr. Konsultan Herbal: ????Peninggi badan ????Pelangsing ????Penggemuk ????Pemutih ????Dll... ????Pin:55e5f515 ????Wa:08972693021 ????Line:@QHF9997U More????klik dibawah????ini... line.me/ti/p/%40qhf9997u'),
(663, 'zukhrufmuslimahanak', 'zukhruf muslimah anak', 'zukhruf muslimah anak Produsen baju muslimah anak  Permata cimanggis, Depok zukhruf.muslimahcantik@gmail.com Wa : 089614847079 Bbm : 5A9FFA40 Line : zukhrufmuslimahanak '),
(664, 'delatelyclothing', 'Palembang Free Ongkir', 'Palembang Free Ongkir New but TRUSTED ??  Start from 50 k until 150 k ???? Line????: imasdmynti ???? 59C6CF02 ???? 08117833900 ???? HIT&RUN = BLACKLIST?? ???? BCA '),
(665, 'baju_ala2_korea', 'mencari baju ala2 korea...!!!', 'mencari baju ala2 korea...!!! -jualan pelbagai jenis PAKAIAN -pre order (1 or 2 week process) -wechat \"RARECLOTHES\" utk order -telegram 0133500140 -harga MURAH dari pasaran '),
(666, 'rainbow.party', 'Rainbow Party', 'Rainbow Party Give u the best rainbow fashion in town Group of: @blackwhiteparty Line: mayatina Whatsapp: 087886702468 BBM&group: 79AD2B52 Jakarta '),
(667, 'poshfancycloset', 'Posh Fancy Closet', 'Posh Fancy Closet contact 01129147392 to purchase | purchase more than 1 item to get discount '),
(668, 'lestaryiswa', '@BajuMurah@MaembongTrendys', '@BajuMurah@MaembongTrendys Baju serendah RM15!! Dropship maembongtrendys Whatsapp/dm insta for order 0123787012 Support please?owner @azra_iswa Lebih bergaya~ Anggun~ Jelita '),
(669, 'sarroshopalovers', 'Sarro Shopalovers', 'Sarro Shopalovers ??????????im online seller ?Read before buy ????skrinshot-wassap 4 order ????Maybank Only ?????Serious buyer ?nett price ????stalker? kindly wassap 013-745-2995 '),
(670, 'a_faashion', 'zahratuljannah joyagh', 'zahratuljannah joyagh Wa: 08888919353 (NO SMS/CALL) line: ridateny REAL PIC Resi h+3 transfer SEMUA READY STOCK|HABIS=DELETE Senin-sabtu jam 10.00-18.00 shopee.co.id/a_faashion'),
(671, 'maay_stores', 'maay store', 'maay store Macam2 ada d Maay Store Ada ????????????????????..  Jom singgah d kiosk kasut kami @ Aeon Bukit Raja Klang ?????????????????????????? Whatsapp : 0102316563 '),
(672, 'kdhshop', '#EANF#', 'Mutif, Qirani, Iqra\', Shasmira, Hazna, Nibras and others brand of Fashion. Wa : 081802520549 Line : 081802520549 We provide high quality  fashion '),
(673, 'rbrshopptk', 'OLSHOP TRUSTED | Pontianak', 'OLSHOP TRUSTED | Pontianak ????Line: rbrshopptk ????BBM: 28FA5579 l 59D40CE6 ???? Nusa Indah Baru Since 2014???? READY JKT & BDG  ????? JNE  RESELLER ARE WELCOMED???????? '),
(674, 'nuzyopsproject', 'DAILY OUTFIT MURMER START 28rb', 'DAILY OUTFIT MURMER START 28rb BCA/JNE | 09.00 - 18.00 @nuzyops_shoes | DELETE = SOLD ????BBM   : 53AF2004 ????WA     : 089630338342 ????Line    : @oqp4011Z (use @) or click link bellow???? line.me/ti/p/%40oqp4011z'),
(675, 'finceclothing', 'FINCE CLOTHING', 'FINCE CLOTHING FASHION SPECIALIST Line : graceliem18 Pembayaran via BCA  Sold Out = Delete '),
(676, 'nizabustiers', 'nizabustier', 'nizabustier BUSTIER TULANG 4, 8, CROP BUSTIER Size S - XL Pesanan-Nama-Alamat-Telf LINE : nizabustiers BBM : 5902F678 WA: 08989393669 Fast order click below : line.me/ti/p/%40hgd3443i'),
(677, 'victorious_indo', 'FASHION GIRL UNDER 100K*????????', 'FASHION GIRL UNDER 100K*???????? NEW IG, @VICTORIOUS_OLS DI HACK ???? Since 2013 ???? resel disc 5rb/pcs ????Line: @eud3136m (Pake @) ???? JNE ???? BCA ???? Ragu? Cek #testimonialvictoriousols KOMENTAR.DI.IG.JARANG.DIBALAS.LANGSUNG.HUBUNGI.LINE'),
(678, 'lineshop_coll', 'lineshop', 'lineshop cardigan, dress blouse, pants, etc.. Sms/whatsapp 085770868766 (no call) LINE : lineshopcoll reseller & dropshiper wellcome :) '),
(679, 'antiiqahijab', 'Antiiqa Hijab', 'Antiiqa Hijab LOCAL BRAND DESAIN By Intan M. \"Simply, Beauty With Elegance\" ???? Line: Antiiqa ???? Wa: 081284181969 ???? Bbm : 5A09F72E www.zalora.co.id/antiiqa-hijab'),
(680, 'blinkids_collection', 'BABY SHOP of @Blink_Collection', 'BABY SHOP of @Blink_Collection Melayani RETAIL dan GROSIR OPEN : 09:00AM-17:00PM FOR ORDER ???? ????LINE : @RVY9713T ???? BCA  ???? Testimoni #Blinktesti ???????? line.me/ti/p/%40rvy9713t'),
(681, 'ainun_hijabcorner', 'SUPPLIER ALAT KECANTIKAN,HIJAB', 'SUPPLIER ALAT KECANTIKAN,HIJAB Contact us Id line : kikiainun Wa : 085655359103, bbm : (bila perlu) Format pemesanan : nama(spasi)alamat(spasi)kodebarang SBY TRUSTED. '),
(682, 'mickotindonesia', 'Kemeja Mickot', 'Kemeja Mickot Kemeja - Jaket - Koko Multifungsi  Premium Style  Contact ????+62 878 8616 7100 '),
(683, 'simply_shop88', 'ig lama simply_beauty88 dihack', 'ig lama simply_beauty88 dihack ???? Anty Bbm:2922e27d(fast respon) Line: anty1407 Wa:081310193934 Keep:transfer Ship senin-jumat JAKARTA Klo mau tanya capture tulisan ket/kode baju '),
(684, 'hbwhiteningolshop', '#hbwhitening original & halal', '#hbwhitening original & halal Order Via » BBM: 5299D3C7 » SMS/WA: 0821.9198.2465 » LINE ID: hbwhiteningolshop  Pngiriman: JNE YES Klik di bawah utk fast respon lwt line: line.me/ti/p/O144G6Us4f'),
(685, 'jojo_dshop', 'Trusted Online Shop ^^', 'Trusted Online Shop ^^ BBM 746AF799  BBM 5964447F (Group) LINE \'thisis.na\' WELCOME Resellers ^.^ P.s: All items NO garansi,brg selalu dicheck sblum dkirim.Happy shopping!! www.tokopedia.com/jojodshop'),
(686, 'buat_iklan', 'buatiklan', 'buatiklan ORDERNYA DI AKUN ?????? @Pusat Blazer  Akun ini hanya untuk beriklan !! '),
(687, 'julayshop', 'promo all items 45rb', 'promo all items 45rb Bismillah.. ????Line : yulyaaa ????BBM : 5964B4FF (yulia) OPEN ORDER ???? senin s.d kamis SHIPPING ???? jumat Bandung '),
(688, 'irishopid_kids', 'ALL ITEMS UNDER 100K', 'ALL ITEMS UNDER 100K ???? Line: irishopid_clothes ???? WA: 081808333041 (?call) ???? BBM: 53F0265D ???? H+1 Payment Confirm ???? Jakarta https://www.tokopedia.com/irishopid '),
(689, 'ladies.gallery', 'All about ladies ????????????????????????', 'All about ladies ???????????????????????? READY STOCK Owner: ratuusyafitrii Line:ratuusyafitrii Bbm: 5A032843 WA: 085774633598 Fb: Grosir tas baju jakarta(Ratu Syafitri) Menerima dropshiper ???? '),
(690, 'lazada_royshop', 'lazada royshop', 'lazada royshop Ayo kunjungi http://ho.lazada.co.id/SH4yqq dapatkan diskon 50rb untuk setiap pembelian 300rb!!! Sms : 081375159595 BB : 28D31502 ho.lazada.co.id/SH4yqq'),
(691, 'jual_peninggi_langsing_dll6', 'Dr. Konsultan Herbal:', 'Dr. Konsultan Herbal: ????Peninggi ????Pelangsing ????Penggemuk ????pemutih ????Dll... ????bbm:pin: 515f95c9 / 55e5f515 ????wa:08972693021 ????Line: @QHF9997U more klik????disini???? line.me/ti/p/%40qhf9997u'),
(692, 'pasarbranded', 'pasar-branded.com', 'pasar-branded.com Surf & Branded Men\'s Apparel Branded Apparel (Burberry, Armani, Calvin Klein, Topman, Levis, and many more) Sms / Wa : 082113601010 Pin BB : 282CEAEE pasar-branded.com'),
(693, 'berchi_grosir', 'Berchi Grosir', 'Berchi Grosir Contact person: *Adm 1 : pin 2828D927 //// *Wa :0878.2560.9024  *Adm 2 : pin 28760635 /// sms :083821712575 (khusus fashion pria) '),
(694, 'uciw_collections', 'Baju  Hijab Bandung', 'Baju  Hijab Bandung || SEMUA READY STOCK || Bandung $ BRI ORDER / JOIN GRUP     085723884221     7F2E63FD     Line : guslianah '),
(695, 'hafizhan_collection', 'Mey', 'Mey Line : Hafizhan_collection WA : 082127074363 PIN : 327E8E53 Agen milo premiun Chiarashop_tokosegala ada JAFRA consultant MCI Nanospray Boneka wisuda '),
(696, 'glossybeautyshop', 'TRUSTED ONLINE SHOP?', 'TRUSTED ONLINE SHOP? Comfy Jegging, Softlens, Clothes, Replica Bag, etc JAKARTA ???? Order by BBM : 2B4AEFFC LINE : @rvk4528v (with \'@\') OFFICIAL LINE klik : ???????? line.me/ti/p/%40rvk4528v'),
(697, 'babycutecollections', 'On Sale Baby/Kids Item', 'On Sale Baby/Kids Item ????????SSM Registered: SA0248939-U ??(Since 2010) ?????Ready Stock / Pre Order ????????????Baby/Kids Item ???????? Mommies Item ????? Toys ???????? Whatsapp : 0123596561 '),
(698, 'quilababy_clothes', 'Homemade Baby&kids Clothing????', 'Homemade Baby&kids Clothing???? Order ???? Line : @hwy0927y (pake tanda @) Wa : 08973787320 or klik link di bawah ???? line.me/ti/p/@hwy0927y'),
(699, 'kidznmom_stop', 'KIDSWEAR |DRESSBABY|BREASTPUMP', 'KIDSWEAR |DRESSBABY|BREASTPUMP ???????????? STOPP!!! SCROLL BAWAH DULU TENGOK ADA TAK APA YG MOMMY CARI.. ????Whatsapp +601110827715 to order ????BERIKAN YG TERBAIK BUAT SIKECIL ANDA???? '),
(700, 'houseofrompers', '#EANF#', '????SSM Reg (JM0741890-K) ????Selling baby products ????Selling kids attire ????Ready Stock ????Brand New Item ????Online only ????COD JB area only ????Whatsapp +60107606244 '),
(701, 'promosi_shop_id', 'Jasa Promosi Online Shop', 'Jasa Promosi Online Shop Trusted!  1 bulan 75rb (5 foto / hari)  Promo inquiry ke line id: @promoolshop  (pake @) atau click link bio ???????????? line.me/ti/p/%40promoolshop'),
(702, 'petitapetito', 'Petita Petito', 'Petita Petito For the feet of little ones Contacts: • petitapetito (line) • +62 838 769 769 09 (whatsapp) Avail. size: 0-6 mos 6-12 mos '),
(703, 'thelittle_sara', '#EANF#', '100% authentic (Next UK, Sport Brands) Pre-Order 10-12 Days  Items direct from UK Inquiry & order : +60175937386 COD: Seri Iskandar, Perak '),
(704, 'addhuhakidsgallery', 'BAJU BUDAK MURAH.BABY KIDS..', 'BAJU BUDAK MURAH.BABY KIDS.. ????Dress ????legging/jeans ????tshirt ????pynjama - ????????????.. ????Trusted seller ????whatsapp/tele 0103342384 SUE ????Postage MON-FRI ????Sold-gmbar akn delete '),
(705, 'fiftyclothes', '50 Clothes', '50 Clothes Baju Murah Ready Stock Jakarta ???? ????BCA Shipping every Monday and Thursday ???? Fast response :  ????BBM : 7CA26ACB Line klik link dibawah ini ???? line.me/ti/p/%40ltk0333t'),
(706, 'aelleworks', 'KIMONO OUTER 65RB-AN', 'KIMONO OUTER 65RB-AN ???? MLG ???? BRI-MANDIRI ???? FAST RESPONSE by: WA/sms: 085232993052 BBM: 58AED134 LINE: ditaindraf ????comment ig SLOW RESPONSE ????READY & PO MAX 3 HARI '),
(707, 'jambishopping', 'Tas murah banget', 'Tas murah banget Bank : BRI senin-sabtu  Pin : 5AFFC289 Line : arefika Belum termasuk ongkir '),
(708, 'biqianzi_house', 'Biqianzi House', 'Biqianzi House Welcome  ????Line:biqianzihouse ????BBM:5ABC85C4 ???????? : BRI Syariah Semua ada disini???? Happy shoping guys???? No tipu ya guys!!! '),
(709, 'heninoor_fashionshop', 'Endah D Istiko', 'Endah D Istiko Bandung Bbm: 5a903bf3 FAST RESPON! Line: @40ehv2051i (ketik @ nya ya) TIKI/JNE MANDIRI SOLD=DELETE RESELLER WELCOME ECER/GROSIR NO KEEP KEEP=TRANSFER '),
(710, 'tasyananu_batik', 'BATIK GROSIR BATIK MURAH', 'BATIK GROSIR BATIK MURAH Pertanyaan dan pemesanan langsung hub kontak, fast respon Pin BB : 5876B161 SMS/WA : 087835649355 Line : rinatasyananu Transfer via MANDIRI & BCA SOLO '),
(711, 'sarah_butik_batik', 'Sarah Butik Batik Online Shop', 'Sarah Butik Batik Online Shop Batik Cantik Sarah (Comment di ig slowrespon yaa) Order:  ?pin bbm : 757E3D90 ?Line ID : olshop.sarah ?WA: 08529-2505-378 Happy shopping ???? '),
(712, 'agnesaputribatik', 'agnesaputribatik', 'agnesaputribatik  '),
(713, 'mayaslight.galeriempat', 'Maya\'s Light Galeri Empat', 'Maya\'s Light Galeri Empat Tangerang poris  WA/sms 081545410261 pin 7E5EDBA0 PAGE FB : Maya\'s Light Galeri Empat FACEBOOK : Maya LightBeauty Satu TOKOPEDIA : Maya Light Shop mobile.facebook.com/mayaslight.galeriempat'),
(714, 'zaylahijab', 'ZaylaHijab', 'ZaylaHijab Assalamualaikum, bismilah  ???? Bandung,jawa barat Indonesia ?/WA : 083822632208 Pin Bb : 57D74ECB  Line/Twitter : @ZaylaHijab www.ZaylaHijab.com'),
(715, 'flowering_evergreenshop', 'Most Item Is BELOW THAN RM100', 'Most Item Is BELOW THAN RM100 ????: menjual dress,jubah,baju korea dan mcm2 ???? ???? whatsapp : +60108934198 (msg me if u didnt find my ws) ???? ???? wechat : cuppachups ????All READY STOCK item '),
(716, 'nooaru', 'Nooaru', 'Nooaru Qawi Lillahi\'taala.   3rdintifada#Palestine Kedai Online Woay-M  ? Ws- 0138936534 '),
(717, 'sist.rareee_shoppe', 'FREE POSTAGE!', 'FREE POSTAGE! ???? Malaysia Based ! ???? First Pay, First Serve! ???? Ready stock & Pre Order ???? Ws/Tele : +60135405105 (NO CALL!) ? Wc : sist_rare  ? know Mandarin ? '),
(718, 'heycantikstore', 'FREE SWAP REVIEW', 'FREE SWAP REVIEW SHOES ? BLOUSE ? FANCY BOOKS ????ATTENTION : Serious buyer only ? Whatsapp: +60134447044 (Min) ????GIVEAWAY: 5K FOLLOWER!! ??SCROLL TO PURCHASE?? '),
(719, 'chelis_shop', 'Chelis shop', 'Chelis shop SUPPLIER BAJU TERMURAH  JAKARTA ?NO GROUP? ???? WA : 087877709025/ BBM : 7446BB75 #testimonichelisshop Official account LINE klik LINK dibawah ???????????? line.me/ti/p/%40ftc8255o'),
(720, 'big_size_2', 'Just Big Size', 'Just Big Size Kami hadir untuk memenuhi kebutuhan Anda yang memiliki tubuh besar tetapi ingin tetap  tampil gaya dan fashionable PIN : 237B7888 SMS : 087889555561 www.justbigsize.com'),
(721, 'ohayobigsize', 'fashion big size', 'fashion big size Fashion BIG SIZE JAKARTA SORI TDK BISA NAWAR???? Weekend slow respond PO wajib SABAR TDK MELAYANI KOMEN DI IG line: marni.ohayo wa: 087781678699 facebook.com/ohayobigshop'),
(722, 'grast_', 'Hidden Treasure', 'Hidden Treasure READYSTOCK Branded Clothes  New & 100%ORIGINAL ????JAKARTA More collection/order? ????LINE: hoala SCROLL DOWN???? Est.2012 '),
(723, 'iklano_indo4', 'iklano_indo4', 'iklano_indo4 Minat jadi member For info/hub: ????LINE:iklan_olshop87_2 ????BBM:53D7A140 IKLAN TERMURAH DAN TERPERCAYA!!!!! '),
(724, 'advertious6', 'PROMO GRATIS OLSHOP', 'PROMO GRATIS OLSHOP ????Close Free Promote ????Paid Promo Transfer, Pulsa, Endorse ?Add IG @advertious6 & @luxnious  ????Contact Official Line @advertious (pake \"@\")???? line.me/ti/p/%40advertious'),
(725, 'promoteolshop28', 'PromoteOlshop28', 'PromoteOlshop28 Open member open paid promote Minat kontak bio ^^ Bbm.  :5A8A6B3E Line.  :promoteolshop28 Buruan ya guys ^^ '),
(726, 'popcorn_shoppanista', 'grosir baju couple,tas,sepatu', 'grosir baju couple,tas,sepatu Grosir /ecer kaos couple Order via SMS/WA 081997797376 Line Popcornshp Pin 53830660 Payment Mandiri / bca Disc order 3psg #popcornshopanistatesi jakarta.shipping.everyday.minggu.libur'),
(727, 'jaket.couple.indonesia', 'kaos couple  TEBAL BERKUALITAS', 'kaos couple  TEBAL BERKUALITAS Cari Baju Couple BERKUALITAS ? DISINI TEMPATNYA !! RIBUAN TESTIMONI !! TERBUKTI KUALITASNYA !! SMS/WA : 0857.234.77888 LINE(Pake @) :   @KaosCouple www.bajucoupleonline.com'),
(728, 'manstyle_os', 'Promote Onlineshop', 'Promote Onlineshop Ini adalah instagram PROMOSI  ???????????????????????? ????OPEN & FOLLOW @urbanclothes_id???? ????OPEN & FOLLOW @urbanclothes_id???? ????OPEN & FOLLOW @urbanclothes_id???? www.instagram.com/urbanclothes_id'),
(729, 'cyrillaqq', 'Cyrilla Rara Oki E.', 'Cyrilla Rara Oki E. A daughter, a sister, a friend, and a proud wife of a wonderful husband! ?Antonius Hartono? #Dreamer #Traveller #Explorer #FoodLover #Yogyakarta '),
(730, 'juallingeriemurah', 'Ready Lingerie, Bikini, Bra', 'Ready Lingerie, Bikini, Bra Ready stock, all items imported SMS/WA: 08170854534 BBM: 5271D6D3 Line: @ lingerieindonesia (pakai @) www.lingerieindonesia.co.id'),
(731, 'lingeriesurabaya', 'SUPPLIER LINGERIE IMPORT MURAH', 'SUPPLIER LINGERIE IMPORT MURAH READYSTOCK!  ???? NO COD/MEET UP ???? JNE ONLY ???? LINE: @BIH6827F (pakai @) BBM: 5A6F1EC4   PART OF @PIYAMALUCU    Fast respon @LINE, klik ???? line.me/ti/p/%40bih6827f');
INSERT INTO `instagram` (`id`, `username`, `first_name`, `bio`) VALUES
(732, 'cotton.button', 'Cotton & Button | Sleepwear', 'Cotton & Button | Sleepwear Simple in style. Jakarta based  --------------------------------------- ? Contact ? Line: cotton.button '),
(733, 'alifiagallery', 'ALIFIA GALLERY', 'ALIFIA GALLERY Kediri ?Bbm 52A8CF5A ?Line cynthiaalifia ?Wa/sms 085749163208 (fastrespon) Pay Via BRI JNE Reseller/dropship welcome. Happy shopping. olshopkediri.pengirimanlangsungdarikediri.pengirimansetiaphariseninsampaijumat.khususharisabtudanmingguuntuktanyatanyadanpemesanan.happyshopping'),
(734, 'bisneshop_', '????freepos????', '????freepos???? ????serious buyers only ????Kain pasang  ????Batik sarawak borneo ????Telekung kanak2 | dewasa ????shawl labuh ???? 019-3715781 '),
(735, '4womens.my', '4womens.my', '4womens.my ????ALL ABOUT WOMEN???? TRUSTED SELLER Do visit: @saudagar_malaccan @saudagar_malaccan.official PENTING!! SEBELUM ORDER UKUR BETUL2 DULU YE :) www.facebook.com/womencloset.my'),
(736, 'ina_nieda', 'ina.nieda', 'ina.nieda ????Muslimah Attire???? ????Trusted Seller ?No COD ????WhatsApp : 013.687.2121 ????Wechat : irosenida '),
(737, 'syalinscollection', 'Linda Admin Syalin Collection', 'Linda Admin Syalin Collection Happy Customer  search  : #syalinsCcustomer Clearance search : #syalinsCoffer To purchase Whatapp/Telegram/SMS 0103986386 Linda Serious Agent Needed!! www.syalincollections.blogspot.com'),
(738, 'princessoris', 'FREE ONGKIR BAJU IMPORT MURAH', 'FREE ONGKIR BAJU IMPORT MURAH ????FASHION IMPORT & LOCALE ???? ???? Follow + Spam???? = voucher 20k ? Free Ongkir JaBoDeTaBek - 20 Nov ???? Jakarta Order WA/SMS 087885336294 BBM 5977F56A www.tokopedia.com/princessoris'),
(739, 'gaun_pesta', 'pinky store', 'pinky store Pakaian berkualitas  Pakaian Import Bbm : 5716bbfd Line ???? ernaaphing '),
(740, 'jual_peninggi_langsing_dll_5', 'Dr. Konsultan Herbal:', 'Dr. Konsultan Herbal: ????Peninggi Badan  ????Pelangsing Badan ????Pemutih Badan,  ????dll ????WA:08972693021 ????BBM:55e5f515 ????LINE: @QHF9997U ????KLIK Link di????bawah???? u/ add LINE line.me/ti/p/%40qhf9997u'),
(741, 'tutuloph', 'OPEN', 'OPEN kontak admin di bio u/ tnya STOCK admin IG dan stock beda???? ORDER: BB : 584A4F64 LINE 1: tutuloph YOGYAKARTA www.tutuloph.com'),
(742, 'taaleabutik', 'SUPPLIER,IMA SCRAF MURAH 37RB!', 'SUPPLIER,IMA SCRAF MURAH 37RB! ????JILBAB RAWIS STOK SELALU ADA ?Sms/Wa: 085 747 4477 14 (NO CALL) ????Bbm: 544044D7 ????NO KEEP???? TRUSTED???? JNE only: senin-jumat ????LINE (Fast) ???????????????????? line.me/ti/p/%40awq5406e'),
(743, 'my_fashionman', 'Jual Baju Cowok (FASHIONMAN)', 'Jual Baju Cowok (FASHIONMAN) trusted seller 100% !! Semua barang READY Sold = Delete For order : Bbm : 51B6378E line : fashionman Wa : 083899779904 Delivery senin-sabtu via JNE '),
(744, 'saystosaid', 'SAYS TO SAID', 'SAYS TO SAID Haters Art Project  ???? wa : 081289054069 ???? line 1 : @ujs8680s (pakai @) ???? Line 2 : lalugilangfs ???? Twitter : saystosaid Fvcking Love Haters !!! '),
(745, 'osramshop01', 'OSRAM OUTLET', 'OSRAM OUTLET Distro and Fashion    ????Bbm: 59215A84         Sms: 082 242 248 711 Open 10.00 - 17.00  Welcome Reseller  Jln.Raya pucang Km 3.Banjarnegara,Jawa Tengah '),
(746, 'akunpromosiresmi2', 'Akunpromosi', 'Akunpromosi Akun promosi @allaboutman.id | olshop TRUSTED 100% ! | fast respon :) go follow ?? @allaboutman.id '),
(747, 'ramdhanihidayat', 'Ramdhani Hikmat Hidayat', 'Ramdhani Hikmat Hidayat Unravel Student at Chemistry IUI '),
(748, 'theyulissa.clothes', 'The Yulissa.Co >Trusted Seller', 'The Yulissa.Co >Trusted Seller + Part Time. Same owner @theyulissa + NO BACKOUT BUYERS!  + Payment: CIMB, Postage: Weekdays + Fast respond whatsapp/ telegram +6017-3604158 Tolong.Baca.Caption.'),
(749, '_yscollection', '???? MurahJimat\'s Shop ????', '???? MurahJimat\'s Shop ???? . ???? STRICTLY NO CALLS? #HAZARDOUS ???? . ???? DROPSHIPS NEEDED ???? . WHATSAPP: +60149378595 | WECHAT ID: irra96 ???? . HAPPY SHOPPING! ???? . BACA.CAPTION.SAMPAI.HABIS.'),
(750, 'atehana', 'Ilyana Bt Sharif', 'Ilyana Bt Sharif ?jual tudung aidijuma ?jam tangan branded ?0195901483 '),
(751, 'jaminan_murah', 'muhd akmal', 'muhd akmal Menjual perbagai barangan seperti jam,handbag,kasut,purse,vimax,dan gelang tngan..wow  For serius buyer only Can wssap me 0175342718 '),
(752, 'sashaweddingplanner', 'sashaweddingplanner', 'sashaweddingplanner Whatsapp 0132776012 Call 0105021729 @sashabridal #sashabridal Selangor.KualaLumpur www.facebook.com/Sashabridal'),
(753, 'nyaholshop', 'olshoptulungagung', 'olshoptulungagung Pin : 2AC7EB5A Hape : 0877-9414-3444  Line : nyahintan ????.Balesono-ngunut-tulungagung ????FREE ONGKIR TULUNGAGUNG KOTA ???? SUNDAY on CLOSE? ? SOLD=DELETE MINAT.LANGSUNG.CHAT.ADMIN.'),
(754, 'dillajan_produkmurah', '100% Original Product', '100% Original Product ????Nak Cantik , Slim & Sihat- Jom Follow ????SSM ( KT0358***-H ) ????Postage : Monday & Thursday ????Trusted Seller '),
(755, 'fortunestore1_testi', 'TESTIMONIAL FORTUNESTORE1', 'TESTIMONIAL FORTUNESTORE1 khusus TESTI @fortunestore1 @fortunestore2  @fortunecatalog???? ???? LINE : @fortunestore1 (gunakan @) ???? WA : 082280634073 '),
(756, 'fayolahouse', 'HANDMADE BAG & CLUTCH', 'HANDMADE BAG & CLUTCH ????Surabaya ???? BCA ???? JNE/Pos/Gojek ???? BBM.7546e574 ???? WA.085712345820 ???? Order Line : line.me/ti/p/%40ewa2797c'),
(757, 'pinkemmaid', 'PinkEmma', 'PinkEmma All Ready Stock! LINE@ PinkEmmaID > http://bit.ly/PE-LINE Pesan lebih cepat via website :) www.pinkemma.com/ig'),
(758, 'ariphasion', 'ariphasion', 'ariphasion ???? AriPhasion olshop ???? ???? LINE - WA - 0857 1290 6713 ???? 574184BF Piyungan, Yogyakarta '),
(759, 'armina_hijab', '????Armina Hijab????', '????Armina Hijab???? Jualan jilbab Pin : 5A428B63 Wa : 082242039787 Line : ditaalfirawati JOGJA Berminat hubungi salah 1 kontak diatas '),
(760, 'sepatumurah_plg2', 'GUDANG SEPATU MURAH', 'GUDANG SEPATU MURAH ???? Bbm: 5A05FEA7 ???? Line: jingganadita (FAST) ???? WA  : 0819 - 9664 - 2930 Menerima reseller ???? BRI ???? JNE TF ? PROSES ? BARANG DIKIRIM ~ NO COD FREE.ONGKIR.TANJUNG.ENIM.MUARA.ENIM.PALEMBANG.JABODETABEK'),
(761, 'snapxstore', 'STRIPTEE PARKA SAMARINDA', 'STRIPTEE PARKA SAMARINDA Welcome???? ???? Samarinda  Order? Tanya-tanya?  KATALOG LENGKAP DI LINE Line : klik link dibawah ? line.me/ti/p/%40qri2751k'),
(762, 'salwasgallery', 'Salwa\'s Gallery', 'Salwa\'s Gallery fashion, skincare, cosmetics, apps/panel, etc. WA 089695155170  BBM 7D4554F4 LINE : salwagallery (slowres) LINE2 : siisalwa (owner) BANDUNG, BEKASI '),
(763, 'mrsprica', '?Simply Elegant Accesories', '?Simply Elegant Accesories ?Supplier Tas Replika Good Quality <200rb?ReadyStock ?Quality jauh di atas harga ?Trusted Seller ????????LINE: aprica ????WA: 08195610018????BBM: 76AB294A ???? '),
(764, 'krisnapuspitashop', 'NyemutOS', 'NyemutOS SEMUA BARANG PO ???? PIN : 537EE000 ???? WA : +6285704107100 ???? TULUNGAGUNG FREE ONGKIR  ???? BRI KEEP NO CANCEL RESSELER WELCOME ???????? '),
(765, 'asshopid', 'AS_SHOPID', 'AS_SHOPID ???????? ?Serius order hub ???? ?BBM: 28140E1A ?Line: afriyansaputra28 ?pengiriman  via JNE ?harga belum termasuk ongkir '),
(766, 'redpocketshop', 'Shoes & Bags Branded', 'Shoes & Bags Branded ????ORIGINAL 100% Guarantee!! ????TRUSTED! NO Hit and Run! ????PO follow @redpocketshop2 ????Line: RedPocketShop ?BBM: 5A1782D1 FirstPay.FirstGet.SeriousBuyerOnly'),
(767, 'jersey_calcio', 'Calcio Sports', 'Calcio Sports Sepatu : original 100% made in indonesia utk serius buyer hub : bbm 7df88831 (087774685586) /line : calcio_sport Via delivery Gojek & jne Indonesia '),
(768, 'ec.25shop', 'FREE ONGKIR jakarta, bekasi.', 'FREE ONGKIR jakarta, bekasi. Klik Line ???????????? line.me/ti/p/%40fmh7145f'),
(769, 'dgustyshop', 'realpict & readystok surabaya', 'realpict & readystok surabaya ???? koment kadang jarang masuk lsg hub. Contact aja yak ???? ???? BCA, mandiri, BRI a/n gusti nurul madina  ???? sms/WA : 081217709946  ???? line : dgustyshop '),
(770, 'hasilavarietyshoppe', '#EANF#', 'BelI sekali pasti nak lagi.. 100% homemade..pasti halal.. SMS/Whatsapp 0146200074 '),
(771, 'storedeputree', 'SSM REGISTERED', 'SSM REGISTERED ???? Whatsapp : 014 817 8320 ???? Delivery on sun, tues & thurs ???? Call/Return/Exchange/Refund ???? Agent sparkling skincare Terengganu '),
(772, 'aretykah_hijabstailista', 'miSs kepOh Jual tudunG ^^,', 'miSs kepOh Jual tudunG ^^, ????Hijab High Quality ????Dress & Blouse???? ????Produk kecantikan???? ????Perfume Che Ta ?WHATSAPP 01114946196 ????????BACKOUT BUYER ????????NO REFUND ????TRUSTED SELLER '),
(773, 'nak_sihat_cantik_kurus', '#EANF#', 'iir rozz Shaklee ID 1193454 0129875555 '),
(774, '_riska_fashion', 'JAKET ON LINE STORE', 'JAKET ON LINE STORE JAKET HOODIE SWEATER SEPATU Welcome Reseller & Dropship  Info Pemesanan: WA: 085659649226 BB : 7ead66a3 Line: riskastore Kota Bandung '),
(775, 'bythesister__', 'Macam-macam Ada!!', 'Macam-macam Ada!! ? Business hour: 9am - 11.30pm ???? 0123574542 (Jaja) 0172548380 (Otoi) ???? bythesister@yahoo.com ???? swapreviewig | giftreviewig '),
(776, 'shakleebybella', 'SHAKLEE TERMURAH SHAH ALAM', 'SHAKLEE TERMURAH SHAH ALAM ????PROMO VITA C BUY 2 @ 5% OFF SID Nabilah ????COD UiTM Shah Alam ????POST SM/SS ? 0176567697 for price ???????????????? ????????????? ????????? '),
(777, 'butikcafeimansha', 'BUTIK CAFE IMANSHA', 'BUTIK CAFE IMANSHA ????LAKSA KETAM ????LAKSA TOMYAM ????MEE BESEN(NEW) ???? TAMAN SRI GOMBAK FASA 3 ?0176730041 www.facebook.com/imanshacloset'),
(778, 'nz_beauty_shop_murah', 'NurZulaikha AbdRahim(EqaRahim)', 'NurZulaikha AbdRahim(EqaRahim) ????TAK PERLU BELI BANYAK NAK DAPAT HARGA MURAH..BORONG LAGI MURAH???? ????MAYBANK ????SUN-THU ????WHATSAPP ONLY 0136339498 ????ORIGINAL..NO FAKE ? ????TRUST SELLER '),
(779, 'aballin', 'Shawl under Rm20!!!', 'Shawl under Rm20!!! ?Ws 011-16280375 - Abby ???? #aballinfeedback  ?Swap review ????No Dropship ???? FOUNDER :       @abshobe ???? TRUSTED SELLER  ????Agent topup needed Rm25 www.facebook.com/Aballin-1408136722829499/timeline/?ref=hl'),
(780, 'rahimizulkarnain', 'Rahimi Zulkarnain', 'Rahimi Zulkarnain Founder and CEO Riyamie Marketing, Operation Assistant at Excel Minds Consultancy,CoD4 gamer, Basketballer.... '),
(781, 'lieyana_syazwanie', 'Noor Lieyana Syazwanie', 'Noor Lieyana Syazwanie Beautiful dresses are HERE ???????????? Kindly WhatsApp: 0145412612 Wechat: lieyana2506 Sold out items will be DELETED ???? Prices: ALL INC POSTAGE ???????????? lieyanaaiizack.blogspot.com'),
(782, 'surdcloset', 'PROMO VITAMIN C Shaklee ??????', 'PROMO VITAMIN C Shaklee ?????? ????????PROMO NOV ?? VITAMIN C ???????? _ ••Nak join bisnes? Hubungi Noni•• ????+60172659735 ????COD Klang/S.alam/Subang '),
(783, 'galeri_azlana', '#jubah#dress#bajukurung#baju', '#jubah#dress#bajukurung#baju SERIOUS BUYER ONLY PLEASE!!!! HAPPY SHOPPING WHATSAPP ME IF U SURE TO PURCHASE 0146767035 POS ONLY NO COD BAG/BAJU/KASUT/JAM '),
(784, 'nad_aikhalish', 'pemborong termurah', 'pemborong termurah 018 3846907...wassap/line/wechat Fb : nad aikhalish Page : n&z real collection Page : pembekal product termurah www.beautyinjection89.blogspot.com'),
(785, 'thetwinsgarden', 'ThetwinsGarden', 'ThetwinsGarden ????10, Jalan Padi Emas 1/7, Bandar Baru Uda, 81200 Jb  ????10 Am- 9 Pm Vendor Enquiry ???? 013 792 8860 Order Online ???? 013 710 8860 ? 07 - 244 8886 www.ttg.com'),
(786, 'melovehandbag', 'DRESS, BLOUSE & HANDBAG', 'DRESS, BLOUSE & HANDBAG ? Ready Stock ????Trusted Seller ???? Pos : Isnin - Jumaat ? Whatsapp : 019 378 1476 '),
(787, 'cuteitem_here', 'Cute Cath Kidston Purse', 'Cute Cath Kidston Purse WA 0196292408 ????Trusted Seller ????Cath Kidston Stuff ! Sold item will be delete LIMITED STOCK! FIRST COME FIRST SERVED BASIS TRUSTED.SELLER'),
(788, 'ms_secretroom', 'Secret Room', 'Secret Room Good Quality with Reasonable Price Reseller & Drop ship are welcome Phone. +628179122800 (Whatsapp / SMS Only) Pin BB 2BC9C5A7 Delete = Sold Out www.facebook.com/MS.secretroom'),
(789, 'dnkshoes_shoponline', 'DNKSHOES Shop Online Bandung', 'DNKSHOES Shop Online Bandung WA 087825261196 PIN 5838F58F  ID line  dnkshoes_onlineshop  Tokped dnk shoes shop online  bukalapak dnkshoes shop  carousell  dnkshoes_onlineshop '),
(790, 'ayesha.hijab', 'new and second stuff?', 'new and second stuff? DEPOK New but TRUSTED! WA/sms: 081513481513 Line: saniyamaryaman Buy 3 --> GET DISCOUNT! ?Terima dropship ?Hit and run=blacklist! ?no COD '),
(791, 'dndr.os', 'yicam,stringbag jogja', 'yicam,stringbag jogja •Xiaomi Yi action cam & aksesoris nya •pb xiaomi ori •bag •etc  Line : Dputrita WA/sms : 089688516393 Pin BB : 7EB32C3B COD jogja kota/JNE???? trusted???? '),
(792, 'zmgrosirkaosdistro05', 'zmgrosirkaosdistro05', 'zmgrosirkaosdistro05 Produk distro murah berkulitas Harga kaos mulai dari 28.000/kaos Dan jaket mulai  70.000/jaket Invite pin bbm 58341700 Sms/tlp/WA/line: 081221465545 www.zmgrosirkaosdistrobandung.simplesite.com'),
(793, 'zmgrosirkaosdistro04', 'zmgrosirkaosdistro04', 'zmgrosirkaosdistro04 Menyedikan kaos distro murah berkualitas Harga mulai dari 28.000/kaos Dan jaket mulai  70.000/jaket Sms/tlp/WA/line: 081221465545 Pin bbm 58341700 www.zmgrosirkaosdistrobandung.simplesite.com'),
(794, 'syasakosmetik', 'syasyajewellery', 'syasyajewellery 1-membantu wanita untuk bergaya ke tempat kerja 2-membantu wanita memikat kekasih hati 3-membantu wanita menawan hati suami&yakin untuk tampil menawan Fb.com/syasyajewellery'),
(795, 'kadkahwinkami', 'kadkahwinkami', 'kadkahwinkami service printing kad kahwin sticker bussiness card flyers guestbook etc located kuantan pahang whatsapp 0179021127 www.facebook.com/kadkahwin.kami.1'),
(796, 'jannahsofina97', 'Jannah Sofina', 'Jannah Sofina FOLLOW lepastu UNFOLLOW . Setakat nak FOLLOWER ramai baik X PAYAH FOLLOW saye . ???? ???? Original ???? Trusted Seller ???? whatsapp ; 0174930107 '),
(797, 'mar_cmd', 'kurus cantik sihat', 'kurus cantik sihat Handle by @madieyjoey Id: MYGNS1159183 Nak kurus CEPAT!! Tanpa berlapar!! Mencairkan Lemak!! Tanpa bersenam!! Halal!! MY only!! ???? Wassap 017 932 5750 '),
(798, 'kekabuonlinedotcom', 'KEKABU ? Kak Kabu OFFICIAL ?', 'KEKABU ? Kak Kabu OFFICIAL ? ???? SA0286060-V ????????% Kekabu Kampung! ???? PreOrder 5 weeks! ????B.Harian 2014 ????TV1 \'14 &\'15 ????KOSMO 2014 ????A.AWANI 2015 ????H.METRO 2015 ????FOCUS MSIA 2015 '),
(799, 'handbagmurah4u', 'gorgeous_boutiquez', 'gorgeous_boutiquez ????SSM Registered ????Trusted Seller Since 2013 ????Ready Stock | Postage Monday to Friday ????Whatsapp / Telegram : 0162253249 (fast respond) www.facebook.com/handbagmurahgile'),
(800, 'shop_online_power', 'shop_online_power', 'shop_online_power Beli secara borong amat dialukan!!! Barang Original, duit akan dikembalikan jika barang tiruan!!! No cod, pos only Whatapp/sms :01126084577 '),
(801, 'jogger_lab_catalog', 'SUPPLIER JOGGER PANTS', 'SUPPLIER JOGGER PANTS [Part of Apparel Lab] GROSIR JOGGER CUMA 120K* Jogger / Chino / Jeans SPAM LIKES = FREE JOGGER BBM 5A3FD818 LINE @lab.group (pakai @) WA 081217995460 instagram.com/jogger_lab'),
(802, 'whiteline3.apparel', 'SUPPLIER JOGGER PANTS', 'SUPPLIER JOGGER PANTS [Part Of Apparel Lab] GROSIR JOGGER CUMA 120K* CEWEK COWOK Bisa Pakai SEMUA MODEL JOGGER ADA SPAM LIKES= FREE JOGGER BBM 75AC91E6 Add LINE disini ya!? line.me/ti/p/%40uay6366f'),
(803, 'whiteline2.apparel', 'SUPPLIER JOGGER PANTS', 'SUPPLIER JOGGER PANTS [Part Of Apparel Lab] GROSIR JOGGER CUMA 120K* CEWEK COWOK Bisa Pakai SEMUA MODEL JOGGER ADA SPAM LIKES= FREE JOGGER BBM 75AC91E6 Add LINE disini ya!? line.me/ti/p/%40uay6366f'),
(804, 'whiteline.apparel', 'SUPPLIER JOGGER PANTS', 'SUPPLIER JOGGER PANTS GROSIR JOGGER CUMA 120K* CEWEK COWOK Bisa Pakai SEMUA MODEL JOGGER ADA SPAM LIKES FREE JOGGER BBM 75AC91E6 Add LINE disini ya!? line.me/ti/p/%40uay6366f'),
(805, 'skincare_pbg', 'Florin Skincare • Softlen', 'Florin Skincare • Softlen Assalamualaikum wr .wb Order dan pertanyaan: ????PIN : 543F0EB3 ????Line : aprilliadf ????WA : 083844313000 NO CALL ???? Mandiri ???? JNE ???? Purbalingga/Jakarta '),
(806, 'stardust.apparels.02', 'SUPPLIER JOGGER PANTS', 'SUPPLIER JOGGER PANTS GROSIR JOGGER CUMA 120K* CEWEK COWOK bisa pakai SEMUA MODEL JOGGER ADA  Untuk info harga dan pemesanan lanagsung contact: BBM 21cb8cb6 Add line klik ? line.me/ti/p/%40xmj6244t'),
(807, 'stardust.apparels', 'SUPPLIER JOGGER PANTS', 'SUPPLIER JOGGER PANTS GROSIR JOGGER CUMA 120K* CEWEK COWOK bisa pakai SEMUA MODEL JOGGER ADA  Info harga dan pemesanan langsung contact: BBM 21cb8cb6  Add line klik ? line.me/ti/p/%40xmj6244t'),
(808, 'holyslime', 'CLEAN and FUN', 'CLEAN and FUN Surabaya, Indonesia #InfoHolyslime  WA : 081333909119 BBM :  5977AC38 / 58327F20 YouTube Channel : Holy Slimd LineID : @holyslime (with @) ???? line.me/ti/p/%40holyslime'),
(809, 'ddstarisda', 'Dd Starisda Beauty Shop ????', 'Dd Starisda Beauty Shop ???? ????Produk  Beauty & Healthy.????kenapa nak bayar lebih?   ????whatsapp/sms   013 676 9392 unicorich69.com/share'),
(810, 'iamcoolteerepost', 'Iamcooltee', 'Iamcooltee IG ini utk PROMOTE IAMCOOLTEE Order bisa langsung ke • LINE: iamcooltee • WHATSAPP: 08151605999 READYSTOCK IG IAMCOOLTEE ????Jakarta,Indonesia instagram.com/iamcooltee'),
(811, 'pernakperniklady', 'Kalung Zara termurah se IG!', 'Kalung Zara termurah se IG! KUALITAS PREMIUM!  HARGA KHUSUS untuk grosir Bisa beli satuan READY STOCK !!???????? TERMURAH ! TERLENGKAP ! TERBARU ! line : mariatyh bbm : 7e6aedab '),
(812, 'drone_murah_gila', 'Drone TerMurah di Malaysia', 'Drone TerMurah di Malaysia Lets FLY. Feel the SKY. Jom lah BUY. I will REPLY! ????????\" ???? TOYS & HOBBY GRADE ???? HARGA MURAH GILA ????COD - GOMBAK AREA ???? 0163308784 Check our Youtube www.youtube.com/channel/UCpAm2xNM3U8BDklNOlTtvrg'),
(813, 'khaifashop_', 'Barang Murah Gila sini', 'Barang Murah Gila sini ?????????Trusted Seller ????Wechatid:khaiFa95  ????Whatsapp:011-27138046 KHAIFASHOP ENTERPRISE SSM KT0365596-X  Swap review free #KhaiFa www.facebook.com/khaifashop'),
(814, 'struesel.co', 'struesel.co', 'struesel.co ALL item are ready stock! ???? Whatsapp 0192272530 COD CHERAS AND SELANGOR ONLY Giveaway at 1.5k followers???????? '),
(815, 'forever21_closet', 'Victoria secret include post!', 'Victoria secret include post! ????Homemade Wax for hair removal ???? ????Victoria Secret\'s Mist ???? ???? 017-8773842  ????Postage Selasa n Khamis? '),
(816, 'uispar', 'Unique Ispa Resources', 'Unique Ispa Resources SSM0024539xx-T ???? Whtsap0122944162 REPLYWITHIN24HOURS ????follow @uispar #sayajualPERFUME???? #sayajualHANDBAG???? #sayajualHEALTHYPRODUCT???????????? '),
(817, 'styleyourkloset', 'iPhone 6s RM350', 'iPhone 6s RM350 Gadget & Fashion Whatsapp: 0149644268 Shop with hashtag: #SYKgadget #SYKclone #SYKapple #SYKwatch Feedback: #SYKcustomerfeedback ? By @ihsanzulkaflee '),
(818, 'srfamille_by_salehahariffin', 'SR Famille by Salehah Ariffin', 'SR Famille by Salehah Ariffin Menjual dengan hati yang ikhlas,  In shaa Allah mudahnye menjemput rezeki... '),
(819, 'realchocojar', 'TheRealChocoJar', 'TheRealChocoJar Selling ???????????? ? All item ready stock ???? Postage : Monday , Wednesday & Saturday ????018-9898199 ??SERIOUS BUYER? OPEN FOR --> Wholesale/Dropship?agent '),
(820, 'amda.co_collections', 'BAJU MURAH RM15 ???????? ????????', 'BAJU MURAH RM15 ???????? ???????? NO GST. TIADA PEMULANGAN BAKI YA! SERIOUS BUYER ONLY!!   #TRUSTEDSELLER!!! #DROPSHIP OPEN DM/WS/WECHAT :hiddayah99 @ 0173468102 #BELI 10 -> RM 1X / PC '),
(821, 'thesxblings_', 'Thesxblings_', 'Thesxblings_ Sell preloved items with affordable price & ???? good quality. Interested? (Whatsapp : 0126304738) Sold items will be deleted ?? '),
(822, 'thegangtudung', 'SCARFBUCKS | AMEERAZAINIHIJAB', 'SCARFBUCKS | AMEERAZAINIHIJAB ????STOKIS SCARFBUCKS | AMEERAZAINIHIJABREVIVAL ???? ????017-9855 686 / 017-902 8805 / 017-9898805 ????COD area Kuala Terengganu dan Gong Badak '),
(823, 'si_penjual_hijabs', 'SOUL SISTERS TRADING', 'SOUL SISTERS TRADING ???? SOUL SISTERS TRADING SSM 002xxxxx ???? Agent Dropship Diperlukan ???? WHATSAPP 019 7575849/ 014 5448382 ???? PEMBORONG TUDUNG '),
(824, 'cahayaamata', 'cahayamata', 'cahayamata BISMILLAH ?hargaruntuh ?hargakasihsayang ?produkoriginal ?nocod ?trustedseller ?seriusbuyeronly ?beautyshop ?whatsapp ?sms ?wechat ? 0125650670 ? '),
(825, 'dhiya_hanashoppe', '????BEAUTY????HEALTH????FASHION ????MURAH', '????BEAUTY????HEALTH????FASHION ????MURAH ????Original only ????Trusted agent ????Harga pamer Tidak termasuk postage.kecuali dinyatakan ????Order/inquiry wassap 0135434255 (xcall) '),
(826, 'maizuraciwawa', 'VENDOR NEEDED ;-)', 'VENDOR NEEDED ;-) 11am ~ 9.30pm  OPEN : SUNDAY-FRIDAY CLOSE : SATURDAY ====================== Boutique & Cafe ConCept  Whatsapp OnLy : +60 13731 9351????/ +60 127182518 www.facebook.com/maizuraciwawa'),
(827, 'dsyz.co', '#EANF#', '??? ???? ?????? ??????? Let\'s grab yours now!! ???? Owner: @syazzaimi ???? Whatsapp: +60172025336 ???? Wechat: syazzaimi ? Serious buyer only! '),
(828, 'jersimania_shirtshop', 'jersi,shirt,kasut,cetak baju', 'jersi,shirt,kasut,cetak baju ????branded jersi,cool shirts, poloshirt,shorts,stoking.Cetak baju print/embroidery! +FREE gift. ????Whtsp/sms:6010-2257355 jerseymania8@gmail.com (paypal) www.jerseymania.blogspot.com'),
(829, 'selling_hot', 'awesome casing in da house', 'awesome casing in da house Serious buyer only Strictly no call, will reply in 24hours Whatapps +6018-2902468 Interest in pandora bracelet? Follow \" awesome_pandora \" '),
(830, 'gerobok.hfs', 'MALAYSIA ????????', 'MALAYSIA ???????? ???? TRUSTED SELLER ?sold items will be deleted ? No Swap, No reserved. ???? Range : RM 5 - RM 30 ???? EM // WM - Rm 10 ???? 017-5988036 ???? Based in Perlis '),
(831, 'alia.ainaa', 'Alia Ainaa', 'Alia Ainaa »this is my second ig »telegram/whatapps: 0199711703/0179546033 »first ig: aliaa_ainaa_ »fb :aliaa aainaa binti che azman »fb 2: alia ainaa '),
(832, 'zerahbutiqhouse', 'CikPuan Zerah', 'CikPuan Zerah ???? Zerah Butiq House ???? Zerah Rahemy ???? 012 - 6618138 WASSAP  ????Instant Shawl in ButiQ House www.facebook.com/zerah'),
(833, 'rifdi_khat_kufi', 'Rifdi ARZR Marketing', 'Rifdi ARZR Marketing Menjual Khat Kufi  Harga berpatutan  Ready made Custom made  whatsapp contact no 01132191555 www.facebook.com/pages/Rifdi-Khat-Kufi/1406303689665139?ref=aymt_homepage_panel'),
(834, 'gi_r.ls.362n_earu', 'Adult Hookup Meet Today', 'Adult Hookup Meet Today Me?t Hot Loc?l G?rls Tonight  W? Gu?rante? Y?u\'ll H?ok Up! girlsnearu.xyz/XXXMeet_Horny_Girls'),
(835, 'kakifesyen', 'kakifesyen', 'kakifesyen MALAYSIA || RA0029884-X ???? 013-7904417/0137015022 (Whatapp) ???? all items are pre order (2-3 weeks delivery) www.facebook.com/kakifesyen'),
(836, 'assez.fashion', 'ASSEZ LACEY SHAWL SALE! ????????', 'ASSEZ LACEY SHAWL SALE! ???????? ???? Every girl is beautiful ???????????? ???? Sharia Compliance Wide Shawl ???? WA - 019 789 3372 / 014 541 0053 '),
(837, 'yati971_bizzshoppe', 'Yati Latip', 'Yati Latip Jubah,Dress,Blouse,Handsocks,Shawl,As Seen On TV Products,Kids Cloth,Mrs. Iron,Watch,Perfume,Handbag,Sambal Kentang,Books '),
(838, 'have_a.n_affair_to.night85', '100%?FREE?Adult?Dating', '100%?FREE?Adult?Dating H?ve ?n ?ffair T?night  Me?t h?t loc?l singl?s ?nlin? n?w ???????????? freecasualsexfinder.xyz/Singles_for_Sex'),
(839, 'fydawahab', 'Fyda Wahab', 'Fyda Wahab  '),
(840, 'handphone.clone.murah', 'PROMOSI IPHONE 6 RM350', 'PROMOSI IPHONE 6 RM350 Serious buyer ! Trusted seller ! No refund ! No call ! Cod Cheras Kl 0178408031 Ragu2 ? Ni fedback kami #hp_murah_gylefeedback Dropship wc '),
(841, 'afhouse', 'Accessorizing you,Under 50K!????', 'Accessorizing you,Under 50K!???? READY STOCK?? ????For more Info & Order Contac : ????Line ID : Afhouse29 ????Sold Out = Delete ????Shipping from palembang???? m.facebook.com/groups/1676402552576196?ref=bookmarks'),
(842, 'cath.yshop', 'c a t h y s h o p????????', 'c a t h y s h o p???????? ???? BCA WA/SMS 0821.79423591 '),
(843, 'rossestore_adv', 'IKLAN ROSSESTORE', 'IKLAN ROSSESTORE Hanya IKLAN bukan jualan  Follow akunnya asli @rossestore www.instagram.com/rossestore'),
(844, 'indofabric', 'Indofabric', 'Indofabric Phone/whatsapp: 085692474047 Line: fikrichairi06 Pin BB: 7CF7D6A3 Jakarta Selatan Ready Stock!!! '),
(845, 'shifterstore', 'SHIFTERSTORE READY STOCK !!', 'SHIFTERSTORE READY STOCK !! Beli 3 dpt diskon  Grosir? Harga spesial contact us:  BBM : 7D76276B LINE ID: mailperdana Wlcm reseller // dropshiper DELETED= SOLD OUT Line Order ???? line.me/ti/p/A3Rw7CTwpM'),
(846, 'sevenforpm', 'APLIKASI IOS & ANDROID,CASE HP', 'APLIKASI IOS & ANDROID,CASE HP ???? LINE : @sxc7060i  ????Solo,id HARAP BACA CAPTION DGN BAIK SEBELUM MEMESAN  CANCEL=BLACKLIST???? line.me/ti/p/@sxc7060i'),
(847, 'vyliavariella_aiucollection', 'Aiu Collection', 'Aiu Collection My online shop, (clothes,bag,.shoes) 57663FBB weekend slow respon Format pemesanan : Nama,no hp,alamat lengkap+kec+kode pos,kode brg, uk & warna '),
(848, 'mbagadyazh', '#EANF#', 'Local S?x -> goo.gl/c0LjKI'),
(849, 'fufu_stone', 'FUFU STONE', 'FUFU STONE WA : 085778976547 BB : 7D583934  Pengiriman : Jne /Pos Lokasi : Bintaro (kota Tangsel) Info lebih lanjut..!! Selamat Berbelanja '),
(850, 'gi_r.ls.323n_earu', 'Girls Near You', 'Girls Near You H?ve ?n affair T?n?ght  M??t hot l?cal s?ngles ?nline now girlsnearu.xyz/XXXSingles-for-Sex'),
(851, 'irahikaru', 'hikaru', 'hikaru WA 082127810156 Line iraprincess BB 7D08DD87 Lokasi Solo Jateng Pengiriman pakai JNE Pembayaran melalui BCA  happy shopping mom ???? '),
(852, 'semuaiklan50', 'Semuaiklan50', 'Semuaiklan50 Promosikan produk olshop di @semuaiklan50 TARIF PROMO Whatsapp : +6281281408279 Pin bb : 5ABC73C8 Line : semuaiklan50 '),
(853, 'customcasing', 'Custom Casing Indonesia', 'Custom Casing Indonesia Premium Glow in the Dark Full Wrap custom case , processing time 4-6 days , \"Create Your Own Style\", whatsapp 085716712840 / line: @customcasing www.customcasing.co.id'),
(854, 'vinnychungos', 'Vinny Chung OS', 'Vinny Chung OS ?TRUSTED SELLER since 2011 ?Fast Responds ?Pin: 2872B8AA ?WA: 0812-2838-2627 ?New Line : vinny.vinny ? Rule+testimoni #thankiestvinny SERIOUS.ORDER.ONLY.MAIN2.BLACKLIST'),
(855, 'one_shoesstore', 'Cipuut ONLINSHOP', 'Cipuut ONLINSHOP JUAL GROSIR,ECERAN&MELAYANI RESELLER PIN=7D09E1A0 LINE/WECHAT.Ciput.SHOES.STORE WHATSAP..085258523733 TRANSFER VIA Mandiri Jakarta '),
(856, 'adorablevintageproject', '????Adorable Vintage Project????', '????Adorable Vintage Project???? ????Jakarta ????SMS/WA: +628888525995 ????Line: fukumodeline atau klik link di bawah ????Sold=Delete ????Booked 1x24jam , ???? JNE ONLY every Wednesday and Friday line.me/ti/p/%40wzw2970u'),
(857, 'malhijab', 'Hijab Fashion Berkualitas', 'Hijab Fashion Berkualitas Low price, high quality???? . . For order:  ????Line: amaliahdwi ????WA: 085772749894 (no call) ????Pengiriman via JNE . . Ready all item!!! '),
(858, 'tyashop_sepatucantik', 'Tyashop Sepatu Cantik', 'Tyashop Sepatu Cantik Untuk Order   SMS/WHATSAAP 0838-9540-9545  PIN BB 21B253E8  (SMS ONLY PLEASE NO CALL) www.tyashopbelanjaonline.blogspot.com/'),
(859, 'manddy_onlineshop', 'MANDDY HOMESHOP COLLECTION', 'MANDDY HOMESHOP COLLECTION Selling various brand of handbag, clothes, shoes and baby wear. Group of dropship available TELEGRAM, WHATSAPP 0148682989 TRUSTED SELLER ONLINE™\" carousell.com/sicy.sicilysia'),
(860, 'wallstickerbanjarbaru', 'iinagustina', 'iinagustina AGEN WALLSTICKER ????BBM : 75B48F2B(Fast Respons) ????LINE : IINARZM ???? WA : 085248494343 ????Lokasi Banjarbaru,Kalsel BARANG READYSTOCK s/d Border NEWSTOCK line.me/ti/p/%40icx7874v'),
(861, 'sayaiklankedaiawak', 'Paid Review RM 10', 'Paid Review RM 10 ???? Pelbagai Insta Shop  ???? PAID REVIEW - RM 10/7 reviews (Never DELETE) ????Whatsapp 013-594 3641 ???? UNFOLLOW DETECTED '),
(862, 'indhiraratnabutik', 'Indhiraratna Butik', 'Indhiraratna Butik ???????? shipping by JNE,Wahana &Pos  ????Mandiri & BCA More detail & order :  ???? WA : 085779777808  ???? Line : ???????????? line.me/ti/p/%40ibq9161g'),
(863, 'meldafebria93', 'Melda Febria', 'Melda Febria Modeling | presenter | entertainment Ready fashion wanita dan skincare Add pin 75a30951 Liad koleksinya guys '),
(864, 'viv_store', 'OS SKINY,SLINBAG,KOSMTIK MURAH', 'OS SKINY,SLINBAG,KOSMTIK MURAH Lokasi:mojokerto-sdoarjo-sby Pengiriman:JNE Cod:mojosari-krian-sidoarjo Line:noktavivi '),
(865, 'arjuna_agus', 'Arjuna Agus S', 'Arjuna Agus S Allah SWT ????Jual jaket murah ? WA/SMS 085733224211 ???? BBM 55341225 ???? Transfer via BCA / BRI / CIMB ???? Pengiriman Via JNE & POS #ZentCollection '),
(866, 'cherish_babyshop', 'Cherish Shop', 'Cherish Shop SMS / WA / LINE : 085852008181 Pin bb : 5A26034E / 27E74A91 www.cherish-shop.com'),
(867, 'azdzikraolshop', 'azdzikraolshop', 'azdzikraolshop ???? (New & Preloved) / NO NEGO? ???? Line : dianaazzahra ???? Pengiriman(Jakarta) : Senin - Jumat ???? Slow respon for weekend ???? Rek BCA - JNE yes / reg '),
(868, 'febyeby2', 'bby_fashionshop medan', 'bby_fashionshop medan ID Line ???? bby_fashion ????PIN  ???? 25A15C2B ???? Text/WA ???? 082168969258 ???? Mandiri Only New & Second stuff Medan free ongkir dear???? '),
(869, 'annajahshop.melaka', '#EANF#', '????pemborong cadar patchwork ????pemborong barangan as seen on tv ????butik vendor ????0193411986 ????0195592155 ????Kompleks Bukit Palah,Melaka '),
(870, 'ichbinbeba.design', 'Design????Print????Photobooth Deco', 'Design????Print????Photobooth Deco ????????SuperMAMI • ????????Malaysia • ????JB ?????WhatsApp/+6016-630 2164 ????????Facebook/ichbinbeba.design ??Emel/ichbinbeba@yahoo.com '),
(871, 'nurezmyzaii_9088', 'AGENT TOPUP MURAH', 'AGENT TOPUP MURAH Agent Topup 3 Way Income LETS SWAP REVIEW FREE ???? ????TESTIMONI ORI BY ME ???? ????WHATSAPP/TELEGRAM 017-8257565 (+6 jika xjumpa contact no saya) www.benzmobile.com/p/q-a.html?m=1'),
(872, 'gi_r.ls.358n_earu', 'Adult Hookup Meet Today', 'Adult Hookup Meet Today ? Qu?ck Start Guid? f?r Pl??sur? Se?k?rs girlsnearu.xyz/XXXLocal_Free_Hook_Up');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `code`, `sort`, `status`) VALUES
(2, 'English', 'en', 1, '1'),
(3, 'Indonesia', 'id', 2, '0');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `username`, `activity`, `time`) VALUES
(1, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-06 02:02:26'),
(2, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-20 04:22:26'),
(3, 'info@markdesign.net', 'Create Category 1', '2018-06-20 04:34:26'),
(4, 'info@markdesign.net', 'Create Category 2', '2018-06-20 04:34:44'),
(5, 'info@markdesign.net', 'Create Category 3', '2018-06-20 04:35:08'),
(6, 'info@markdesign.net', 'Create Category 4', '2018-06-20 04:35:20'),
(7, 'info@markdesign.net', 'Create Category 5', '2018-06-20 04:35:29'),
(8, 'info@markdesign.net', 'Gallery Controller Create 6', '2018-06-20 04:53:21'),
(9, 'info@markdesign.net', 'GalleryController Update 6', '2018-06-20 05:02:52'),
(10, 'deoryzpandu@gmail.com', 'Login: deoryzpandu@gmail.com', '2018-06-21 03:26:08'),
(11, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-21 04:07:16'),
(12, 'info@markdesign.net', 'Gallery Controller Create 7', '2018-06-21 04:16:00'),
(13, 'info@markdesign.net', 'Gallery Controller Create 8', '2018-06-21 04:18:29'),
(14, 'info@markdesign.net', 'Gallery Controller Create 9', '2018-06-21 04:22:38'),
(15, 'info@markdesign.net', 'Gallery Controller Create 10', '2018-06-21 04:24:27'),
(16, 'info@markdesign.net', 'Gallery Controller Create 11', '2018-06-21 04:28:17'),
(17, 'info@markdesign.net', 'Gallery Controller Create 12', '2018-06-21 04:29:42'),
(18, 'info@markdesign.net', 'Gallery Controller Create 13', '2018-06-21 04:32:28'),
(19, 'info@markdesign.net', 'Gallery Controller Create 14', '2018-06-21 04:36:04'),
(20, 'info@markdesign.net', 'Gallery Controller Create 15', '2018-06-21 04:37:23'),
(21, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-21 04:37:44'),
(22, 'info@markdesign.net', 'SlideController Update 8', '2018-06-21 04:38:13'),
(23, 'info@markdesign.net', 'Gallery Controller Create 16', '2018-06-21 04:38:59'),
(24, 'info@markdesign.net', 'Gallery Controller Create 17', '2018-06-21 04:43:26'),
(25, 'info@markdesign.net', 'Gallery Controller Create 18', '2018-06-21 04:46:25'),
(26, 'info@markdesign.net', 'Gallery Controller Create 19', '2018-06-21 04:54:52'),
(27, 'info@markdesign.net', 'Gallery Controller Create 20', '2018-06-21 04:56:08'),
(28, 'info@markdesign.net', 'Gallery Controller Create 21', '2018-06-21 04:57:17'),
(29, 'info@markdesign.net', 'Gallery Controller Create 22', '2018-06-21 05:00:52'),
(30, 'info@markdesign.net', 'Gallery Controller Create 23', '2018-06-21 05:02:32'),
(31, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-21 06:27:19'),
(32, 'info@markdesign.net', 'Gallery Controller Create 24', '2018-06-21 06:29:09'),
(33, 'info@markdesign.net', 'Gallery Controller Create 25', '2018-06-21 06:33:04'),
(34, 'info@markdesign.net', 'Gallery Controller Create 26', '2018-06-21 06:35:25'),
(35, 'info@markdesign.net', 'Gallery Controller Create 27', '2018-06-21 06:36:30'),
(36, 'info@markdesign.net', 'Gallery Controller Create 28', '2018-06-21 06:49:59'),
(37, 'info@markdesign.net', 'Gallery Controller Create 29', '2018-06-21 06:52:10'),
(38, 'info@markdesign.net', 'Gallery Controller Create 30', '2018-06-21 06:53:21'),
(39, 'info@markdesign.net', 'Gallery Controller Create 31', '2018-06-21 07:25:26'),
(40, 'info@markdesign.net', 'Gallery Controller Create 32', '2018-06-21 07:28:02'),
(41, 'info@markdesign.net', 'Gallery Controller Create 33', '2018-06-21 07:30:00'),
(42, 'info@markdesign.net', 'Gallery Controller Create 34', '2018-06-21 07:31:58'),
(43, 'info@markdesign.net', 'Gallery Controller Create 35', '2018-06-21 07:35:53'),
(44, 'info@markdesign.net', 'Gallery Controller Create 36', '2018-06-21 07:37:04'),
(45, 'info@markdesign.net', 'Gallery Controller Create 37', '2018-06-21 07:38:25'),
(46, 'info@markdesign.net', 'Gallery Controller Create 38', '2018-06-21 07:44:44'),
(47, 'info@markdesign.net', 'Gallery Controller Create 39', '2018-06-21 07:45:35'),
(48, 'info@markdesign.net', 'Gallery Controller Create 40', '2018-06-21 07:48:11'),
(49, 'info@markdesign.net', 'Gallery Controller Create 41', '2018-06-21 07:50:21'),
(50, 'info@markdesign.net', 'Gallery Controller Create 42', '2018-06-21 07:55:40'),
(51, 'info@markdesign.net', 'Gallery Controller Create 43', '2018-06-21 08:01:05'),
(52, 'info@markdesign.net', 'Gallery Controller Create 44', '2018-06-21 08:02:25'),
(53, 'info@markdesign.net', 'Gallery Controller Create 45', '2018-06-21 08:04:35'),
(54, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 01:22:20'),
(55, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 03:05:23'),
(56, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 05:16:45'),
(57, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 07:28:02'),
(58, 'info@markdesign.net', 'GalleryController Update 42', '2018-06-22 07:34:28'),
(59, 'info@markdesign.net', 'GalleryController Update 36', '2018-06-22 07:34:47'),
(60, 'info@markdesign.net', 'GalleryController Update 37', '2018-06-22 07:35:25'),
(61, 'info@markdesign.net', 'GalleryController Update 38', '2018-06-22 07:35:46'),
(62, 'info@markdesign.net', 'GalleryController Update 40', '2018-06-22 07:36:10'),
(63, 'info@markdesign.net', 'GalleryController Update 41', '2018-06-22 07:36:25'),
(64, 'info@markdesign.net', 'GalleryController Update 43', '2018-06-22 07:36:40'),
(65, 'info@markdesign.net', 'GalleryController Update 45', '2018-06-22 07:37:00'),
(66, 'info@markdesign.net', 'GalleryController Update 44', '2018-06-22 07:37:19'),
(67, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 07:53:03'),
(68, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 08:00:06'),
(69, 'info@markdesign.net', 'GalleryController Update 12', '2018-06-22 08:11:30'),
(70, 'info@markdesign.net', 'GalleryController Update 11', '2018-06-22 08:11:55'),
(71, 'info@markdesign.net', 'GalleryController Update 13', '2018-06-22 08:12:19'),
(72, 'info@markdesign.net', 'GalleryController Update 14', '2018-06-22 08:12:36'),
(73, 'info@markdesign.net', 'GalleryController Update 15', '2018-06-22 08:12:54'),
(74, 'info@markdesign.net', 'GalleryController Update 18', '2018-06-22 08:13:29'),
(75, 'info@markdesign.net', 'GalleryController Update 16', '2018-06-22 08:13:45'),
(76, 'info@markdesign.net', 'GalleryController Update 20', '2018-06-22 08:14:00'),
(77, 'info@markdesign.net', 'GalleryController Update 21', '2018-06-22 08:14:29'),
(78, 'info@markdesign.net', 'GalleryController Update 7', '2018-06-22 08:14:57'),
(79, 'info@markdesign.net', 'GalleryController Update 8', '2018-06-22 08:15:12'),
(80, 'info@markdesign.net', 'GalleryController Update 9', '2018-06-22 08:15:29'),
(81, 'info@markdesign.net', 'GalleryController Update 10', '2018-06-22 08:15:46'),
(82, 'info@markdesign.net', 'GalleryController Update 35', '2018-06-22 08:16:14'),
(83, 'info@markdesign.net', 'GalleryController Update 33', '2018-06-22 08:16:28'),
(84, 'info@markdesign.net', 'GalleryController Update 34', '2018-06-22 08:17:52'),
(85, 'info@markdesign.net', 'GalleryController Update 30', '2018-06-22 08:18:07'),
(86, 'info@markdesign.net', 'GalleryController Update 31', '2018-06-22 08:18:22'),
(87, 'info@markdesign.net', 'GalleryController Update 27', '2018-06-22 08:18:39'),
(88, 'info@markdesign.net', 'GalleryController Update 28', '2018-06-22 08:18:58'),
(89, 'info@markdesign.net', 'GalleryController Update 27', '2018-06-22 08:19:21'),
(90, 'info@markdesign.net', 'GalleryController Update 6', '2018-06-22 08:19:46'),
(91, 'info@markdesign.net', 'GalleryController Update 24', '2018-06-22 08:20:28'),
(92, 'info@markdesign.net', 'GalleryController Update 25', '2018-06-22 08:20:48'),
(93, 'info@markdesign.net', 'GalleryController Update 23', '2018-06-22 08:21:25'),
(94, 'info@markdesign.net', 'GalleryController Update 23', '2018-06-22 08:21:42'),
(95, 'info@markdesign.net', 'GalleryController Update 22', '2018-06-22 08:23:00'),
(96, 'info@markdesign.net', 'Setting Update', '2018-06-22 08:24:10'),
(97, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 08:51:21'),
(98, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 10:12:22'),
(99, 'info@markdesign.net', 'Setting Update', '2018-06-22 10:12:28'),
(100, 'info@markdesign.net', 'Setting Update', '2018-06-22 10:13:40'),
(101, 'info@markdesign.net', 'Setting Update', '2018-06-22 10:13:51'),
(102, 'info@markdesign.net', 'Setting Update', '2018-06-22 10:14:50'),
(103, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 12:39:01'),
(104, 'info@markdesign.net', 'Setting Update', '2018-06-22 12:39:11'),
(105, 'info@markdesign.net', 'Setting Update', '2018-06-22 12:40:40'),
(106, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 14:39:51'),
(107, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 15:12:01'),
(108, 'info@markdesign.net', 'Setting Update', '2018-06-22 15:25:35'),
(109, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-22 17:37:53'),
(110, 'info@markdesign.net', 'Slide Controller Create 11', '2018-06-22 17:38:34'),
(111, 'info@markdesign.net', 'Slide Controller Create 12', '2018-06-22 17:39:00'),
(112, 'info@markdesign.net', 'Slide Controller Create 13', '2018-06-22 17:39:25'),
(113, 'info@markdesign.net', 'Slide Controller Create 14', '2018-06-22 17:39:47'),
(114, 'info@markdesign.net', 'Slide Controller Create 15', '2018-06-22 17:40:11'),
(115, 'info@markdesign.net', 'GalleryController Update 27', '2018-06-22 17:40:37'),
(116, 'info@markdesign.net', 'SlideController Update 15', '2018-06-22 17:42:51'),
(117, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-23 10:56:24'),
(118, 'info@markdesign.net', 'Setting Update', '2018-06-23 10:56:36'),
(119, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-24 18:34:17'),
(120, 'info@markdesign.net', 'GalleryController Update 45', '2018-06-24 18:35:07'),
(121, 'info@markdesign.net', 'GalleryController Update 44', '2018-06-24 18:36:49'),
(122, 'info@markdesign.net', 'GalleryController Update 43', '2018-06-24 18:37:32'),
(123, 'info@markdesign.net', 'GalleryController Update 41', '2018-06-24 18:44:06'),
(124, 'info@markdesign.net', 'GalleryController Update 40', '2018-06-24 18:44:38'),
(125, 'info@markdesign.net', 'GalleryController Update 39', '2018-06-24 18:45:02'),
(126, 'info@markdesign.net', 'GalleryController Update 38', '2018-06-24 18:45:31'),
(127, 'info@markdesign.net', 'GalleryController Update 37', '2018-06-24 18:47:44'),
(128, 'info@markdesign.net', 'GalleryController Update 36', '2018-06-24 18:51:42'),
(129, 'info@markdesign.net', 'GalleryController Update 35', '2018-06-24 18:52:08'),
(130, 'info@markdesign.net', 'GalleryController Update 34', '2018-06-24 18:52:34'),
(131, 'info@markdesign.net', 'GalleryController Update 33', '2018-06-24 18:53:04'),
(132, 'info@markdesign.net', 'GalleryController Update 32', '2018-06-24 18:53:31'),
(133, 'info@markdesign.net', 'GalleryController Update 31', '2018-06-24 18:53:57'),
(134, 'info@markdesign.net', 'GalleryController Update 30', '2018-06-24 18:54:34'),
(135, 'info@markdesign.net', 'GalleryController Update 29', '2018-06-24 18:54:57'),
(136, 'info@markdesign.net', 'GalleryController Update 28', '2018-06-24 18:55:25'),
(137, 'info@markdesign.net', 'GalleryController Update 42', '2018-06-24 18:56:13'),
(138, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-25 03:49:33'),
(139, 'info@markdesign.net', 'Setting Update', '2018-06-25 03:49:49'),
(140, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-25 03:52:08'),
(141, 'info@markdesign.net', 'Setting Update', '2018-06-25 03:52:55'),
(142, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-25 04:03:22'),
(143, 'info@markdesign.net', 'Setting Update', '2018-06-25 04:04:06'),
(144, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-25 04:14:26'),
(145, 'info@markdesign.net', 'Gallery Controller Create 46', '2018-06-25 04:43:12'),
(146, 'info@markdesign.net', 'Gallery Controller Create 47', '2018-06-25 04:44:03'),
(147, 'info@markdesign.net', 'Gallery Controller Create 48', '2018-06-25 04:44:55'),
(148, 'info@markdesign.net', 'Gallery Controller Create 49', '2018-06-25 04:45:56'),
(149, 'info@markdesign.net', 'Gallery Controller Create 50', '2018-06-25 04:47:14'),
(150, 'info@markdesign.net', 'Gallery Controller Create 51', '2018-06-25 04:48:18'),
(151, 'info@markdesign.net', 'Gallery Controller Create 52', '2018-06-25 04:49:23'),
(152, 'info@markdesign.net', 'Gallery Controller Create 53', '2018-06-25 04:50:12'),
(153, 'info@markdesign.net', 'Gallery Controller Create 54', '2018-06-25 04:53:02'),
(154, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-25 06:52:28'),
(155, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-25 06:58:45'),
(156, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-25 09:40:18'),
(157, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-26 01:02:06'),
(158, 'info@markdesign.net', 'Gallery Controller Create 55', '2018-06-26 01:34:15'),
(159, 'info@markdesign.net', 'Gallery Controller Create 56', '2018-06-26 01:36:43'),
(160, 'info@markdesign.net', 'Gallery Controller Create 57', '2018-06-26 01:37:40'),
(161, 'info@markdesign.net', 'Gallery Controller Create 58', '2018-06-26 01:38:34'),
(162, 'info@markdesign.net', 'Gallery Controller Create 59', '2018-06-26 01:39:39'),
(163, 'info@markdesign.net', 'Gallery Controller Create 60', '2018-06-26 01:41:11'),
(164, 'info@markdesign.net', 'Gallery Controller Create 61', '2018-06-26 01:45:27'),
(165, 'info@markdesign.net', 'Gallery Controller Create 62', '2018-06-26 01:46:34'),
(166, 'info@markdesign.net', 'Gallery Controller Create 63', '2018-06-26 01:47:36'),
(167, 'info@markdesign.net', 'Gallery Controller Create 64', '2018-06-26 01:48:32'),
(168, 'info@markdesign.net', 'Gallery Controller Create 65', '2018-06-26 01:49:04'),
(169, 'info@markdesign.net', 'Gallery Controller Create 66', '2018-06-26 01:49:58'),
(170, 'info@markdesign.net', 'Gallery Controller Create 67', '2018-06-26 01:50:33'),
(171, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-27 06:06:21'),
(172, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-28 01:28:19'),
(173, 'info@markdesign.net', 'Gallery Controller Create 68', '2018-06-28 02:10:08'),
(174, 'info@markdesign.net', 'Gallery Controller Create 69', '2018-06-28 02:11:42'),
(175, 'info@markdesign.net', 'Gallery Controller Create 70', '2018-06-28 02:12:22'),
(176, 'info@markdesign.net', 'GalleryController Update 70', '2018-06-28 03:29:50'),
(177, 'info@markdesign.net', 'GalleryController Update 70', '2018-06-28 03:32:50'),
(178, 'info@markdesign.net', 'GalleryController Update 69', '2018-06-28 03:55:05'),
(179, 'info@markdesign.net', 'GalleryController Update 69', '2018-06-28 03:57:21'),
(180, 'info@markdesign.net', 'GalleryController Update 68', '2018-06-28 04:57:08'),
(181, 'info@markdesign.net', 'GalleryController Update 64', '2018-06-28 06:14:42'),
(182, 'info@markdesign.net', 'GalleryController Update 63', '2018-06-28 06:33:48'),
(183, 'info@markdesign.net', 'GalleryController Update 62', '2018-06-28 06:48:19'),
(184, 'info@markdesign.net', 'GalleryController Update 61', '2018-06-28 06:57:58'),
(185, 'info@markdesign.net', 'GalleryController Update 60', '2018-06-28 07:05:59'),
(186, 'info@markdesign.net', 'GalleryController Update 59', '2018-06-28 07:11:00'),
(187, 'info@markdesign.net', 'GalleryController Update 58', '2018-06-28 07:18:31'),
(188, 'info@markdesign.net', 'GalleryController Update 57', '2018-06-28 07:25:58'),
(189, 'info@markdesign.net', 'GalleryController Update 56', '2018-06-28 07:32:04'),
(190, 'info@markdesign.net', 'GalleryController Update 57', '2018-06-28 07:34:22'),
(191, 'info@markdesign.net', 'GalleryController Update 55', '2018-06-28 07:36:29'),
(192, 'info@markdesign.net', 'GalleryController Update 54', '2018-06-28 07:42:57'),
(193, 'info@markdesign.net', 'GalleryController Update 53', '2018-06-28 07:50:36'),
(194, 'info@markdesign.net', 'GalleryController Update 52', '2018-06-28 07:58:29'),
(195, 'info@markdesign.net', 'GalleryController Update 51', '2018-06-28 08:02:33'),
(196, 'info@markdesign.net', 'GalleryController Update 50', '2018-06-28 08:05:46'),
(197, 'info@markdesign.net', 'GalleryController Update 49', '2018-06-28 08:07:19'),
(198, 'info@markdesign.net', 'GalleryController Update 48', '2018-06-28 08:14:04'),
(199, 'info@markdesign.net', 'GalleryController Update 47', '2018-06-28 08:18:40'),
(200, 'info@markdesign.net', 'GalleryController Update 46', '2018-06-28 08:23:15'),
(201, 'info@markdesign.net', 'GalleryController Update 27', '2018-06-28 08:40:52'),
(202, 'info@markdesign.net', 'GalleryController Update 25', '2018-06-28 08:45:07'),
(203, 'info@markdesign.net', 'GalleryController Update 24', '2018-06-28 08:48:40'),
(204, 'info@markdesign.net', 'GalleryController Update 23', '2018-06-28 08:52:15'),
(205, 'info@markdesign.net', 'GalleryController Update 22', '2018-06-28 08:57:56'),
(206, 'info@markdesign.net', 'GalleryController Update 21', '2018-06-28 09:01:11'),
(207, 'info@markdesign.net', 'GalleryController Update 20', '2018-06-28 09:04:26'),
(208, 'info@markdesign.net', 'GalleryController Update 19', '2018-06-28 09:09:22'),
(209, 'info@markdesign.net', 'GalleryController Update 18', '2018-06-28 09:13:00'),
(210, 'info@markdesign.net', 'GalleryController Update 17', '2018-06-28 09:15:38'),
(211, 'info@markdesign.net', 'GalleryController Update 16', '2018-06-28 09:18:57'),
(212, 'info@markdesign.net', 'GalleryController Update 15', '2018-06-28 09:22:07'),
(213, 'info@markdesign.net', 'GalleryController Update 14', '2018-06-28 09:25:35'),
(214, 'info@markdesign.net', 'GalleryController Update 14', '2018-06-28 09:26:35'),
(215, 'info@markdesign.net', 'GalleryController Update 14', '2018-06-28 09:26:36'),
(216, 'info@markdesign.net', 'GalleryController Update 14', '2018-06-28 09:26:37'),
(217, 'info@markdesign.net', 'GalleryController Update 14', '2018-06-28 09:26:39'),
(218, 'info@markdesign.net', 'GalleryController Update 14', '2018-06-28 09:26:41'),
(219, 'info@markdesign.net', 'GalleryController Update 14', '2018-06-28 09:26:41'),
(220, 'info@markdesign.net', 'GalleryController Update 14', '2018-06-28 09:26:42'),
(221, 'info@markdesign.net', 'GalleryController Update 14', '2018-06-28 09:26:42'),
(222, 'info@markdesign.net', 'GalleryController Update 14', '2018-06-28 09:26:43'),
(223, 'info@markdesign.net', 'GalleryController Update 67', '2018-06-28 09:30:53'),
(224, 'info@markdesign.net', 'GalleryController Update 66', '2018-06-28 09:33:37'),
(225, 'info@markdesign.net', 'GalleryController Update 13', '2018-06-28 09:47:45'),
(226, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-29 03:30:05'),
(227, 'info@markdesign.net', 'GalleryController Update 13', '2018-06-29 04:26:51'),
(228, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-29 06:13:53'),
(229, 'info@markdesign.net', 'GalleryController Update 12', '2018-06-29 06:48:37'),
(230, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-29 08:29:32'),
(231, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-29 08:39:45'),
(232, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-30 01:50:20'),
(233, 'info@markdesign.net', 'Setting Update', '2018-06-30 01:56:28'),
(234, 'info@markdesign.net', 'Setting Update', '2018-06-30 02:31:31'),
(235, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-06-30 16:07:13'),
(236, 'deoryzpandu@gmail.com', 'Login: deoryzpandu@gmail.com', '2018-07-01 04:42:34'),
(237, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-01 04:53:49'),
(238, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-02 01:28:48'),
(239, 'info@markdesign.net', 'GalleryController Update 56', '2018-07-02 01:29:45'),
(240, 'info@markdesign.net', 'GalleryController Update 56', '2018-07-02 01:30:47'),
(241, 'info@markdesign.net', 'GalleryController Update 11', '2018-07-02 02:09:51'),
(242, 'info@markdesign.net', 'GalleryController Update 10', '2018-07-02 02:24:17'),
(243, 'info@markdesign.net', 'GalleryController Update 10', '2018-07-02 02:29:33'),
(244, 'info@markdesign.net', 'GalleryController Update 9', '2018-07-02 02:51:38'),
(245, 'info@markdesign.net', 'GalleryController Update 8', '2018-07-02 03:04:05'),
(246, 'info@markdesign.net', 'GalleryController Update 8', '2018-07-02 03:16:44'),
(247, 'info@markdesign.net', 'GalleryController Update 7', '2018-07-02 03:21:12'),
(248, 'info@markdesign.net', 'GalleryController Update 6', '2018-07-02 03:33:10'),
(249, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-06 01:07:38'),
(250, 'info@markdesign.net', 'GalleryController Update 58', '2018-07-06 01:11:24'),
(251, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-06 02:28:08'),
(252, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-06 07:20:28'),
(253, 'info@markdesign.net', 'GalleryController Update 65', '2018-07-06 07:20:49'),
(254, 'info@markdesign.net', 'GalleryController Update 62', '2018-07-06 07:23:47'),
(255, 'info@markdesign.net', 'GalleryController Update 62', '2018-07-06 07:24:13'),
(256, 'info@markdesign.net', 'GalleryController Update 61', '2018-07-06 07:26:21'),
(257, 'info@markdesign.net', 'GalleryController Update 57', '2018-07-06 07:29:13'),
(258, 'info@markdesign.net', 'GalleryController Update 56', '2018-07-06 07:31:10'),
(259, 'info@markdesign.net', 'GalleryController Update 55', '2018-07-06 07:35:45'),
(260, 'info@markdesign.net', 'GalleryController Update 54', '2018-07-06 07:42:53'),
(261, 'info@markdesign.net', 'GalleryController Update 54', '2018-07-06 07:48:57'),
(262, 'info@markdesign.net', 'GalleryController Update 52', '2018-07-06 07:49:08'),
(263, 'info@markdesign.net', 'GalleryController Update 48', '2018-07-06 07:51:16'),
(264, 'info@markdesign.net', 'GalleryController Update 47', '2018-07-06 07:52:01'),
(265, 'info@markdesign.net', 'GalleryController Update 46', '2018-07-06 07:53:31'),
(266, 'deoryzpandu@gmail.com', 'Login: deoryzpandu@gmail.com', '2018-07-15 12:28:22'),
(267, 'deoryzpandu@gmail.com', 'Slide Controller Create 16', '2018-07-15 12:29:37'),
(268, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-16 02:12:17'),
(269, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-16 02:13:55'),
(270, 'info@markdesign.net', 'Slide Controller Create 17', '2018-07-16 02:16:38'),
(271, 'info@markdesign.net', 'Slide Controller Create 18', '2018-07-16 02:17:02'),
(272, 'info@markdesign.net', 'Slide Controller Create 19', '2018-07-16 02:17:23'),
(273, 'info@markdesign.net', 'Gallery Controller Create 71', '2018-07-16 02:23:03'),
(274, 'info@markdesign.net', 'Gallery Controller Create 72', '2018-07-16 02:33:31'),
(275, 'info@markdesign.net', 'Gallery Controller Create 73', '2018-07-16 02:43:25'),
(276, 'info@markdesign.net', 'Gallery Controller Create 74', '2018-07-16 02:44:35'),
(277, 'info@markdesign.net', 'Gallery Controller Create 75', '2018-07-16 02:47:14'),
(278, 'info@markdesign.net', 'Gallery Controller Create 76', '2018-07-16 02:48:47'),
(279, 'info@markdesign.net', 'Slide Controller Create 20', '2018-07-16 02:54:10'),
(280, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-16 04:06:19'),
(281, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-16 06:07:41'),
(282, 'info@markdesign.net', 'GalleryController Update 76', '2018-07-16 06:08:14'),
(283, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-16 07:26:39'),
(284, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-16 08:19:36'),
(285, 'info@markdesign.net', 'GalleryController Update 75', '2018-07-16 08:19:59'),
(286, 'info@markdesign.net', 'GalleryController Update 75', '2018-07-16 08:20:13'),
(287, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-16 09:16:44'),
(288, 'info@markdesign.net', 'GalleryController Update 74', '2018-07-16 09:17:09'),
(289, 'info@markdesign.net', 'GalleryController Update 62', '2018-07-16 09:21:19'),
(290, 'info@markdesign.net', 'GalleryController Update 73', '2018-07-16 09:55:58'),
(291, 'info@markdesign.net', 'GalleryController Update 72', '2018-07-16 09:59:02'),
(292, 'info@markdesign.net', 'GalleryController Update 71', '2018-07-16 10:02:55'),
(293, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-23 01:48:29'),
(294, 'info@markdesign.net', 'GalleryController Update 75', '2018-07-23 01:51:26'),
(295, 'info@markdesign.net', 'GalleryController Update 74', '2018-07-23 01:51:29'),
(296, 'info@markdesign.net', 'GalleryController Update 73', '2018-07-23 01:51:32'),
(297, 'info@markdesign.net', 'GalleryController Update 72', '2018-07-23 01:51:37'),
(298, 'info@markdesign.net', 'GalleryController Update 64', '2018-07-23 01:52:46'),
(299, 'info@markdesign.net', 'GalleryController Update 63', '2018-07-23 01:52:52'),
(300, 'info@markdesign.net', 'GalleryController Update 54', '2018-07-23 01:53:00'),
(301, 'info@markdesign.net', 'GalleryController Update 52', '2018-07-23 01:53:06'),
(302, 'info@markdesign.net', 'GalleryController Update 34', '2018-07-23 01:53:43'),
(303, 'info@markdesign.net', 'GalleryController Update 33', '2018-07-23 01:54:15'),
(304, 'info@markdesign.net', 'GalleryController Update 32', '2018-07-23 01:54:19'),
(305, 'info@markdesign.net', 'GalleryController Update 30', '2018-07-23 01:54:22'),
(306, 'info@markdesign.net', 'GalleryController Update 35', '2018-07-23 01:54:25'),
(307, 'info@markdesign.net', 'GalleryController Update 76', '2018-07-23 02:20:03'),
(308, 'info@markdesign.net', 'GalleryController Update 57', '2018-07-23 02:20:30'),
(309, 'info@markdesign.net', 'GalleryController Update 58', '2018-07-23 02:20:31'),
(310, 'info@markdesign.net', 'GalleryController Update 59', '2018-07-23 02:20:34'),
(311, 'info@markdesign.net', 'GalleryController Update 60', '2018-07-23 02:20:36'),
(312, 'info@markdesign.net', 'GalleryController Update 61', '2018-07-23 02:20:38'),
(313, 'info@markdesign.net', 'GalleryController Update 62', '2018-07-23 02:20:40'),
(314, 'info@markdesign.net', 'GalleryController Update 56', '2018-07-23 02:22:06'),
(315, 'info@markdesign.net', 'GalleryController Update 55', '2018-07-23 02:22:14'),
(316, 'info@markdesign.net', 'GalleryController Update 29', '2018-07-23 02:22:21'),
(317, 'info@markdesign.net', 'GalleryController Update 28', '2018-07-23 02:22:29'),
(318, 'info@markdesign.net', 'GalleryController Update 27', '2018-07-23 02:22:36'),
(319, 'info@markdesign.net', 'GalleryController Update 25', '2018-07-23 02:22:43'),
(320, 'info@markdesign.net', 'GalleryController Update 24', '2018-07-23 02:22:49'),
(321, 'info@markdesign.net', 'GalleryController Update 23', '2018-07-23 02:22:56'),
(322, 'info@markdesign.net', 'GalleryController Update 22', '2018-07-23 02:23:03'),
(323, 'info@markdesign.net', 'GalleryController Update 6', '2018-07-23 02:23:11'),
(324, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-07-23 03:46:19'),
(325, 'info@markdesign.net', 'Gallery Controller Create 77', '2018-07-23 03:49:31'),
(326, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-08-30 01:20:08'),
(327, 'info@markdesign.net', 'Gallery Controller Create 78', '2018-08-30 01:49:37'),
(328, 'info@markdesign.net', 'Gallery Controller Create 79', '2018-08-30 01:52:47'),
(329, 'info@markdesign.net', 'GalleryController Update 79', '2018-08-30 01:56:28'),
(330, 'info@markdesign.net', 'Gallery Controller Create 80', '2018-08-30 02:00:23'),
(331, 'info@markdesign.net', 'GalleryController Update 79', '2018-08-30 02:37:05'),
(332, 'info@markdesign.net', 'GalleryController Update 78', '2018-08-30 02:37:30'),
(333, 'info@markdesign.net', 'GalleryController Update 80', '2018-08-30 02:39:08'),
(334, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-09-10 01:52:42'),
(335, 'info@markdesign.net', 'Gallery Controller Create 81', '2018-09-10 01:58:21'),
(336, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-09-10 02:53:26'),
(337, 'info@markdesign.net', 'GalleryController Update 81', '2018-09-10 03:00:59'),
(338, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-09-18 04:15:35'),
(339, 'info@markdesign.net', 'Setting Update', '2018-09-18 04:15:51'),
(340, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-09-18 06:47:10'),
(341, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-09-19 01:15:50'),
(342, 'info@markdesign.net', 'Gallery Controller Create 82', '2018-09-19 01:55:38'),
(343, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-09-26 15:57:00'),
(344, 'info@markdesign.net', 'SlideController Update 20', '2018-09-26 15:58:50'),
(345, 'info@markdesign.net', 'GalleryController Update 82', '2018-09-26 16:30:06'),
(346, 'info@markdesign.net', 'Setting Update', '2018-09-26 16:31:02'),
(347, 'info@markdesign.net', 'Setting Update', '2018-09-26 16:31:25'),
(348, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-09-27 02:49:05'),
(349, 'info@markdesign.net', 'User Delete deoryzpandu@gmail.com 1', '2018-09-27 02:49:14'),
(350, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-09-27 02:52:50'),
(351, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-09-27 03:05:03'),
(352, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-10-22 03:19:46'),
(353, 'info@markdesign.net', 'Gallery Controller Create 83', '2018-10-22 03:25:58'),
(354, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-10-22 03:26:28'),
(355, 'info@markdesign.net', 'Gallery Controller Create 84', '2018-10-22 03:38:07'),
(356, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-11-02 02:29:46'),
(357, 'info@markdesign.net', 'Gallery Controller Create 85', '2018-11-02 02:40:02'),
(358, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-11-02 10:43:32'),
(359, 'info@markdesign.net', 'Setting Update', '2018-11-02 10:44:43'),
(360, 'info@markdesign.net', 'Setting Update', '2018-11-02 10:44:51'),
(361, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-10 03:17:27'),
(362, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-10 04:52:29'),
(363, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-10 06:10:27'),
(364, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-10 06:10:30'),
(365, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-10 06:51:18'),
(366, 'info@markdesign.net', 'Gallery Controller Create 86', '2018-12-10 07:06:22'),
(367, 'info@markdesign.net', 'Gallery Controller Create 87', '2018-12-10 07:19:19'),
(368, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-10 07:22:20'),
(369, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 08:34:51'),
(370, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 08:48:05'),
(371, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 08:48:22'),
(372, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 08:56:05'),
(373, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 08:57:05'),
(374, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 08:57:05'),
(375, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 08:58:23'),
(376, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 09:00:43'),
(377, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 09:00:43'),
(378, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 09:00:44'),
(379, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 09:11:15'),
(380, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 09:28:18'),
(381, 'info@markdesign.net', 'GalleryController Update 86', '2018-12-10 09:52:37'),
(382, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-11 01:57:12'),
(383, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-11 03:27:05'),
(384, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-12 03:18:05'),
(385, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-12 07:56:50'),
(386, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-12 08:52:11'),
(387, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-12 09:58:25'),
(388, 'info@markdesign.net', 'Gallery Controller Create 88', '2018-12-12 09:59:18'),
(389, 'info@markdesign.net', 'GalleryController Update 88', '2018-12-12 10:01:29'),
(390, 'info@markdesign.net', 'Gallery Controller Create 89', '2018-12-12 10:08:26'),
(391, 'info@markdesign.net', 'Gallery Controller Create 90', '2018-12-12 10:13:16'),
(392, 'info@markdesign.net', 'Gallery Controller Create 91', '2018-12-12 10:17:58'),
(393, 'info@markdesign.net', 'Gallery Controller Create 92', '2018-12-12 10:21:49'),
(394, 'info@markdesign.net', 'Gallery Controller Create 93', '2018-12-12 10:25:06'),
(395, 'info@markdesign.net', 'Gallery Controller Create 94', '2018-12-12 10:28:31'),
(396, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-13 02:47:34'),
(397, 'info@markdesign.net', 'GalleryController Update 92', '2018-12-13 02:48:20'),
(398, 'info@markdesign.net', 'GalleryController Update 91', '2018-12-13 02:53:02'),
(399, 'info@markdesign.net', 'GalleryController Update 91', '2018-12-13 02:53:23'),
(400, 'info@markdesign.net', 'Login: info@markdesign.net', '2018-12-19 01:47:25'),
(401, 'info@markdesign.net', 'GalleryController Update 88', '2018-12-19 01:51:38'),
(402, 'info@markdesign.net', 'GalleryController Update 88', '2018-12-19 01:51:40'),
(403, 'info@markdesign.net', 'GalleryController Update 88', '2018-12-19 01:55:05'),
(404, 'info@markdesign.net', 'GalleryController Update 88', '2018-12-19 02:00:10'),
(405, 'info@markdesign.net', 'Gallery Controller Create 95', '2018-12-19 02:38:28'),
(406, 'info@markdesign.net', 'GalleryController Update 95', '2018-12-19 02:38:41'),
(407, 'info@markdesign.net', 'GalleryController Update 95', '2018-12-19 02:40:30'),
(408, 'info@markdesign.net', 'GalleryController Update 95', '2018-12-19 02:40:37'),
(409, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-02 02:52:59'),
(410, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-02 04:26:43'),
(411, 'info@markdesign.net', 'GalleryController Update 95', '2019-01-02 04:36:42'),
(412, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-02 05:06:48'),
(413, 'info@markdesign.net', 'Gallery Controller Create 96', '2019-01-02 05:21:28'),
(414, 'info@markdesign.net', 'Gallery Controller Create 97', '2019-01-02 05:32:28'),
(415, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-02 08:53:04'),
(416, 'info@markdesign.net', 'Gallery Controller Create 98', '2019-01-02 08:59:11'),
(417, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-04 01:37:12'),
(418, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-04 01:38:37'),
(419, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-04 01:41:06'),
(420, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-04 02:03:50'),
(421, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-04 02:51:03'),
(422, 'info@markdesign.net', 'GalleryController Update 94', '2019-01-04 02:53:39'),
(423, 'info@markdesign.net', 'GalleryController Update 93', '2019-01-04 02:53:57'),
(424, 'info@markdesign.net', 'GalleryController Update 92', '2019-01-04 02:54:17'),
(425, 'info@markdesign.net', 'GalleryController Update 92', '2019-01-04 02:54:24'),
(426, 'info@markdesign.net', 'GalleryController Update 89', '2019-01-04 02:55:25'),
(427, 'info@markdesign.net', 'GalleryController Update 90', '2019-01-04 02:55:28'),
(428, 'info@markdesign.net', 'GalleryController Update 91', '2019-01-04 02:55:31'),
(429, 'info@markdesign.net', 'GalleryController Update 85', '2019-01-04 02:57:48'),
(430, 'info@markdesign.net', 'GalleryController Update 84', '2019-01-04 02:58:01'),
(431, 'info@markdesign.net', 'GalleryController Update 83', '2019-01-04 02:58:11'),
(432, 'info@markdesign.net', 'GalleryController Update 81', '2019-01-04 02:58:20'),
(433, 'info@markdesign.net', 'GalleryController Update 80', '2019-01-04 02:58:31'),
(434, 'info@markdesign.net', 'GalleryController Update 79', '2019-01-04 02:58:38'),
(435, 'info@markdesign.net', 'GalleryController Update 78', '2019-01-04 02:58:43'),
(436, 'info@markdesign.net', 'GalleryController Update 77', '2019-01-04 02:58:48'),
(437, 'info@markdesign.net', 'GalleryController Update 76', '2019-01-04 02:58:55'),
(438, 'info@markdesign.net', 'GalleryController Update 75', '2019-01-04 02:59:03'),
(439, 'info@markdesign.net', 'GalleryController Update 74', '2019-01-04 03:00:50'),
(440, 'info@markdesign.net', 'GalleryController Update 73', '2019-01-04 03:00:53'),
(441, 'info@markdesign.net', 'GalleryController Update 72', '2019-01-04 03:00:55'),
(442, 'info@markdesign.net', 'GalleryController Update 71', '2019-01-04 03:00:57'),
(443, 'info@markdesign.net', 'GalleryController Update 70', '2019-01-04 03:00:59'),
(444, 'info@markdesign.net', 'GalleryController Update 69', '2019-01-04 03:01:01'),
(445, 'info@markdesign.net', 'GalleryController Update 68', '2019-01-04 03:01:03'),
(446, 'info@markdesign.net', 'GalleryController Update 67', '2019-01-04 03:01:04'),
(447, 'info@markdesign.net', 'GalleryController Update 66', '2019-01-04 03:01:06'),
(448, 'info@markdesign.net', 'GalleryController Update 65', '2019-01-04 03:01:09'),
(449, 'info@markdesign.net', 'GalleryController Update 55', '2019-01-04 03:02:37'),
(450, 'info@markdesign.net', 'GalleryController Update 56', '2019-01-04 03:02:39'),
(451, 'info@markdesign.net', 'GalleryController Update 57', '2019-01-04 03:02:41'),
(452, 'info@markdesign.net', 'GalleryController Update 58', '2019-01-04 03:02:43'),
(453, 'info@markdesign.net', 'GalleryController Update 59', '2019-01-04 03:02:44'),
(454, 'info@markdesign.net', 'GalleryController Update 60', '2019-01-04 03:02:47'),
(455, 'info@markdesign.net', 'GalleryController Update 61', '2019-01-04 03:02:49'),
(456, 'info@markdesign.net', 'GalleryController Update 62', '2019-01-04 03:02:50'),
(457, 'info@markdesign.net', 'GalleryController Update 63', '2019-01-04 03:02:53'),
(458, 'info@markdesign.net', 'GalleryController Update 64', '2019-01-04 03:02:55'),
(459, 'info@markdesign.net', 'GalleryController Update 45', '2019-01-04 03:07:22'),
(460, 'info@markdesign.net', 'GalleryController Update 46', '2019-01-04 03:07:23'),
(461, 'info@markdesign.net', 'GalleryController Update 47', '2019-01-04 03:07:27'),
(462, 'info@markdesign.net', 'GalleryController Update 48', '2019-01-04 03:07:47'),
(463, 'info@markdesign.net', 'GalleryController Update 49', '2019-01-04 03:07:54'),
(464, 'info@markdesign.net', 'GalleryController Update 50', '2019-01-04 03:07:59'),
(465, 'info@markdesign.net', 'GalleryController Update 51', '2019-01-04 03:08:05'),
(466, 'info@markdesign.net', 'GalleryController Update 52', '2019-01-04 03:08:20'),
(467, 'info@markdesign.net', 'GalleryController Update 53', '2019-01-04 03:08:22'),
(468, 'info@markdesign.net', 'GalleryController Update 54', '2019-01-04 03:08:25'),
(469, 'info@markdesign.net', 'GalleryController Update 44', '2019-01-04 03:15:45'),
(470, 'info@markdesign.net', 'GalleryController Update 43', '2019-01-04 03:15:47'),
(471, 'info@markdesign.net', 'GalleryController Update 42', '2019-01-04 03:15:51'),
(472, 'info@markdesign.net', 'GalleryController Update 41', '2019-01-04 03:15:52'),
(473, 'info@markdesign.net', 'GalleryController Update 40', '2019-01-04 03:15:53'),
(474, 'info@markdesign.net', 'GalleryController Update 39', '2019-01-04 03:15:55'),
(475, 'info@markdesign.net', 'GalleryController Update 38', '2019-01-04 03:15:57'),
(476, 'info@markdesign.net', 'GalleryController Update 37', '2019-01-04 03:15:58'),
(477, 'info@markdesign.net', 'GalleryController Update 36', '2019-01-04 03:16:00'),
(478, 'info@markdesign.net', 'GalleryController Update 35', '2019-01-04 03:16:02'),
(479, 'info@markdesign.net', 'GalleryController Update 27', '2019-01-04 03:30:35'),
(480, 'info@markdesign.net', 'GalleryController Update 27', '2019-01-04 03:33:34'),
(481, 'info@markdesign.net', 'GalleryController Update 25', '2019-01-04 03:33:35'),
(482, 'info@markdesign.net', 'GalleryController Update 24', '2019-01-04 03:33:36'),
(483, 'info@markdesign.net', 'GalleryController Update 28', '2019-01-04 03:33:38'),
(484, 'info@markdesign.net', 'GalleryController Update 29', '2019-01-04 03:33:40'),
(485, 'info@markdesign.net', 'GalleryController Update 30', '2019-01-04 03:33:42'),
(486, 'info@markdesign.net', 'GalleryController Update 31', '2019-01-04 03:33:44'),
(487, 'info@markdesign.net', 'GalleryController Update 32', '2019-01-04 03:33:46'),
(488, 'info@markdesign.net', 'GalleryController Update 33', '2019-01-04 03:33:48'),
(489, 'info@markdesign.net', 'GalleryController Update 34', '2019-01-04 03:33:52'),
(490, 'info@markdesign.net', 'GalleryController Update 23', '2019-01-04 03:41:30'),
(491, 'info@markdesign.net', 'GalleryController Update 22', '2019-01-04 03:41:32'),
(492, 'info@markdesign.net', 'GalleryController Update 21', '2019-01-04 03:41:34'),
(493, 'info@markdesign.net', 'GalleryController Update 20', '2019-01-04 03:41:36'),
(494, 'info@markdesign.net', 'GalleryController Update 19', '2019-01-04 03:41:43'),
(495, 'info@markdesign.net', 'GalleryController Update 18', '2019-01-04 03:41:55'),
(496, 'info@markdesign.net', 'GalleryController Update 17', '2019-01-04 03:41:59'),
(497, 'info@markdesign.net', 'GalleryController Update 16', '2019-01-04 03:42:02'),
(498, 'info@markdesign.net', 'GalleryController Update 15', '2019-01-04 03:42:09'),
(499, 'info@markdesign.net', 'GalleryController Update 14', '2019-01-04 03:42:12'),
(500, 'info@markdesign.net', 'GalleryController Update 13', '2019-01-04 03:44:28'),
(501, 'info@markdesign.net', 'GalleryController Update 12', '2019-01-04 03:44:30'),
(502, 'info@markdesign.net', 'GalleryController Update 11', '2019-01-04 03:44:32'),
(503, 'info@markdesign.net', 'GalleryController Update 10', '2019-01-04 03:44:35'),
(504, 'info@markdesign.net', 'GalleryController Update 9', '2019-01-04 03:44:37'),
(505, 'info@markdesign.net', 'GalleryController Update 8', '2019-01-04 03:44:41'),
(506, 'info@markdesign.net', 'GalleryController Update 7', '2019-01-04 03:44:44'),
(507, 'info@markdesign.net', 'GalleryController Update 6', '2019-01-04 03:44:46'),
(508, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-07 04:32:40'),
(509, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-09 02:25:09'),
(510, 'info@markdesign.net', 'Gallery Controller Create 99', '2019-01-09 02:37:17'),
(511, 'info@markdesign.net', 'Gallery Controller Create 100', '2019-01-09 02:49:42'),
(512, 'info@markdesign.net', 'Gallery Controller Create 101', '2019-01-09 02:57:03'),
(513, 'info@markdesign.net', 'GalleryController Update 98', '2019-01-09 02:59:28'),
(514, 'info@markdesign.net', 'GalleryController Update 97', '2019-01-09 03:00:32'),
(515, 'info@markdesign.net', 'GalleryController Update 96', '2019-01-09 03:01:15'),
(516, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-21 01:29:43'),
(517, 'info@markdesign.net', 'GalleryController Update 94', '2019-01-21 01:30:16'),
(518, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-21 04:28:53'),
(519, 'info@markdesign.net', 'GalleryController Update 99', '2019-01-21 04:31:26'),
(520, 'info@markdesign.net', 'GalleryController Update 95', '2019-01-21 04:32:27'),
(521, 'info@markdesign.net', 'GalleryController Update 90', '2019-01-21 04:33:39'),
(522, 'info@markdesign.net', 'GalleryController Update 89', '2019-01-21 04:34:38'),
(523, 'info@markdesign.net', 'GalleryController Update 76', '2019-01-21 04:35:47'),
(524, 'info@markdesign.net', 'GalleryController Update 62', '2019-01-21 04:37:17'),
(525, 'info@markdesign.net', 'GalleryController Update 61', '2019-01-21 04:38:23'),
(526, 'info@markdesign.net', 'GalleryController Update 60', '2019-01-21 04:39:50'),
(527, 'info@markdesign.net', 'GalleryController Update 59', '2019-01-21 04:41:44'),
(528, 'info@markdesign.net', 'GalleryController Update 58', '2019-01-21 04:42:10'),
(529, 'info@markdesign.net', 'GalleryController Update 57', '2019-01-21 04:43:07'),
(530, 'info@markdesign.net', 'GalleryController Update 56', '2019-01-21 04:43:39'),
(531, 'info@markdesign.net', 'GalleryController Update 55', '2019-01-21 04:44:11'),
(532, 'info@markdesign.net', 'GalleryController Update 29', '2019-01-21 04:46:29'),
(533, 'info@markdesign.net', 'GalleryController Update 28', '2019-01-21 04:47:20'),
(534, 'info@markdesign.net', 'GalleryController Update 27', '2019-01-21 04:48:38'),
(535, 'info@markdesign.net', 'GalleryController Update 25', '2019-01-21 04:49:12'),
(536, 'info@markdesign.net', 'GalleryController Update 24', '2019-01-21 04:49:32'),
(537, 'info@markdesign.net', 'GalleryController Update 23', '2019-01-21 04:49:49'),
(538, 'info@markdesign.net', 'GalleryController Update 22', '2019-01-21 04:50:01'),
(539, 'info@markdesign.net', 'GalleryController Update 6', '2019-01-21 04:50:41'),
(540, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-01-21 04:51:09'),
(541, 'info@markdesign.net', 'GalleryController Update 99', '2019-01-21 04:51:26'),
(542, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-02-22 01:23:57'),
(543, 'info@markdesign.net', 'Gallery Controller Create 102', '2019-02-22 01:53:47'),
(544, 'info@markdesign.net', 'Gallery Controller Create 103', '2019-02-22 02:01:20'),
(545, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-03-18 07:24:38'),
(546, 'info@markdesign.net', 'Gallery Controller Create 104', '2019-03-18 07:54:41'),
(547, 'info@markdesign.net', 'GalleryController Update 104', '2019-03-18 08:00:56'),
(548, 'info@markdesign.net', 'GalleryController Update 104', '2019-03-18 08:04:42'),
(549, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-03-18 08:17:46'),
(550, 'info@markdesign.net', 'GalleryController Update 104', '2019-03-18 08:41:07'),
(551, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-03-18 09:26:01'),
(552, 'info@markdesign.net', 'Gallery Controller Create 105', '2019-03-18 09:36:20'),
(553, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-03-18 10:10:59'),
(554, 'info@markdesign.net', 'Gallery Controller Create 106', '2019-03-18 10:24:59'),
(555, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-03-19 01:18:43'),
(556, 'info@markdesign.net', 'GalleryController Update 106', '2019-03-19 01:21:57'),
(557, 'info@markdesign.net', 'GalleryController Update 105', '2019-03-19 01:39:15'),
(558, 'info@markdesign.net', 'GalleryController Update 105', '2019-03-19 02:07:19'),
(559, 'info@markdesign.net', 'GalleryController Update 106', '2019-03-19 02:07:45'),
(560, 'info@markdesign.net', 'GalleryController Update 105', '2019-03-19 02:10:33'),
(561, 'info@markdesign.net', 'GalleryController Update 106', '2019-03-19 02:12:45'),
(562, 'info@markdesign.net', 'GalleryController Update 106', '2019-03-19 02:13:33'),
(563, 'info@markdesign.net', 'GalleryController Update 106', '2019-03-19 02:14:38'),
(564, 'info@markdesign.net', 'GalleryController Update 106', '2019-03-19 02:15:55'),
(565, 'info@markdesign.net', 'GalleryController Update 106', '2019-03-19 02:16:42'),
(566, 'info@markdesign.net', 'GalleryController Update 106', '2019-03-19 02:17:32'),
(567, 'info@markdesign.net', 'GalleryController Update 105', '2019-03-19 02:18:16'),
(568, 'info@markdesign.net', 'GalleryController Update 105', '2019-03-19 02:18:21'),
(569, 'info@markdesign.net', 'GalleryController Update 105', '2019-03-19 02:18:22'),
(570, 'info@markdesign.net', 'GalleryController Update 105', '2019-03-19 02:18:39'),
(571, 'info@markdesign.net', 'GalleryController Update 105', '2019-03-19 02:18:57'),
(572, 'info@markdesign.net', 'GalleryController Update 105', '2019-03-19 02:27:17'),
(573, 'info@markdesign.net', 'GalleryController Update 104', '2019-03-19 02:29:18'),
(574, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-03-19 03:46:39'),
(575, 'info@markdesign.net', 'Gallery Controller Create 107', '2019-03-19 04:16:06'),
(576, 'info@markdesign.net', 'GalleryController Update 107', '2019-03-19 04:18:25'),
(577, 'info@markdesign.net', 'GalleryController Update 107', '2019-03-19 04:20:35'),
(578, 'info@markdesign.net', 'GalleryController Update 107', '2019-03-19 04:20:54'),
(579, 'info@markdesign.net', 'GalleryController Update 107', '2019-03-19 04:21:45'),
(580, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-05-16 01:20:41'),
(581, 'info@markdesign.net', 'Gallery Controller Create 108', '2019-05-16 02:06:11'),
(582, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-05-16 02:42:08'),
(583, 'info@markdesign.net', 'Gallery Controller Create 109', '2019-05-16 02:57:30'),
(584, 'info@markdesign.net', 'Gallery Controller Create 110', '2019-05-16 03:09:24'),
(585, 'info@markdesign.net', 'Gallery Controller Create 111', '2019-05-16 03:15:41'),
(586, 'info@markdesign.net', 'GalleryController Update 43', '2019-05-16 03:16:45'),
(587, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-05-16 08:25:36'),
(588, 'info@markdesign.net', 'GalleryController Update 111', '2019-05-16 08:28:10'),
(589, 'info@markdesign.net', 'GalleryController Update 110', '2019-05-16 08:28:54'),
(590, 'info@markdesign.net', 'GalleryController Update 109', '2019-05-16 08:29:11'),
(591, 'info@markdesign.net', 'GalleryController Update 108', '2019-05-16 08:29:29'),
(592, 'info@markdesign.net', 'GalleryController Update 107', '2019-05-16 08:29:46'),
(593, 'info@markdesign.net', 'GalleryController Update 106', '2019-05-16 08:30:08'),
(594, 'info@markdesign.net', 'GalleryController Update 105', '2019-05-16 08:30:18'),
(595, 'info@markdesign.net', 'GalleryController Update 104', '2019-05-16 08:30:28'),
(596, 'info@markdesign.net', 'GalleryController Update 103', '2019-05-16 08:30:37'),
(597, 'info@markdesign.net', 'GalleryController Update 102', '2019-05-16 08:30:48'),
(598, 'info@markdesign.net', 'GalleryController Update 101', '2019-05-16 08:31:30'),
(599, 'info@markdesign.net', 'GalleryController Update 100', '2019-05-16 08:31:39'),
(600, 'info@markdesign.net', 'GalleryController Update 99', '2019-05-16 08:31:49'),
(601, 'info@markdesign.net', 'GalleryController Update 98', '2019-05-16 08:32:00'),
(602, 'info@markdesign.net', 'GalleryController Update 97', '2019-05-16 08:32:09'),
(603, 'info@markdesign.net', 'GalleryController Update 96', '2019-05-16 08:32:17'),
(604, 'info@markdesign.net', 'GalleryController Update 95', '2019-05-16 08:32:25'),
(605, 'info@markdesign.net', 'GalleryController Update 94', '2019-05-16 08:32:36'),
(606, 'info@markdesign.net', 'GalleryController Update 93', '2019-05-16 08:32:52'),
(607, 'info@markdesign.net', 'GalleryController Update 92', '2019-05-16 08:33:02'),
(608, 'info@markdesign.net', 'GalleryController Update 91', '2019-05-16 08:33:49'),
(609, 'info@markdesign.net', 'GalleryController Update 90', '2019-05-16 08:34:05'),
(610, 'info@markdesign.net', 'GalleryController Update 89', '2019-05-16 08:34:20'),
(611, 'info@markdesign.net', 'GalleryController Update 85', '2019-05-16 08:34:30'),
(612, 'info@markdesign.net', 'GalleryController Update 84', '2019-05-16 08:34:37'),
(613, 'info@markdesign.net', 'GalleryController Update 83', '2019-05-16 08:34:47');
INSERT INTO `log` (`id`, `username`, `activity`, `time`) VALUES
(614, 'info@markdesign.net', 'GalleryController Update 81', '2019-05-16 08:34:55'),
(615, 'info@markdesign.net', 'GalleryController Update 80', '2019-05-16 08:35:03'),
(616, 'info@markdesign.net', 'GalleryController Update 79', '2019-05-16 08:35:11'),
(617, 'info@markdesign.net', 'GalleryController Update 78', '2019-05-16 08:35:19'),
(618, 'info@markdesign.net', 'GalleryController Update 77', '2019-05-16 08:36:01'),
(619, 'info@markdesign.net', 'GalleryController Update 76', '2019-05-16 08:36:08'),
(620, 'info@markdesign.net', 'GalleryController Update 75', '2019-05-16 08:36:15'),
(621, 'info@markdesign.net', 'GalleryController Update 74', '2019-05-16 08:36:24'),
(622, 'info@markdesign.net', 'GalleryController Update 73', '2019-05-16 08:36:31'),
(623, 'info@markdesign.net', 'GalleryController Update 72', '2019-05-16 08:36:40'),
(624, 'info@markdesign.net', 'GalleryController Update 71', '2019-05-16 08:36:48'),
(625, 'info@markdesign.net', 'GalleryController Update 70', '2019-05-16 08:36:57'),
(626, 'info@markdesign.net', 'GalleryController Update 69', '2019-05-16 08:37:05'),
(627, 'info@markdesign.net', 'GalleryController Update 68', '2019-05-16 08:37:14'),
(628, 'info@markdesign.net', 'GalleryController Update 67', '2019-05-16 08:38:00'),
(629, 'info@markdesign.net', 'GalleryController Update 66', '2019-05-16 08:38:09'),
(630, 'info@markdesign.net', 'GalleryController Update 65', '2019-05-16 08:38:25'),
(631, 'info@markdesign.net', 'GalleryController Update 64', '2019-05-16 08:38:32'),
(632, 'info@markdesign.net', 'GalleryController Update 63', '2019-05-16 08:38:40'),
(633, 'info@markdesign.net', 'GalleryController Update 62', '2019-05-16 08:38:47'),
(634, 'info@markdesign.net', 'GalleryController Update 61', '2019-05-16 08:38:55'),
(635, 'info@markdesign.net', 'GalleryController Update 60', '2019-05-16 08:39:03'),
(636, 'info@markdesign.net', 'GalleryController Update 59', '2019-05-16 08:39:10'),
(637, 'info@markdesign.net', 'GalleryController Update 58', '2019-05-16 08:39:17'),
(638, 'info@markdesign.net', 'GalleryController Update 57', '2019-05-16 08:39:47'),
(639, 'info@markdesign.net', 'GalleryController Update 56', '2019-05-16 08:39:54'),
(640, 'info@markdesign.net', 'GalleryController Update 55', '2019-05-16 08:40:00'),
(641, 'info@markdesign.net', 'GalleryController Update 54', '2019-05-16 08:40:08'),
(642, 'info@markdesign.net', 'GalleryController Update 53', '2019-05-16 08:40:15'),
(643, 'info@markdesign.net', 'GalleryController Update 52', '2019-05-16 08:40:23'),
(644, 'info@markdesign.net', 'GalleryController Update 51', '2019-05-16 08:40:30'),
(645, 'info@markdesign.net', 'GalleryController Update 50', '2019-05-16 08:40:37'),
(646, 'info@markdesign.net', 'GalleryController Update 49', '2019-05-16 08:40:44'),
(647, 'info@markdesign.net', 'GalleryController Update 48', '2019-05-16 08:40:51'),
(648, 'info@markdesign.net', 'GalleryController Update 47', '2019-05-16 08:43:04'),
(649, 'info@markdesign.net', 'GalleryController Update 46', '2019-05-16 08:43:12'),
(650, 'info@markdesign.net', 'GalleryController Update 45', '2019-05-16 08:43:18'),
(651, 'info@markdesign.net', 'GalleryController Update 44', '2019-05-16 08:43:25'),
(652, 'info@markdesign.net', 'GalleryController Update 43', '2019-05-16 08:43:32'),
(653, 'info@markdesign.net', 'GalleryController Update 42', '2019-05-16 08:43:38'),
(654, 'info@markdesign.net', 'GalleryController Update 41', '2019-05-16 08:43:46'),
(655, 'info@markdesign.net', 'GalleryController Update 40', '2019-05-16 08:43:53'),
(656, 'info@markdesign.net', 'GalleryController Update 39', '2019-05-16 08:44:01'),
(657, 'info@markdesign.net', 'GalleryController Update 38', '2019-05-16 08:44:11'),
(658, 'info@markdesign.net', 'GalleryController Update 37', '2019-05-16 08:44:47'),
(659, 'info@markdesign.net', 'GalleryController Update 36', '2019-05-16 08:44:52'),
(660, 'info@markdesign.net', 'GalleryController Update 35', '2019-05-16 08:44:58'),
(661, 'info@markdesign.net', 'GalleryController Update 34', '2019-05-16 08:45:04'),
(662, 'info@markdesign.net', 'GalleryController Update 33', '2019-05-16 08:45:11'),
(663, 'info@markdesign.net', 'GalleryController Update 32', '2019-05-16 08:45:17'),
(664, 'info@markdesign.net', 'GalleryController Update 31', '2019-05-16 08:45:42'),
(665, 'info@markdesign.net', 'GalleryController Update 30', '2019-05-16 08:45:48'),
(666, 'info@markdesign.net', 'GalleryController Update 29', '2019-05-16 08:45:56'),
(667, 'info@markdesign.net', 'GalleryController Update 28', '2019-05-16 08:46:03'),
(668, 'info@markdesign.net', 'GalleryController Update 27', '2019-05-16 08:46:48'),
(669, 'info@markdesign.net', 'GalleryController Update 25', '2019-05-16 08:46:55'),
(670, 'info@markdesign.net', 'GalleryController Update 24', '2019-05-16 08:47:01'),
(671, 'info@markdesign.net', 'GalleryController Update 23', '2019-05-16 08:47:07'),
(672, 'info@markdesign.net', 'GalleryController Update 22', '2019-05-16 08:47:13'),
(673, 'info@markdesign.net', 'GalleryController Update 21', '2019-05-16 08:47:19'),
(674, 'info@markdesign.net', 'GalleryController Update 20', '2019-05-16 08:47:24'),
(675, 'info@markdesign.net', 'GalleryController Update 19', '2019-05-16 08:47:28'),
(676, 'info@markdesign.net', 'GalleryController Update 18', '2019-05-16 08:47:35'),
(677, 'info@markdesign.net', 'GalleryController Update 17', '2019-05-16 08:47:42'),
(678, 'info@markdesign.net', 'GalleryController Update 16', '2019-05-16 08:48:33'),
(679, 'info@markdesign.net', 'GalleryController Update 15', '2019-05-16 08:48:38'),
(680, 'info@markdesign.net', 'GalleryController Update 14', '2019-05-16 08:48:43'),
(681, 'info@markdesign.net', 'GalleryController Update 13', '2019-05-16 08:48:49'),
(682, 'info@markdesign.net', 'GalleryController Update 12', '2019-05-16 08:48:55'),
(683, 'info@markdesign.net', 'GalleryController Update 11', '2019-05-16 08:49:00'),
(684, 'info@markdesign.net', 'GalleryController Update 10', '2019-05-16 08:49:13'),
(685, 'info@markdesign.net', 'GalleryController Update 7', '2019-05-16 08:49:18'),
(686, 'info@markdesign.net', 'GalleryController Update 8', '2019-05-16 08:49:25'),
(687, 'info@markdesign.net', 'GalleryController Update 9', '2019-05-16 08:49:31'),
(688, 'info@markdesign.net', 'GalleryController Update 6', '2019-05-16 08:49:37'),
(689, 'ibnudrift@gmail.com', 'Login: ibnudrift@gmail.com', '0000-00-00 00:00:00'),
(690, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(691, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(692, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(693, 'info@markdesign.net', 'Gallery Controller Create 112', '0000-00-00 00:00:00'),
(694, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(695, 'info@markdesign.net', 'Gallery Controller Create 113', '0000-00-00 00:00:00'),
(696, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(697, 'info@markdesign.net', 'Gallery Controller Create 114', '0000-00-00 00:00:00'),
(698, 'info@markdesign.net', 'Gallery Controller Create 115', '0000-00-00 00:00:00'),
(699, 'info@markdesign.net', 'Gallery Controller Create 116', '0000-00-00 00:00:00'),
(700, 'info@markdesign.net', 'Gallery Controller Create 117', '0000-00-00 00:00:00'),
(701, 'info@markdesign.net', 'Gallery Controller Create 118', '0000-00-00 00:00:00'),
(702, 'info@markdesign.net', 'Gallery Controller Create 119', '0000-00-00 00:00:00'),
(703, 'info@markdesign.net', 'Gallery Controller Create 120', '0000-00-00 00:00:00'),
(704, 'info@markdesign.net', 'GalleryController Update 120', '0000-00-00 00:00:00'),
(705, 'info@markdesign.net', 'GalleryController Update 72', '0000-00-00 00:00:00'),
(706, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(707, 'info@markdesign.net', 'Gallery Controller Create 121', '0000-00-00 00:00:00'),
(708, 'info@markdesign.net', 'GalleryController Update 121', '0000-00-00 00:00:00'),
(709, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(710, 'info@markdesign.net', 'GalleryController Update 121', '0000-00-00 00:00:00'),
(711, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(712, 'info@markdesign.net', 'Gallery Controller Create 122', '0000-00-00 00:00:00'),
(713, 'info@markdesign.net', 'GalleryController Update 122', '0000-00-00 00:00:00'),
(714, 'info@markdesign.net', 'GalleryController Update 122', '0000-00-00 00:00:00'),
(715, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(716, 'info@markdesign.net', 'Gallery Controller Create 123', '0000-00-00 00:00:00'),
(717, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-12-23 15:42:04'),
(718, 'info@markdesign.net', 'Setting Update', '2019-12-23 16:25:09'),
(719, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-12-24 08:44:53'),
(720, 'info@markdesign.net', 'Create Category 1', '2019-12-24 08:45:25'),
(721, 'info@markdesign.net', 'Login: info@markdesign.net', '2019-12-25 09:22:47'),
(722, 'info@markdesign.net', 'Setting Update', '2019-12-25 09:41:14'),
(723, 'info@markdesign.net', 'Setting Update', '2019-12-25 09:42:02'),
(724, 'info@markdesign.net', 'Setting Update', '2019-12-25 09:42:09'),
(725, 'info@markdesign.net', 'Setting Update', '2019-12-25 09:42:26'),
(726, 'info@markdesign.net', 'Setting Update', '2019-12-25 09:42:55'),
(727, 'info@markdesign.net', 'Setting Update', '2019-12-25 09:44:39'),
(728, 'info@markdesign.net', 'Setting Update', '2019-12-25 10:19:29'),
(729, 'info@markdesign.net', 'Setting Update', '2019-12-25 10:22:09'),
(730, 'info@markdesign.net', 'Setting Update', '2019-12-25 11:05:36'),
(731, 'info@markdesign.net', 'Slide Controller Create 1', '2019-12-25 11:17:48'),
(732, 'info@markdesign.net', 'Gallery Controller Create 1', '2019-12-25 15:12:20'),
(733, 'info@markdesign.net', 'Setting Update', '2019-12-26 02:45:20'),
(734, 'info@markdesign.net', 'Setting Update', '2019-12-26 02:48:49'),
(735, 'info@markdesign.net', 'Setting Update', '2019-12-26 03:01:49'),
(736, 'info@markdesign.net', 'Setting Update', '2019-12-26 03:04:08'),
(737, 'info@markdesign.net', 'Setting Update', '2019-12-26 03:29:00'),
(738, 'info@markdesign.net', 'Gallery Controller Create 2', '2019-12-26 03:39:10'),
(739, 'info@markdesign.net', 'Gallery Controller Create 3', '2019-12-26 03:40:01'),
(740, 'info@markdesign.net', 'Gallery Controller Create 4', '2019-12-26 03:40:34'),
(741, 'info@markdesign.net', 'Setting Update', '2019-12-26 03:57:17'),
(742, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(743, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(744, 'info@markdesign.net', 'Slide Controller Create 2', '0000-00-00 00:00:00'),
(745, 'info@markdesign.net', 'Slide Controller Create 3', '0000-00-00 00:00:00'),
(746, 'info@markdesign.net', 'Slide Controller Create 4', '0000-00-00 00:00:00'),
(747, 'info@markdesign.net', 'GalleryController Update 3', '0000-00-00 00:00:00'),
(748, 'info@markdesign.net', 'GalleryController Update 2', '0000-00-00 00:00:00'),
(749, 'info@markdesign.net', 'GalleryController Update 1', '0000-00-00 00:00:00'),
(750, 'info@markdesign.net', 'GalleryController Update 4', '0000-00-00 00:00:00'),
(751, 'info@markdesign.net', 'GalleryController Update 3', '0000-00-00 00:00:00'),
(752, 'info@markdesign.net', 'GalleryController Update 3', '0000-00-00 00:00:00'),
(753, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(754, 'info@markdesign.net', 'GalleryController Update 3', '0000-00-00 00:00:00'),
(755, 'info@markdesign.net', 'GalleryController Update 2', '0000-00-00 00:00:00'),
(756, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(757, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(758, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(759, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(760, 'ibnudrift@gmail.com', 'Login: ibnudrift@gmail.com', '0000-00-00 00:00:00'),
(761, 'ibnudrift@gmail.com', 'Setting Update', '0000-00-00 00:00:00'),
(762, 'ibnudrift@gmail.com', 'Login: ibnudrift@gmail.com', '0000-00-00 00:00:00'),
(763, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(764, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(765, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(766, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(767, 'ibnudrift@gmail.com', 'Login: ibnudrift@gmail.com', '0000-00-00 00:00:00'),
(768, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(769, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(770, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(771, 'info@markdesign.net', 'Slide Controller Create 5', '0000-00-00 00:00:00'),
(772, 'info@markdesign.net', 'SlideController Update 2', '0000-00-00 00:00:00'),
(773, 'info@markdesign.net', 'SlideController Update 1', '0000-00-00 00:00:00'),
(774, 'info@markdesign.net', 'SlideController Update 5', '0000-00-00 00:00:00'),
(775, 'info@markdesign.net', 'SlideController Update 3', '0000-00-00 00:00:00'),
(776, 'info@markdesign.net', 'SlideController Update 4', '0000-00-00 00:00:00'),
(777, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(778, 'info@markdesign.net', 'SlideController Update 5', '0000-00-00 00:00:00'),
(779, 'info@markdesign.net', 'SlideController Update 3', '0000-00-00 00:00:00'),
(780, 'info@markdesign.net', 'SlideController Update 4', '0000-00-00 00:00:00'),
(781, 'info@markdesign.net', 'SlideController Update 2', '0000-00-00 00:00:00'),
(782, 'info@markdesign.net', 'SlideController Update 1', '0000-00-00 00:00:00'),
(783, 'info@markdesign.net', 'SlideController Update 1', '0000-00-00 00:00:00'),
(784, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(785, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(786, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(787, 'info@markdesign.net', 'GalleryController Update 3', '0000-00-00 00:00:00'),
(788, 'info@markdesign.net', 'GalleryController Update 4', '0000-00-00 00:00:00'),
(789, 'info@markdesign.net', 'GalleryController Update 2', '0000-00-00 00:00:00'),
(790, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(791, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(792, 'info@markdesign.net', 'Slide Controller Create 1', '0000-00-00 00:00:00'),
(793, 'ibnudrift@gmail.com', 'Login: ibnudrift@gmail.com', '2020-05-17 05:25:15'),
(794, 'ibnudrift@gmail.com', 'SlideController Update 1', '2020-05-17 06:01:52'),
(795, 'ibnudrift@gmail.com', 'SlideController Update 1', '2020-05-17 06:02:18'),
(796, 'ibnudrift@gmail.com', 'Login: ibnudrift@gmail.com', '2020-05-18 04:04:45'),
(797, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-18 04:04:53'),
(798, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-18 04:29:02'),
(799, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-18 04:29:12'),
(800, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-18 09:43:35'),
(801, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-18 10:26:46'),
(802, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-18 10:43:16'),
(803, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-18 11:59:19'),
(804, 'ibnudrift@gmail.com', 'SlideController Update 1', '2020-05-19 06:00:36'),
(805, 'ibnudrift@gmail.com', 'Slide Controller Create 2', '2020-05-19 06:01:10'),
(806, 'ibnudrift@gmail.com', 'Slide Controller Create 3', '2020-05-19 06:01:23'),
(807, 'ibnudrift@gmail.com', 'Slide Controller Create 4', '2020-05-19 06:01:33'),
(808, 'ibnudrift@gmail.com', 'Slide Controller Create 5', '2020-05-19 06:01:46'),
(809, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 00:54:51'),
(810, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 00:55:14'),
(811, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 00:55:46'),
(812, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 00:56:09'),
(813, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 02:30:26'),
(814, 'info@markdesign.net', 'Login: info@markdesign.net', '2020-05-22 03:22:11'),
(815, 'info@markdesign.net', 'PrdProduct Controller Create 1', '2020-05-22 03:49:11'),
(816, 'info@markdesign.net', 'ProductController Update 1', '2020-05-22 03:49:47'),
(817, 'info@markdesign.net', 'PrdProduct Controller Create 2', '2020-05-22 03:51:43'),
(818, 'info@markdesign.net', 'PrdProduct Controller Create 3', '2020-05-22 03:54:35'),
(819, 'info@markdesign.net', 'PrdProduct Controller Create 4', '2020-05-22 04:02:42'),
(820, 'info@markdesign.net', 'PrdProduct Controller Create 5', '2020-05-22 04:05:41'),
(821, 'info@markdesign.net', 'PrdProduct Controller Create 6', '2020-05-22 04:17:00'),
(822, 'info@markdesign.net', 'PrdProduct Controller Create 7', '2020-05-22 04:18:24'),
(823, 'info@markdesign.net', 'PrdProduct Controller Create 8', '2020-05-22 04:19:53'),
(824, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 04:22:58'),
(825, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 04:27:23'),
(826, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 04:33:08'),
(827, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 04:35:35'),
(828, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 04:36:16'),
(829, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 05:00:31'),
(830, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 05:01:23'),
(831, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 05:02:07'),
(832, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 05:02:30'),
(833, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 05:04:27'),
(834, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 05:35:41'),
(835, 'info@markdesign.net', 'PrdProduct Controller Create 9', '2020-05-22 06:10:57'),
(836, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 06:16:57'),
(837, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 06:17:24'),
(838, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 06:18:09'),
(839, 'info@markdesign.net', 'ProductController Update 9', '2020-05-22 06:19:14'),
(840, 'info@markdesign.net', 'ProductController Update 9', '2020-05-22 06:20:02'),
(841, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 06:20:39'),
(842, 'info@markdesign.net', 'ProductController Update 9', '2020-05-22 06:20:49'),
(843, 'info@markdesign.net', 'PrdProduct Controller Create 10', '2020-05-22 06:26:26'),
(844, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 06:26:54'),
(845, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 06:27:27'),
(846, 'info@markdesign.net', 'PrdProduct Controller Create 11', '2020-05-22 06:29:05'),
(847, 'info@markdesign.net', 'PrdProduct Controller Create 12', '2020-05-22 06:32:29'),
(848, 'info@markdesign.net', 'PrdProduct Controller Create 13', '2020-05-22 06:36:00'),
(849, 'info@markdesign.net', 'PrdProduct Controller Create 14', '2020-05-22 06:38:05'),
(850, 'info@markdesign.net', 'PrdProduct Controller Create 15', '2020-05-22 06:39:58'),
(851, 'info@markdesign.net', 'PrdProduct Controller Create 16', '2020-05-22 06:42:29'),
(852, 'info@markdesign.net', 'PrdProduct Controller Create 17', '2020-05-22 06:52:45'),
(853, 'info@markdesign.net', 'PrdProduct Controller Create 18', '2020-05-22 06:54:41'),
(854, 'info@markdesign.net', 'PrdProduct Controller Create 19', '2020-05-22 06:57:19'),
(855, 'info@markdesign.net', 'PrdProduct Controller Create 20', '2020-05-22 06:58:52'),
(856, 'info@markdesign.net', 'ProductController Update 1', '2020-05-22 07:01:22'),
(857, 'info@markdesign.net', 'ProductController Update 2', '2020-05-22 07:02:19'),
(858, 'info@markdesign.net', 'ProductController Update 3', '2020-05-22 07:02:59'),
(859, 'info@markdesign.net', 'ProductController Update 4', '2020-05-22 07:03:41'),
(860, 'info@markdesign.net', 'ProductController Update 5', '2020-05-22 07:04:39'),
(861, 'info@markdesign.net', 'ProductController Update 6', '2020-05-22 07:05:23'),
(862, 'info@markdesign.net', 'ProductController Update 7', '2020-05-22 07:06:05'),
(863, 'info@markdesign.net', 'ProductController Update 8', '2020-05-22 07:06:46'),
(864, 'info@markdesign.net', 'ProductController Update 10', '2020-05-22 07:07:09'),
(865, 'ibnudrift@gmail.com', 'TcareerController Create 1', '2020-05-22 07:29:17'),
(866, 'ibnudrift@gmail.com', 'TcareerController Create 2', '2020-05-22 07:30:16'),
(867, 'ibnudrift@gmail.com', 'TcareerController Update 2', '2020-05-22 07:37:04'),
(868, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 07:45:55'),
(869, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 07:46:41'),
(870, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 08:10:43'),
(871, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 08:11:29'),
(872, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 08:12:22'),
(873, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 08:12:52'),
(874, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 08:14:05'),
(875, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 08:14:26'),
(876, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 08:21:22'),
(877, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 08:24:34'),
(878, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 08:25:15'),
(879, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 08:25:31'),
(880, 'ibnudrift@gmail.com', 'Setting Update', '2020-05-22 08:25:45'),
(881, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(882, 'ibnudrift@gmail.com', 'Login: ibnudrift@gmail.com', '2020-06-04 08:04:48'),
(883, 'ibnudrift@gmail.com', 'Create Category 1', '2020-06-04 08:40:36'),
(884, 'ibnudrift@gmail.com', 'Create Category 2', '2020-06-04 08:40:46'),
(885, 'ibnudrift@gmail.com', 'Create Category 3', '2020-06-04 08:41:00'),
(886, 'ibnudrift@gmail.com', 'ProductController Update 1', '2020-06-04 08:41:46'),
(887, 'ibnudrift@gmail.com', 'ProductController Update 2', '2020-06-04 08:41:51'),
(888, 'ibnudrift@gmail.com', 'ProductController Update 3', '2020-06-04 08:41:56'),
(889, 'ibnudrift@gmail.com', 'ProductController Update 4', '2020-06-04 08:42:01'),
(890, 'ibnudrift@gmail.com', 'ProductController Update 5', '2020-06-04 08:42:07'),
(891, 'ibnudrift@gmail.com', 'ProductController Update 6', '2020-06-04 08:42:14'),
(892, 'ibnudrift@gmail.com', 'ProductController Update 7', '2020-06-04 08:42:20'),
(893, 'ibnudrift@gmail.com', 'ProductController Update 8', '2020-06-04 08:42:26'),
(894, 'ibnudrift@gmail.com', 'ProductController Update 9', '2020-06-04 08:42:36'),
(895, 'ibnudrift@gmail.com', 'ProductController Update 10', '2020-06-04 08:42:53'),
(896, 'ibnudrift@gmail.com', 'ProductController Update 19', '2020-06-04 08:43:42'),
(897, 'ibnudrift@gmail.com', 'ProductController Update 20', '2020-06-04 08:43:51'),
(898, 'ibnudrift@gmail.com', 'ProductController Update 19', '2020-06-04 08:43:59'),
(899, 'ibnudrift@gmail.com', 'ProductController Update 15', '2020-06-04 08:44:27'),
(900, 'ibnudrift@gmail.com', 'ProductController Update 16', '2020-06-04 08:44:36'),
(901, 'ibnudrift@gmail.com', 'ProductController Update 17', '2020-06-04 08:44:45'),
(902, 'ibnudrift@gmail.com', 'ProductController Update 18', '2020-06-04 08:44:54'),
(903, 'ibnudrift@gmail.com', 'ProductController Update 20', '2020-06-04 08:45:12'),
(904, 'info@markdesign.net', 'Login: info@markdesign.net', '2020-06-26 10:29:57'),
(905, 'info@markdesign.net', 'Create Category 4', '2020-06-26 10:30:19'),
(906, 'info@markdesign.net', 'Create Category 5', '2020-06-26 10:30:35'),
(907, 'info@markdesign.net', 'PrdProduct Controller Create 21', '2020-06-26 10:34:06'),
(908, 'info@markdesign.net', 'PrdProduct Controller Create 22', '2020-06-26 10:36:13'),
(909, 'info@markdesign.net', 'PrdProduct Controller Create 23', '2020-06-26 10:37:19'),
(910, 'info@markdesign.net', 'PrdProduct Controller Create 24', '2020-06-26 10:39:16'),
(911, 'info@markdesign.net', 'PrdProduct Controller Create 25', '2020-06-26 10:40:17'),
(912, 'info@markdesign.net', 'Login: info@markdesign.net', '2020-07-27 06:37:37'),
(913, 'info@markdesign.net', 'Slide Controller Create 1', '2020-07-27 10:01:33'),
(914, 'ibnudrift@gmail.com', 'Login: ibnudrift@gmail.com', '2020-07-29 03:48:29'),
(915, 'ibnudrift@gmail.com', 'SlideController Update 1', '2020-08-07 13:59:41'),
(916, 'ibnudrift@gmail.com', 'Login: ibnudrift@gmail.com', '2020-08-10 12:20:55'),
(917, 'ibnudrift@gmail.com', 'SlideController Update 1', '2020-08-10 13:52:49'),
(918, 'ibnudrift@gmail.com', 'Login: ibnudrift@gmail.com', '2020-08-15 08:31:48'),
(919, 'ibnudrift@gmail.com', 'SlideController Update 1', '2020-08-15 09:26:03'),
(920, 'ibnudrift@gmail.com', 'SlideController Update 1', '2020-08-15 09:28:42'),
(921, 'ibnudrift@gmail.com', 'SlideController Update 1', '2020-08-15 09:29:49'),
(922, 'ibnudrift@gmail.com', 'Login: ibnudrift@gmail.com', '2020-08-24 16:05:07'),
(923, 'ibnudrift@gmail.com', 'SlideController Update 1', '2020-08-24 16:05:41'),
(924, 'ibnudrift@gmail.com', 'SlideController Update 1', '2020-08-24 16:07:11');

-- --------------------------------------------------------

--
-- Table structure for table `me_member`
--

CREATE TABLE `me_member` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `login_terakhir` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `aktivasi` int(11) NOT NULL,
  `aktif` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `hp` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `postcode` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `me_member`
--

INSERT INTO `me_member` (`id`, `email`, `first_name`, `last_name`, `pass`, `login_terakhir`, `aktivasi`, `aktif`, `image`, `hp`, `address`, `city`, `province`, `postcode`) VALUES
(3, 'deo@markdesign.net', 'sales', 'dv', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2015-07-10 04:44:34', 0, 1, '', '584651561', 'ajasdklaj', 'akjdsklja', 'kjsalkdjl', 'akjsd'),
(2, 'deoryzpandu@gmail.com', 'deory pandu putra', 'wahyu', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2015-07-10 03:31:34', 0, 1, '', '0854646464', 'jl test test', 'batu', 'Australian Capital Territory', '65656');

-- --------------------------------------------------------

--
-- Table structure for table `or_order`
--

CREATE TABLE `or_order` (
  `id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `invoice_prefix` varchar(20) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(128) NOT NULL,
  `payment_first_name` varchar(128) NOT NULL,
  `payment_last_name` varchar(128) NOT NULL,
  `payment_company` varchar(128) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(128) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `shipping_first_name` varchar(128) NOT NULL,
  `shipping_last_name` varchar(128) NOT NULL,
  `shipping_company` varchar(128) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(128) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_area` int(11) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `tax` decimal(15,4) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(100) NOT NULL,
  `currency_value` decimal(15,4) NOT NULL,
  `ip` varchar(128) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_modif` datetime NOT NULL,
  `delivery_from` varchar(100) NOT NULL,
  `delivery_to` varchar(100) NOT NULL,
  `delivery_package` varchar(100) NOT NULL,
  `delivery_price` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `delivery_weight` int(11) NOT NULL,
  `token` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `or_order`
--

INSERT INTO `or_order` (`id`, `invoice_no`, `invoice_prefix`, `customer_id`, `customer_group_id`, `first_name`, `last_name`, `email`, `phone`, `payment_first_name`, `payment_last_name`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_zone`, `payment_country`, `shipping_first_name`, `shipping_last_name`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_zone`, `shipping_area`, `shipping_country`, `comment`, `tax`, `total`, `order_status_id`, `affiliate_id`, `commission`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `date_add`, `date_modif`, `delivery_from`, `delivery_to`, `delivery_package`, `delivery_price`, `payment_method_id`, `delivery_weight`, `token`) VALUES
(3, 5342, 'DV-20150618', 2, 0, 'deory pandu putra', 'wahyu', 'deoryzpandu@gmail.com', '0854646464', 'deory pandu putra', 'wahyu', '', 'jl test', '', 'batu', '65656', 'Australian Capital Territory', '', 'deory pandu putra', 'wahyu', '', 'jl test', '', 'batu', '65656', 'Australian Capital Territory', 3, '', '', 13.4545, 123.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', 25, 0, 10000, ''),
(4, 8189, 'DV-20150618', 2, 0, 'deory pandu putra', 'wahyu', 'deoryzpandu@gmail.com', '0854646464', 'deory pandu putra', 'wahyu', '', 'jl test', '', 'batu', '65656', 'Australian Capital Territory', '', 'deory pandu putra', 'wahyu', '', 'jl test', '', 'batu', '65656', 'Australian Capital Territory', 6, '', '', 14.2727, 123.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', 34, 0, 10000, '');

-- --------------------------------------------------------

--
-- Table structure for table `or_order_history`
--

CREATE TABLE `or_order_history` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(4) NOT NULL,
  `comment` text NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `or_order_history`
--

INSERT INTO `or_order_history` (`id`, `member_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_add`) VALUES
(3, 2, 3, 1, 0, 'Your order DV-20150618-5342 successfully placed with status \"Pending\"', '2015-06-18 09:01:43'),
(4, 2, 4, 1, 0, 'Your order DV-20150618-8189 successfully placed with status \"Pending\"', '2015-06-18 09:02:25');

-- --------------------------------------------------------

--
-- Table structure for table `or_order_product`
--

CREATE TABLE `or_order_product` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `name` varchar(256) NOT NULL,
  `kode` varchar(256) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `attributes_id` int(11) NOT NULL,
  `attributes_name` varchar(256) NOT NULL,
  `attributes_price` decimal(15,4) NOT NULL,
  `berat` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `or_order_product`
--

INSERT INTO `or_order_product` (`id`, `order_id`, `product_id`, `image`, `name`, `kode`, `qty`, `price`, `total`, `attributes_id`, `attributes_name`, `attributes_price`, `berat`) VALUES
(3, 3, 213, 'e6c97-11867.jpg', 'HP - 15 g040 au', 'HP-15g040au', 1, 123.0000, 123.0000, 1, 'test', 123.0000, 10000),
(4, 4, 213, 'e6c97-11867.jpg', 'HP - 15 g040 au', 'HP-15g040au', 1, 123.0000, 123.0000, 1, 'test', 123.0000, 10000);

-- --------------------------------------------------------

--
-- Table structure for table `or_order_status`
--

CREATE TABLE `or_order_status` (
  `order_status_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `or_order_status`
--

INSERT INTO `or_order_status` (`order_status_id`, `name`) VALUES
(2, 'Processing'),
(3, 'Shipped'),
(7, 'Canceled'),
(5, 'Complete'),
(8, 'Denied'),
(9, 'Canceled Reversal'),
(10, 'Failed'),
(11, 'Refunded'),
(12, 'Reversed'),
(13, 'Chargeback'),
(1, 'Pending'),
(16, 'Voided'),
(15, 'Processed'),
(14, 'Expired'),
(17, 'Paid');

-- --------------------------------------------------------

--
-- Table structure for table `pg_bank`
--

CREATE TABLE `pg_bank` (
  `id` int(25) NOT NULL,
  `id_bank` int(25) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `rekening` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_bank`
--

INSERT INTO `pg_bank` (`id`, `id_bank`, `nama`, `rekening`) VALUES
(1, 2, 'test nama bank', 2147483647),
(2, 4, 'test nama', 928374837);

-- --------------------------------------------------------

--
-- Table structure for table `pg_blog`
--

CREATE TABLE `pg_blog` (
  `id` int(11) NOT NULL,
  `topik_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `date_input` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `last_update_by` varchar(255) NOT NULL,
  `writer` int(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_blog`
--

INSERT INTO `pg_blog` (`id`, `topik_id`, `image`, `active`, `date_input`, `date_update`, `insert_by`, `last_update_by`, `writer`) VALUES
(1, 16, '7e0ec-5.jpg', 1, '2014-10-27 15:12:19', '2014-11-06 18:50:37', 'ibnu@markdesign.net', 'deoryzpandu@gmail.com', 32),
(2, 16, 'ebc90-91.jpg', 1, '2014-10-27 15:28:51', '2014-11-06 18:50:51', 'ibnu@markdesign.net', 'deoryzpandu@gmail.com', 32),
(5, 3, '3c1cf-example-article.jpg', 1, '2013-10-30 10:48:42', '2014-10-28 10:48:42', 'ibnu@markdesign.net', 'ibnu@markdesign.net', 32),
(4, 16, '1d897-Demarco-Latest-Bridal-Jewelry-Rings-Design-2013-5.jpg', 1, '2014-08-30 10:45:52', '2014-11-06 18:50:18', 'ibnu@markdesign.net', 'deoryzpandu@gmail.com', 32),
(6, 16, 'beac3-Bridal-Necklace-jewelry-hd-wallpapers-top-desktop-jewelry-images-in-widescreen.jpg', 1, '2014-11-05 16:51:02', '2014-11-06 18:28:03', 'deoryzpandu@gmail.com', 'deoryzpandu@gmail.com', 1),
(7, 16, '45739-custom-jewelry-design-300x300.jpeg', 1, '2014-11-06 18:25:55', '2014-11-06 18:43:45', 'deoryzpandu@gmail.com', 'deoryzpandu@gmail.com', 1),
(8, 16, '6ac2a-Bridal-Necklace-jewelry-hd-wallpapers-top-desktop-jewelry-images-in-widescreen.jpg', 1, '2014-11-06 18:26:56', '2014-11-06 18:43:23', 'deoryzpandu@gmail.com', 'deoryzpandu@gmail.com', 1),
(9, 18, '9cfbc-Diamond_Jewelry_Gold_Jewellery_Diamond_Rings.jpg', 1, '2014-11-06 18:44:24', '2014-11-06 18:44:24', 'deoryzpandu@gmail.com', 'deoryzpandu@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pg_blog_description`
--

CREATE TABLE `pg_blog_description` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_blog_description`
--

INSERT INTO `pg_blog_description` (`id`, `blog_id`, `language_id`, `title`, `content`) VALUES
(25, 2, 2, 'manajemen kualifikasi', '<p>\r\n	    test aja\r\n</p>'),
(23, 4, 2, 'test data on few month ago', '<p>\r\n	   asfasdfasdfasdfasdfasdfadfadf\r\n</p>'),
(7, 5, 2, 'test data on few year ago', '<p>\r\n	aklsdjflkajsdflkajsfkjskdjflkasjflkjsafkjlasdf\r\n</p>'),
(13, 6, 2, 'Model minute: Tips cantik ala Raisa untuk daily activity!', '<p>\r\n	   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, odio quis accusamus officiis ipsam libero quaerat distinctio delectus accusantium, ullam fuga consequuntur provident? Voluptate facere architecto aut deleniti a mollitia, eaque blanditiis recusandae tenetur libero minima quod beatae hic dolorem.\r\n</p>\r\n<p>\r\n	   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, odio quis accusamus officiis ipsam libero quaerat distinctio delectus accusantium, ullam fuga consequuntur provident? Voluptate facere architecto aut deleniti a mollitia, eaque blanditiis recusandae tenetur libero minima quod beatae hic dolorem.\r\n</p>'),
(16, 7, 2, 'Bagaimana tampil sederhana, namun tetap “Fabulous” seperti Chelsea Islan', '<p>\r\n	  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam  convallis mauris et arcu bibendum, in placerat nisi semper. Vivamus eu  risus aliquam, vehicula magna eu, sodales elit. Quisque pellentesque est  sed dolor fermentum, eget porta diam ultricies. Mauris id viverra diam.  Vestibulum pretium purus accumsan molestie bibendum. Nullam sed purus  vitae mi viverra volutpat. Vestibulum vulputate metus quis velit semper  venenatis. Curabitur sed sapien justo. Integer eu urna lorem. Phasellus  ac auctor quam. Vivamus gravida posuere tortor, eu placerat metus  tincidunt non. Nulla ultrices eu purus quis mattis. Praesent ut orci  molestie, molestie leo ac, ornare est. Maecenas ornare leo faucibus,  rutrum diam a, finibus risus. Nunc congue laoreet porttitor.\r\n</p>\r\n<p>\r\n	  Nulla ullamcorper ipsum vel dui egestas  mattis. Aenean ut convallis augue. Integer nec tempor nibh. Fusce quis  odio id est tempor mollis vitae at ex. In ut est pellentesque, porttitor  est eu, eleifend lectus. Sed sollicitudin leo non ante elementum  ultricies. Quisque vel varius orci, in elementum urna.\r\n</p>'),
(15, 8, 2, 'Model minute: Tips cantik ala Raisa untuk daily activity!', '<p>\r\n	   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam  convallis mauris et arcu bibendum, in placerat nisi semper. Vivamus eu  risus aliquam, vehicula magna eu, sodales elit. Quisque pellentesque est  sed dolor fermentum, eget porta diam ultricies. Mauris id viverra diam.  Vestibulum pretium purus accumsan molestie bibendum. Nullam sed purus  vitae mi viverra volutpat. Vestibulum vulputate metus quis velit semper  venenatis. Curabitur sed sapien justo. Integer eu urna lorem. Phasellus  ac auctor quam. Vivamus gravida posuere tortor, eu placerat metus  tincidunt non. Nulla ultrices eu purus quis mattis. Praesent ut orci  molestie, molestie leo ac, ornare est. Maecenas ornare leo faucibus,  rutrum diam a, finibus risus. Nunc congue laoreet porttitor.\r\n</p>\r\n<p>\r\n	   Nulla ullamcorper ipsum vel dui egestas  mattis. Aenean ut convallis augue. Integer nec tempor nibh. Fusce quis  odio id est tempor mollis vitae at ex. In ut est pellentesque, porttitor  est eu, eleifend lectus. Sed sollicitudin leo non ante elementum  ultricies. Quisque vel varius orci, in elementum urna.\r\n</p>'),
(17, 9, 2, 'Masquerade party? How to “still” look fabulous with nude pallete.', '<p>\r\n	    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam   convallis mauris et arcu bibendum, in placerat nisi semper. Vivamus eu   risus aliquam, vehicula magna eu, sodales elit. Quisque pellentesque est   sed dolor fermentum, eget porta diam ultricies. Mauris id viverra  diam.  Vestibulum pretium purus accumsan molestie bibendum. Nullam sed  purus  vitae mi viverra volutpat. Vestibulum vulputate metus quis velit  semper  venenatis. Curabitur sed sapien justo. Integer eu urna lorem.  Phasellus  ac auctor quam. Vivamus gravida posuere tortor, eu placerat  metus  tincidunt non. Nulla ultrices eu purus quis mattis. Praesent ut  orci  molestie, molestie leo ac, ornare est. Maecenas ornare leo  faucibus,  rutrum diam a, finibus risus. Nunc congue laoreet porttitor.\r\n</p>\r\n<p>\r\n	    Nulla ullamcorper ipsum vel dui egestas  mattis. Aenean ut convallis  augue. Integer nec tempor nibh. Fusce quis  odio id est tempor mollis  vitae at ex. In ut est pellentesque, porttitor  est eu, eleifend lectus.  Sed sollicitudin leo non ante elementum  ultricies. Quisque vel varius  orci, in elementum urna.\r\n</p>'),
(24, 1, 2, 'coba lagi', '<p>\r\n	   anda belum beruntung\r\n</p>');

-- --------------------------------------------------------

--
-- Table structure for table `pg_faq`
--

CREATE TABLE `pg_faq` (
  `id` int(20) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_faq`
--

INSERT INTO `pg_faq` (`id`, `status`) VALUES
(1, 1),
(2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pg_faq_description`
--

CREATE TABLE `pg_faq_description` (
  `id` int(11) NOT NULL,
  `faq_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_faq_description`
--

INSERT INTO `pg_faq_description` (`id`, `faq_id`, `language_id`, `question`, `answer`) VALUES
(1, 1, 1, 'test tanya indo', '<p>\r\n	test jawab indo\r\n</p>'),
(2, 1, 2, 'test tanya?', '<p>\r\n	test jawab 1\r\n</p>'),
(3, 2, 2, 'test question 1', '<p>\r\n	test answer 1\r\n</p>');

-- --------------------------------------------------------

--
-- Table structure for table `pg_list_bank`
--

CREATE TABLE `pg_list_bank` (
  `id` int(50) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `label` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_list_bank`
--

INSERT INTO `pg_list_bank` (`id`, `nama`, `label`) VALUES
(1, 'bank_mega', 'Bank Mega'),
(2, 'bca', 'BCA'),
(3, 'bca_syariah', 'BCA Syariah'),
(4, 'bii', 'BII'),
(5, 'bni', 'BNI'),
(6, 'bni_syariah', 'BNI Syariah'),
(7, 'bri', 'BRI'),
(8, 'bri_syariah', 'BRI Syariah'),
(9, 'cimb_niaga', 'CIMB Niaga'),
(10, 'cimb_niaga_syariah', 'CIMB Niaga Syariah'),
(11, 'citibank', 'Citibank'),
(12, 'danamon', 'Danamon'),
(13, 'hsbc', 'HSBC'),
(14, 'mandiri', 'Mandiri'),
(15, 'mandiri_syariah', 'Mandiri Syariah'),
(16, 'money_gram', 'Money Gram'),
(17, 'muamalat', 'Muamalat'),
(18, 'paypal', 'Paypal'),
(19, 'permata', 'Permata'),
(20, 'visa', 'Visa'),
(21, 'western_union', 'Western Union');

-- --------------------------------------------------------

--
-- Table structure for table `pg_pages`
--

CREATE TABLE `pg_pages` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `type` int(1) NOT NULL DEFAULT 1,
  `group` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_pages`
--

INSERT INTO `pg_pages` (`id`, `name`, `type`, `group`) VALUES
(1, 'testimonial', 0, 'testimonial'),
(2, 'articles', 0, 'blog'),
(3, 'about', 0, 'static'),
(4, 'contact', 0, 'static'),
(5, 'faq', 0, 'faq'),
(6, 'how_to_shop', 0, 'static'),
(7, 'payment_confirmation', 0, 'static'),
(8, 'bank', 0, 'bank');

-- --------------------------------------------------------

--
-- Table structure for table `pg_pages_description`
--

CREATE TABLE `pg_pages_description` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `page_name` varchar(225) NOT NULL,
  `content` longtext NOT NULL,
  `meta_title` varchar(225) NOT NULL,
  `meta_keyword` text NOT NULL,
  `meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_pages_description`
--

INSERT INTO `pg_pages_description` (`id`, `page_id`, `language_id`, `page_name`, `content`, `meta_title`, `meta_keyword`, `meta_description`) VALUES
(11, 4, 1, 'Kontak Kami', '<p>\r\n	Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; Contact\r\n</p>\r\n<p>\r\n	  CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'Kontak Kami', 'edit di admin panel -> Pages -> Contact', 'edit di admin panel -> Pages -> Contact'),
(12, 4, 2, 'Contact', '<p>\r\n	Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; Contact\r\n</p>\r\n<p>\r\n	  CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'Contact', 'edit di admin panel -> Pages -> Contact', 'edit di admin panel -> Pages -> Contact'),
(13, 6, 1, 'How To Shop', '<p>\r\n	Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; How To Shop\r\n</p>\r\n<p>\r\n	   CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'How To Shop', 'Edit di admin panel -> Pages -> How To Shop', 'Edit di admin panel -> Pages -> How To Shop'),
(14, 6, 2, 'How To Shop', '<p>\r\n	Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; How To Shop\r\n</p>\r\n<p>\r\n	   CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'How To Shop', 'Edit di admin panel -> Pages -> How To Shop', 'Edit di admin panel -> Pages -> How To Shop'),
(15, 7, 1, 'Konfirmasi Pembayaran', '<p>\r\n	     Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; Payment Confirmation\r\n</p>\r\n<p>\r\n	   CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'Konfirmasi Pembayaran', 'Edit di admin panel -> Pages -> Payment Confirmation', 'Edit di admin panel -> Pages -> Payment Confirmation'),
(16, 7, 2, 'Payment Confirmation', '<p>\r\n	     Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; Payment Confirmation\r\n</p>\r\n<p>\r\n	   CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'Payment Confirmation', 'Edit di admin panel -> Pages -> Payment Confirmation', 'Edit di admin panel -> Pages -> Payment Confirmation'),
(17, 3, 2, 'About', '<p>\r\n	     Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; About\r\n</p>\r\n<p>\r\n	   CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa lain di website anda di admin panel -&gt; General Setting -&gt; Language(Bahasa)\r\n</p>', 'About', 'edit di admin panel -> Pages -> About', 'edit di admin panel -> Pages -> About');

-- --------------------------------------------------------

--
-- Table structure for table `pg_testimonial`
--

CREATE TABLE `pg_testimonial` (
  `id` int(25) NOT NULL,
  `name` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `testimonial` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_testimonial`
--

INSERT INTO `pg_testimonial` (`id`, `name`, `email`, `testimonial`, `status`, `date`) VALUES
(1, 'Ibnu', 'ibnu@markdesign.net', '', 1, '2014-07-14 09:51:53');

-- --------------------------------------------------------

--
-- Table structure for table `pg_testimonial_description`
--

CREATE TABLE `pg_testimonial_description` (
  `id` int(11) NOT NULL,
  `testimonial_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_testimonial_description`
--

INSERT INTO `pg_testimonial_description` (`id`, `testimonial_id`, `language_id`, `content`) VALUES
(8, 1, 2, 'test'),
(7, 1, 1, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `pg_type_letak`
--

CREATE TABLE `pg_type_letak` (
  `id` int(11) NOT NULL,
  `letak` varchar(225) NOT NULL,
  `page_id` int(11) NOT NULL,
  `tampil` int(11) NOT NULL,
  `sort` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_type_letak`
--

INSERT INTO `pg_type_letak` (`id`, `letak`, `page_id`, `tampil`, `sort`) VALUES
(177, 'header', 2, 1, 1),
(178, 'header', 6, 1, 2),
(179, 'header', 4, 1, 3),
(180, 'header', 1, 0, 4),
(181, 'header', 3, 0, 5),
(182, 'header', 5, 0, 6),
(183, 'header', 7, 0, 7),
(184, 'header', 8, 0, 8),
(185, 'footer', 3, 1, 1),
(186, 'footer', 6, 1, 2),
(187, 'footer', 4, 1, 3),
(188, 'footer', 1, 0, 4),
(189, 'footer', 2, 0, 5),
(190, 'footer', 5, 0, 6),
(191, 'footer', 7, 0, 7),
(192, 'footer', 8, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `prd_brand`
--

CREATE TABLE `prd_brand` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `date_input` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `last_update_by` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_brand_description`
--

CREATE TABLE `prd_brand_description` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_category`
--

CREATE TABLE `prd_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `sort` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `data` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_category`
--

INSERT INTO `prd_category` (`id`, `parent_id`, `sort`, `image`, `type`, `data`) VALUES
(1, 0, 3, '6fbd1-49b7d78fbfhome2-s1-icon.png', 'category', NULL),
(2, 0, 1, '2a78d-a5edfe7540home2-s2-icon.png', 'category', NULL),
(3, 0, 2, 'bfe22-2de648206fhome2-s3-icon.png', 'category', NULL),
(4, 3, 2, NULL, 'category', NULL),
(5, 3, 1, NULL, 'category', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prd_category_description`
--

CREATE TABLE `prd_category_description` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `data` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_category_description`
--

INSERT INTO `prd_category_description` (`id`, `category_id`, `language_id`, `name`, `data`) VALUES
(12, 1, 3, 'WOODWORKING', NULL),
(14, 2, 3, 'PLYWOOD', NULL),
(13, 2, 2, 'PLYWOOD', NULL),
(16, 3, 3, 'WOODEN DOORS', NULL),
(15, 3, 2, 'WOODEN DOORS', NULL),
(11, 1, 2, 'WOODWORKING', NULL),
(17, 4, 2, 'Flush Doors', NULL),
(18, 4, 3, 'Flush Doors', NULL),
(19, 5, 2, 'Panel Doors', NULL),
(20, 5, 3, 'Panel Doors', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prd_product`
--

CREATE TABLE `prd_product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `brand_id` int(11) NOT NULL DEFAULT 0,
  `image` varchar(200) DEFAULT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `harga_coret` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `berat` int(11) DEFAULT NULL,
  `terbaru` int(11) DEFAULT NULL,
  `terlaris` int(11) DEFAULT NULL,
  `out_stock` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `date_input` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `data` text DEFAULT NULL,
  `tag` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_product`
--

INSERT INTO `prd_product` (`id`, `category_id`, `brand_id`, `image`, `kode`, `harga`, `harga_coret`, `stock`, `berat`, `terbaru`, `terlaris`, `out_stock`, `status`, `date`, `date_input`, `date_update`, `data`, `tag`) VALUES
(1, 1, 0, '36461-products_n_ifura_20.jpg', '23423432', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 10:49:11', '2020-05-22 10:49:11', '2020-06-04 15:41:46', 's:21:\"s:13:\"s:6:\"a:0:{}\";\";\";', 'WOODWORKING'),
(2, 1, 0, '63ceb-products_n_ifura_19.jpg', '326541', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 10:51:43', '2020-05-22 10:51:43', '2020-06-04 15:41:51', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(3, 1, 0, '37b4a-products_n_ifura_18.jpg', '121364', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 10:54:35', '2020-05-22 10:54:35', '2020-06-04 15:41:56', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(4, 1, 0, 'e7f8a-products_n_ifura_17.jpg', '65846115', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 11:02:42', '2020-05-22 11:02:42', '2020-06-04 15:42:01', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(5, 1, 0, '1c207-products_n_ifura_16.jpg', '15656', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 11:05:41', '2020-05-22 11:05:41', '2020-06-04 15:42:07', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(6, 1, 0, '75c59-products_n_ifura_15.jpg', '85848', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 11:17:00', '2020-05-22 11:17:00', '2020-06-04 15:42:14', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(7, 1, 0, '252ab-products_n_ifura_14.jpg', '8154645', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 11:18:24', '2020-05-22 11:18:24', '2020-06-04 15:42:20', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(8, 1, 0, '57d07-products_n_ifura_13.jpg', '4656464', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 11:19:53', '2020-05-22 11:19:53', '2020-06-04 15:42:26', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(9, 1, 0, '2bd6a-products_n_ifura_12.jpg', '5465486', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:10:57', '2020-05-22 13:10:57', '2020-06-04 15:42:36', 's:29:\"s:21:\"s:13:\"s:6:\"a:0:{}\";\";\";\";', 'WOODWORKING'),
(10, 1, 0, 'b7b1c-products_n_ifura_11.jpg', '1861661', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:26:26', '2020-05-22 13:26:26', '2020-06-04 15:42:53', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(11, 1, 0, '6b5d5-products_n_ifura_10.jpg', '548456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:26:32', '2020-05-22 13:29:05', '2020-05-22 13:29:05', 'a:0:{}', ''),
(12, 1, 0, 'f793e-products_n_ifura_9.jpg', '6456561', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:29:51', '2020-05-22 13:32:29', '2020-05-22 13:32:29', 'a:0:{}', ''),
(13, 1, 0, '017f7-products_n_ifura_8.jpg', '156165', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:34:03', '2020-05-22 13:36:00', '2020-05-22 13:36:00', 'a:0:{}', ''),
(14, 1, 0, '892f7-products_n_ifura_7.jpg', '5565556', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:37:53', '2020-05-22 13:38:05', '2020-05-22 13:38:05', 'a:0:{}', ''),
(15, 2, 0, 'b6e2a-products_n_ifura_6.jpg', '9949449', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:39:58', '2020-05-22 13:39:58', '2020-06-04 15:44:27', 's:6:\"a:0:{}\";', 'PLYWOOD'),
(16, 2, 0, '392b9-products_n_ifura_5.jpg', '14184988', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:42:29', '2020-05-22 13:42:29', '2020-06-04 15:44:36', 's:6:\"a:0:{}\";', 'PLYWOOD'),
(17, 2, 0, '43fcb-products_n_ifura_4.jpg', '48484', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:52:45', '2020-05-22 13:52:45', '2020-06-04 15:44:45', 's:6:\"a:0:{}\";', 'PLYWOOD'),
(18, 2, 0, 'ff250-products_n_ifura_3.jpg', '419846', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:54:41', '2020-05-22 13:54:41', '2020-06-04 15:44:54', 's:6:\"a:0:{}\";', 'PLYWOOD'),
(19, 2, 0, '0a82a-products_n_ifura_2.jpg', '165649', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:57:19', '2020-05-22 13:57:19', '2020-06-04 15:43:59', 's:13:\"s:6:\"a:0:{}\";\";', 'PLYWOOD'),
(20, 2, 0, 'd6b0b-products_n_ifura_1.jpg', '548564', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:58:52', '2020-05-22 13:58:52', '2020-06-04 15:45:12', 's:13:\"s:6:\"a:0:{}\";\";', 'PLYWOOD'),
(21, 5, 0, '49dd6-lignum.jpg', 'door-001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-06-26 17:30:59', '2020-06-26 17:34:06', '2020-06-26 17:34:06', 'a:0:{}', 'WOODWORKING, Panel Doors'),
(22, 4, 0, 'aeb29-arktik.jpg', 'door-002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-06-26 17:35:59', '2020-06-26 17:36:13', '2020-06-26 17:36:13', 'a:0:{}', 'WOODWORKING, Flush Doors'),
(23, 4, 0, 'd9fc2-sahara.jpg', 'door-003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-06-26 17:36:17', '2020-06-26 17:37:19', '2020-06-26 17:37:19', 'a:0:{}', 'WOODWORKING, Flush Doors'),
(24, 4, 0, '02edf-sandy.jpg', 'door-004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-06-26 17:37:22', '2020-06-26 17:39:16', '2020-06-26 17:39:16', 'a:0:{}', 'WOODWORKING, Flush Doors'),
(25, 4, 0, 'd3d77-ventus.jpg', 'door-005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-06-26 17:39:19', '2020-06-26 17:40:17', '2020-06-26 17:40:17', 'a:0:{}', 'WOODWORKING, Flush Doors');

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_attributes`
--

CREATE TABLE `prd_product_attributes` (
  `id` int(11) NOT NULL,
  `id_str` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `attribute` varchar(200) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_color`
--

CREATE TABLE `prd_product_color` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `image_color` varchar(200) DEFAULT NULL,
  `label` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_description`
--

CREATE TABLE `prd_product_description` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT 0,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `desc` text DEFAULT NULL,
  `varian` longtext DEFAULT NULL,
  `application` longtext DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_desc` text DEFAULT NULL,
  `meta_key` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_product_description`
--

INSERT INTO `prd_product_description` (`id`, `product_id`, `language_id`, `name`, `desc`, `varian`, `application`, `meta_title`, `meta_desc`, `meta_key`) VALUES
(71, 7, 3, 'Merbau FJL Board Stair Thread', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', NULL, NULL, NULL),
(74, 9, 2, 'Merbau E4E Decking', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(75, 9, 3, 'Merbau E4E Decking', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(76, 10, 2, 'Merbau FJL Board', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(77, 10, 3, 'Merbau FJL Board', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(20, 11, 2, 'Merbau Reeded Decking', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(21, 11, 3, 'Merbau Reeded Decking', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(22, 12, 2, 'Merbau Clip Decking', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways</p>', NULL, NULL, NULL),
(23, 12, 3, 'Merbau Clip Decking', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(24, 13, 2, 'Merbau Clip Decking', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(25, 13, 3, 'Merbau Clip Decking', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(26, 14, 2, 'Merbau Solid Flooring T&G', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(27, 14, 3, 'Merbau Solid Flooring T&G', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(84, 15, 2, 'Costum Size Falcata LVL Stick', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(85, 15, 3, 'Costum Size Falcata LVL Stick', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(86, 16, 2, 'Falcata FJL Board', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(87, 16, 3, 'Falcata FJL Board', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.<br></p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(88, 17, 2, 'Plywood Falcata 7.5 - 11.5mm', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(89, 17, 3, 'Plywood Falcata 7.5 - 11.5mm', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(90, 18, 2, 'Plywood Falcata 12-23mm', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(91, 18, 3, 'Plywood Falcata 12-23mm', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(83, 19, 3, 'BlockBoard Falcata', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.<br></p>', NULL, NULL, NULL),
(93, 20, 3, 'BlockBoard MLH', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(58, 1, 2, 'Merbau Solid Flooring T&G', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(59, 1, 3, 'Merbau Solid Flooring T&G', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(60, 2, 2, 'Merbau FJL Bread Loaf Handrail', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(61, 2, 3, 'Merbau FJL Bread Loaf Handrail', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(62, 3, 2, 'Merbau FJL Ladies Waist Handrail', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(63, 3, 3, 'Merbau FJL Ladies Waist Handrail', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(64, 4, 2, 'Merbau Solid Bottom Rail', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(65, 4, 3, 'Merbau Solid Bottom Rail', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(66, 5, 2, 'Merbau FJL Post', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(67, 5, 3, 'Merbau FJL Post', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(68, 6, 2, 'Merbau FJL Beams', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(69, 6, 3, 'Merbau FJL Beams', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(70, 7, 2, 'Merbau FJL Board Stair Thread', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(72, 8, 2, 'Merbau AntiSlip Decking', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(73, 8, 3, 'Merbau AntiSlip Decking', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(82, 19, 2, 'BlockBoard Falcata', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(92, 20, 2, 'BlockBoard MLH', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(94, 21, 2, 'Lignum', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(95, 21, 3, 'Lignum', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(96, 22, 2, 'Arktik', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>lorem ipsum</p>', NULL, NULL, NULL),
(97, 22, 3, 'Arktik', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>lorem ipsum</p>', NULL, NULL, NULL),
(98, 23, 2, 'Sahara', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>lorem ipsum dolor isu amet.</p>', NULL, NULL, NULL),
(99, 23, 3, 'Sahara', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>lorem ipsum dolor isu amet.</p>', NULL, NULL, NULL),
(100, 24, 2, 'Sandy', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>lorem ipsum dolor isu amet.</p>', NULL, NULL, NULL),
(101, 24, 3, 'Sandy', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>lorem ipsum dolor isu amet.</p>', NULL, NULL, NULL),
(102, 25, 2, 'Ventus', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>lorem ipsum...</p>', NULL, NULL, NULL),
(103, 25, 3, 'Ventus', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>lorem ipsum dolor isu amet</p>', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_image`
--

CREATE TABLE `prd_product_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `realfood`
--

CREATE TABLE `realfood` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `bio` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `label` varchar(200) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(100) NOT NULL,
  `hide` int(11) NOT NULL,
  `group` varchar(100) NOT NULL,
  `dual_language` enum('n','y') NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `name`, `label`, `value`, `type`, `hide`, `group`, `dual_language`, `sort`) VALUES
(1, 'default_meta_title', 'Title', '', 'text', 0, 'default_meta', 'y', 1),
(2, 'default_meta_keywords', 'Keywords', '', 'textarea', 0, 'default_meta', 'y', 2),
(3, 'default_meta_description', 'Description', '', 'textarea', 0, 'default_meta', 'y', 3),
(4, 'google_tools_webmaster', 'Google Webmaster Code', '', 'textarea', 0, 'google_tools', 'n', 4),
(5, 'google_tools_analytic', 'Google Analytic Code', '', 'textarea', 0, 'google_tools', 'n', 5),
(6, 'purechat_status', 'Show Hide Widget', '', 'select', 0, 'purechat', 'n', 1),
(7, 'purechat_code', 'PureChat Code', '', 'textarea', 0, 'purechat', 'n', 1),
(8, 'invoice_start_number', 'Invoice Start Number', '1000', 'text', 0, 'invoice', 'n', 0),
(9, 'invoice_increment', 'Invoice Increment', '5', 'text', 0, 'invoice', 'n', 0),
(10, 'invoice_auto_cancel_after', 'Invoice Auto Cancel After', '72', 'text', 0, 'invoice', 'n', 0),
(11, 'lang_deff', 'Language Default', 'en', 'text', 0, 'data', 'n', 0),
(12, 'email', 'email', 'info@nippoindonesia.com', 'text', 0, 'data', 'n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `setting_description`
--

CREATE TABLE `setting_description` (
  `id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shp_shipping_price`
--

CREATE TABLE `shp_shipping_price` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shp_shipping_price`
--

INSERT INTO `shp_shipping_price` (`id`, `type`, `weight`, `price`) VALUES
(1, 1, 1, 20.0000),
(2, 1, 2, 20.0000),
(3, 1, 0, 20.0000),
(4, 1, 3, 20.0000),
(5, 1, 4, 20.0000),
(6, 1, 5, 20.0000),
(7, 1, 6, 20.0000),
(8, 1, 7, 20.0000),
(9, 1, 8, 20.0000),
(10, 1, 9, 20.0000),
(11, 1, 10, 20.0000),
(12, 1, 11, 20.0000),
(13, 1, 12, 20.0000),
(14, 1, 13, 20.0000),
(15, 1, 14, 20.0000),
(16, 1, 15, 20.0000),
(17, 1, 16, 20.0000),
(18, 1, 17, 20.0000),
(19, 1, 18, 20.0000),
(20, 1, 19, 20.0000),
(21, 1, 20, 40.0000),
(22, 1, 21, 40.0000),
(23, 1, 22, 40.0000),
(24, 1, 23, 40.0000),
(25, 1, 24, 40.0000),
(26, 1, 25, 40.0000),
(27, 1, 26, 40.0000),
(28, 1, 27, 40.0000),
(29, 1, 28, 40.0000),
(30, 1, 29, 40.0000),
(31, 1, 30, 40.0000),
(32, 1, 31, 40.0000),
(33, 1, 32, 40.0000),
(34, 1, 33, 40.0000),
(35, 1, 34, 40.0000),
(36, 1, 35, 40.0000),
(37, 1, 36, 40.0000),
(38, 1, 37, 40.0000),
(39, 1, 38, 40.0000),
(40, 1, 39, 40.0000),
(41, 1, 40, 60.0000),
(42, 1, 41, 60.0000),
(43, 1, 42, 60.0000),
(44, 1, 43, 60.0000),
(45, 1, 44, 60.0000),
(46, 1, 45, 60.0000),
(47, 1, 46, 60.0000),
(48, 1, 47, 60.0000),
(49, 1, 48, 60.0000),
(50, 1, 49, 60.0000),
(51, 1, 50, 60.0000),
(52, 1, 51, 60.0000),
(53, 1, 52, 60.0000),
(54, 1, 53, 60.0000),
(55, 1, 54, 60.0000),
(56, 1, 55, 60.0000),
(57, 1, 56, 60.0000),
(58, 1, 57, 60.0000),
(59, 1, 58, 60.0000),
(60, 1, 59, 60.0000),
(61, 1, 60, 80.0000),
(62, 1, 61, 80.0000),
(63, 1, 62, 80.0000),
(64, 1, 63, 80.0000),
(65, 1, 64, 80.0000),
(66, 1, 65, 80.0000),
(67, 1, 66, 80.0000),
(68, 1, 67, 80.0000),
(69, 1, 68, 80.0000),
(70, 1, 69, 80.0000),
(71, 1, 70, 80.0000),
(72, 1, 71, 80.0000),
(73, 1, 72, 80.0000),
(74, 1, 73, 80.0000),
(75, 1, 74, 80.0000),
(76, 1, 75, 80.0000),
(77, 1, 76, 80.0000),
(78, 1, 77, 80.0000),
(79, 1, 78, 80.0000),
(80, 1, 79, 80.0000),
(81, 1, 80, 100.0000),
(82, 1, 81, 100.0000),
(83, 1, 82, 100.0000),
(84, 1, 83, 100.0000),
(85, 1, 84, 100.0000),
(86, 1, 85, 100.0000),
(87, 1, 86, 100.0000),
(88, 1, 87, 100.0000),
(89, 1, 88, 100.0000),
(90, 1, 89, 100.0000),
(91, 1, 90, 100.0000),
(92, 1, 91, 100.0000),
(93, 1, 92, 100.0000),
(94, 1, 93, 100.0000),
(95, 1, 94, 100.0000),
(96, 1, 95, 100.0000),
(97, 1, 96, 100.0000),
(98, 1, 97, 100.0000),
(99, 1, 98, 100.0000),
(100, 1, 99, 100.0000),
(101, 2, 0, 12.0000),
(102, 2, 1, 12.0000),
(103, 2, 2, 12.0000),
(104, 2, 3, 12.0000),
(105, 2, 4, 12.0000),
(106, 2, 5, 12.0000),
(107, 2, 6, 12.0000),
(108, 2, 7, 12.0000),
(109, 2, 8, 12.0000),
(110, 2, 9, 12.0000),
(111, 2, 10, 12.0000),
(112, 2, 11, 12.0000),
(113, 2, 12, 12.0000),
(114, 2, 13, 12.0000),
(115, 2, 14, 12.0000),
(116, 2, 15, 12.0000),
(117, 2, 16, 12.0000),
(118, 2, 17, 12.0000),
(119, 2, 18, 12.0000),
(120, 2, 19, 12.0000),
(121, 2, 20, 24.0000),
(122, 2, 21, 24.0000),
(123, 2, 22, 24.0000),
(124, 2, 23, 24.0000),
(125, 2, 24, 24.0000),
(126, 2, 25, 24.0000),
(127, 2, 26, 24.0000),
(128, 2, 27, 24.0000),
(129, 2, 28, 24.0000),
(130, 2, 29, 24.0000),
(131, 2, 30, 24.0000),
(132, 2, 31, 24.0000),
(133, 2, 32, 24.0000),
(134, 2, 33, 24.0000),
(135, 2, 34, 24.0000),
(136, 2, 35, 24.0000),
(137, 2, 36, 24.0000),
(138, 2, 37, 24.0000),
(139, 2, 38, 24.0000),
(140, 2, 39, 24.0000),
(141, 2, 40, 48.0000),
(142, 2, 41, 48.0000),
(143, 2, 42, 48.0000),
(144, 2, 43, 48.0000),
(145, 2, 44, 48.0000),
(146, 2, 45, 48.0000),
(147, 2, 46, 48.0000),
(148, 2, 47, 48.0000),
(149, 2, 48, 48.0000),
(150, 2, 49, 48.0000),
(151, 2, 50, 48.0000),
(152, 2, 51, 48.0000),
(153, 2, 52, 48.0000),
(154, 2, 53, 48.0000),
(155, 2, 54, 48.0000),
(156, 2, 55, 48.0000),
(157, 2, 56, 48.0000),
(158, 2, 57, 48.0000),
(159, 2, 58, 48.0000),
(160, 2, 59, 48.0000),
(161, 2, 60, 60.0000),
(162, 2, 61, 60.0000),
(163, 2, 62, 60.0000),
(164, 2, 63, 60.0000),
(165, 2, 64, 60.0000),
(166, 2, 65, 60.0000),
(167, 2, 66, 60.0000),
(168, 2, 67, 60.0000),
(169, 2, 68, 60.0000),
(170, 2, 69, 60.0000),
(171, 2, 70, 60.0000),
(172, 2, 71, 60.0000),
(173, 2, 72, 60.0000),
(174, 2, 73, 60.0000),
(175, 2, 74, 60.0000),
(176, 2, 75, 60.0000),
(177, 2, 76, 60.0000),
(178, 2, 77, 60.0000),
(179, 2, 78, 60.0000),
(180, 2, 79, 60.0000),
(181, 2, 80, 72.0000),
(182, 2, 81, 72.0000),
(183, 2, 82, 72.0000),
(184, 2, 83, 72.0000),
(185, 2, 84, 72.0000),
(186, 2, 85, 72.0000),
(187, 2, 86, 72.0000),
(188, 2, 87, 72.0000),
(189, 2, 88, 72.0000),
(190, 2, 89, 72.0000),
(191, 2, 90, 72.0000),
(192, 2, 91, 72.0000),
(193, 2, 92, 72.0000),
(194, 2, 93, 72.0000),
(195, 2, 94, 72.0000),
(196, 2, 95, 72.0000),
(197, 2, 96, 72.0000),
(198, 2, 97, 72.0000),
(199, 2, 98, 72.0000),
(200, 2, 99, 72.0000),
(201, 3, 0, 25.0000),
(202, 3, 1, 25.0000),
(203, 3, 2, 25.0000),
(204, 3, 3, 25.0000),
(205, 3, 4, 25.0000),
(206, 3, 5, 25.0000),
(207, 3, 6, 25.0000),
(208, 3, 7, 25.0000),
(209, 3, 8, 25.0000),
(210, 3, 9, 25.0000),
(211, 3, 10, 25.0000),
(212, 3, 11, 25.0000),
(213, 3, 12, 25.0000),
(214, 3, 13, 25.0000),
(215, 3, 14, 25.0000),
(216, 3, 15, 25.0000),
(217, 3, 16, 25.0000),
(218, 3, 17, 25.0000),
(219, 3, 18, 25.0000),
(220, 3, 19, 25.0000),
(221, 3, 20, 50.0000),
(222, 3, 21, 50.0000),
(223, 3, 22, 50.0000),
(224, 3, 23, 50.0000),
(225, 3, 24, 50.0000),
(226, 3, 25, 50.0000),
(227, 3, 26, 50.0000),
(228, 3, 27, 50.0000),
(229, 3, 28, 50.0000),
(230, 3, 29, 50.0000),
(231, 3, 30, 50.0000),
(232, 3, 31, 50.0000),
(233, 3, 32, 50.0000),
(234, 3, 33, 50.0000),
(235, 3, 34, 50.0000),
(236, 3, 35, 50.0000),
(237, 3, 36, 50.0000),
(238, 3, 37, 50.0000),
(239, 3, 38, 50.0000),
(240, 3, 39, 50.0000),
(241, 3, 40, 75.0000),
(242, 3, 41, 75.0000),
(243, 3, 42, 75.0000),
(244, 3, 43, 75.0000),
(245, 3, 44, 75.0000),
(246, 3, 45, 75.0000),
(247, 3, 46, 75.0000),
(248, 3, 47, 75.0000),
(249, 3, 48, 75.0000),
(250, 3, 49, 75.0000),
(251, 3, 50, 75.0000),
(252, 3, 51, 75.0000),
(253, 3, 52, 75.0000),
(254, 3, 53, 75.0000),
(255, 3, 54, 75.0000),
(256, 3, 55, 75.0000),
(257, 3, 56, 75.0000),
(258, 3, 57, 75.0000),
(259, 3, 58, 75.0000),
(260, 3, 59, 75.0000),
(261, 3, 60, 100.0000),
(262, 3, 61, 100.0000),
(263, 3, 62, 100.0000),
(264, 3, 63, 100.0000),
(265, 3, 64, 100.0000),
(266, 3, 65, 100.0000),
(267, 3, 66, 100.0000),
(268, 3, 67, 100.0000),
(269, 3, 68, 100.0000),
(270, 3, 69, 100.0000),
(271, 3, 70, 100.0000),
(272, 3, 71, 100.0000),
(273, 3, 72, 100.0000),
(274, 3, 73, 100.0000),
(275, 3, 74, 100.0000),
(276, 3, 75, 100.0000),
(277, 3, 76, 100.0000),
(278, 3, 77, 100.0000),
(279, 3, 78, 100.0000),
(280, 3, 79, 100.0000),
(281, 3, 80, 125.0000),
(282, 3, 81, 125.0000),
(283, 3, 82, 125.0000),
(284, 3, 83, 125.0000),
(285, 3, 84, 125.0000),
(286, 3, 85, 125.0000),
(287, 3, 86, 125.0000),
(288, 3, 87, 125.0000),
(289, 3, 88, 125.0000),
(290, 3, 89, 125.0000),
(291, 3, 90, 125.0000),
(292, 3, 91, 125.0000),
(293, 3, 92, 125.0000),
(294, 3, 93, 125.0000),
(295, 3, 94, 125.0000),
(296, 3, 95, 125.0000),
(297, 3, 96, 125.0000),
(298, 3, 97, 125.0000),
(299, 3, 98, 125.0000),
(300, 3, 99, 125.0000),
(301, 4, 1, 29.0000),
(302, 4, 2, 29.0000),
(303, 4, 0, 29.0000),
(304, 4, 3, 29.0000),
(305, 4, 4, 29.0000),
(306, 4, 5, 29.0000),
(307, 4, 6, 29.0000),
(308, 4, 7, 29.0000),
(309, 4, 8, 29.0000),
(310, 4, 9, 29.0000),
(311, 4, 10, 29.0000),
(312, 4, 11, 29.0000),
(313, 4, 12, 29.0000),
(314, 4, 13, 29.0000),
(315, 4, 14, 29.0000),
(316, 4, 15, 29.0000),
(317, 4, 16, 29.0000),
(318, 4, 17, 29.0000),
(319, 4, 18, 29.0000),
(320, 4, 19, 29.0000),
(321, 4, 20, 58.0000),
(322, 4, 21, 58.0000),
(323, 4, 22, 58.0000),
(324, 4, 23, 58.0000),
(325, 4, 24, 58.0000),
(326, 4, 25, 58.0000),
(327, 4, 26, 58.0000),
(328, 4, 27, 58.0000),
(329, 4, 28, 58.0000),
(330, 4, 29, 58.0000),
(331, 4, 30, 58.0000),
(332, 4, 31, 58.0000),
(333, 4, 32, 58.0000),
(334, 4, 33, 58.0000),
(335, 4, 34, 58.0000),
(336, 4, 35, 58.0000),
(337, 4, 36, 58.0000),
(338, 4, 37, 58.0000),
(339, 4, 38, 58.0000),
(340, 4, 39, 58.0000),
(341, 4, 40, 87.0000),
(342, 4, 41, 87.0000),
(343, 4, 42, 87.0000),
(344, 4, 43, 87.0000),
(345, 4, 44, 87.0000),
(346, 4, 45, 87.0000),
(347, 4, 46, 87.0000),
(348, 4, 47, 87.0000),
(349, 4, 48, 87.0000),
(350, 4, 49, 87.0000),
(351, 4, 50, 87.0000),
(352, 4, 51, 87.0000),
(353, 4, 52, 87.0000),
(354, 4, 53, 87.0000),
(355, 4, 54, 87.0000),
(356, 4, 55, 87.0000),
(357, 4, 56, 87.0000),
(358, 4, 57, 87.0000),
(359, 4, 58, 87.0000),
(360, 4, 59, 87.0000),
(361, 4, 60, 116.0000),
(362, 4, 61, 116.0000),
(363, 4, 62, 116.0000),
(364, 4, 63, 116.0000),
(365, 4, 64, 116.0000),
(366, 4, 65, 116.0000),
(367, 4, 66, 116.0000),
(368, 4, 67, 116.0000),
(369, 4, 68, 116.0000),
(370, 4, 69, 116.0000),
(371, 4, 70, 116.0000),
(372, 4, 71, 116.0000),
(373, 4, 72, 116.0000),
(374, 4, 73, 116.0000),
(375, 4, 74, 116.0000),
(376, 4, 75, 116.0000),
(377, 4, 76, 116.0000),
(378, 4, 77, 116.0000),
(379, 4, 78, 116.0000),
(380, 4, 79, 116.0000),
(381, 4, 80, 145.0000),
(382, 4, 81, 145.0000),
(383, 4, 82, 145.0000),
(384, 4, 83, 145.0000),
(385, 4, 84, 145.0000),
(386, 4, 85, 145.0000),
(387, 4, 86, 145.0000),
(388, 4, 87, 145.0000),
(389, 4, 88, 145.0000),
(390, 4, 89, 145.0000),
(391, 4, 90, 145.0000),
(392, 4, 91, 145.0000),
(393, 4, 92, 145.0000),
(394, 4, 93, 145.0000),
(395, 4, 94, 145.0000),
(396, 4, 95, 145.0000),
(397, 4, 96, 145.0000),
(398, 4, 97, 145.0000),
(399, 4, 98, 145.0000),
(400, 4, 99, 145.0000),
(401, 5, 0, 21.0000),
(402, 5, 1, 21.0000),
(403, 5, 2, 21.0000),
(404, 5, 3, 21.0000),
(405, 5, 4, 21.0000),
(406, 5, 5, 21.0000),
(407, 5, 6, 21.0000),
(408, 5, 7, 21.0000),
(409, 5, 8, 21.0000),
(410, 5, 9, 21.0000),
(411, 5, 10, 21.0000),
(412, 5, 11, 21.0000),
(413, 5, 12, 21.0000),
(414, 5, 13, 21.0000),
(415, 5, 14, 21.0000),
(416, 5, 15, 21.0000),
(417, 5, 16, 21.0000),
(418, 5, 17, 21.0000),
(419, 5, 18, 21.0000),
(420, 5, 19, 21.0000),
(421, 5, 20, 42.0000),
(422, 5, 21, 42.0000),
(423, 5, 22, 42.0000),
(424, 5, 23, 42.0000),
(425, 5, 24, 42.0000),
(426, 5, 25, 42.0000),
(427, 5, 26, 42.0000),
(428, 5, 27, 42.0000),
(429, 5, 28, 42.0000),
(430, 5, 29, 42.0000),
(431, 5, 30, 42.0000),
(432, 5, 31, 42.0000),
(433, 5, 32, 42.0000),
(434, 5, 33, 42.0000),
(435, 5, 34, 42.0000),
(436, 5, 35, 42.0000),
(437, 5, 36, 42.0000),
(438, 5, 37, 42.0000),
(439, 5, 38, 42.0000),
(440, 5, 39, 42.0000),
(441, 5, 40, 75.0000),
(442, 5, 41, 75.0000),
(443, 5, 42, 75.0000),
(444, 5, 43, 75.0000),
(445, 5, 44, 75.0000),
(446, 5, 45, 75.0000),
(447, 5, 46, 75.0000),
(448, 5, 47, 75.0000),
(449, 5, 48, 75.0000),
(450, 5, 49, 75.0000),
(451, 5, 50, 75.0000),
(452, 5, 51, 75.0000),
(453, 5, 52, 75.0000),
(454, 5, 53, 75.0000),
(455, 5, 54, 75.0000),
(456, 5, 55, 75.0000),
(457, 5, 56, 75.0000),
(458, 5, 57, 75.0000),
(459, 5, 58, 75.0000),
(460, 5, 59, 75.0000),
(461, 5, 60, 96.0000),
(462, 5, 61, 96.0000),
(463, 5, 62, 96.0000),
(464, 5, 63, 96.0000),
(465, 5, 64, 96.0000),
(466, 5, 65, 96.0000),
(467, 5, 66, 96.0000),
(468, 5, 67, 96.0000),
(469, 5, 68, 96.0000),
(470, 5, 69, 96.0000),
(471, 5, 70, 96.0000),
(472, 5, 71, 96.0000),
(473, 5, 72, 96.0000),
(474, 5, 73, 96.0000),
(475, 5, 74, 96.0000),
(476, 5, 75, 96.0000),
(477, 5, 76, 96.0000),
(478, 5, 77, 96.0000),
(479, 5, 78, 96.0000),
(480, 5, 79, 96.0000),
(481, 5, 80, 117.0000),
(482, 5, 81, 117.0000),
(483, 5, 82, 117.0000),
(484, 5, 83, 117.0000),
(485, 5, 84, 117.0000),
(486, 5, 85, 117.0000),
(487, 5, 86, 117.0000),
(488, 5, 87, 117.0000),
(489, 5, 88, 117.0000),
(490, 5, 89, 117.0000),
(491, 5, 90, 117.0000),
(492, 5, 91, 117.0000),
(493, 5, 92, 117.0000),
(494, 5, 93, 117.0000),
(495, 5, 94, 117.0000),
(496, 5, 95, 117.0000),
(497, 5, 96, 117.0000),
(498, 5, 97, 117.0000),
(499, 5, 98, 117.0000),
(500, 5, 99, 117.0000),
(501, 6, 0, 34.0000),
(502, 6, 1, 34.0000),
(503, 6, 2, 34.0000),
(504, 6, 3, 34.0000),
(505, 6, 4, 34.0000),
(506, 6, 5, 34.0000),
(507, 6, 6, 34.0000),
(508, 6, 7, 34.0000),
(509, 6, 8, 34.0000),
(510, 6, 9, 34.0000),
(511, 6, 10, 34.0000),
(512, 6, 11, 34.0000),
(513, 6, 12, 34.0000),
(514, 6, 13, 34.0000),
(515, 6, 14, 34.0000),
(516, 6, 15, 34.0000),
(517, 6, 16, 34.0000),
(518, 6, 17, 34.0000),
(519, 6, 18, 34.0000),
(520, 6, 19, 34.0000),
(521, 6, 20, 68.0000),
(522, 6, 21, 68.0000),
(523, 6, 22, 68.0000),
(524, 6, 23, 68.0000),
(525, 6, 24, 68.0000),
(526, 6, 25, 68.0000),
(527, 6, 26, 68.0000),
(528, 6, 27, 68.0000),
(529, 6, 28, 68.0000),
(530, 6, 29, 68.0000),
(531, 6, 30, 68.0000),
(532, 6, 31, 68.0000),
(533, 6, 32, 68.0000),
(534, 6, 33, 68.0000),
(535, 6, 34, 68.0000),
(536, 6, 35, 68.0000),
(537, 6, 36, 68.0000),
(538, 6, 37, 68.0000),
(539, 6, 38, 68.0000),
(540, 6, 39, 68.0000),
(541, 6, 40, 102.0000),
(542, 6, 41, 102.0000),
(543, 6, 42, 102.0000),
(544, 6, 43, 102.0000),
(545, 6, 44, 102.0000),
(546, 6, 45, 102.0000),
(547, 6, 46, 102.0000),
(548, 6, 47, 102.0000),
(549, 6, 48, 102.0000),
(550, 6, 49, 102.0000),
(551, 6, 50, 102.0000),
(552, 6, 51, 102.0000),
(553, 6, 52, 102.0000),
(554, 6, 53, 102.0000),
(555, 6, 54, 102.0000),
(556, 6, 55, 102.0000),
(557, 6, 56, 102.0000),
(558, 6, 57, 102.0000),
(559, 6, 58, 102.0000),
(560, 6, 59, 102.0000),
(561, 6, 60, 136.0000),
(562, 6, 61, 136.0000),
(563, 6, 62, 136.0000),
(564, 6, 63, 136.0000),
(565, 6, 64, 136.0000),
(566, 6, 65, 136.0000),
(567, 6, 66, 136.0000),
(568, 6, 67, 136.0000),
(569, 6, 68, 136.0000),
(570, 6, 69, 136.0000),
(571, 6, 70, 136.0000),
(572, 6, 71, 136.0000),
(573, 6, 72, 136.0000),
(574, 6, 73, 136.0000),
(575, 6, 74, 136.0000),
(576, 6, 75, 136.0000),
(577, 6, 76, 136.0000),
(578, 6, 77, 136.0000),
(579, 6, 78, 136.0000),
(580, 6, 79, 136.0000),
(581, 6, 80, 170.0000),
(582, 6, 81, 170.0000),
(583, 6, 82, 170.0000),
(584, 6, 83, 170.0000),
(585, 6, 84, 170.0000),
(586, 6, 85, 170.0000),
(587, 6, 86, 170.0000),
(588, 6, 87, 170.0000),
(589, 6, 88, 170.0000),
(590, 6, 89, 170.0000),
(591, 6, 90, 170.0000),
(592, 6, 91, 170.0000),
(593, 6, 92, 170.0000),
(594, 6, 93, 170.0000),
(595, 6, 94, 170.0000),
(596, 6, 95, 170.0000),
(597, 6, 96, 170.0000),
(598, 6, 97, 170.0000),
(599, 6, 98, 170.0000),
(600, 6, 99, 170.0000),
(601, 7, 1, 0.0000),
(602, 7, 2, 0.0000),
(603, 7, 0, 0.0000),
(604, 7, 3, 0.0000),
(605, 7, 4, 0.0000),
(606, 7, 5, 0.0000),
(607, 7, 6, 0.0000),
(608, 7, 7, 0.0000),
(609, 7, 8, 0.0000),
(610, 7, 9, 0.0000),
(611, 7, 10, 0.0000),
(612, 7, 11, 0.0000),
(613, 7, 12, 0.0000),
(614, 7, 13, 0.0000),
(615, 7, 14, 0.0000),
(616, 7, 15, 0.0000),
(617, 7, 16, 0.0000),
(618, 7, 17, 0.0000),
(619, 7, 18, 0.0000),
(620, 7, 19, 0.0000),
(621, 7, 20, 0.0000),
(622, 7, 21, 0.0000),
(623, 7, 22, 0.0000),
(624, 7, 23, 0.0000),
(625, 7, 24, 0.0000),
(626, 7, 25, 0.0000),
(627, 7, 26, 0.0000),
(628, 7, 27, 0.0000),
(629, 7, 28, 0.0000),
(630, 7, 29, 0.0000),
(631, 7, 30, 0.0000),
(632, 7, 31, 0.0000),
(633, 7, 32, 0.0000),
(634, 7, 33, 0.0000),
(635, 7, 34, 0.0000),
(636, 7, 35, 0.0000),
(637, 7, 36, 0.0000),
(638, 7, 37, 0.0000),
(639, 7, 38, 0.0000),
(640, 7, 39, 0.0000),
(641, 7, 40, 0.0000),
(642, 7, 41, 0.0000),
(643, 7, 42, 0.0000),
(644, 7, 43, 0.0000),
(645, 7, 44, 0.0000),
(646, 7, 45, 0.0000),
(647, 7, 46, 0.0000),
(648, 7, 47, 0.0000),
(649, 7, 48, 0.0000),
(650, 7, 49, 0.0000),
(651, 7, 50, 0.0000),
(652, 7, 51, 0.0000),
(653, 7, 52, 0.0000),
(654, 7, 53, 0.0000),
(655, 7, 54, 0.0000),
(656, 7, 55, 0.0000),
(657, 7, 56, 0.0000),
(658, 7, 57, 0.0000),
(659, 7, 58, 0.0000),
(660, 7, 59, 0.0000),
(661, 7, 60, 0.0000),
(662, 7, 61, 0.0000),
(663, 7, 62, 0.0000),
(664, 7, 63, 0.0000),
(665, 7, 64, 0.0000),
(666, 7, 65, 0.0000),
(667, 7, 66, 0.0000),
(668, 7, 67, 0.0000),
(669, 7, 68, 0.0000),
(670, 7, 69, 0.0000),
(671, 7, 70, 0.0000),
(672, 7, 71, 0.0000),
(673, 7, 72, 0.0000),
(674, 7, 73, 0.0000),
(675, 7, 74, 0.0000),
(676, 7, 75, 0.0000),
(677, 7, 76, 0.0000),
(678, 7, 77, 0.0000),
(679, 7, 78, 0.0000),
(680, 7, 79, 0.0000),
(681, 7, 80, 0.0000),
(682, 7, 81, 0.0000),
(683, 7, 82, 0.0000),
(684, 7, 83, 0.0000),
(685, 7, 84, 0.0000),
(686, 7, 85, 0.0000),
(687, 7, 86, 0.0000),
(688, 7, 87, 0.0000),
(689, 7, 88, 0.0000),
(690, 7, 89, 0.0000),
(691, 7, 90, 0.0000),
(692, 7, 91, 0.0000),
(693, 7, 92, 0.0000),
(694, 7, 93, 0.0000),
(695, 7, 94, 0.0000),
(696, 7, 95, 0.0000),
(697, 7, 96, 0.0000),
(698, 7, 97, 0.0000),
(699, 7, 98, 0.0000),
(700, 7, 99, 0.0000);

-- --------------------------------------------------------

--
-- Table structure for table `sl_slide`
--

CREATE TABLE `sl_slide` (
  `id` int(11) NOT NULL,
  `topik_id` int(11) DEFAULT 0,
  `image` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `date_input` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `insert_by` varchar(255) DEFAULT NULL,
  `last_update_by` varchar(255) DEFAULT NULL,
  `writer` varchar(200) DEFAULT NULL,
  `urutan` int(4) DEFAULT NULL,
  `image2` varchar(225) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sl_slide`
--

INSERT INTO `sl_slide` (`id`, `topik_id`, `image`, `active`, `date_input`, `date_update`, `insert_by`, `last_update_by`, `writer`, `urutan`, `image2`) VALUES
(1, 0, '9c34f-slides-1.jpg', 1, '2020-07-27 17:01:33', '2020-08-24 23:07:11', 'info@markdesign.net', 'ibnudrift@gmail.com', NULL, NULL, 'feb2e-slides-1_mob.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sl_slide_description`
--

CREATE TABLE `sl_slide_description` (
  `id` int(11) NOT NULL,
  `slide_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sl_slide_description`
--

INSERT INTO `sl_slide_description` (`id`, `slide_id`, `language_id`, `title`, `content`, `url`) VALUES
(8, 1, 2, 'sdfgsdfgsfdg', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_career`
--

CREATE TABLE `tb_career` (
  `id` bigint(20) NOT NULL,
  `position` varchar(225) DEFAULT NULL,
  `location` varchar(225) DEFAULT NULL,
  `desc_en` text DEFAULT NULL,
  `desc_id` text DEFAULT NULL,
  `kualifikasi_en` text DEFAULT NULL,
  `kualifikasi_id` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_career`
--

INSERT INTO `tb_career` (`id`, `position`, `location`, `desc_en`, `desc_id`, `kualifikasi_en`, `kualifikasi_id`) VALUES
(1, 'Sales Executive', 'Pasuruan, Indonesia', '<ul><li>Maintain the existing global email solution in our international organisation (12000 accounts worldwide)</li><li>Be the main point of contact for messaging related topics</li><li>Coordinate integration between Givaudan messaging systems and other platforms (e.g.: CRM, home grown solutions, SAP, etc.)</li><li>Leverage, design and implement evolution in messaging solutions as part of large projects</li><li>Ensure smooth deployment and handover to operations (managed services partner)</li><li>Produce technical specifications, document solutions and ensure the consistency of the messaging platform in our evolving environment<br>Drive data and platform migrations (in case of company acquisitions)<br>Overlook Cpanel configurations changes and new feature implementations<br>Suggest platform improvements<br>L3 Support and troubleshoot issues</li></ul>', '<ul><li>Pertahankan solusi email global yang ada di organisasi internasional kami (12000 akun di seluruh dunia)</li><li>Menjadi titik kontak utama untuk topik yang berkaitan dengan pengiriman pesan</li><li>Koordinasi integrasi antara sistem pesan Givaudan dan platform lainnya (mis .: CRM, solusi buatan sendiri, SAP, dll.)</li><li>Memanfaatkan, merancang, dan mengimplementasikan evolusi dalam solusi pengiriman pesan sebagai bagian dari proyek besar</li><li>Memastikan penyebaran dan penyerahan yang lancar ke operasi (mitra layanan terkelola)</li><li>Menghasilkan spesifikasi teknis, solusi dokumen dan memastikan konsistensi platform perpesanan di lingkungan kami yang terus berkembang</li><li>Mendorong migrasi data dan platform (dalam hal akuisisi perusahaan)</li><li>Abaikan perubahan konfigurasi Cpanel dan implementasi fitur baru</li><li>Sarankan peningkatan platform</li><li>L3 Dukungan dan memecahkan masalah</li></ul>', '<ul>\r\n	<li>University Degree in IT or related field</li>\r\n	<li>Minimum 7 years’ experience ideally with strong experience in Google messaging systems.</li>\r\n	<li>Ability to analyze complex requirements, recommend and implement deployment strategies</li>\r\n	<li>Ensure implemented solutions abide by security standards</li>\r\n	<li>Excellent communication and presentation skills with strong analytical and problem-solving skills.</li>\r\n	<li>Capable of understanding, articulating and translating business requirements to system solution at a conceptual level (business to IT translation)</li>\r\n	<li>Culturally aware and adaptable to flexible business requirements</li>\r\n	<li>Working knowledge of directory integration, of authentication, identity, and access management technologies and certificate management, network topology, DNS and common communication protocols including HTTP/S, TCP and UDP</li>\r\n</ul>', '<ul><li>Gelar Universitas di bidang IT atau bidang terkait</li><li>Pengalaman minimum 7 tahun idealnya dengan pengalaman yang kuat dalam sistem pengiriman pesan Google.</li><li>Kemampuan untuk menganalisis persyaratan yang kompleks, merekomendasikan dan menerapkan strategi penempatan</li><li>Pastikan solusi yang diimplementasikan mematuhi standar keamanan</li><li>Keterampilan komunikasi dan presentasi yang sangat baik dengan keterampilan analitis dan pemecahan masalah yang kuat.</li><li>Mampu memahami, mengartikulasikan, dan menerjemahkan persyaratan bisnis ke solusi sistem pada tingkat konseptual (terjemahan bisnis ke TI)</li><li>Sadar budaya dan mudah beradaptasi dengan persyaratan bisnis yang fleksibel</li><li>Pengetahuan kerja tentang integrasi direktori, otentikasi, identitas, dan teknologi manajemen akses dan manajemen sertifikat, topologi jaringan, DNS dan protokol komunikasi umum termasuk HTTP / S, TCP dan UDP</li></ul>'),
(2, 'Marketing Director', 'Pasuruan, Indonesia', '<ul><li>Maintain the existing global email solution in our international organisation (12000 accounts worldwide)</li><li>Be the main point of contact for messaging related topics</li><li>Coordinate integration between Givaudan messaging systems and other platforms (e.g.: CRM, home grown solutions, SAP, etc.)</li><li>Leverage, design and implement evolution in messaging solutions as part of large projects</li><li>Ensure smooth deployment and handover to operations (managed services partner)</li><li>Produce technical specifications, document solutions and ensure the consistency of the messaging platform in our evolving environment<br>Drive data and platform migrations (in case of company acquisitions)<br>Overlook Cpanel configurations changes and new feature implementations<br>Suggest platform improvements<br>L3 Support and troubleshoot issues</li></ul>', '<ul><li>Pertahankan solusi email global yang ada di organisasi internasional kami (12000 akun di seluruh dunia)</li><li>Menjadi titik kontak utama untuk topik yang berkaitan dengan pengiriman pesan</li><li>Koordinasi integrasi antara sistem pesan Givaudan dan platform lainnya (mis .: CRM, solusi buatan sendiri, SAP, dll.)</li><li>Memanfaatkan, merancang, dan mengimplementasikan evolusi dalam solusi pengiriman pesan sebagai bagian dari proyek besar</li><li>Memastikan penyebaran dan penyerahan yang lancar ke operasi (mitra layanan terkelola)</li><li>Menghasilkan spesifikasi teknis, solusi dokumen dan memastikan konsistensi platform perpesanan di lingkungan kami yang terus berkembang</li><li>Mendorong migrasi data dan platform (dalam hal akuisisi perusahaan)</li><li>Abaikan perubahan konfigurasi Cpanel dan implementasi fitur baru</li><li>Sarankan peningkatan platform</li><li>L3 Dukungan dan memecahkan masalah</li></ul>', '<ul><li>University Degree in IT or related field</li><li>Minimum 7 years’ experience ideally with strong experience in Google messaging systems.</li><li>Ability to analyze complex requirements, recommend and implement deployment strategies</li><li>Ensure implemented solutions abide by security standards</li><li>Excellent communication and presentation skills with strong analytical and problem-solving skills.</li><li>Capable of understanding, articulating and translating business requirements to system solution at a conceptual level (business to IT translation)</li><li>Culturally aware and adaptable to flexible business requirements</li><li>Working knowledge of directory integration, of authentication, identity, and access management technologies and certificate management, network topology, DNS and common communication protocols including HTTP/S, TCP and UDP</li></ul>', '<ul><li>Gelar Universitas di bidang IT atau bidang terkait</li><li>Pengalaman minimum 7 tahun idealnya dengan pengalaman yang kuat dalam sistem pengiriman pesan Google.</li><li>Kemampuan untuk menganalisis persyaratan yang kompleks, merekomendasikan dan menerapkan strategi penempatan</li><li>Pastikan solusi yang diimplementasikan mematuhi standar keamanan</li><li>Keterampilan komunikasi dan presentasi yang sangat baik dengan keterampilan analitis dan pemecahan masalah yang kuat.</li><li>Mampu memahami, mengartikulasikan, dan menerjemahkan persyaratan bisnis ke solusi sistem pada tingkat konseptual (terjemahan bisnis ke TI)</li><li>Sadar budaya dan mudah beradaptasi dengan persyaratan bisnis yang fleksibel</li><li>Pengetahuan kerja tentang integrasi direktori, otentikasi, identitas, dan teknologi manajemen akses dan manajemen sertifikat, topologi jaringan, DNS dan protokol komunikasi umum termasuk HTTP / S, TCP dan UDP</li></ul>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_group`
--

CREATE TABLE `tb_group` (
  `id` int(11) NOT NULL,
  `group` varchar(50) NOT NULL,
  `aktif` int(11) NOT NULL,
  `akses` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_group`
--

INSERT INTO `tb_group` (`id`, `group`, `aktif`, `akses`) VALUES
(8, 'Administrator', 1, 0x613a33373a7b693a303b733a31363a2261646d696e2e757365722e696e646578223b693a313b733a31373a2261646d696e2e757365722e637265617465223b693a323b733a31373a2261646d696e2e757365722e757064617465223b693a333b733a31373a2261646d696e2e757365722e64656c657465223b693a343b733a31373a2261646d696e2e736c6964652e696e646578223b693a353b733a31383a2261646d696e2e736c6964652e637265617465223b693a363b733a31383a2261646d696e2e736c6964652e757064617465223b693a373b733a31383a2261646d696e2e736c6964652e64656c657465223b693a383b733a31363a2261646d696e2e62616e6b2e696e646578223b693a393b733a31373a2261646d696e2e62616e6b2e637265617465223b693a31303b733a31373a2261646d696e2e62616e6b2e757064617465223b693a31313b733a31373a2261646d696e2e62616e6b2e64656c657465223b693a31323b733a31393a2261646d696e2e73657474696e672e696e646578223b693a31333b733a31383a2261646d696e2e6d656d6265722e696e646578223b693a31343b733a31393a2261646d696e2e6d656d6265722e637265617465223b693a31353b733a31393a2261646d696e2e6d656d6265722e757064617465223b693a31363b733a31393a2261646d696e2e6d656d6265722e64656c657465223b693a31373b733a31373a2261646d696e2e6f726465722e696e646578223b693a31383b733a31383a2261646d696e2e6f726465722e637265617465223b693a31393b733a31383a2261646d696e2e6f726465722e757064617465223b693a32303b733a31383a2261646d696e2e6f726465722e64656c657465223b693a32313b733a31373a2261646d696e2e6f726465722e7072696e74223b693a32323b733a32313a2261646d696e2e73657474696e672e636f6e74616374223b693a32333b733a31393a2261646d696e2e73657474696e672e61626f7574223b693a32343b733a32303a2261646d696e2e63617465676f72792e696e646578223b693a32353b733a32313a2261646d696e2e63617465676f72792e637265617465223b693a32363b733a32313a2261646d696e2e63617465676f72792e757064617465223b693a32373b733a32313a2261646d696e2e63617465676f72792e64656c657465223b693a32383b733a31393a2261646d696e2e73657474696e672e686f77746f223b693a32393b733a31393a2261646d696e2e70726f647563742e696e646578223b693a33303b733a32303a2261646d696e2e70726f647563742e637265617465223b693a33313b733a32303a2261646d696e2e70726f647563742e757064617465223b693a33323b733a32303a2261646d696e2e70726f647563742e64656c657465223b693a33333b733a32303a2261646d696e2e64656c69766572792e696e646578223b693a33343b733a32313a2261646d696e2e64656c69766572792e637265617465223b693a33353b733a32313a2261646d696e2e64656c69766572792e757064617465223b693a33363b733a32313a2261646d696e2e64656c69766572792e64656c657465223b7d);

-- --------------------------------------------------------

--
-- Table structure for table `tb_menu_akses`
--

CREATE TABLE `tb_menu_akses` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `action` blob NOT NULL,
  `sub_action` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_menu_akses`
--

INSERT INTO `tb_menu_akses` (`id`, `type`, `name`, `controller`, `action`, `sub_action`) VALUES
(22, 'admin', 'User', 'user', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(21, 'admin', 'Slide', 'slide', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(40, 'admin', 'Bank', 'bank', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(18, 'admin', 'Setting', 'setting', 0x613a313a7b733a353a22696e646578223b733a31373a22456469742053657474696e6720556d756d223b7d, 0x613a303a7b7d),
(39, 'admin', 'Member', 'member', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(38, 'admin', 'Order', 'order', 0x613a353a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b733a353a227072696e74223b733a353a225072696e74223b7d, 0x613a303a7b7d),
(32, 'admin', 'Contact Us', 'setting', 0x613a313a7b733a373a22636f6e74616374223b733a32323a2245646974205061676520487562756e6769204b616d69223b7d, 0x613a303a7b7d),
(13, 'admin', 'About Us', 'setting', 0x613a313a7b733a353a2261626f7574223b733a31303a22456469742041626f7574223b7d, 0x613a303a7b7d),
(37, 'admin', 'Category', 'category', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(36, 'admin', 'How To Order', 'setting', 0x613a313a7b733a353a22686f77746f223b733a31323a22486f7720546f204f72646572223b7d, 0x613a303a7b7d),
(30, 'admin', 'Products', 'product', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(41, 'admin', 'Delivery Price', 'delivery', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `group_id` int(11) NOT NULL,
  `login_terakhir` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `aktivasi` int(11) NOT NULL,
  `aktif` int(11) NOT NULL,
  `user_input` varchar(200) NOT NULL,
  `tanggal_input` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `initial` varchar(255) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `email`, `nama`, `pass`, `type`, `group_id`, `login_terakhir`, `aktivasi`, `aktif`, `user_input`, `tanggal_input`, `initial`, `image`) VALUES
(30, 'info@markdesign.net', '', '564fda17f517ae04a86734c2b2341327ed4fd565', 'root', 8, '2019-12-23 15:41:59', 0, 1, '', '0000-00-00 00:00:00', 'Admin', ''),
(35, 'ibnudrift@gmail.com', 'ibnu fajar', '564fda17f517ae04a86734c2b2341327ed4fd565', 'root', 8, '2019-08-15 07:30:36', 0, 1, '', '0000-00-00 00:00:00', 'ibnu', '');

-- --------------------------------------------------------

--
-- Table structure for table `to_toko`
--

CREATE TABLE `to_toko` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `login_terakhir` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `aktivasi` int(11) NOT NULL,
  `aktif` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `hp` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `nama_toko` varchar(200) NOT NULL,
  `lokasi` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `to_toko`
--

INSERT INTO `to_toko` (`id`, `email`, `first_name`, `last_name`, `pass`, `login_terakhir`, `aktivasi`, `aktif`, `image`, `hp`, `address`, `city`, `province`, `postcode`, `nama_toko`, `lokasi`) VALUES
(1, 'deoryzpandu@gmail.com', 'Deory', 'Pandu', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2014-11-07 02:32:38', 0, 1, 'a448d-calourette-woodland-creature-jewelry-1.jpg', 'HP', 'Address', 'City', 'Province', 'PostCode', 'JewelryShop', 'surabaya'),
(4, 'ibnu@markdesign.net', 'Ibnu', 'Fajar', '564fda17f517ae04a86734c2b2341327ed4fd565', '2014-11-07 02:32:49', 0, 1, '3e491-calourette-woodland-creature-jewelry-1.jpg', 'HP', 'Address', 'City', 'Province', 'PostCode', 'Toko Handoko', 'surabaya');

-- --------------------------------------------------------

--
-- Table structure for table `to_toko_product`
--

CREATE TABLE `to_toko_product` (
  `id` int(11) NOT NULL,
  `toko_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `to_toko_product`
--

INSERT INTO `to_toko_product` (`id`, `toko_id`, `product_id`) VALUES
(8, 1, 960),
(7, 1, 105),
(6, 1, 719),
(5, 1, 264),
(9, 1, 223),
(10, 1, 930),
(11, 1, 475),
(12, 1, 732),
(13, 4, 264),
(14, 4, 560),
(15, 4, 960),
(16, 4, 505),
(17, 4, 719),
(18, 4, 678),
(19, 4, 475),
(20, 4, 277);

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `awal` int(11) NOT NULL,
  `akhir` int(11) NOT NULL,
  `trip` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`id`, `year`, `month`, `awal`, `akhir`, `trip`) VALUES
(1, 2016, 1, 1, 3, 'Surabaya'),
(4, 2016, 2, 8, 10, 'Singapore'),
(3, 2016, 2, 7, 10, 'Malaysia');

-- --------------------------------------------------------

--
-- Table structure for table `tt_text`
--

CREATE TABLE `tt_text` (
  `id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL,
  `message` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tt_text`
--

INSERT INTO `tt_text` (`id`, `category`, `message`) VALUES
(1, 'admin', 'Produk'),
(2, 'admin', 'Pages'),
(3, 'admin', 'Orders'),
(4, 'admin', 'Customers'),
(5, 'admin', 'Promotions'),
(6, 'admin', 'Reports'),
(7, 'admin', 'General Setting'),
(8, 'admin', 'Data Edited'),
(9, 'admin', 'New Orders'),
(10, 'admin', 'New Customers'),
(11, 'admin', 'Payment Confirmation'),
(12, 'admin', 'Edit Profile'),
(13, 'admin', 'Change Password'),
(14, 'admin', 'Sign Out'),
(15, 'admin', 'Gallery'),
(16, 'admin', 'Slide Home'),
(17, 'admin', 'Toko'),
(18, 'admin', 'Slides'),
(19, 'admin', 'Product'),
(20, 'admin', 'Products'),
(21, 'admin', 'About Us'),
(22, 'admin', 'Contact Us'),
(23, 'admin', 'Trip'),
(24, 'admin', 'Trips'),
(25, 'admin', 'Slide'),
(26, 'admin', 'Healty'),
(27, 'admin', 'ge-ma'),
(28, 'admin', 'Our Services'),
(29, 'admin', 'Services Content'),
(30, 'admin', 'Projects'),
(31, 'admin', 'Peojects'),
(32, 'admin', 'Gallerys'),
(33, 'admin', 'Our Quality'),
(34, 'admin', 'Home'),
(35, 'admin', 'Career');

-- --------------------------------------------------------
--
-- Structure for view `view_category`
--
DROP TABLE IF EXISTS `view_category`;

CREATE VIEW `view_category`  AS  select `prd_category`.`id` AS `id`,`prd_category`.`parent_id` AS `parent_id`,`prd_category`.`sort` AS `sort`,`prd_category`.`image` AS `image`,`prd_category`.`type` AS `type`,`prd_category`.`data` AS `data`,`prd_category_description`.`id` AS `id2`,`prd_category_description`.`category_id` AS `category_id`,`prd_category_description`.`language_id` AS `language_id`,`prd_category_description`.`name` AS `name`,`prd_category_description`.`data` AS `data2` from (`prd_category` join `prd_category_description` on(`prd_category`.`id` = `prd_category_description`.`category_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_gallery`
--
DROP TABLE IF EXISTS `view_gallery`;

CREATE VIEW `view_gallery`  AS  select `gal_gallery`.`id` AS `id`,`gal_gallery`.`topik_id` AS `topik_id`,`gal_gallery`.`image` AS `image`,`gal_gallery`.`image2` AS `image2`,`gal_gallery`.`active` AS `active`,`gal_gallery`.`date_input` AS `date_input`,`gal_gallery`.`date_update` AS `date_update`,`gal_gallery`.`insert_by` AS `insert_by`,`gal_gallery`.`last_update_by` AS `last_update_by`,`gal_gallery`.`writer` AS `writer`,`gal_gallery`.`city` AS `city`,`gal_gallery_description`.`id` AS `id2`,`gal_gallery_description`.`gallery_id` AS `gallery_id`,`gal_gallery_description`.`language_id` AS `language_id`,`gal_gallery_description`.`title` AS `title`,`gal_gallery_description`.`sub_title` AS `sub_title`,`gal_gallery_description`.`content` AS `content` from (`gal_gallery` join `gal_gallery_description` on(`gal_gallery`.`id` = `gal_gallery_description`.`gallery_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_slide`
--
DROP TABLE IF EXISTS `view_slide`;

CREATE VIEW `view_slide`  AS  select `sl_slide`.`id` AS `id`,`sl_slide`.`topik_id` AS `topik_id`,`sl_slide`.`image` AS `image`,`sl_slide`.`active` AS `active`,`sl_slide`.`date_input` AS `date_input`,`sl_slide`.`date_update` AS `date_update`,`sl_slide`.`insert_by` AS `insert_by`,`sl_slide`.`last_update_by` AS `last_update_by`,`sl_slide`.`writer` AS `writer`,`sl_slide_description`.`id` AS `id2`,`sl_slide_description`.`slide_id` AS `slide_id`,`sl_slide_description`.`language_id` AS `language_id`,`sl_slide_description`.`title` AS `title`,`sl_slide_description`.`content` AS `content`,`sl_slide_description`.`url` AS `url` from (`sl_slide` join `sl_slide_description` on(`sl_slide_description`.`slide_id` = `sl_slide`.`id`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_image`
--
ALTER TABLE `about_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_customer`
--
ALTER TABLE `cs_customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `cs_customer_address`
--
ALTER TABLE `cs_customer_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `gal_gallery`
--
ALTER TABLE `gal_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gal_gallery_description`
--
ALTER TABLE `gal_gallery_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `gal_gallery_image`
--
ALTER TABLE `gal_gallery_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`gallery_id`);

--
-- Indexes for table `instagram`
--
ALTER TABLE `instagram`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `me_member`
--
ALTER TABLE `me_member`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `or_order`
--
ALTER TABLE `or_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `or_order_history`
--
ALTER TABLE `or_order_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `or_order_product`
--
ALTER TABLE `or_order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `or_order_status`
--
ALTER TABLE `or_order_status`
  ADD PRIMARY KEY (`order_status_id`);

--
-- Indexes for table `pg_bank`
--
ALTER TABLE `pg_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pg_blog`
--
ALTER TABLE `pg_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pg_blog_description`
--
ALTER TABLE `pg_blog_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `pg_faq`
--
ALTER TABLE `pg_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pg_faq_description`
--
ALTER TABLE `pg_faq_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `pg_list_bank`
--
ALTER TABLE `pg_list_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pg_pages`
--
ALTER TABLE `pg_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pg_pages_description`
--
ALTER TABLE `pg_pages_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pg_testimonial`
--
ALTER TABLE `pg_testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pg_testimonial_description`
--
ALTER TABLE `pg_testimonial_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pg_type_letak`
--
ALTER TABLE `pg_type_letak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_brand`
--
ALTER TABLE `prd_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_brand_description`
--
ALTER TABLE `prd_brand_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `prd_category`
--
ALTER TABLE `prd_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `prd_category_description`
--
ALTER TABLE `prd_category_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`,`language_id`);

--
-- Indexes for table `prd_product`
--
ALTER TABLE `prd_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `prd_product_attributes`
--
ALTER TABLE `prd_product_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `prd_product_color`
--
ALTER TABLE `prd_product_color`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `prd_product_description`
--
ALTER TABLE `prd_product_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`,`language_id`);

--
-- Indexes for table `prd_product_image`
--
ALTER TABLE `prd_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `realfood`
--
ALTER TABLE `realfood`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_description`
--
ALTER TABLE `setting_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shp_shipping_price`
--
ALTER TABLE `shp_shipping_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sl_slide`
--
ALTER TABLE `sl_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sl_slide_description`
--
ALTER TABLE `sl_slide_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `tb_career`
--
ALTER TABLE `tb_career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_group`
--
ALTER TABLE `tb_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_menu_akses`
--
ALTER TABLE `tb_menu_akses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `to_toko`
--
ALTER TABLE `to_toko`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `to_toko_product`
--
ALTER TABLE `to_toko_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `toko_id` (`toko_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tt_text`
--
ALTER TABLE `tt_text`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_image`
--
ALTER TABLE `about_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `cs_customer`
--
ALTER TABLE `cs_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_customer_address`
--
ALTER TABLE `cs_customer_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gal_gallery`
--
ALTER TABLE `gal_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `gal_gallery_description`
--
ALTER TABLE `gal_gallery_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `gal_gallery_image`
--
ALTER TABLE `gal_gallery_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `instagram`
--
ALTER TABLE `instagram`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=873;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=925;

--
-- AUTO_INCREMENT for table `me_member`
--
ALTER TABLE `me_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `or_order`
--
ALTER TABLE `or_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `or_order_history`
--
ALTER TABLE `or_order_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `or_order_product`
--
ALTER TABLE `or_order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `or_order_status`
--
ALTER TABLE `or_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pg_bank`
--
ALTER TABLE `pg_bank`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pg_blog`
--
ALTER TABLE `pg_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pg_blog_description`
--
ALTER TABLE `pg_blog_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `pg_faq`
--
ALTER TABLE `pg_faq`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pg_faq_description`
--
ALTER TABLE `pg_faq_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pg_list_bank`
--
ALTER TABLE `pg_list_bank`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `pg_pages`
--
ALTER TABLE `pg_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pg_pages_description`
--
ALTER TABLE `pg_pages_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pg_testimonial`
--
ALTER TABLE `pg_testimonial`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pg_testimonial_description`
--
ALTER TABLE `pg_testimonial_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pg_type_letak`
--
ALTER TABLE `pg_type_letak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `prd_brand`
--
ALTER TABLE `prd_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prd_brand_description`
--
ALTER TABLE `prd_brand_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prd_category`
--
ALTER TABLE `prd_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `prd_category_description`
--
ALTER TABLE `prd_category_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `prd_product`
--
ALTER TABLE `prd_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `prd_product_attributes`
--
ALTER TABLE `prd_product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prd_product_color`
--
ALTER TABLE `prd_product_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prd_product_description`
--
ALTER TABLE `prd_product_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `prd_product_image`
--
ALTER TABLE `prd_product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `realfood`
--
ALTER TABLE `realfood`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `setting_description`
--
ALTER TABLE `setting_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shp_shipping_price`
--
ALTER TABLE `shp_shipping_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=701;

--
-- AUTO_INCREMENT for table `sl_slide`
--
ALTER TABLE `sl_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sl_slide_description`
--
ALTER TABLE `sl_slide_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_career`
--
ALTER TABLE `tb_career`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_group`
--
ALTER TABLE `tb_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_menu_akses`
--
ALTER TABLE `tb_menu_akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `to_toko`
--
ALTER TABLE `to_toko`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `to_toko_product`
--
ALTER TABLE `to_toko_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `trips`
--
ALTER TABLE `trips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tt_text`
--
ALTER TABLE `tt_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
